import { Request, Response, Router } from "express";

class PaycometRouter {
    router: Router;
  
    constructor() {
      this.router = Router();
      this.routes();
    }
  
    routes() {
      this.router.post("/create-paycomet-user", async (req: Request, res: Response) => {

      })

      this.router.get("/paycomet", (req: Request, res: Response) => {
        res.render("paycomet");
      });
    }
  }
  
  const paycometRouter = new PaycometRouter();
  
  export default paycometRouter.router;