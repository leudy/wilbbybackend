import { IOrders } from "../models/newOrder/order";
import moment from "moment";
export const OrderData = (order: IOrders) => {
  const items = order.items.map((element) => {
    const SubItem = element.items.subItems.map((elements) => {
      const item = {
        posRef: elements.plu,
        qty: elements.quantity,
        price: elements.price,
      };
      return item;
    });

    const item = {
      posRef: element.items.plu,
      name: element.items.name,
      price: element.items.price,
      qty: element.items.quantity,
      clientNote: "",
      modifiers: SubItem,
    };
    return item;
  });

  return {
    order: {
      channelSlug: "ecommerce",
      storeId: order.storeData.isOrdaticPartnerID,
      orderCode: `${order.channelOrderDisplayId}`,
      orderId: order._id,
      placedAt: order.deliveryTime,
      currency: "EUR",
      total: order.payment,
      items: items,
      address: {
        street: order.deliveryAddressData.formatted_address,
        streetNumber: order.deliveryAddressData.puertaPiso,
        doorNumber: order.deliveryAddressData.puertaPiso,
        city: order.deliveryAddressData.city,
        zipCode: order.deliveryAddressData.postalcode,
        country: "ES",
        coordinates: {
          lat: order.deliveryAddressData.lat,
          lng: order.deliveryAddressData.lgn,
        },
        notes: order.note,
        formattedAddress: order.deliveryAddressData.formatted_address,
      },
      promotions: [],
      courier: {
        name: "Wilbby",
        phone: "+34664028161",
      },
      client: {
        name: order.customerData.name,
        phone: order.customerData.telefono,
        documentId: "",
      },
      clientComments: order.note,
      asap: true,
      preparationAt: order.deliveryTime,
      //@ts-ignore
      suggestedPickUpAt: moment(order.deliveryTime).add(25, "minutes").format(),
      suggestedDeliveryAt: order.deliveryTime,
      fulfillment: {
        type: "delivery-by-location",
      },
      payment: {
        type: order.paymentMethod === "Tarjeta de credito" ? "card" : "paypal",
      },
      priceDetails: {
        deliveryFee: 0,
        amountPaid: order.payment,
        surchargeFee: order.serviceCharge,
        packageFee: 0,
        taxes: 0,
      },
      kitchenComments: order.note,
    },
  };
};
