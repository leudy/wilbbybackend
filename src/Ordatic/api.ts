import { authURL, appId, appSecret, ordersURL, webHookURL } from "./config";
import { IOrders } from "../models/newOrder/order";
import { OrderData } from "./data";
var request = require("request");

const data = JSON.stringify({
  appId: appId,
  appSecret: appSecret,
  name: "Wilbby DEMO",
  scopes: "api",
});

var options = {
  method: "POST",
  url: authURL,
  headers: {
    "Content-Type": "application/json",
  },
  body: data,
};

export const pushOrderTopOrdatic = (order: IOrders) => {
  request(options, function (error, response) {
    const credential = JSON.parse(response.body);

    var optionsHook = {
      method: "PATCH",
      url: `${webHookURL}/${appId}`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${credential.token}`,
      },
      body: JSON.stringify({
        app: {
          webhookAuthHeader: "",
          webhookUrl: "https://api.v1.wilbby.com/api/ordaticEvents",
        },
      }),
    };

    request(optionsHook, function (error, response) {
      if (error) throw new Error(error);
      console.log(response.body);
    });

    var options = {
      method: "POST",
      url: ordersURL,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${credential.token}`,
      },
      body: JSON.stringify(OrderData(order)),
    };
    request(options, function (error, response) {
      if (error) throw new Error(error);
      console.log(response.body);
    });
  });
};
