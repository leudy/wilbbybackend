import { ordenUpdate } from "../Deliverect/ordenUpdate";
import {
  sendNotification,
  sendNotificationRider,
} from "../Deliverect/sendNotification";
import OrderSchema from "../models/newOrder/order";

export const OrderUpdate = async (data) => {
  const orden = await OrderSchema.findOne({ _id: data.data.order.orderId });
  switch (data.eventType) {
    case "order-created":
      sendNotification(
        //@ts-ignore
        orden?.customerData.OnesignalID,
        `${orden?.storeData.title} Ha recibido tu pedido tu pedido.`,
        orden
      );
      break;

    case "order-acepted":
      ordenUpdate(orden?._id, "Confirmada", 60);
      sendNotification(
        //@ts-ignore
        orden?.customerData.OnesignalID,
        `${orden?.storeData.title} Ha confirmado tu pedido.`,
        orden
      );
      break;

    case "order-accepted":
      ordenUpdate(orden?._id, "Confirmada", 60);
      sendNotification(
        //@ts-ignore
        orden?.customerData.OnesignalID,
        `${orden?.storeData.title} Ha confirmado tu pedido.`,
        orden
      );
      break;

    case "order-in-kitchen":
      sendNotification(
        //@ts-ignore
        orden?.customerData.OnesignalID,
        `${orden?.storeData.title} esta preparando tu comida en la cocina.`,
        orden
      );
      break;

    case "order-ready-for-delivery":
      sendNotification(
        //@ts-ignore
        orden?.customerData.OnesignalID,
        `${orden?.storeData.title} ya ha terminado de preparar tu pedido en la cocina.`,
        orden
      );
      break;

    case "order-ready-for-delivery":
      ordenUpdate(orden?._id, "Listo para recoger", 70);
      sendNotification(
        //@ts-ignore
        orden?.customerData.OnesignalID,
        `${orden?.storeData.title} tiene tu pedido listo para recoger.`,
        orden
      );
      break;

    case "order-picked-up":
      ordenUpdate(orden?._id, "En camino", 80);
      sendNotification(
        //@ts-ignore
        orden?.customerData.OnesignalID,
        `Tu pedido ha salido del establecimiento y va de camino.`,
        orden
      );
      break;

    case "order-ready-for-delivery":
      ordenUpdate(orden?._id, "Listo para recoger", 70);
      if (orden?.orderType === "delivery") {
        sendNotificationRider(
          orden.courierData.OnesignalID,
          `${orden?.storeData.title} tiene el pedido listo para recoger.`
        );
      }
      sendNotification(
        //@ts-ignore
        orden?.customerData.OnesignalID,
        `${orden?.storeData.title} tiene el pedido listo para recoger por el repartidor.`,
        orden
      );
      break;

    case "order-canceled":
      ordenUpdate(orden?._id, "Rechazada por la tienda", 10);
      sendNotification(
        //@ts-ignore
        orden?.customerData.OnesignalID,
        `Upps ${orden?.storeData.title} no ha podido aceptar tu pedido lo sentimos.`,
        orden
      );
      break;

    case "order-canceled":
      ordenUpdate(orden?._id, "Rechazada por la tienda", 10);
      sendNotification(
        //@ts-ignore
        orden?.customerData.OnesignalID,
        `Upps ${orden?.storeData.title} no ha podido aceptar tu pedido lo sentimos.`,
        orden
      );
      break;

    default:
      break;
  }
};
