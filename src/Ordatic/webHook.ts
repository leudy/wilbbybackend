import { Request, Response, Router } from "express";
import Store from "../models/restaurant";
import { setMenuFromOrdatic } from "./Menu";
import { SnoozedProduct } from "./SnoozedProduct";
import { OrderUpdate } from "./OrderUpdate";

class OrdaticRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.post(
      "/api/ordaticEvents",
      async (req: Request, res: Response) => {
        const data = req.body;
        const store = await Store.findOne({
          isOrdaticPartnerID: data.storeId,
        });
        if (data.eventType === "catalog-updated") {
          setMenuFromOrdatic(data, store);
        } else if (data.eventType === "item-disabled" || "item-enabled") {
          SnoozedProduct(data);
        } else {
          OrderUpdate(data);
        }
        res.status(200).end();
      }
    );
  }
}

const ordaticRouter = new OrdaticRouter();
ordaticRouter.routes();

export default ordaticRouter.router;
