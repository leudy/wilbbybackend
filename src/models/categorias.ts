import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const categoriaSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    text: true,
    unique: true,
  },
  image: {
    type: String,
    required: true,
    text: true,
  },
  description: {
    type: String,
    required: true,
    unique: true,
  },
  sorting: { type: Number },
  excludeCity: { type: [String] },
  visible: { type: Boolean },
  navigate: { type: Boolean },
  url: { type: String },
  largue: { type: Boolean },
});

export interface ICategory extends Document {
  title: string;
  image: String;
  description: string;
  sorting: number;
  excludeCity: string[];
  visible: boolean;
  navigate: boolean;
  url: string;
}

export default mongoose.model<ICategory>("categoria", categoriaSchema);
