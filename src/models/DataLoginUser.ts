import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const loginSchema = new mongoose.Schema({
  user: {
    type: String,
    required: true,
  },
  data: {
    type: mongoose.Schema.Types.Mixed,
  },
});

export interface ILogin extends Document {
  data: any;
  user: String;
}

export default mongoose.model<ILogin>("login", loginSchema);
