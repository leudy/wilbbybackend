import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const productSchema = new mongoose.Schema(
  {
    name: { type: String, text: true },
    description: { type: String, text: true },
    nameTranslations: { type: mongoose.Schema.Types.Mixed },
    descriptionTranslations: { type: mongoose.Schema.Types.Mixed },
    account: { type: String },
    location: { type: String },
    productType: { type: Number },
    plu: { type: String },
    price: { type: Number },
    previous_price: { type: Number },
    priceLevels: { type: mongoose.Schema.Types.Mixed },
    sortOrder: { type: Number },
    deliveryTax: { type: Number },
    takeawayTax: { type: Number },
    multiply: { type: Number },
    multiMax: { type: Number, default: 1 },
    posProductId: { type: String },
    posProductCategoryId: { type: [String] },
    subProducts: { type: [String] },
    productTags: { type: [String] },
    posCategoryIds: { type: [String] },
    imageUrl: { type: String },
    max: { type: Number },
    min: { type: Number },
    capacityUsages: { type: [String] },
    parentId: { type: String },
    visible: { type: Boolean, default: true },
    snoozed: { type: Boolean },
    subProductSortOrder: { type: [String] },
    internalId: { type: String },
    new: { type: Boolean, default: false },
    popular: { type: Boolean, default: false },
    offert: { type: Boolean, default: false },
    recomended: { type: Boolean, default: false },
    quantity: { type: Number, default: 1 },
    storeId: { type: [String] },
    isBundled: { type: Boolean, default: false },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IProducts extends Document {
  name: string;
  description: string;
  nameTranslations: any;
  descriptionTranslations: any;
  account: string;
  location: string;
  productType: number;
  plu: string;
  price: number;
  priceLevels: any;
  sortOrder: number;
  deliveryTax: number;
  takeawayTax: number;
  multiply: number;
  posProductId: string;
  posProductCategoryId: string[];
  subProducts: string[];
  productTags: string[];
  posCategoryIds: string[];
  imageUrl: string;
  max: number;
  min: number;
  capacityUsages: string[];
  parentId: string;
  visible: boolean;
  snoozed: boolean;
  internalId: string;
  subProductSortOrder: string[];
  new: boolean;
  popular: boolean;
  offert: boolean;
  previous_price: number;
  recomended: boolean;
  quantity: number;
  storeId: [string];
  isBundled: boolean;
}

export default mongoose.model<IProducts>("product", productSchema);
