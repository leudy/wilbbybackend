import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const subCollectionSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  collectiontype: {
    type: String,
    required: true,
  },
  sorting: {
    type: Number,
    default: 0,
  },
});

export interface ICategory extends Document {
  title: string;
  image: String;
  description: string;
}

export default mongoose.model<ICategory>(
  "subCollectionSchema",
  subCollectionSchema
);
