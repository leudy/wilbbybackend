import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const DounloadAppSchema = new mongoose.Schema({
  source: {
    type: String,
  },
  referred: {
    type: String,
  },
  date: {
    type: Date,
  },

  campaing: {
    type: String,
  },
});

export interface IDownloadapp extends Document {
  referred: string;
  source: String;
  date: string;
  campaing: string;
}

export default mongoose.model<IDownloadapp>("Downloadapp", DounloadAppSchema);
