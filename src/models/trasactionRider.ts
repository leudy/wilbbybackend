import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const transactionSchema = new mongoose.Schema(
  {
    rider: {
      type: String,
      required: true,
    },
    km: {
      type: Number,
      required: true,
    },

    order: {
      type: Number,
    },

    total: {
      type: Number,
    },

    propina: {
      type: Number,
    },

    iva: {
      type: Number,
    },
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface ITipo extends Document {
  rider: string;
  order: number;
  total: number;
}

export default mongoose.model<ITipo>("transactionRider", transactionSchema);
