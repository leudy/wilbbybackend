import mongoose, { Document } from "mongoose";
import bcrypt from "bcryptjs";

mongoose.Promise = global.Promise;

const restaurantSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    text: true,
  },
  image: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },

  shipping: {
    type: Number,
    default: 199,
  },

  extras: {
    type: Number,
    default: 120,
  },

  minime: {
    type: Number,
  },

  menu: {
    type: String,
  },

  rating: {
    type: String,
  },

  apertura: {
    type: Number,
  },

  cierre: {
    type: Number,
  },

  aperturaMin: {
    type: Number,
  },

  cierreMin: {
    type: Number,
  },

  diaslaborales: {
    type: [String],
  },

  categoryName: {
    type: String,
  },

  categoryID: {
    type: [String],
  },

  phone: {
    type: String,
  },

  email: {
    type: String,
  },

  logo: {
    type: String,
  },

  password: {
    type: String,
  },

  type: {
    type: String,
  },

  tipo: {
    type: [String],
  },

  schedule: {
    type: mongoose.Schema.Types.Mixed,
  },

  scheduleOnly: {
    type: mongoose.Schema.Types.Mixed,
  },

  ispartners: {
    type: Boolean,
    default: true,
  },

  isnew: {
    type: Boolean,
    default: true,
  },

  llevar: {
    type: Boolean,
    default: true,
  },

  alegeno_url: {
    type: String,
  },

  autoshipping: {
    type: Boolean,
    default: false,
  },

  open: {
    type: Boolean,
    default: true,
  },

  previous_shipping: {
    type: Number,
  },

  stimateTime: {
    type: String,
  },

  slug: {
    type: String,
  },

  highkitchen: {
    type: Boolean,
    default: false,
  },

  inOffert: {
    type: Boolean,
    default: false,
  },

  includeCity: {
    type: [String],
  },

  OnesignalID: {
    type: String,
    unique: true,
  },
  channelLinkId: {
    type: String,
  },

  collections: {
    type: Boolean,
    default: false,
  },

  isDeliverectPartner: {
    type: Boolean,
    default: false,
  },

  salvingPack: {
    type: mongoose.Schema.Types.Mixed,
  },

  isOrdaticPartner: {
    type: Boolean,
    default: false,
  },

  isOrdaticPartnerID: {
    type: String,
  },

  adress: {
    type: mongoose.Schema.Types.Mixed,
  },
  city: {
    type: String,
  },

  contactCode: {
    type: String,
  },
  socialLink: {
    type: mongoose.Schema.Types.Mixed,
  },

  isOtterPartner: {
    type: Boolean,
    default: false,
  },

  OtterPartnerId: {
    type: String,
  },

  shorting: {
    type: Number,
  },

  noScheduled: {
    type: Boolean,
  },

  tagOffert: { type: String, default: null },

  cashPayment: { type: Boolean, default: false },

  forttalezaURL: { type: String, default: null },
});

export interface IRestaurant extends Document {
  email: string;
  password: string;
  isDeliverectPartner: boolean;
  image: string;
  isOrdaticPartner: boolean;
  isOrdaticPartnerID: string;
  title: string;
  isOtterPartner: boolean;
  OtterPartnerId: string;
  city: string;
  shorting: number;
  noScheduled: boolean;
  OnesignalID: string;
  categoryName: string;
  ispartners: boolean;
  autoshipping: boolean;
  adress: any;
  tagOffert: any;
  cashPayment: boolean;
  forttalezaURL: any;
  scheduleOnly: any;
  includeCity: string[];
}

// hashear los password antes de guardar
restaurantSchema.pre<IRestaurant>("save", function (next) {
  // Si el password no esta hasheado...
  if (!this.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);

    bcrypt.hash(this.password, salt, (err, hash) => {
      if (err) return next(err);
      this.password = hash;
      next();
    });
  });
});

export default mongoose.model<IRestaurant>("restaurant", restaurantSchema);
