import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const PlatoSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      text: true,
    },
    ingredientes: {
      type: String,
      text: true,
      unique: true,
    },
    price: {
      type: String,
      required: true,
    },

    previous_price: {
      type: String,
    },

    out_of_stock: {
      type: Boolean,
      default: false,
    },

    imagen: {
      type: String,
    },

    restaurant: {
      type: String,
    },

    menu: {
      type: String,
    },

    oferta: {
      type: Boolean,
      default: false,
    },

    popular: {
      type: Boolean,
      default: false,
    },

    news: {
      type: Boolean,
      default: false,
    },

    plu: {
      type: String,
    },

    deliverectID: {
      type: String,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IPlato extends Document {
  deliverectID: string;
  plu: string;
}

export default mongoose.model<IPlato>("plato", PlatoSchema);
