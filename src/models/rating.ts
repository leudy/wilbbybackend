import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const RatingSchema = new mongoose.Schema(
  {
    user: {
      type: String,
    },
    comment: {
      type: String,
    },
    value: {
      type: Number,
      required: true,
    },

    restaurant: {
      type: String,
      required: true,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IRating extends Document {
  user: string;
  comment: String;
  value: number;
}

export default mongoose.model<IRating>("rating", RatingSchema);
