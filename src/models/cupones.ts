import mongoose, { Document } from "mongoose";

//javiermartinez1990@gmail.com

mongoose.Promise = global.Promise;

const cuponesSchema = new mongoose.Schema({
  clave: { type: String, unique: true },
  descuento: Number,
  tipo: {
    type: String,
    enum: ["porcentaje", "dinero"],
    default: "porcentaje",
  },
  usage: {
    type: Number,
    default: 1,
  },
  expire: {
    type: Date,
    default: new Date(),
  },
  exprirable: {
    type: Boolean,
    default: false,
  },
  user: {
    type: String,
  },
  description: {
    type: String,
  },

  private: {
    type: Boolean,
    default: false,
  },

  uniqueStore: {
    type: Boolean,
    default: false,
  },

  store: {
    type: String,
  },
  city: { type: [String] },
  isShipping: {
    type: Boolean,
    default: false,
  },
});

export interface ICupon extends Document {
  clave: string;
  tipo: String;
  descuento: string;
  usage: number;
  expire: any;
  exprirable: boolean;
  user: string;
  description: string;
  city: string[];
  isShipping: boolean;
}

export default mongoose.model<ICupon>("cupones", cuponesSchema);
