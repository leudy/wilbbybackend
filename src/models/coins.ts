import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const conisSchema = new mongoose.Schema(
  {
    value: {
      type: Number,
      required: true,
      default: 0,
    },
    user: { type: mongoose.Schema.Types.ObjectId, ref: "user", required: true },
    order: { type: mongoose.Schema.Types.ObjectId, ref: "newOrder" },
    type: {
      type: String,
      enum: ["Discount", "Plus", "Detained"],
      required: true,
    },
    orderData: { type: mongoose.Schema.Types.Mixed },
    title: { type: String },
    description: { type: String },
    status: {
      type: String,
      enum: ["Success", "Cancelled", "Retired", "Pending", "Error"],
      required: true,
    },
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface ICoins extends Document {
  value: number;
  user: string;
  order: string;
  title: string;
  description: string;
  orderData: any;
  created_at: Date;
  updated_at: Date;
}

export default mongoose.model<ICoins>("coin", conisSchema);
