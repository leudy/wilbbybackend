import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const offertsSchema = new mongoose.Schema({
  imagen: {
    type: String,
  },
  city: {
    type: String,
  },
  store: {
    type: String,
  },

  slug: {
    type: String,
  },

  apertura: {
    type: Number,
  },

  cierre: {
    type: Number,
  },

  open: {
    type: Boolean,
    default: true,
  },
});

export interface ICity extends Document {
  city: string;
  image: String;
  store: string;
}

export default mongoose.model<ICity>("offert", offertsSchema);
