import mongoose, { Document } from "mongoose";
import { IUser } from "../user";
import { IRider } from "../riders";
import { IRestaurant } from "../restaurant";

mongoose.Promise = global.Promise;

const orderSchema = new mongoose.Schema(
  {
    channelOrderDisplayId: { type: Number, unique: true },
    orderType: { type: String, enum: ["pickup", "delivery", "eat in"] },
    pickupTime: { type: Date },
    estimatedPickupTime: { type: Date },
    deliveryTime: { type: Date },
    courier: { type: mongoose.Schema.Types.ObjectId, ref: "rider" },
    courierData: { type: mongoose.Schema.Types.Mixed },
    customerData: { type: mongoose.Schema.Types.Mixed },
    customer: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    store: { type: mongoose.Schema.Types.ObjectId, ref: "restaurant" },
    storeData: { type: mongoose.Schema.Types.Mixed },
    deliveryAddressData: { type: mongoose.Schema.Types.Mixed },
    deliveryAddress: { type: mongoose.Schema.Types.ObjectId, ref: "adress" },
    orderIsAlreadyPaid: { type: Boolean, default: true },
    payment: { type: mongoose.Schema.Types.Mixed },
    note: { type: String },
    items: { type: mongoose.Schema.Types.Mixed },
    numberOfCustomers: { type: Number },
    deliveryCost: { type: Number },
    serviceCharge: { type: Number },
    discountTotal: { type: Number },
    IntegerValue: {
      type: Number,
      enum: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140],
      default: 10,
    },
    tip: { type: Number, default: 0 },
    paymentMethod: {
      type: String,
      enum: [
        "Paypal",
        "Tarjeta de credito",
        "Efectivo",
        "Apple Pay",
        "Google Pay",
        "Datafono",
      ],
      default: "Tarjeta de credito",
    },
    cupon: { type: mongoose.Schema.Types.ObjectId, ref: "cupones" },
    statusProcess: { type: mongoose.Schema.Types.Mixed },
    pagoPaypal: { type: mongoose.Schema.Types.Mixed },
    stripePaymentIntent: { type: mongoose.Schema.Types.Mixed },
    Needcutlery: {
      type: Boolean,
      default: false,
    },
    status: {
      type: String,
      enum: [
        "Pendiente de pago",
        "Nueva",
        "Confirmada",
        "En la cocina",
        "Listo para recoger",
        "Preparando para el envío",
        "En camino",
        "Entregada",
        "Rechazado",
        "Rechazada por la tienda",
        "Rechazada por el rider",
        "Devuelto",
        "Finalizada",
        "Resolución",
      ],
      default: "Nueva",
    },
    holdedID: {
      type: String,
    },

    holdedRidersID: {
      type: String,
    },

    holdedPartnerID: {
      type: String,
    },

    invoiceUrl: {
      type: String,
    },

    reportRiders: {
      type: mongoose.Schema.Types.Mixed,
    },

    offertFromRider: {
      type: [mongoose.Schema.Types.Mixed],
    },

    budgetRider: {
      type: [String],
    },

    riderAcceptOrder: {
      type: Boolean,
      default: false,
    },
    scheduled: {
      type: Boolean,
      default: false,
    },

    eventId: {
      type: String,
    },

    cuponName: {
      type: String,
    },
    cuponTipe: {
      type: String,
    },
    cuponValue: {
      type: Number,
    },

    source: {
      type: mongoose.Schema.Types.Mixed,
    },
    platform: {
      type: String,
    },
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

orderSchema.virtual("descuento", {
  ref: "cupones",
  localField: "cupon",
  foreignField: "_id",
  justOne: true,
});

export interface IOrders extends Document {
  cuponName: string;
  cuponTipe: string;
  cuponValue: number;
  channelOrderDisplayId: number;
  orderType: string;
  pickupTime: { type: Date };
  estimatedPickupTime: { type: Date };
  deliveryTime: { type: Date };
  courier: any;
  courierData: IRider;
  customerData: IUser;
  customer: string;
  store: string;
  storeData: IRestaurant;
  deliveryAddressData: any;
  deliveryAddress: string;
  orderIsAlreadyPaid: boolean;
  payment: number;
  note: string;
  items: any;
  numberOfCustomers: number;
  deliveryCost: number;
  serviceCharge: number;
  discountTotal: number;
  IntegerValue: number;
  paymentMethod: string;
  cupon: string;
  statusProcess: any[];
  pagoPaypal: any;
  stripePaymentIntent: any;
  Needcutlery: boolean;
  status: string;
  holdedID: string;
  holdedRidersID: string;
  holdedPartnerID: string;
  invoiceUrl: string;
  reportRiders: any;
  tip: number;
  offertFromRider: any[];
  riderAcceptOrder: boolean;
  scheduled: boolean;
  budgetRider: string[];
  created_at: string;
  eventId: string;
  source: string;
  platform: string;
}

export default mongoose.model<IOrders>("newOrder", orderSchema);
