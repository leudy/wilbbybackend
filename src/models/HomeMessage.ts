import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const homeMessageSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    text: true,
  },
  message: {
    type: String,
    required: true,
    text: true,
  },
  active: {
    type: Boolean,
    required: true,
  },

  city: {
    type: String,
    required: true,
  },
});

export interface IHome extends Document {
  title: string;
  message: String;
  active: boolean;
  city: string;
}

export default mongoose.model<IHome>("homeMessage", homeMessageSchema);
