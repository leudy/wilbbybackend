export const storeFormat = () => {
  return {
    title: "La Taberna de Sancho Panza",
    image: "https://header-store-wilbby.s3.eu-west-3.amazonaws.com/sancho.webp",
    description: `Disfruta de la mejor comida de la mano de La Taberna de Sancho Panza a tráves de Wilbby te lo llevamos en minutos.`,
    shipping: 0,
    extras: 50,
    minime: 100,
    rating: "0.0",
    diaslaborales: [
      "Lunes",
      "Marte",
      "Miécoles",
      "Jueves",
      "Viernes",
      "Sábado",
      "Domingo",
    ],
    categoryName: "Restaurantes",
    categoryID: ["5fb7a32cb234a46c09297804"],
    phone: "+34 618 41 80 63",
    email: "cbcrichard1974@gmail.com",
    logo: "https://header-store-wilbby.s3.eu-west-3.amazonaws.com/sancho.webp",
    password: "admin",
    type: "Mediterránea",
    tipo: ["601004a6a60465276b82abf3"],
    schedule: {
      Monday: {
        day: "Lunes",
        isOpen: true,
        openHour1: 9,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 59,
        closetMinute2: null,
      },
      Tuesday: {
        day: "Martes",
        isOpen: true,
        openHour1: 9,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 59,
        closetMinute2: null,
      },
      Wednesday: {
        day: "Miércoles",
        isOpen: true,
        openHour1: 9,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 59,
        closetMinute2: null,
      },
      Thursday: {
        day: "Jueves",
        isOpen: false,
        openHour1: 9,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 59,
        closetMinute2: null,
      },
      Friday: {
        day: "Viernes",
        isOpen: true,
        openHour1: 9,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 59,
        closetMinute2: null,
      },
      Saturday: {
        day: "Sábado",
        isOpen: true,
        openHour1: 9,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 59,
        closetMinute2: null,
      },
      Sunday: {
        day: "Domingo",
        isOpen: true,
        openHour1: 9,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 59,
        closetMinute2: null,
      },
    },
    ispartners: true,
    isnew: true,
    llevar: true,
    alegeno_url: "",
    autoshipping: false,
    open: true,
    previous_shipping: null,
    stimateTime: "35-45 min",
    slug: "la-taberna-de-sancho-panza",
    highkitchen: false,
    inOffert: false,
    includeCity: ["Valdepeñas"],
    OnesignalID: "",
    channelLinkId: "",
    collections: false,
    isDeliverectPartner: false,
    salvingPack: {},
    isOrdaticPartner: false,
    isOrdaticPartnerID: "",
    adress: {
      calle: "C. de la Virgen",
      numero: "59",
      codigoPostal: "13300",
      ciudad: "Valdepeñas",
      lat: "38.7575204",
      lgn: "-3.3856546",
    },
    city: "Valdepeñas",
    contactCode: "",
    socialLink: {
      instagram: "",
      facebook: "",
      twitter: "",
      web: "",
    },
    isOtterPartner: false,
    OtterPartnerId: "",
    scheduleOnly: {
      available: false,
      hour: 0,
    },
    shorting: 0,
    noScheduled: false,
    cashPayment: true,
    tagOffert: null,
  };
};

export const setHorario = () => {
  return {
    schedule: {
      Monday: {
        day: "Lunes",
        isOpen: true,
        openHour1: 12,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 30,
        closetMinute2: null,
      },
      Tuesday: {
        day: "Martes",
        isOpen: true,
        openHour1: 12,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 30,
        closetMinute2: null,
      },
      Wednesday: {
        day: "Miércoles",
        isOpen: true,
        openHour1: 12,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 30,
        closetMinute2: null,
      },
      Thursday: {
        day: "Jueves",
        isOpen: true,
        openHour1: 12,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 30,
        closetMinute2: null,
      },
      Friday: {
        day: "Viernes",
        isOpen: true,
        openHour1: 12,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 30,
        closetMinute2: null,
      },
      Saturday: {
        day: "Sábado",
        isOpen: true,
        openHour1: 12,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 30,
        closetMinute2: null,
      },
      Sunday: {
        day: "Domingo",
        isOpen: true,
        openHour1: 12,
        openHour2: null,
        openMinute1: 0,
        openMinute2: null,
        closetHour1: 23,
        closetHour2: null,
        closetMinute1: 30,
      },
    },
  };
};
