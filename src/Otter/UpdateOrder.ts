import { IOrders } from "../models/newOrder/order";
import { orderURL, client_id, client_secret, authURL } from "./config";
import { dataUpdate } from "./data";
var request = require("request");

var details = {
  client_id: client_id,
  client_secret: client_secret,
  grant_type: "client_credentials",
  scope: "orders.status_update",
};

let formBody = [];
for (var property in details) {
  var encodedKey = encodeURIComponent(property);
  var encodedValue = encodeURIComponent(details[property]);
  //@ts-ignore
  formBody.push(encodedKey + "=" + encodedValue);
}
//@ts-ignore
formBody = formBody.join("&");

var options = {
  method: "POST",
  url: authURL,
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
  },
  body: formBody,
};

export const UpdateOrder = (order: IOrders, status: string) => {
  request(options, function (error, response) {
    const auth = JSON.parse(response.body);
    var opciones = {
      method: "POST",
      url: `${orderURL}/${order._id}/status`,
      headers: {
        "Content-Type": "application/json",
        "X-Application-Id": client_id,
        "X-Event-Id": order.eventId,
        "X-Store-Id": order.storeData.OtterPartnerId,
        Authorization: `Bearer ${auth.access_token}`,
      },
      body: JSON.stringify(dataUpdate(status)),
    };

    request(opciones, function (error, response: any) {
      console.log(response.body, response.statusCode, response.statusMessage);
    });
  });
};
