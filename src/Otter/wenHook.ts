import { Request, Response, Router } from "express";
import { ordenUpdate } from "../Deliverect/ordenUpdate";
import {
  sendNotification,
  sendNotificationRider,
} from "../Deliverect/sendNotification";
import OrderSchema from "../models/newOrder/order";
import { UpdateOrder } from "./UpdateOrder";

class OtterRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.post(
      "/v1/otter/update-status",
      async (req: Request, res: Response) => {
        const data = req.body;

        const orden = await OrderSchema.findOne({
          _id: data.metadata.resourceId,
        });

        data.metadata.payload.orderStatusHistory.forEach((element) => {
          if (element.status === "RECIVED_ORDER") {
            sendNotification(
              //@ts-ignore
              orden?.customerData.OnesignalID,
              `${orden?.storeData.title} Ha recibido tu pedido.`,
              orden
            );
          } else if (element.status === "ORDER_ACCEPTED") {
            OrderSchema.findOneAndUpdate(
              { _id: data.metadata.resourceId },
              {
                $set: {
                  eventId: data.eventId,
                },
              },
              (err, order) => {}
            );
            if (data.metadata.payload.orderStatusHistory.length === 1) {
              ordenUpdate(orden?._id, "Confirmada", 60);
              sendNotification(
                //@ts-ignore
                orden?.customerData.OnesignalID,
                `${orden?.storeData.title} Ha confirmado tu pedido.`,
                orden
              );
            }
          } else if (element.status === "ORDER_READY_TO_PICKUP") {
            if (orden?.storeData.autoshipping) {
              UpdateOrder(orden, "FULFILLED");
              ordenUpdate(orden?._id, "Entregada", 90);
              sendNotification(
                //@ts-ignore
                orden?.customerData.OnesignalID,
                `${orden?.storeData.title} a salido a entregar tu pedido.`,
                orden
              );
            } else {
              ordenUpdate(orden?._id, "Listo para recoger", 70);
              sendNotification(
                //@ts-ignore
                orden?.customerData.OnesignalID,
                `${orden?.storeData.title} tiene tu pedido listo para recoger.`,
                orden
              );

              sendNotificationRider(
                orden?.courierData.OnesignalID,
                `${orden?.storeData.title} tiene el pedido listo para recoger.`
              );
            }
          } else if (element.status === "ORDER_FULFILLED") {
            sendNotification(
              //@ts-ignore
              orden?.customerData.OnesignalID,
              `${orden?.storeData.title} a finalizado tu pedido.`,
              orden
            );
          } else if (element.status === "CANCELED") {
            ordenUpdate(orden?._id, "Devuelto", 10);
            sendNotification(
              //@ts-ignore
              orden?.customerData.OnesignalID,
              `Upps el pedido ha fallado lo sentimos.`,
              orden
            );
          }
        });
        res.status(200).end();
      }
    );
  }
}

const otterRouter = new OtterRouter();
otterRouter.routes();

export default otterRouter.router;
