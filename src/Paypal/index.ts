import { Request, Response, Router } from "express";
import paypal from "paypal-rest-sdk";
import dotenv from "dotenv";
import custonoderSchema from "../models/custonorder";
import { scheduleTime } from "../funtions/scheduleTime";
import { getMariaHorario } from "../funtions/ScheduledMaria";
import { AsingRiderCustonOrder } from "../funtions/asingRidersCustonOrder";
dotenv.config({ path: "variables.env" });

paypal.configure({
  mode: "live", //sandbox or live
  client_id: process.env.PAYPALCLIENTID || "",
  client_secret: process.env.PAYPALCLIENTSECRET || "",
});

class PaypalRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/paypal", (req: Request, res: Response) => {
      const { price } = req.query;
      let create_payment_json = {
        intent: "sale",
        payer: {
          payment_method: "paypal",
        },
        redirect_urls: {
          return_url: `https://api.v2.wilbby.com/success?price=${price}`,
          cancel_url: "https://api.v2.wilbby.com/cancel",
        },
        transactions: [
          {
            amount: {
              currency: "EUR",
              total: req.query.price,
            },
            description: "Peinado & Perales SL (Wilbby)",
          },
        ],
      };
      // @ts-ignore
      paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
          res.render("cancel");
        } else {
          // @ts-ignore
          res.redirect(payment.links[1].href);
        }
      });
    });

    this.router.get("/success", (req: Request, res: Response) => {
      var PayerID = req.query.PayerID;
      var paymentId = req.query.paymentId;

      const { price } = req.query;

      var execute_payment_json = {
        payer_id: PayerID,
        transactions: [
          {
            amount: {
              currency: "EUR",
              total: price,
            },
          },
        ],
      };
      // @ts-ignore
      paypal.payment.execute(
        // @ts-ignore
        paymentId,
        execute_payment_json,
        function (error, payment) {
          if (error) {
            res.render("cancel");
          }
          if (payment) {
            res.render("success");
          }
        }
      );
    });
    this.router.get("/cancel", (req: Request, res: Response) => {
      res.render("cancel");
    });

    this.router.get("/paypal-custom-order", (req: Request, res: Response) => {
      const { price, order, currency } = req.query;
      let create_payment_json = {
        intent: "sale",
        payer: {
          payment_method: "paypal",
        },
        redirect_urls: {
          return_url: `https://api.v2.wilbby.com/success-custom?price=${price}&order=${order}&currency=${currency}`,
          cancel_url: "https://api.v2.wilbby.com/cancel-custom",
        },
        transactions: [
          {
            amount: {
              currency: "EUR",
              total: req.query.price,
            },
            description: "Peinado & Perales SL (Wilbby)",
          },
        ],
      };
      // @ts-ignore
      paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
          res.render("cancel");
        } else {
          // @ts-ignore
          res.redirect(payment.links[1].href);
        }
      });
    });

    this.router.get("/success-custom", (req: Request, res: Response) => {
      var PayerID = req.query.PayerID;
      var paymentId = req.query.paymentId;
      const { price, order, currency } = req.query;
      var execute_payment_json = {
        payer_id: PayerID,
        transactions: [
          {
            amount: {
              currency: "EUR",
              total: price,
            },
          },
        ],
      };
      // @ts-ignore
      paypal.payment.execute(
        // @ts-ignore
        paymentId,
        execute_payment_json,
        function (error, payment) {
          if (error) {
            res.render("cancel");
          }
          if (payment) {
            custonoderSchema.findOneAndUpdate(
              { _id: order },
              {
                estado: "Pagado",
                progreso: "25",
                status: "active",
                pagoPaypal: payment,
              },
              (err, orde: any) => {
                if (orde) {
                  if (scheduleTime(getMariaHorario().schedule)) {
                    AsingRiderCustonOrder(orde);
                  }
                  res.render("success");
                }
              }
            );
          }
        }
      );
    });
    this.router.get("/cancel-custom", (req: Request, res: Response) => {
      res.render("cancel");
    });
  }
}

const paypalRouter = new PaypalRouter();
paypalRouter.routes();

export default paypalRouter.router;
