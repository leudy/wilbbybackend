import orderSchema from "../models/newOrder/order";
import { setOrderToDeliverect } from "../Deliverect/Deliverect";
import { pushOrderTopOrdatic } from "../Ordatic/api";
import { pushOrderToOtter } from "../Otter/api";
import { setOrderToHolded } from "../Holded/Apis";
import { sendNotification } from "./sendNotificationToStore";
import { scheduleTime } from "../funtions/scheduleTime";
import { getMariaHorario } from "../funtions/ScheduledMaria";
import { AsingRider } from "../funtions/asingRiders";

export const UpdateOrder = (order, paymentIntent, res) => {
  orderSchema.findOneAndUpdate(
    { _id: order },
    {
      status: "Nueva",
      IntegerValue: 30,
      paymentMethod: "Tarjeta de credito",
      stripePaymentIntent: paymentIntent,
      orderIsAlreadyPaid: true,
      //@ts-ignore
      $push: {
        statusProcess: { status: "Nueva", date: new Date() },
      },
    },
    { upsert: true },
    // @ts-ignore
    (err, order: IOrders) => {
      if (!err) {
        if (order?.storeData.isDeliverectPartner) {
          setOrderToDeliverect(order);
        } else if (order?.storeData.isOrdaticPartner) {
          pushOrderTopOrdatic(order);
        } else if (order?.storeData.isOtterPartner) {
          pushOrderToOtter(order);
        } else {
          sendNotification(
            order?.storeData.OnesignalID,
            "Has recibido un nuevo pedido a por todas 🌮 🌯 🫔 🥗 🥘"
          );
        }

        if (
          scheduleTime(getMariaHorario().schedule) &&
          order.orderType === "delivery"
        ) {
          AsingRider(order);
        }
        res.status(200).json(paymentIntent).end();
        setOrderToHolded(order);
      }
    }
  );
};
