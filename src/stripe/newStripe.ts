import { Router, Request, Response } from "express";
import dotenv from "dotenv";

dotenv.config({ path: "variables.env" });
var Stripe = require("stripe")(process.env.STRIPECLIENTSECRET);

class NewStripeRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.post("/create-payment-intent", async (req, res) => {
      const {
        amount,
        customer,
        payment_type,
        currency,
        request_three_d_secure,
        payment_method_types = [],
      }: {
        amount: number;
        currency: string;
        payment_method_types: string[];
        request_three_d_secure: "any" | "automatic";
        customer: string;
        uderID: string;
        payment_type: string;
      } = req.body;

      const params = {
        amount: amount,
        currency: "EUR",
        customer: customer,
        payment_method_options: {
          card: {
            request_three_d_secure: request_three_d_secure || "automatic",
          },
        },
        payment_method_types: payment_method_types,
      };

      try {
        const paymentIntent = await Stripe.paymentIntents.create(params);
        return res
          .status(200)
          .send({
            paymentIntent: paymentIntent,
          })
          .end();
      } catch (error) {
        return res
          .send({
            //@ts-ignore
            error: error.raw.message,
          })
          .end();
      }
    });

    this.router.post("/payment-card", async (req: Request, res: Response) => {
      const { payment_method, customers, amount, currency } = req.body;
      const total = amount.toFixed(0);
      try {
        const paymentIntent = await Stripe.paymentIntents.create({
          payment_method: payment_method,
          amount: total,
          currency: "EUR",
          customer: customers,
          confirmation_method: "automatic",
          confirm: true,
        });
        if (paymentIntent.status === "succeeded") {
          res.status(200).json(paymentIntent).end();
        } else if (paymentIntent.status === "canceled") {
          res.status(400).json(paymentIntent).end();
        } else if (paymentIntent.status === "requires_action") {
          res.status(200).json(paymentIntent).end();
        } else {
          res.status(400).json(paymentIntent).end();
        }
      } catch (err) {
        console.log("Error code is: ", err);
        const paymentIntentRetrieved = await Stripe.paymentIntents.retrieve(
          //@ts-ignore
          err.raw.payment_intent.id
        );
        console.log("PI retrieved: ", paymentIntentRetrieved.id);
        res.status(400).json(paymentIntentRetrieved).end();
      }
    });

    this.router.post("/cash-payment", async (req: Request, res: Response) => {
      setTimeout(() => {
        res.status(200).json({ success: true }).end();
      }, 2000);
    });
  }
}

const newStripeRouterRouter = new NewStripeRouter();
newStripeRouterRouter.routes();
export default newStripeRouterRouter.router;
