const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);

export const chargeToken = async (
  amount: any,
  stripeToken: any,
  currency: string
) => {
  const total = Number(amount).toFixed(0);
  let response = await stripe.charges.create({
    amount: total,
    currency: currency ? currency : "EUR",
    source: stripeToken,
    capture: true,
  });
  return response;
};
