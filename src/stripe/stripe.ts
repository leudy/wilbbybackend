import { Request, Response, Router } from "express";
import dotenv from "dotenv";
import userSchema from "../models/user";
import { chargeToken } from "./changeToken";
import orderSchema, { IOrders } from "../models/newOrder/order";
import cartSchema from "../models/newOrder/addToCart";
import customorderSchema from "../models/custonorder";
import { setOrderToDeliverect } from "../Deliverect/Deliverect";
import { pushOrderTopOrdatic } from "../Ordatic/api";
import { pushOrderToOtter } from "../Otter/api";
import { setOrderToHolded } from "../Holded/Apis";
import { sendNotification } from "./sendNotificationToStore";
import { scheduleTime } from "../funtions/scheduleTime";
import { getMariaHorario } from "../funtions/ScheduledMaria";
import { AsingRider } from "../funtions/asingRiders";
import { AsingRiderCustonOrder } from "../funtions/asingRidersCustonOrder";

var stripe = require("stripe")(process.env.STRIPECLIENTSECRET);
dotenv.config({ path: "variables.env" });

class StripeRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/create-client", async (req: Request, res: Response) => {
      const { userID, nameclient, email } = req.query;
      await stripe.customers.create(
        {
          name: nameclient,
          email: email,
          description: "Clientes de Wilbby",
        },
        function (err: any, customer: any) {
          userSchema.findOneAndUpdate(
            { _id: userID },
            {
              $set: {
                StripeID: customer.id,
              },
            },
            (err, customers) => {
              if (err) {
                console.log(err);
              }
            }
          );
        }
      );
    });

    this.router.get("/card-create", async (req: Request, res: Response) => {
      const { customers, token } = req.query;
      const paymentMethod = await stripe.paymentMethods.attach(token, {
        customer: customers,
      });
      if (paymentMethod) {
        res.status(200).json({ success: true, data: paymentMethod }).end();
      } else {
        res
          .status(400)
          .json({ success: false, data: "Algo salio mas intentalo de nuevo" })
          .end();
      }
    });

    this.router.get("/get-card", async (req: Request, res: Response) => {
      const { customers } = req.query;
      if (customers) {
        const paymentMethods = await stripe.paymentMethods.list({
          customer: customers,
          type: "card",
        });
        res.status(200).json(paymentMethods).end();
      } else {
        res.status(400).json(null).end();
      }
    });

    this.router.get("/delete-card", async (req: Request, res: Response) => {
      const { cardID, customers } = req.query;
      await stripe.customers.deleteSource(
        customers,
        cardID,
        function (err: any, confirmation: any) {
          res.json(confirmation);
        }
      );
    });

    this.router.post("/create-card", async (req: Request, res: Response) => {
      const { customer, paymentMethod } = req.body;
      const paymentMethods = await stripe.paymentMethods.attach(paymentMethod, {
        customer: customer,
      });

      if (paymentMethods) {
        res
          .status(200)
          .json({
            success: true,
            message: "success_add_card",
            data: paymentMethods,
          })
          .end();
      } else {
        res
          .status(400)
          .json({ success: false, message: "error_add_card", data: null })
          .end();
      }
    });

    this.router.get("/delete-card-web", async (req: Request, res: Response) => {
      const { cardID } = req.query;
      const done = await stripe.paymentMethods.detach(cardID);
      if (done) {
        res
          .status(200)
          .json({ success: true, message: "success_delete_card" })
          .end();
      } else {
        res
          .status(400)
          .json({ success: false, message: "error_delete_card" })
          .end();
      }
    });

    this.router.post(
      "/payment-existing-card-custom-order",
      async (req: Request, res: Response) => {
        const { card, customers, amount, orders, currency } = req.body;

        console.log(card, customers, amount, orders, currency);

        const total = Number(amount).toFixed(0);

        try {
          const paymentIntent = await stripe.paymentIntents.create({
            payment_method: card,
            amount: total,
            currency: currency ? currency : "EUR",
            customer: customers,
            confirmation_method: "automatic",
            confirm: true,
          });

          if (paymentIntent.status === "succeeded") {
            customorderSchema.findOneAndUpdate(
              { _id: orders },
              {
                estado: "Pagado",
                progreso: "25",
                status: "active",
                stripePaymentIntent: paymentIntent,
              },
              (err: any, orde: any) => {
                if (err) {
                  res.status(400).json(err).end();
                } else {
                  if (scheduleTime(getMariaHorario().schedule)) {
                    AsingRiderCustonOrder(orde);
                  }
                  res.status(200).json(paymentIntent).end();
                }
              }
            );
          } else if (paymentIntent.status === "canceled") {
            res.status(200).json(paymentIntent).end();
          } else if (paymentIntent.status === "requires_action") {
            customorderSchema.findOneAndUpdate(
              { _id: orders },
              {
                estado: "Pagado",
                progreso: "25",
                status: "active",
                stripePaymentIntent: paymentIntent,
              },
              (err: any, orde: any) => {
                if (err) {
                  res.status(400).json(err).end();
                } else {
                  if (scheduleTime(getMariaHorario().schedule)) {
                    AsingRiderCustonOrder(orde);
                  }
                  res.status(200).json(paymentIntent).end();
                }
              }
            );
          } else {
            res.status(400).json(paymentIntent).end();
          }
        } catch (err) {
          console.log("Error code is: ", err);
          const paymentIntentRetrieved = await stripe.paymentIntents.retrieve(
            //@ts-ignore
            err.raw.payment_intent.id
          );
          console.log("PI retrieved: ", paymentIntentRetrieved.id);
          res.status(400).json(paymentIntentRetrieved).end();
        }
      }
    );

    this.router.post("/stripe/chargeToken", (req: Request, res: Response) => {
      const { stripeToken, amount, orders, currency } = req.body;
      if (stripeToken && amount != undefined) {
        chargeToken(amount, stripeToken, currency)
          .then(async (response: any) => {
            if (response.status == "succeeded") {
              customorderSchema.findOneAndUpdate(
                { _id: orders },
                {
                  estado: "Pagado",
                  progreso: "25",
                  status: "active",
                  stripePaymentIntent: response,
                },
                (err, orde: any) => {
                  if (scheduleTime(getMariaHorario().schedule)) {
                    AsingRiderCustonOrder(orde);
                  }
                  res.json(response).status(200).end();
                }
              );
            } else {
              res.status(401);
            }
          })
          .catch((err: any) => {
            res.status(401).send({ success: false, error: err });
          });
      } else {
        res.status(403).send({ success: false, error: "Invalid token" });
      }
    });

    this.router.post(
      "/stripe/chargeToken/order",
      (req: Request, res: Response) => {
        const { stripeToken, amount, orders, userID, currency } = req.body;
        if (stripeToken && amount != undefined) {
          chargeToken(amount, stripeToken, currency)
            .then(async (response: any) => {
              if (response.status == "succeeded") {
                orderSchema.findOneAndUpdate(
                  { _id: orders },
                  {
                    status: "Nueva",
                    IntegerValue: 30,
                    paymentMethod: "Tarjeta de credito",
                    stripePaymentIntent: response,
                    orderIsAlreadyPaid: true,
                    //@ts-ignore
                    $push: {
                      statusProcess: { status: "Nueva", date: new Date() },
                    },
                  },
                  { upsert: true },
                  // @ts-ignore
                  (err, order: IOrders) => {
                    if (!err) {
                      if (order?.storeData.isDeliverectPartner) {
                        setOrderToDeliverect(order);
                      } else if (order?.storeData.isOrdaticPartner) {
                        pushOrderTopOrdatic(order);
                      } else if (order?.storeData.isOtterPartner) {
                        pushOrderToOtter(order);
                      } else {
                        sendNotification(
                          order?.storeData.OnesignalID,
                          "Has recibido un nuevo pedido a por todas 🌮 🌯 🫔 🥗 🥘"
                        );

                        if (
                          scheduleTime(getMariaHorario().schedule) &&
                          order.orderType === "delivery"
                        ) {
                          AsingRider(order);
                        }
                      }
                      setOrderToHolded(order);
                      cartSchema
                        .deleteMany({ userId: String(userID) })
                        .then(function () {
                          console.log("Data deleted cart"); // Success
                        })
                        .catch(function (error) {
                          console.log(error); // Failure
                        });
                    }
                  }
                );
                res.status(200).json(response).end();
              } else {
                res.status(200).json(response).end();
              }
            })
            .catch((err: any) => {
              console.log(err);
              res.status(401).send({ success: false, error: err });
            });
        } else {
          res.status(403).send({ success: false, error: "Invalid token" });
        }
      }
    );
  }
}

const stripeRouter = new StripeRouter();
stripeRouter.routes();

export default stripeRouter.router;
