import { Router, Request, Response } from "express";
import dotenv from "dotenv";
import customorderSchema from "../models/custonorder";
import { scheduleTime } from "../funtions/scheduleTime";
import { getMariaHorario } from "../funtions/ScheduledMaria";
import { AsingRiderCustonOrder } from "../funtions/asingRidersCustonOrder";

dotenv.config({ path: "variables.env" });
var Stripe = require("stripe")(process.env.STRIPECLIENTSECRET);

class NewStripeOrderRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.post("/create-payment-intent-order", async (req, res) => {
      const {
        amount,
        orderID,
        customer,
        uderID,
        payment_type,
        currency,
        request_three_d_secure,
        payment_method_types = [],
      }: {
        amount: number;
        currency: string;
        payment_method_types: string[];
        request_three_d_secure: "any" | "automatic";
        orderID: string;
        customer: string;
        uderID: string;
        payment_type: string;
      } = req.body;

      const params = {
        amount: amount,
        currency: "EUR",
        customer: customer,
        payment_method_options: {
          card: {
            request_three_d_secure: request_three_d_secure || "automatic",
          },
        },
        payment_method_types: payment_method_types,
      };

      try {
        const paymentIntent = await Stripe.paymentIntents.create(params);
        customorderSchema.findOneAndUpdate(
          { _id: orderID },
          {
            stripePaymentIntent: paymentIntent,
          },
          (err: any, orde: any) => {
            if (!err) {
              return res.send({
                clientSecret: paymentIntent.client_secret,
              });
            }
          }
        );
      } catch (error) {
        console.log("error payment", error);
        return res.send({
          //@ts-ignore
          error: error.raw.message,
        });
      }
    });

    this.router.post(
      "/payment-card-order",
      async (req: Request, res: Response) => {
        const { payment_method, customers, amount, currency, order } = req.body;
        const total = amount.toFixed(0);
        try {
          const paymentIntent = await Stripe.paymentIntents.create({
            payment_method: payment_method,
            amount: total,
            currency: "EUR",
            customer: customers,
            confirmation_method: "automatic",
            confirm: true,
          });
          if (paymentIntent.status === "succeeded") {
            customorderSchema.findOneAndUpdate(
              { _id: order },
              {
                estado: "Pagado",
                progreso: "25",
                status: "active",
                stripePaymentIntent: paymentIntent,
              },
              (err: any, orde: any) => {
                if (err) {
                  res.status(400).json(err).end();
                } else {
                  if (scheduleTime(getMariaHorario().schedule)) {
                    AsingRiderCustonOrder(orde);
                  }
                  res.status(200).json(paymentIntent).end();
                }
              }
            );
          } else if (paymentIntent.status === "canceled") {
            res.status(400).json(paymentIntent).end();
          } else if (paymentIntent.status === "requires_action") {
            customorderSchema.findOneAndUpdate(
              { _id: order },
              {
                stripePaymentIntent: paymentIntent,
              },
              (err: any, orde: any) => {
                if (err) {
                  res.status(400).json(err).end();
                } else {
                  res.status(200).json(paymentIntent).end();
                }
              }
            );
          } else {
            res.status(400).json(paymentIntent).end();
          }
        } catch (err) {
          console.log("Error code is: ", err);
          const paymentIntentRetrieved = await Stripe.paymentIntents.retrieve(
            //@ts-ignore
            err.raw.payment_intent.id
          );
          console.log("PI retrieved: ", paymentIntentRetrieved.id);
          res.status(400).json(paymentIntentRetrieved).end();
        }
      }
    );

    this.router.post(
      "/update-order-3d-secure-order",
      async (req: Request, res: Response) => {
        const { order } = req.body;
        customorderSchema.findOneAndUpdate(
          { _id: order },
          {
            estado: "Pagado",
            progreso: "25",
            status: "active",
          },
          (err: any, orde: any) => {
            if (err) {
              res.status(400).json(err).end();
            } else {
              if (scheduleTime(getMariaHorario().schedule)) {
                AsingRiderCustonOrder(orde);
              }
              res.status(200).json({ success: true }).end();
            }
          }
        );
      }
    );
  }
}

const newStripeOrderRouterRouter = new NewStripeOrderRouter();
newStripeOrderRouterRouter.routes();
export default newStripeOrderRouterRouter.router;
