import { IOrders } from "../models/newOrder/order";
import { setOrderToDeliverect } from "../Deliverect/Deliverect";
import { pushOrderTopOrdatic } from "../Ordatic/api";
import { pushOrderToOtter } from "../Otter/api";
import { setOrderToHolded } from "../Holded/Apis";
import { sendNotification } from "../stripe/sendNotificationToStore";
import { scheduleTime } from "../funtions/scheduleTime";
import { getMariaHorario } from "../funtions/ScheduledMaria";
import { AsingRider } from "../funtions/asingRiders";

export const sendNotificationToParnert = (order: IOrders, store: any) => {
  if (store.isDeliverectPartner) {
    setOrderToDeliverect(order);
  } else if (store.isOrdaticPartner) {
    pushOrderTopOrdatic(order);
  } else if (store.isOtterPartner) {
    pushOrderToOtter(order);
  } else {
    sendNotification(
      store.OnesignalID,
      "Has recibido un nuevo pedido a por todas 🌮 🌯 🫔 🥗 🥘"
    );
  }

  if (
    scheduleTime(getMariaHorario().schedule) &&
    order.orderType === "delivery"
  ) {
    AsingRider(order);
  }
  setOrderToHolded(order);
};
