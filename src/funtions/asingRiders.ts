import { Types } from "mongoose";
import ordenSchema, { IOrders } from "../models/newOrder/order";
import riderSchema, { IRider } from "../models/riders";
import { sendNotification } from "../GraphQL/sendNotificationRider";
import { sendNotificationCustomer } from "../GraphQL/sendNotificationCustomer";
import { ordenUpdate } from "../Deliverect/ordenUpdate";

const { ObjectId } = Types;

export const AsingRider = async (orde: IOrders) => {
  const isRider = () => {
    if (orde.storeData.city === "Tomelloso") {
      return "611f847ef0668e8ae9c80605";
    } else if (orde.storeData.city === "Alcázar de San Juan") {
      return "624c5c667fb6d1090c6f03fd";
    } else if (orde.storeData.city === "Valdepeñas") {
      return "61e939f61a762b315010859d";
    } else if (orde.storeData.city === "Socuéllamos") {
      return "62570d762657f25bf53765c6";
    } else if (orde.storeData.city === "Quintanar de la Orden") {
      return "62433995221328477ef567fa";
    }
  };
  //@ts-ignore
  const rider: IRider = await riderSchema.findOne({
    _id: isRider(),
  });

  ordenSchema.findOneAndUpdate(
    { _id: orde._id },
    {
      courier: ObjectId(rider._id),
      courierData: rider,
    },
    { upsert: true },
    (err, order) => {
      if (err) {
        console.log(">>>", err);
      } else {
        if (!orde.storeData.ispartners) {
          ordenUpdate(orde._id, "Listo para recoger", 70);
          setTimeout(() => {
            sendNotification(
              rider.OnesignalID,
              "Tienes el pedido listo para recoger ✌️ ⚡️",
              "Listo para recoger",
              true
            );
          }, 30000);
        }
        sendNotification(
          rider.OnesignalID,
          "Has recibido un nuevo pedido a por todas ✌️ ⚡️",
          "Has recibido un nuevo pedido",
          true
        );
        sendNotificationCustomer(
          orde.customerData.OnesignalID,
          `Te hemos asignado a ${rider.name} para entregarte el pedido`,
          "Repartidor asignado",
          orde
        );
      }
    }
  );
};
