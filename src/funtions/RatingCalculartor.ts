export const RatingCalculator = (ValoracionList) => {
  let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
  ValoracionList.forEach((start: any) => {
    if (start.value === 1) rating["1"] += 1;
    else if (start.value === 2) rating["2"] += 1;
    else if (start.value === 3) rating["3"] += 1;
    else if (start.value === 4) rating["4"] += 1;
    else if (start.value === 5) rating["5"] += 1;
  });

  const ar =
    (5 * rating["5"] +
      4 * rating["4"] +
      3 * rating["3"] +
      2 * rating["2"] +
      1 * rating["1"]) /
    ValoracionList.length;

  let averageRating = 0;
  if (ValoracionList.length) {
    averageRating = Number(ar.toFixed(1));
  }
  return averageRating;
};
