import ordenSchema from "../models/custonorder";
import riderSchema, { IRider } from "../models/riders";
import userSchema, { IUser } from "../models/user";
import { sendNotification } from "../GraphQL/sendNotificationRider";
import { sendNotificationCustomerCuston } from "../GraphQL/sendNotificationCustomerCuston";

export const AsingRiderCustonOrder = async (orde: any) => {
  const isRider = () => {
    if (orde.city === "Tomelloso") {
      return "611f847ef0668e8ae9c80605";
    } else if (orde.city === "Burgos") {
      return "5fb7de125fd35b9c72ce2423";
    } else if (orde.city === "Alcázar de San Juan") {
      return "611f83cdf0668e8ae9c80604";
    }
  };
  //@ts-ignore
  const rider: IRider = await riderSchema.findOne({
    _id: isRider(),
  });

  //@ts-ignore
  const user: IUser = await userSchema.findOne({
    _id: orde.userID,
  });

  ordenSchema.findOneAndUpdate(
    { _id: orde._id },
    {
      riders: rider._id,
      estado: "Asignada",
    },
    { upsert: true },
    (err, order) => {
      if (err) {
        console.log(">>>", err);
      } else {
        sendNotification(
          rider.OnesignalID,
          "Has recibido un nuevo PEDIDO EXPRESS 🚨🚨🚨",
          "🚨 Has recibido un nuevo PEDIDO EXPRESS 🚨",
          true
        );
        sendNotificationCustomerCuston(
          user.OnesignalID,
          `Te hemos asignado a ${rider.name} para entregarte el pedido`,
          "Repartidor asignado"
        );
      }
    }
  );
};
