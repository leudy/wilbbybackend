import userSchema, { IUser } from "../models/user";
import { sendNotificationCustomerCuston } from "../GraphQL/sendNotificationCustomerCuston";

export const SendNotificationCustomOrder = async (orde: any) => {
  //@ts-ignore
  const user: IUser = await userSchema.findOne({
    _id: orde.userID,
  });

  let title = "";
  let description = "";

  switch (orde.estado) {
    case "Aceptada":
      title = "El repartidor ya está buscando tu pedido";
      description =
        "El repartidor ya está buscando tu pedido en la dirección indicada";
      break;

    case "En camino":
      title = "El repartidor ya está de camino";
      description = "El repartidor ya está de camino para entregarte el pedido";
      break;

    case "Entregado":
      title = "⭐️ 🌟 Pedido entregado";
      description =
        "⭐️ 🌟 El pedido ha sido entregado es hora de decirno que te ha parecido";
      break;

    default:
      break;
  }

  sendNotificationCustomerCuston(user.OnesignalID, description, title);
};
