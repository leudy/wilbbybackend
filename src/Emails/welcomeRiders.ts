import emailRiders from "./emailRider";
import riderSchema from "../models/riders";
import crypto from "crypto";
import nodemailer from "nodemailer";
const mailjetTransport = require('nodemailer-mailjet-transport');

export const welcomeRiderEmail = (email: any) => {
  const token = crypto.randomBytes(20).toString("hex");
  var newvalues = { $set: { forgotPasswordToken: token } };

  const transporter = nodemailer.createTransport(mailjetTransport({
    service: 'Mailjet',
    host: "in-v3.mailjet.com",
    secure: true,
    port: 587,
    auth: {
      apiKey: process.env.MJ_APIKEY_PUBLIC,
      apiSecret: process.env.MJ_APIKEY_PRIVATE
    }
  }));
  const mailOptions = {
    from: process.env.EMAIL_ADDRESS,
    to: email,
    subject: `ACTIVAR CUENTA WILBBY - Bienvenido a Wilbby para Riders`,
    text: `ACTIVAR CUENTA WILBBY - Bienvenido a Wilbby para Riders`,
    html: emailRiders(token, email),
  };

  transporter.sendMail(mailOptions, (err: any) => {
    if (err) {
      console.log("Error al enviar");
    } else {
      riderSchema.findOneAndUpdate(
        { email: email },
        newvalues,
        {
          //options
          new: true,
          strict: false,
          useFindAndModify: false,
        },
        (err, updated) => {}
      );
    }
  });
};
