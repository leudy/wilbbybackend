import Order from "../models/newOrder/order";
import Cart from "../models/newOrder/addToCart";
import Store from "../models/restaurant";
import productSchema from "../models/newMenu/products";
import plato from "../models/nemu/platos";
import categoria from "../models/nemu/menu";
import CatSchema from "../models/restaurant";
import collectionSchema from "../models/collections";
import categorySchema from "../models/categorias";
import { RatingCalculator } from "../funtions/RatingCalculartor";
import ratingSchema from "../models/rating";

import { AsingRider } from "../funtions/asingRiders";

import { storeFormat, setHorario } from "../storeFormat";
import restaurant from "../models/restaurant";

import product from "../models/newMenu/products";

import category, { ICategoryProduct } from "../models/newMenu/category";
import bundles from "../models/newMenu/bundles";
import custonorder from "../models/custonorder";
import order from "../models/newOrder/order";
import fs from "fs"

import tipo from "../models/tipo";

export const RecursosUpdate = (): void => {
  //new restaurant(storeFormat()).save();

  const updateMainCategoria = async () => {
    const categorys = await categorySchema.find();

    categorys.forEach((x) => {
      categorySchema.updateMany(
        { _id: x },
        {
          largue: false,
        },
        { upsert: true },
        () => {
          console.log("done");
        }
      );
    });
  };

  //updateMainCategoria();

  const setCategoryDuplicate = async () => {
    const cat = await category.find({ account: "60faf41f0ea294528074a5e9" });

    cat.forEach(async (x: ICategoryProduct, i: number) => {
      const newCat = new category({
        name: x.name,
        description: x.description,
        account: "624426e2221328477ef5686d",
        posLocationId: x.posCategoryId,
        posCategoryType: x.posCategoryType,
        subCategories: [],
        posCategoryId: "",
        imageUrl: "",
        products: x.products,
        menu: "624426e2221328477ef5686d",
        storeId: "624426e2221328477ef5686d",
        sortedChannelProductIds: [],
        subProductSortOrder: [],
        subProducts: [],
        level: 2,
        sorting: x.sorting,
        availabilities: [],
      });

      newCat.save().then(() => {
        console.log("save");
      });
    });
  };

  //setCategoryDuplicate();

  const updateProductPrice = async () => {
    const g = await product.find({
      account: "60faf41f0ea294528074a622",
    });

    g.forEach((x) => {

      const pri = (20 / 100) * x.price
      // 

      console.log(pri)
       product.updateMany(
        { _id: x._id },
        {
  
          price:  x.previous_price,//x.price - pri,
          offert: false,
          previous_price: 0//x.price + Math.round(pri)
        },
        { upsert: true },
        () => {
          console.log("done");
        }
      );
    });
  };

  //updateProductPrice();

  const updateOrders = async () => {
    const g = await Order.find({ status: "Listo para recoger" });
    //@ts-ignore
    g.forEach(async (x) => {
      const st = { status: "Entregada", date: new Date() };
      Order.findOneAndUpdate(
        { _id: x?._id },
        {
          status: "Entregada",
          $push: { statusProcess: st },
        },
        { upsert: true },
        () => {
          console.log("done");
        }
      );
    });
  };

  const updateProducVariables = async () => {
    const products = await product.find({
      account: "61a7a1bbc5cc0874468cc368",
    });

    //@ts-ignore
    products.forEach(async (x) => {
      product.findOneAndUpdate(
        { _id: x?._id },
        {
          //@ts-ignore
          subProducts: ["61b899a18585936d282b35f4"],
        },
        { upsert: true },
        () => {
          console.log("done");
        }
      );
    });
  };

  const updateProducVariablesFromVariables = async () => {
    const products = await category.findOne({
      _id: "61f544c5fbcc65557b3edf70",
    });

    //@ts-ignore
    products.products.forEach(async (x) => {
      product.findOneAndUpdate(
        { _id: x },
        {
          //@ts-ignore
          subProducts: [
            "61f9945887d5507d43040b70",
            "61f994a287d5507d43040b74",
            "61f9953787d5507d43040b7a",
          ],
        },
        { upsert: true },
        () => {
          console.log("done");
        }
      );
    });
  };

  //updateProducVariablesFromVariables();
  //isBundled;
  const updateProducBundled = async () => {
    const g = await bundles.find();
    g &&
      g.forEach(async (x) => {
        x &&
          x.subProducts.forEach(async (y) => {
            const prod = await product.findOne({ _id: y });
            product.findOneAndUpdate(
              { _id: prod?._id },
              {
                isBundled: true,
              },
              { upsert: true },
              () => {
                console.log("done");
              }
            );
          });
      });
  };

  const updateProducS = async () => {
    const Products = await product.find();

    Products &&
      Products.forEach(async (y) => {
        product.findOneAndUpdate(
          { _id: y?._id },
          {
            isBundled: y?.isBundled ? true : false,
          },
          { upsert: true },
          () => {
            console.log("done");
          }
        );
      });
  };

  const updateCollection = async () => {
    const g = await collectionSchema.find({
      store: "5fb24df8c292c23bed148076",
    });

    g.forEach((x) => {
      collectionSchema.updateMany(
        { _id: x._id },
        {
          store: x.store.concat("624423c4221328477ef5686b"),
        },
        { upsert: true },
        () => {
          console.log("done");
        }
      );
    });
  };

  //updateCollection();

  const updateRestaurant = async () => {
    const g = await restaurant.find();

    g.forEach(async (x, i) => {
      restaurant.findOneAndUpdate(
        { _id: x._id },
        {
          scheduleOnly: {
            available: false,
            hour: 0,
          },
          noScheduled: false,
          cashPayment: true,
        },
        () => {
          console.log("done");
        }
      );
    });
  };

  //updateRestaurant();

  const updateRestaurantOne = async () => {
    restaurant.updateMany(
      { _id: "60fc33acfbf3fe598d4c2a36" },
      setHorario(),
      { upsert: true },
      () => {
        console.log("done");
      }
    );
  };

  //updateRestaurantOne();

  const updateProd = async () => {
    const g = await product.find({ account: "610003c7fb1a92e312078816" });
    g.forEach((x) => {
      const price = (x.price * 10) / 100;
      product.updateMany(
        { _id: x._id },
        {
          price: x.previous_price,
          previous_price: 0,
          offert: false,
        },
        { upsert: true },
        () => {
          console.log("done");
        }
      );
    });
  };

  const updateStore = async () => {
    const g = await restaurant.find({ city: "Tomelloso" });
    g.forEach((x) => {
      restaurant.findOneAndUpdate(
        { _id: x._id },
        {
          includeCity: x.includeCity.concat(
            "Vereda de Socuéllamos, s/n, Tomelloso"
          ),
        },
        { upsert: true },
        () => {
          console.log("done");
        }
      );
    });
  };

  //updateProd();

  //updateRestaurant()

  //updateCollection()

  //update();
  
 /*  Order.deleteMany({ store: "61f045bcb3e94555a0caafe5" }).then(() => {
    console.log("hecho");
  }); */

  /* custonorder.deleteMany({ status: "Pendiente de pago" }).then(() => {
    console.log("hecho");
  }); */

  /* category.deleteMany({ account: "60faf41f0ea294528074a602" }).then(() => {
  console.log("hecho");
}); */

  const setCategoryFronViejas = async () => {
    const cat = await categoria.find({
      restaurant: "5ff1d57185c1c068931a3860",
    });

    cat.forEach(async (x, i) => {
      const pro = await plato.find({ menu: x._id });

      const sub = pro.map((y) => {
        //@ts-ignore
        return y._id;
      });
      const newCat = new CatSchema({
        name: x.title,
        description: "",
        account: "5ff1d57185c1c068931a3860",
        posLocationId: "",
        posCategoryType: "",
        subCategories: [],
        posCategoryId: "",
        imageUrl: "",
        products: sub,
        menu: "60be7ee0395b91df865649d5",
        storeId: "5ff1d57185c1c068931a3860",
        sortedChannelProductIds: [],
        subProductSortOrder: [],
        subProducts: [],
        level: 2,
        sorting: i,
        availabilities: [],
      });

      newCat.save().then(() => {
        console.log("save");
      });
    });
  };

  /* setOrders().then(() => {
  console.log("guardado");
}); */

  //setMenu()

  //console.log(new Date());
};

const createdJSON = async () => {
  const g = await tipo.find();

  let data = JSON.stringify(g);

  fs.writeFileSync('tipo.json', data);
};

//createdJSON()


