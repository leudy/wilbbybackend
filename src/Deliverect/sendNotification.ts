export const sendNotification = (
  IdOnesignal: any,
  textmessage: string,
  order
) => {
  var sendNotification = function (data: any) {
    var headers = {
      "Content-Type": "application/json; charset=utf-8",
    };
    var options = {
      host: "onesignal.com",
      port: 443,
      path: "/api/v1/notifications",
      method: "POST",
      headers: headers,
    };
    var https = require("https");
    var req = https.request(options, function (res: any) {
      res.on("data", function (data: any) {
        console.log("Response:");
      });
    });

    req.on("error", function (e: any) {
      console.log("ERROR:");
      console.log(e);
    });

    req.write(JSON.stringify(data));
    req.end();
  };

  var message = {
    app_id: "db79b975-e551-4741-ae43-8968ceab5f09",
    headings: { en: "Wilbby" },
    android_channel_id: "cfc50b79-a76a-4114-8200-0b53d6d7bd30",
    contents: { en: `${textmessage}` },
    ios_badgeCount: 1,
    ios_badgeType: "Increase",
    include_player_ids: [`${IdOnesignal}`],
    ios_attachments: { id1: order.storeData.image },
    big_picture: order.storeData.image,
    data: {
      navigate: true,
      router: "FallowOrder",
      data: { id: order.channelOrderDisplayId },
    },
  };
  sendNotification(message);
};

export const sendNotificationRider = (
  IdOnesignal: any,
  textmessage: string
) => {
  var sendNotification = function (data: any) {
    var headers = {
      "Content-Type": "application/json; charset=utf-8",
    };
    var options = {
      host: "onesignal.com",
      port: 443,
      path: "/api/v1/notifications",
      method: "POST",
      headers: headers,
    };
    var https = require("https");
    var req = https.request(options, function (res: any) {
      res.on("data", function (data: any) {
        console.log("Response:");
      });
    });

    req.on("error", function (e: any) {
      console.log("ERROR:");
      console.log(e);
    });

    req.write(JSON.stringify(data));
    req.end();
  };

  var message = {
    app_id: "e21467de-eaf6-4090-9b6c-a311d37c0b5e",
    headings: { en: "Wilbby" },
    android_channel_id: "9b0682ea-6f2d-4228-8802-280df76cb033",
    ios_sound: "despertador_minion.wav",
    contents: { en: `${textmessage}` },
    ios_badgeCount: 1,
    ios_badgeType: "Increase",
    include_player_ids: [`${IdOnesignal}`],
  };
  sendNotification(message);
};
