import ordenSchema from "../models/newOrder/order";

export const ordenUpdate = (
  orderID: string,
  estado: string,
  progreso: number
) => {

  const st = { status: estado, date: new Date()}

  ordenSchema.findOneAndUpdate(
    { _id: orderID },
    {
      status: estado,
      IntegerValue: progreso,
      $push: { statusProcess: st},
    },
    { upsert: true },
    (err, order) => {}
  );
};
