import { Router } from "express";
import restaurantSchema from "../models/restaurant";
import order from "../models/newOrder/order";
import riderSchema from "../models/riders";
import { ordenUpdate } from "./ordenUpdate";
import { sendNotification, sendNotificationRider } from "./sendNotification";
import categorySchema from "../models/newMenu/category";
import bundlesSchema from "../models/newMenu/bundles";
import MenuSchema from "../models/newMenu/Menu";
import modifierGroupsSchema from "../models/newMenu/modifierGroups";
import modifiersSchema from "../models/newMenu/modifiers";
import productSchema from "../models/newMenu/products";

class WebHookRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.post("/api/deliverect/hook", (req, res) => {
      res.json({
        statusUpdateURL:
          "https://api.v1.wilbby.com/api/deliverect/statusUpdateURL",
        menuUpdateURL: "https://api.v1.wilbby.com/api/deliverect/menuUpdateURL",
        disabledProductsURL:
          "https://api.v1.wilbby.com/api/deliverect/disabledProductsURL",
        snoozeUnsnoozeURL:
          "https://api.v1.wilbby.com/api/deliverect/snoozeUnsnoozeURL",
        busyModeURL: "https://api.v1.wilbby.com/api/deliverect/busyModeURL",
      });
      restaurantSchema.findOneAndUpdate(
        { slug: req.body.channelLocationId },
        { channelLinkId: req.body.channelLinkId, isDeliverectPartner: true },
        { new: true },
        (error) => {
          if (error) {
            res.status(400).end();
          } else {
            res.status(200).end();
          }
        }
      );
    });

    this.router.post("/api/deliverect/statusUpdateURL", async (req, res) => {
      const orden = await order.findById({ _id: req.body.channelOrderId });
      const rider = await riderSchema.findById({
        _id: orden?.courier ? orden?.courier : "5fb7de125fd35b9c72ce2423",
      });
      const orderUpdate = () => {
        switch (req.body.status) {
          case 10:
            sendNotification(
              //@ts-ignore
              orden?.customerData.OnesignalID,
              `${orden?.storeData.title} Ha recibido tu pedido tu pedido.`,
              orden
            );
            break;

          case 20:
            ordenUpdate(orden?._id, "Confirmada", 60);
            sendNotification(
              //@ts-ignore
              orden?.customerData.OnesignalID,
              `${orden?.storeData.title} Ha confirmado tu pedido.`,
              orden
            );
            break;

          case 30:
            ordenUpdate(orden?._id, "Nueva", 20);
            sendNotification(
              //@ts-ignore
              orden?.customerData.OnesignalID,
              `El pedido se ha recibido varias veces en el TPV debido a un error técnico.`,
              orden
            );
            break;

          case 50:
            sendNotification(
              //@ts-ignore
              orden?.customerData.OnesignalID,
              `${orden?.storeData.title} esta preparando tu comida en la cocina.`,
              orden
            );
            break;

          case 60:
            sendNotification(
              //@ts-ignore
              orden?.customerData.OnesignalID,
              `${orden?.storeData.title} ya ha terminado de preparar tu pedido en la cocina.`,
              orden
            );
            break;

          case 70:
            ordenUpdate(orden?._id, "Listo para recoger", 70);
            sendNotification(
              //@ts-ignore
              orden?.customerData.OnesignalID,
              `${orden?.storeData.title} tiene tu pedido listo para recoger.`,
              orden
            );
            break;

          case 80:
            ordenUpdate(orden?._id, "En camino", 80);
            sendNotification(
              //@ts-ignore
              orden?.customerData.OnesignalID,
              `Tu pedido ha salido del establecimiento y va de camino.`,
              orden
            );
            break;

          case 90:
            ordenUpdate(orden?._id, "Listo para recoger", 70);
            if (orden?.orderType === "delivery") {
              sendNotificationRider(
                rider?.OnesignalID,
                `${orden?.storeData.title} tiene el pedido listo para recoger.`
              );
            }
            sendNotification(
              //@ts-ignore
              orden?.customerData.OnesignalID,
              `${orden?.storeData.title} tiene el pedido listo para recoger por el rider.`,
              orden
            );
            break;

          case 100:
            ordenUpdate(orden?._id, "Rechazada por la tienda", 10);
            sendNotification(
              //@ts-ignore
              orden?.customerData.OnesignalID,
              `Upps ${orden?.storeData.title} no ha podido aceptar tu pedido lo sentimos.`,
              orden
            );
            break;

          case 110:
            ordenUpdate(orden?._id, "Rechazada por la tienda", 10);
            sendNotification(
              //@ts-ignore
              orden?.customerData.OnesignalID,
              `Upps ${orden?.storeData.title} no ha podido aceptar tu pedido lo sentimos.`,
              orden
            );
            break;

          case 120:
            ordenUpdate(orden?._id, "Devuelto", 10);
            sendNotification(
              //@ts-ignore
              orden?.customerData.OnesignalID,
              `Upps el pedido ha fallado lo sentimos.`,
              orden
            );
            break;

          default:
            break;
        }
      };
      orderUpdate();
      res.status(200).end();
    });

    this.router.post("/api/deliverect/menuUpdateURL", (req, res) => {
      req.body.forEach(async (element) => {
        console.log(element);
        const restaurant = await restaurantSchema.findOne({
          channelLinkId: element.channelLinkId,
        });
        element.storeId = restaurant?._id;
        const newMenu = new MenuSchema(element);
        MenuSchema.deleteMany({ storeId: restaurant?._id })
          .then(function () {
            newMenu.save();
          })
          .catch(function (error) {
            console.log(error); // Failure
          });

        element.categories.forEach(async (category, i) => {
          const dataCategory = category;
          dataCategory.storeId = restaurant?._id;
          dataCategory.menu = newMenu._id;
          dataCategory.sorting = i;
          categorySchema
            .deleteMany({ storeId: restaurant?._id })
            .then(function () {
              const newCategory = new categorySchema(dataCategory);
              newCategory.save();
            })
            .catch(function (error) {
              console.log(error); // Failure
            });
        });

        var products = element.products;
        var Product = Object.keys(products);

        Product.forEach(async (key) => {
          // product for Key
          const product = products[key];
          product.storeId = restaurant?._id;

          const newProduct = new productSchema(product);

          productSchema
            .deleteMany({ storeId: restaurant?._id })
            .then(async function () {
              newProduct.save();
            })
            .catch(function (error) {
              console.log(error); // Failure
            });
        });

        var bundles = element.bundles;
        var Bundle = Object.keys(bundles);

        Bundle.forEach(async (key) => {
          const bundle = bundles[key];
          const newBundle = new bundlesSchema(bundle);
          bundlesSchema
            .deleteMany({ parentId: bundle.parentId })
            .then(async function () {
              newBundle.save();
            })
            .catch(function (error) {
              console.log(error); // Failure
            });
        });

        var modifierGroup = element.modifierGroups;
        var ModifierGroup = Object.keys(modifierGroup);

        ModifierGroup.forEach(async (key) => {
          const modifierGroups = modifierGroup[key];
          const newmodifierGroupsSchema = new modifierGroupsSchema(
            modifierGroups
          );
          modifierGroupsSchema
            .deleteMany({ parentId: modifierGroups.parentId })
            .then(async function () {
              newmodifierGroupsSchema.save();
            })
            .catch(function (error) {
              console.log(error); // Failure
            });
        });

        var modifier = element.modifiers;
        var Modifier = Object.keys(modifier);

        Modifier.forEach(async (key) => {
          const modifiers = modifier[key];
          const newmodifier = new modifiersSchema(modifiers);
          modifiersSchema
            .deleteMany({ parentId: modifiers.parentId })
            .then(async function () {
              newmodifier.save();
            })
            .catch(function (error) {
              console.log(error); // Failure
            });
        });
      });

      res.status(200).end();
    });

    this.router.post("/api/deliverect/snoozeUnsnoozeURL", (req, res) => {
      req.body.operations.forEach((element) => {
        element.data.items.forEach((x) => {
          productSchema.findOneAndUpdate(
            { plu: x.plu },
            {
              $set: {
                snoozed: element.action === "snooze" ? true : false,
              },
            },
            (e, r) => console.log(e, r)
          );
        });
      });
      res.status(200).end();
    });

    this.router.post("/api/deliverect/busyModeURL", (req, res) => {
      restaurantSchema.findOneAndUpdate(
        { channelLinkId: req.body.channelLinkId },
        {
          $set: {
            open: req.body.status === "PAUSED" ? false : true,
          },
        },
        (e, r) => console.log(e, r)
      );
      res.status(200).end();
    });
  }
}

const webHookRouter = new WebHookRouter();
webHookRouter.routes();

export default webHookRouter.router;
