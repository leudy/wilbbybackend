export const data = [
  {
    title: "Hamburguesa de Pollo",
    ingredientes: "Pechuga de pollo empanada, queso, cebolla, tomate y lechuga",
    price: "9.10 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/vdfxhekvkbcosuf1pkge",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed79022a690483955081b",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Hamburguesa Gourmet",
    ingredientes:
      "Carne 100% vacuno, foie, queso gorgonzola, cebolla crujiente y caramelizada, compota de manzana, y mezclum de lechuga",
    price: "10.50 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/n9wbnbzsrfvy3mij0fmt",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed79022a690483955081b",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Hamburguesa A Puro Huevo",
    ingredientes:
      "Carne mixta, cebolla, queso, bacon, huevo frito, tomate y lechuga",
    price: "9.90 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/fstuiecocqwuhtav4i6s",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed79022a690483955081b",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Hamburguesa con Jamón",
    ingredientes:
      "Carne mixta, cebolla, pepinillo, queso, jamón a la plancha, lechuga y tomate",
    price: "9.50 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/as6e4bjmhu91k007pef9",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed79022a690483955081b",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Hamburguesa Tal Cual",
    ingredientes: "Carne mixta, lechuga, tomate, cebolla, pepinillo y queso",
    price: "8.80 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/o8zozhq3vho8qjfhktai",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed79022a690483955081b",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Sándwich de la Granja",
    ingredientes: "Jamón York, queso, bacon y huevo frito",
    price: "6.75 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/rpkemb8xf1fiqzcbgdj7",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7bd22a690483955081c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sándwich Especial",
    ingredientes:
      "Pechuga de pollo empanada, pimiento verde, jamón, lechuga, tomate y ali-oli",
    price: "7.15 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/hnsaoa76rlfhltofaexo",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7bd22a690483955081c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sándwich con Salmón Ahumado",
    ingredientes: "",
    price: "7.15 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/d1cje7sdu49cfet38ysg",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7bd22a690483955081c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sándwich Vegetal con Huevo Frito",
    ingredientes: "",
    price: "6.50 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/atzwhgsmnof9jppupcvj",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7bd22a690483955081c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sándwich Vegetal",
    ingredientes: "",
    price: "5.85 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/h8cyxwl8lxbtcy8h0jhn",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7bd22a690483955081c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sándwich Mixto con Huevo Frito",
    ingredientes: "",
    price: "5.60 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/hhkgckqcdvaclvblbvpd",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7bd22a690483955081c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sándwich Mixto",
    ingredientes: "",
    price: "4.95 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/bdquamwf8v5rml5keat2",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7bd22a690483955081c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sándwich Vegetariano Mixto",
    ingredientes: "Lonchas vegetales y queso",
    price: "5.45 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/z9ooba0roqgtqcvulf1x",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7bd22a690483955081c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sándwich Vegetariano Mixto y Huevo Frito",
    ingredientes: "",
    price: "6.10 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/xo0pscjzhjahzvsv0eo0",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7bd22a690483955081c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sándwich Vegetariano con Huevo Frito",
    ingredientes: "",
    price: "7.80 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/dpgomxkprkbzqu5vx1rd",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7bd22a690483955081c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sándwich Vegetariano",
    ingredientes:
      "Lonchas vegetales, queso, huevo cocido, tiras de pimiento rojo, tomate, mahonesa y lechuga",
    price: "7.15 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/iarftow7uejci0p19jhk",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7bd22a690483955081c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Perrito Completo",
    ingredientes:
      "Frankfurt, queso, bacon, pepinillo, cebolla crujiente y dos salsas",
    price: "4.95 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/rtcws193tgndmwso9mfu",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7c722a690483955081d",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Perrito Dos Salsas",
    ingredientes: "Frankfurt, bacon, queso y nuestras dos salsas",
    price: "4.55 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/ewuwtryb64ykufmuzlzr",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7c722a690483955081d",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Perrito",
    ingredientes: "Solo frankfurt",
    price: "4.15 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/dokymmcippv2mtoe1igw",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7c722a690483955081d",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Cecina",
    ingredientes: "",
    price: "7.80 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Jamón con Aceite de Oliva",
    ingredientes: "",
    price: "6.75 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Brascada",
    ingredientes:
      "Filete de Ternera, cebolla pochada, jamón, patatas y mahonesa",
    price: "7.50 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Bacon con Queso y Pimiento Rojo",
    ingredientes: "",
    price: "6.90 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Bacon con Queso",
    ingredientes: "",
    price: "6.60 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Bacon con Pimientos Rojos",
    ingredientes: "",
    price: "6.60 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Lomo con Queso y Pimientos Rojos",
    ingredientes: "",
    price: "6.90 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Lomo de Pimientos Rojos",
    ingredientes: "",
    price: "6.60 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Lomo de Queso",
    ingredientes: "",
    price: "6.60 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Bocadillo Español",
    ingredientes: "Rabas con ali-oli y salsa brava",
    price: "7.00 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Rabas",
    ingredientes: "",
    price: "7.00 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Calamares",
    ingredientes: "Con ali-oli",
    price: "7.00 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pechuga de Pollo con Bacon",
    ingredientes: "",
    price: "7.15 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/l0po48eroabbagtqjvnt",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pechuga de Pollo II",
    ingredientes: "Empanada con Pimientos Rojos",
    price: "7.15 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pechuga de Pollo",
    ingredientes: "Empanada con ali oli y lechuga",
    price: "7.15 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pepito de Ternera con Pimientos",
    ingredientes: "",
    price: "7.65 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pepito de Ternera",
    ingredientes: "",
    price: "7.00 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Tortilla Francesa",
    ingredientes: "",
    price: "5.30 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Tortilla de Patatas",
    ingredientes: "",
    price: "5.30 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7d922a690483955081e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "La Ensalada",
    ingredientes: "Lechuga, tomate y cebolla",
    price: "7.15 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7e322a690483955081f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Ensalada Cesar",
    ingredientes:
      "Lechuga, pollo, picatostes, parmesano, tomate, manzana y salsa césar",
    price: "10.80 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7e322a690483955081f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Ensalada Mixta",
    ingredientes:
      "Lechuga, tomate, cebolla, bonito, maiz, huevo cocido, espárragos y zanahoria",
    price: "10.80 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7e322a690483955081f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Ensalada de Frutos Secos",
    ingredientes:
      "Lechuga, tomate, queso fresco y frutos secos con salsa de miel y mostaza",
    price: "10.80 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7e322a690483955081f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Ensalada Templada",
    ingredientes:
      "Lechuga, cebolla, tomates cherry, gulas, langostinos y pimientos",
    price: "10.80 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7e322a690483955081f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Tortilla Rellena",
    ingredientes:
      "Máximo 4 ingredientes a escoger (jamón york, queso barra, queso de cabra, chaka, pollo, salsa brava, salsa alioli, cecina, morcilla, pimiento rojo, gulas, gambas, lechuga)",
    price: "15.60 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/ipne6oeaat8jphkqftdl",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7ea22a6904839550820",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Tortilla de Patatas",
    ingredientes: "",
    price: "13.00 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/n2zygbngkvlvty3ng5ej",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7ea22a6904839550820",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Bravas Dobles",
    ingredientes: "",
    price: "4.00 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/rz9cj8a45gyyszzettps",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7f122a6904839550821",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Ración de Rabas",
    ingredientes: "",
    price: "9.75 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed7fb22a6904839550822",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Fanta Naranja (500 cc)",
    ingredientes: "",
    price: "1.85 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/a1e2pviy3ehodp6vz5fk",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed80622a6904839550823",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Aquarius (500 cc)",
    ingredientes: "",
    price: "1.85 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/mem2yli9byvxhyzedsfy",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed80622a6904839550823",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Coca Cola Zero (500 cc)",
    ingredientes: "",
    price: "1.85 ",
    imagen: null,
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed80622a6904839550823",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Coca Cola (500 cc)",
    ingredientes: "",
    price: "1.85 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/eqalbf7k1xjk7y6qvtbw",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed80622a6904839550823",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Agua de Vilas (500 cc)",
    ingredientes: "",
    price: "1.35 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/blu0etgaiykdrubjcdiz",
    restaurant: "5ff1d6e385c1c068931a3862",
    menu: "600ed80622a6904839550823",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
];
