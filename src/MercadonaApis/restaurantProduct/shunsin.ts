export const data = [
  {
    title: "Menú Shinkansen 6X",
    ingredientes:
      "1 nigiri de salmón noruego + 1 nigiri de atún rojo + 1 nigiri de langostino + 1 nigiri de pez mantequilla con trufa + 2 rolls de salmón con trufa negra + 2 rolls de atún cocido con cebolleta fresca + 2 rolls de aguacate con queso de piña",
    price: "10.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b44c1eb91b2b5b0b689e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Yakisoba con pollo yakitori",
    ingredientes: "Fideos con pollo yakitori y salsa teriyaki",
    price: "7.65",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b44c1eb91b2b5b0b689e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Shinkansen 4",
    ingredientes:
      "6 rolls de aguacate y salmón noruego + 3 rolls de sésamo tostado + 3 rolls spicy con salsa mayonesa japonesa + 6 nigiri de salmón",
    price: "15.35",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b44c1eb91b2b5b0b689e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Roll Volcán Salmón",
    ingredientes:
      "Roll volcán una fusión de sabores increíble para el paladar, manzana, fuji ,hueva ,pescado, recubierto ,salmón con salsa 199 top casera a la brasa.",
    price: "14.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4641eb91b2b5b0b689f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Roll Volcán Atún Rojo",
    ingredientes:
      "Roll volcán una fusión de sabores increíble para el paladar, manzana, fuji ,hueva ,pescado, recubierto atún con salsa 199 top casera a la brasa",
    price: "14.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4641eb91b2b5b0b689f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Menú Shinkansen 1",
    ingredientes:
      "3 nigiri de salmón noruego + 2 nigiri de atún rojo + 1 nigiri de langostino + 3 California de salmón noruego",
    price: "10.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b46e1eb91b2b5b0b68a0",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Shinkansen 2",
    ingredientes:
      "6 nigiri de salmón noruego + 6 rolls de aguacate y salmón noruego",
    price: "12.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b46e1eb91b2b5b0b68a0",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Shinkansen 3",
    ingredientes:
      "4 nigiri de salmón noruego + 4 nigiri de atún rojo + 2 makis de salmón + 2 makis de atún + 2 rolls de sésamo tostado vegetal",
    price: "15.85",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b46e1eb91b2b5b0b68a0",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Shinkansen 4",
    ingredientes:
      "6 rolls de aguacate y salmón noruego + 3 rolls de sésamo tostado + 3 rolls spicy con salsa mayonesa japonesa + 6 nigiri de salmón",
    price: "15.35",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b46e1eb91b2b5b0b68a0",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Shinkansen 5X",
    ingredientes:
      "3 nigiri de salmón noruego + 3 rolls de atún cocido con sésamo tostado y cebollino fresco + 3 rolls de salmón noruego + 3 makis de salmón",
    price: "10.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b46e1eb91b2b5b0b68a0",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Shinkansen 6X",
    ingredientes:
      "1 nigiri de salmón noruego + 1 nigiri de atún rojo + 1 nigiri de langostino + 1 nigiri de pez mantequilla con trufa + 2 rolls de salmón con trufa negra + 2 rolls de atún cocido con cebolleta fresca + 2 rolls de aguacate con queso de piña",
    price: "10.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b46e1eb91b2b5b0b68a0",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Shinkansen 6",
    ingredientes:
      "1 nigiri de salmón noruego + 1 nigiri de atún + 1 nigiri de pez mantequilla + 1 nigiri de langostino + 2 rolls con cebolleta frita + 2 rolls de atún cocido con cebollino fresco + 2 rolls de alga wakame y sésamo",
    price: "9.75",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b46e1eb91b2b5b0b68a0",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Shinkansen 7",
    ingredientes:
      "5 sashimi de salmón noruego + 1 nigiri de salmón + 1 nigiri de atún rojo + 4 makis de aguacate + 4 makis de salmón con queso Philadelphia + 4 rolls con alga wakame y sésamo + 4 rolls con cebolla frita",
    price: "17.98",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b46e1eb91b2b5b0b68a0",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Shinkansen 8",
    ingredientes:
      "2 nigiri de salmón noruego + 2 nigiri de atún rojo + 1 nigiri de langostino + 1 nigiri de pez mantequilla con trufa negra + 3 rolls con salmón y aguacate + 3 rolls con atún cocido y cebolleta fresca + 4 California con hueva de pescado naranja",
    price: "15.45",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b46e1eb91b2b5b0b68a0",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Shinkansen 9X",
    ingredientes:
      "1 nigiri de salmónnoruego + 1 nigiri de atún rojo + 1 nigiri de langostino + 1 nigiri de pez mantequilla con trufa negra + 4 makis de carne de cangrejo con ponzu + 4 makis de atún y cebolleta + 3 rolls de salmón con mango amarillo, Philadelphia y sem...",
    price: "16.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b46e1eb91b2b5b0b68a0",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Shinkansen 9",
    ingredientes:
      "1 nigiri de salmón noruego + 1 nigiri de atún rojo + 1 nigiri de pez mantequilla con trufa negra + 1 nigiri de langostino + 4 makis de salmón + 4 makis de atún rojo + 3 rolls de cebolla frita + 3 rolls de alga wakame con sésamo tostado",
    price: "16.50",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b46e1eb91b2b5b0b68a0",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Shinkansen 1011X",
    ingredientes:
      "2 nigiri de salmón + 2 nigiri de atún + 1 nigiri de langostino + 1 nigiri de pez mantequilla con trufa negra + 1 nigiri de anguila asada con teriyaki y sésamo + 4 dragon de jamón ibérico, melón, queso tomate y canónigo + 4 rolls de langostino, aguaca...",
    price: "21.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b46e1eb91b2b5b0b68a0",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Shinkansen 10",
    ingredientes:
      "1 nigiri de salmón noruego + 1 nigiri de atún rojo + 1 nigiri de langostino + 1 nigiri de pez mantequilla con trufa negra + 5 rolls spicy con tacos de salmón noruego + 4 rolls de cebolla frita + 4 rolls de tobiko + 2 cristal maki de salmón y 2 de atú...",
    price: "23.05",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b46e1eb91b2b5b0b68a0",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de salmón noruego (8 pzas.)",
    ingredientes: "",
    price: "4.89",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4781eb91b2b5b0b68a1",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de langostino con sésamo tostado (8 pzas.)",
    ingredientes: "",
    price: "5.44",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4781eb91b2b5b0b68a1",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title:
      "Makis de anguila asada con salsa teriyaki y sésamo tostado (8 pzas.)",
    ingredientes: "",
    price: "5.45",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4781eb91b2b5b0b68a1",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de salmón noruego y aguacate (8 pzas.)",
    ingredientes: "",
    price: "5.00",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de salmón noruego, queso Philadelphia y cebolleta (8 pzas.)",
    ingredientes: "",
    price: "5.28",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de atún rojo (8 pzas.)",
    ingredientes: "…",
    price: "5.44",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Makis de salmón noruego (8 pzas.)",
    ingredientes: "",
    price: "4.89",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de langostino con sésamo tostado (8 pzas.)",
    ingredientes: "",
    price: "5.44",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title:
      "Makis de anguila asada con salsa teriyaki y sésamo tostado (8 pzas.)",
    ingredientes: "",
    price: "5.45",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de salmón noruego y aguacate (8 pzas.)",
    ingredientes: "",
    price: "5.00",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de salmón noruego, queso Philadelphia y cebolleta (8 pzas.)",
    ingredientes: "",
    price: "5.28",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de atún rojo (8 pzas.)",
    ingredientes: "Con salsa picante y cebolleta fresca",
    price: "5.44",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de salmón noruego (8 pzas.)",
    ingredientes: "Con salsa picante y cebolleta",
    price: "5.06",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de mango y queso Philadelphia (8 pzas.)",
    ingredientes: "",
    price: "5.17",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis vegetales (8 pzas.)",
    ingredientes: "Pepino holandés, zanahoria, queso y sésamo tostado",
    price: "4.29",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de salmón noruego y mango (8 pzas.)",
    ingredientes: "",
    price: "5.00",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de salmón noruego y piña (8 pzas.)",
    ingredientes: "",
    price: "5.28",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de salmón noruego, anguila asada y aguacate (8 pzas.)",
    ingredientes: "",
    price: "5.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de carne de cangrejo con salsa ponzu (8 pzas.)",
    ingredientes: "",
    price: "4.18",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de pez mantequilla con cebolla fresca (8 pzas.)",
    ingredientes: "",
    price: "4.18",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de atún rojo (8 pzas.)",
    ingredientes: "",
    price: "5.67",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Makis de atún rojo y aguacate (8 pzas.)",
    ingredientes: "",
    price: "5.78",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Temaki de salmón noruego  y huevas de pescado (1 pza.)",
    ingredientes: "…",
    price: "6.10",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Temaki de anguila asada con salsa teriyaki (1 pza.)",
    ingredientes: "Lechuga, salsa de anguila, aguacate, pepino y sésamo",
    price: "6.10",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Temaki de atún rojo y huevas de pescado (1 pza.)",
    ingredientes: "Lechuga, mayonesa, aguacate, pepinos y sésamo",
    price: "6.60",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4821eb91b2b5b0b68a2",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Nigiri de salmón noruego (3 pzas.)",
    ingredientes: "",
    price: "4.56",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nigiri de salmón flameado teriyaki (3 pzas.)",
    ingredientes: "",
    price: "5.06",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nigiri de salmón caramelizado ponzu (3 pzas.)",
    ingredientes: "",
    price: "5.30",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nigiri de salmón flameado con huevas de salmón (3 pzas.)",
    ingredientes: "",
    price: "5.40",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nigiri de anguila asada teriyaki (3 pzas.)",
    ingredientes: "",
    price: "5.50",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nigiri de langostino con sésamo tostado (3 pzas.)",
    ingredientes: "",
    price: "5.40",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nigiri de anguila asada teriyaki flameada (3 pzas.)",
    ingredientes: "",
    price: "5.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nigiri de pez mantequilla (3 pzas.)",
    ingredientes: "",
    price: "4.56",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nigiri de pez mantequilla con trufa (3 pzas.)",
    ingredientes: "",
    price: "5.25",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nigiri pez mantequilla caramelizada ponzu (3 pzas.)",
    ingredientes: "",
    price: "5.30",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nigiri de pez mantequilla con trufa negra flameado (3 pzas.)",
    ingredientes: "",
    price: "5.50",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nigiri de atún rojo (3 pzas.)",
    ingredientes: "",
    price: "5.90",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nigiri de atún rojo flameado teriyaki (3 pzas.)",
    ingredientes: "",
    price: "6.45",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b48a1eb91b2b5b0b68a3",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Sashimi de salmón noruego (5 cortes)",
    ingredientes: "",
    price: "6.10",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4951eb91b2b5b0b68a4",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sashimi salmón flameado con salsa teriyaki (5 cortes)",
    ingredientes: "Con cebolleta y sésamo tostado",
    price: "7.26",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4951eb91b2b5b0b68a4",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sashimi de atún flameado con salsa teriyaki (5 cortes)",
    ingredientes: "Con cebolleta y sésamo tostado",
    price: "7.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4951eb91b2b5b0b68a4",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sashimi de atún rojo (5 cortes)",
    ingredientes: "",
    price: "7.55",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4951eb91b2b5b0b68a4",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Shinkansen G1 (74 piezas)",
    ingredientes:
      "9 rolls jamón y queso, 9 de salmón queso y nueces, 9 de mango aguacate y 9 de atún cebollino + 8 makis salmón queso, 8 de aguacate, 8 de pepino sésamo + 3 nigiri salmón, 3 de atún rojo, 3 de pez mantequilla y 3 de langostino + 2 ikura con huevas",
    price: "68.75",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b49f1eb91b2b5b0b68a5",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Shinkansen G3 (58 piezas)",
    ingredientes:
      "6 California de atún con cebollino+ 6 uramaki salmón y aguacates + 8 Caifornia cebolla frita y 8 de spicy + 6 cristal maki salmón y 6 de pollo katsu + 6 makis pepino + 4 nigiri salmón, 1 de atún rojo, 1 pez mantequilla y 2 de langostino + 2 sashimi",
    price: "53.35",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b49f1eb91b2b5b0b68a5",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Shinkansen G2 (53 piezas)",
    ingredientes:
      "8 rolls de alga wakame, 8 de cebolla frita, 6 de atún cocido con cebolleta fresca, 6 de salmón noruego con mango amarillo y queso + 6 rolls uramaki de salmón noruego + 6 rolls spicy vegetal y 6 de queso con salmón + 3 sashimi de salmón, 1 de atún roj...",
    price: "49.50",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b49f1eb91b2b5b0b68a5",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Gyozas de pollo (3 pzas.)",
    ingredientes: "",
    price: "3.09",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4aa1eb91b2b5b0b68a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Gyozas de langostino (3 pzas.)",
    ingredientes: "",
    price: "3.09",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4aa1eb91b2b5b0b68a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Yakisoba con verdura",
    ingredientes: "Fideo con verdura y salsa teriyaki",
    price: "6.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4aa1eb91b2b5b0b68a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Yakisoba con pollo yakitori",
    ingredientes: "Fideos con pollo yakitori y salsa teriyaki",
    price: "7.65",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4aa1eb91b2b5b0b68a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Brochetas de pollo yakitori (4 uds.)",
    ingredientes: "",
    price: "5.35",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4aa1eb91b2b5b0b68a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Ensalada wakame con sésamo tostado",
    ingredientes: "",
    price: "3.85",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4aa1eb91b2b5b0b68a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sappro (650 ml.)",
    ingredientes: "",
    price: "5.30",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4aa1eb91b2b5b0b68a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Tartar de salmón (1 bol)",
    ingredientes:
      "Con aguacates, cangrejo, aceite, sésamo, pepino holandés, huevas de pescado, aliño de 4 salsas y grosella",
    price: "11.85",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4b51eb91b2b5b0b68a7",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Tartar de pez mantequilla (1 bol)",
    ingredientes:
      "Con aguacate en tacos, hueva, cangrejo, aliño de yuzu, aceite, sésamo, grosella y 4 salsas",
    price: "10.85",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4b51eb91b2b5b0b68a7",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Tartar de atún (1 bol)",
    ingredientes:
      "Con aguacate en tacos, huevas de cangrejo, grosella, aceite, sésamo y aliño especial",
    price: "12.85",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4b51eb91b2b5b0b68a7",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Uramaki vegetal de aguacate (5 pzas.)",
    ingredientes: "Con pepino, palito de cangrejo y recubierto de sésamo",
    price: "3.58",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4c01eb91b2b5b0b68a8",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Uramaki vegetal de cebolla frita (5 pzas.)",
    ingredientes:
      "Con aguacate, pepino, palito de cangrejo y recubierto de mayonesa y salsa anguila",
    price: "4.15",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4c01eb91b2b5b0b68a8",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Uramaki vegetal spicy (5 pzas.)",
    ingredientes:
      "Recubierto de sésamo, mayonesa, 7 especias, aguacate, pepino y palito de cangrejo",
    price: "3.85",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4c01eb91b2b5b0b68a8",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Uramaki vegetal de queso Philadelphia (5 pzas.)",
    ingredientes: "Recubierto de sésamo, aguacate, pepino y palito de cangrejo",
    price: "3.80",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4c01eb91b2b5b0b68a8",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Uramaki veggie (5 pzas.)",
    ingredientes:
      "Con pepino, aguacate, zanahoria, lechuga, tomate, queso y sésamo tostado",
    price: "4.38",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4c01eb91b2b5b0b68a8",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Uramaki vegetal de alga wakame y sésamo (5 pzas.)",
    ingredientes: "Relleno de aguacate, pepino y palito de cangrejo",
    price: "4.57",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4c01eb91b2b5b0b68a8",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Uramaki vegetal de manzana (5 pzas.)",
    ingredientes:
      "Con fuji, pepino holandés, palito de cangrejo y recubierto de huevas de pescado",
    price: "4.15",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4c01eb91b2b5b0b68a8",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Rolls con queso Philadelphia y salmón noruego (5 pzas.)",
    ingredientes: "Recubierto con aguacate",
    price: "4.33",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4d81eb91b2b5b0b68a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Rolls con queso Philadelphia y langostino (5 pzas.)",
    ingredientes: "Recubierto de aguacate",
    price: "4.55",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4d81eb91b2b5b0b68a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title:
      "Rolls con queso Philadelphia, salmón noruego, almendra y queso (5 pzas.)",
    ingredientes: "Con pepino y aguacate",
    price: "5.15",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4d81eb91b2b5b0b68a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title:
      "Rolls con queso Philadelphia, salmón noruego, pepino y queso (5 pzas.)",
    ingredientes: "Con sésamo totado y recubierto de hueva de cangrejo",
    price: "4.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4d81eb91b2b5b0b68a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title:
      "Rolls con queso Philadelphia, langostino ponzu y aguacate (5 pzas.)",
    ingredientes: "Con pepino, queso, sésamo, hueva de pescado y tobiko",
    price: "5.15",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4d81eb91b2b5b0b68a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title:
      "Rolls con queso Philadelphia, salmón noruego, mango y queso (5 pzas.)",
    ingredientes: "Recubierto de semillita chía y paipola tostado",
    price: "4.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4d81eb91b2b5b0b68a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title:
      "Rolls con queso Philadelphia, melón amarillo y corazón de buey (5 pzas.)",
    ingredientes:
      "Con queso, canónigo y tomate, recubierto de jamón ibérico y salsa teriyaki",
    price: "5.51",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4d81eb91b2b5b0b68a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title:
      "Rolls con queso Philadelphia, corazón de buey y jamón ibérico (5 pzas.)",
    ingredientes:
      "Con queso, canónigo, recubierto con tofu frito y salsa teriyaki",
    price: "5.51",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4d81eb91b2b5b0b68a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "California rolls Shinkansen (5 pzas.)",
    ingredientes:
      "Combinan la cremosidad del queso y el aguacate, con la dulzura de las almendras y la piña para ofrecerte un verdadero placer",
    price: "4.40",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4d81eb91b2b5b0b68a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Rolls con queso Philadelphia y atún rojo (5 pzas.)",
    ingredientes: "Recubierto de aguacate",
    price: "4.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4d81eb91b2b5b0b68a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Roll especial de salmón noruego y trufa negra (5 pzas.)",
    ingredientes:
      "Aceite de trufa, aguacate, pepino, sésamo tostado y mayonesa",
    price: "4.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4e11eb91b2b5b0b68aa",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Roll especial de atún cocido y pepino holandés (5 pzas.)",
    ingredientes:
      "Con aguacate, recubierto de mayonesa japonesa, sésamo tostado y cebolleta fresca",
    price: "5.15",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4e11eb91b2b5b0b68aa",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Roll especial de langostino, pepino y mayonesa japonesa (5 pzas.)",
    ingredientes:
      "Con aguacate, recubierto de sésamo tostado y hueva de pescado",
    price: "4.58",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4e11eb91b2b5b0b68aa",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Roll especial de salmón noruego, aguacate y pepino (5 pzas.)",
    ingredientes: "Recubierto de sésamo tostado, mayonesa y hueva de salmón",
    price: "5.15",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4e11eb91b2b5b0b68aa",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Roll spicy de salmón noruego (5 pzas.)",
    ingredientes: "Con pepino y aguacate, recubierto de 7 especias",
    price: "3.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4e11eb91b2b5b0b68aa",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Roll especial de trufa negra y atún rojo (5 pzas.)",
    ingredientes:
      "Con aceite, trufa, aguacate, pepino, sésamo y mayonesa japonesa",
    price: "5.65",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4e11eb91b2b5b0b68aa",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Roll spicy de atún rojo (5 pzas.)",
    ingredientes: "Con pepino y aguacate, recubierto de 7 especias",
    price: "4.75",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4e11eb91b2b5b0b68aa",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Dragón aburi roll (8 pzas.)",
    ingredientes: "Salmón, queso, salmón a la plancha, cebollino y 7 especias",
    price: "11.85",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4ec1eb91b2b5b0b68ab",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Dragón especial de salmón flameado (8 pzas.)",
    ingredientes:
      "Con salsa teriyaki y salsa shinkan, recubierto de salmón, aguacates y sésamos tostados con cebolletas",
    price: "11.85",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4ec1eb91b2b5b0b68ab",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Dragón especial de anguila asada flameado (8 pzas.)",
    ingredientes:
      "Con salsa teriyaki, recubierto de anguila asada, aguacate, sésamos tostados y hueva de pescado",
    price: "13.15",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4ec1eb91b2b5b0b68ab",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Dragón roll bacon ibérico (8 pzas.)",
    ingredientes:
      "Con tamago, canónigo flameado, recubierto de mayonesa japonesa, cebolleta y 7 especias",
    price: "9.85",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4ec1eb91b2b5b0b68ab",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Dragón roll salmón spicy (8 pzas.)",
    ingredientes:
      "Relleno de tataki flameado con salsa chili, recubierto de salmón y hueva ikura",
    price: "11.95",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4ec1eb91b2b5b0b68ab",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Dragón especial de atún flameado (8 pzas.)",
    ingredientes:
      "Con salsa teriyaki y salsa shinkan, recubierto de atún rojo, aguacates y sésamos tostados con cebolletas",
    price: "13.35",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4ec1eb91b2b5b0b68ab",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Dragón rolls especiales de atún (8 pzas.)",
    ingredientes:
      "Caramelizados con salsa ponzu agridulce, con almendra, manzana, fuji, salsa shin y tamago",
    price: "11.90",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4f91eb91b2b5b0b68ac",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Dragón rolls especiales de salmón (8 pzas.)",
    ingredientes:
      "Caramelizados con salsa ponzu agridulce, con almendra, manzana, fuji, salsa shin y tamago",
    price: "11.80",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b4f91eb91b2b5b0b68ac",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Doroyaki de chocolate (1 pza.)",
    ingredientes: "",
    price: "1.50",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b5071eb91b2b5b0b68ad",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Doroyaki de judías rojas (1 pza.)",
    ingredientes: "",
    price: "1.50",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b5071eb91b2b5b0b68ad",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Mochis (2 pzas.)",
    ingredientes: "",
    price: "2.50",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b5071eb91b2b5b0b68ad",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Cerveza japonesa Sapporo (330 ml.)",
    ingredientes: "",
    price: "2.80",
    imagen: null,
    restaurant: "5ff1cfa885c1c068931a3859",
    menu: "6001b5101eb91b2b5b0b68ae",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
];
