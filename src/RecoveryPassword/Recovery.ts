import { Response, Router } from "express";
import userSchema from "../models/user";
import restaurantSchema from "../models/restaurant";
import riderstSchema from "../models/riders";
import crypto from "crypto";
import bcrypt from "bcryptjs";
const nodemailer = require("nodemailer");
import recoverPasswordEmail from "../RecoveryPassword/Email/RecoveryPassword";
import recoverPasswordEmailRestaurant from "../RecoveryPassword/Email/RecoveryPasswordrestaurant";
import recoverPasswordEmailRiders from "../RecoveryPassword/Email/RecoveryPasswordriders";

const mailjetTransport = require('nodemailer-mailjet-transport');

class RecoveryRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/forgotpassword", async (req: any, res: Response) => {
      const email = req.query.email;
      await userSchema.find({ email: email }, (err: any, data: any) => {
        if (data.length < 1 || err) {
          res.status(200).json({
            message: "Aún no tenemos este correo eletrónico",
            success: false,
            noEmail: true,
          });
        } else {
          const token = crypto.randomBytes(20).toString("hex");
          // write to database
          const transporter = nodemailer.createTransport(mailjetTransport({
            service: 'Mailjet',
            host: "in-v3.mailjet.com",
            secure: true,
            port: 587,
            auth: {
              apiKey: process.env.MJ_APIKEY_PUBLIC,
              apiSecret: process.env.MJ_APIKEY_PRIVATE
            }
          }));
          const mailOptions = {
            from: process.env.EMAIL_ADDRESS,
            to: email,
            subject: "Recuperar contraseña Wilbby App",
            text: "Recuperar contraseña Wilbby App",
            html: recoverPasswordEmail(token, email),
          };
          userSchema.findOneAndUpdate(
            { email: email },
            { forgotPasswordToken: token },
            {
              new: true,
            },
            (err, updated) => {
              if(err) {
                res
                .status(200)
                .json({ message: "Algo salió mal intentalo de nuevo", success: false });
              } else {
                res
                .status(200)
                .json({ message: "Email enviado a tu bandeja de entrada", success: true });
              }
            }
          );
         transporter.sendMail(mailOptions)
        }
      });
    });

    this.router.post("/tokenValidation", (req, res) => {
      userSchema.find({ forgotPasswordToken: req.body.token }, (err, data) => {
        if (err || data.length < 1) {
          if (err) console.log(err);
          res.status(200).json({ isValid: false, email: "" });
        } else if (data.length > 0) {
          res.status(200).json({ isValid: true, email: data[0].email });
        }
      });
    });

    this.router.post("/resetPassword", (req, res) => {
      var newvalues = { $unset: { forgotPasswordToken: req.body.token } };
      userSchema.findOneAndUpdate(
        { email: req.body.email },
        newvalues,
        {
          //options
          new: true,
          strict: false,
          useFindAndModify: false,
        },
        (err, updated) => {
          if (err) console.log(err);
          bcrypt.genSalt(10, (err: any, salt: any) => {
            if (err) console.log(err);

            bcrypt.hash(req.body.password, salt, (err, hash) => {
              if (err) console.log(err);
              var newvalues2 = { $set: { password: hash } };
              userSchema.findOneAndUpdate(
                { email: req.body.email },
                newvalues2,
                {
                  //options
                  new: true,
                  strict: false,
                  useFindAndModify: false,
                },
                (err, updated) => {
                  if (err) console.log(err);
                  res.status(200).json({ changed: true });
                }
              );
            });
          });
        }
      );
    });

    /// recovery restaurant

    this.router.get(
      "/forgotpassword-restaurant",
      async (req: any, res: Response) => {
        const email = req.query.email;
        await restaurantSchema.find({ email: email }, (err: any, data: any) => {
          if (data.length < 1 || err) {
            res.status(400).json({
              message: "Aún no tenemos este correo eletrónico",
              success: false,
              noEmail: true,
            });
          } else {
            const token = crypto.randomBytes(20).toString("hex");
            // write to database
            const transporter = nodemailer.createTransport(mailjetTransport({
              service: 'Mailjet',
              host: "in-v3.mailjet.com",
              secure: true,
              port: 587,
              auth: {
                apiKey: process.env.MJ_APIKEY_PUBLIC,
                apiSecret: process.env.MJ_APIKEY_PRIVATE
              }
            }));
            const mailOptions = {
              from: process.env.EMAIL_ADDRESS,
              to: email,
              subject: "Recuperar contraseña Wilbby para restaurantes",
              text: "Recuperar contraseña Wilbby para restaurantes",
              html: recoverPasswordEmailRestaurant(token, email),
            };
            restaurantSchema.findOneAndUpdate(
              { email: email },
              { forgotPasswordToken: token },
              {
                new: true,
              },
              (err, updated) => {
                if(err) {
                  res
                  .status(200)
                  .json({ message: "Algo salió mal intentalo de nuevo", success: false });
                } else {
                  res
                  .status(200)
                  .json({ message: "Email enviado a tu bandeja de entrada", success: true });
                }
              }
            );
           transporter.sendMail(mailOptions)
          }
        });
      }
    );

    this.router.post("/tokenValidation-restaurant", (req, res) => {
      restaurantSchema.find(
        { forgotPasswordToken: req.body.token },
        (err, data) => {
          if (err || data.length < 1) {
            if (err) console.log(err);
            res.status(200).json({ isValid: false, email: "" });
          } else if (data.length > 0) {
            res.status(200).json({ isValid: true, email: data[0].email });
          }
        }
      );
    });

    this.router.post("/resetPassword-restaurant", (req, res) => {
      var newvalues = { $unset: { forgotPasswordToken: req.body.token } };
      restaurantSchema.findOneAndUpdate(
        { email: req.body.email },
        newvalues,
        {
          //options
          new: true,
          strict: false,
          useFindAndModify: false,
        },
        (err, updated) => {
          if (err) console.log(err);
          bcrypt.genSalt(10, (err: any, salt: any) => {
            if (err) console.log(err);

            bcrypt.hash(req.body.password, salt, (err, hash) => {
              if (err) console.log(err);
              var newvalues2 = { $set: { password: hash } };
              restaurantSchema.findOneAndUpdate(
                { email: req.body.email },
                newvalues2,
                {
                  //options
                  new: true,
                  strict: false,
                  useFindAndModify: false,
                },
                (err, updated) => {
                  if (err) console.log(err);
                  res.status(200).json({ changed: true });
                }
              );
            });
          });
        }
      );
    });

    /// recovery riders

    this.router.get(
      "/forgotpassword-riders",
      async (req: any, res: Response) => {
        const email = req.query.email;
        await riderstSchema.find({ email: email }, (err: any, data: any) => {
          if (data.length < 1 || err) {
            res.status(400).json({
              message: "Aún no tenemos este correo eletrónico",
              success: false,
              noEmail: true,
            });
          } else {
            const token = crypto.randomBytes(20).toString("hex");
            // write to database
            const transporter = nodemailer.createTransport(mailjetTransport({
              service: 'Mailjet',
              host: "in-v3.mailjet.com",
              secure: true,
              port: 587,
              auth: {
                apiKey: process.env.MJ_APIKEY_PUBLIC,
                apiSecret: process.env.MJ_APIKEY_PRIVATE
              }
            }));
            const mailOptions = {
              from: process.env.EMAIL_ADDRESS,
              to: email,
              subject: "Recuperar contraseña Wilbby para Riders",
              text: "Recuperar contraseña Wilbby para Riders",
              html: recoverPasswordEmailRiders(token, email),
            };
            riderstSchema.findOneAndUpdate(
              { email: email },
              { forgotPasswordToken: token },
              {
                new: true,
              },
              (err, updated) => {
                if(err) {
                  res
                  .status(200)
                  .json({ message: "Algo salió mal intentalo de nuevo", success: false });
                } else {
                  res
                  .status(200)
                  .json({ message: "Email enviado a tu bandeja de entrada", success: true });
                }
              }
            );
           transporter.sendMail(mailOptions)
          }
        });
      }
    );

    this.router.post("/tokenValidation-riders", (req, res) => {
      riderstSchema.find(
        { forgotPasswordToken: req.body.token },
        (err, data) => {
          if (err || data.length < 1) {
            if (err) console.log(err);
            res.status(200).json({ isValid: false, email: "" });
          } else if (data.length > 0) {
            res.status(200).json({ isValid: true, email: data[0].email });
          }
        }
      );
    });

    this.router.post("/resetPassword-riders", (req, res) => {
      var newvalues = { $unset: { forgotPasswordToken: req.body.token } };
      riderstSchema.findOneAndUpdate(
        { email: req.body.email },
        newvalues,
        {
          //options
          new: true,
          strict: false,
          useFindAndModify: false,
        },
        (err, updated) => {
          if (err) console.log(err);
          bcrypt.genSalt(10, (err: any, salt: any) => {
            if (err) console.log(err);

            bcrypt.hash(req.body.password, salt, (err, hash) => {
              if (err) console.log(err);
              var newvalues2 = { $set: { password: hash } };
              riderstSchema.findOneAndUpdate(
                { email: req.body.email },
                newvalues2,
                {
                  //options
                  new: true,
                  strict: false,
                  useFindAndModify: false,
                },
                (err, updated) => {
                  if (err) console.log(err);
                  res.status(200).json({ changed: true });
                }
              );
            });
          });
        }
      );
    });
  }
}

const recoveryRouter = new RecoveryRouter();
recoveryRouter.routes();

export default recoveryRouter.router;
