const nodemailer = require("nodemailer");
import welcomeEmails from "./welcomeEmail";

const mailjetTransport = require('nodemailer-mailjet-transport');

export const welcomeEmail = (email: string, name: string) => {
  const transporter = nodemailer.createTransport(mailjetTransport({
    service: 'Mailjet',
    host: "in-v3.mailjet.com", 
    secure: true,
    port: 587,
    auth: {
      apiKey: process.env.MJ_APIKEY_PUBLIC,
      apiSecret: process.env.MJ_APIKEY_PRIVATE
    }
  }));
  const mailOptions = {
    from: process.env.EMAIL_ADDRESS,
    to: email ? email : "info@wilbby.com",
    subject: "Bienvenido a Wilbby",
    text: "Con Wilbby lo tienes todo",
    html: welcomeEmails(name),
  };

  transporter.sendMail(mailOptions);
};
