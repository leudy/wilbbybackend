import emailSchema from "../models/EmailsDatas";

export const SaveEmailWilbby = async (
  email: string,
  name: string,
  lastName: string
) => {
  const isExist = await emailSchema.findOne({ email: email });
  if (isExist) {
    null;
  } else {
    const newEmail = new emailSchema({
      name: name,
      lastName: lastName,
      email: email,
    });
    newEmail.save();
  }
};
