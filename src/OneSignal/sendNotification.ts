export const sendNotificationCustomer = (
  IdOnesignal: any,
  textmessage: string,
  title: string
) => {
  var sendNotification = function (data: any) {
    var headers = {
      "Content-Type": "application/json; charset=utf-8",
    };
    var options = {
      host: "onesignal.com",
      port: 443,
      path: "/api/v1/notifications",
      method: "POST",
      headers: headers,
    };
    var https = require("https");
    var req = https.request(options, function (res: any) {
      res.on("data", function (data: any) {
        console.log("Response:");
      });
    });

    req.on("error", function (e: any) {
      console.log("ERROR:");
      console.log(e);
    });
    req.write(JSON.stringify(data));
    req.end();
  };

  var message = {
    app_id: "db79b975-e551-4741-ae43-8968ceab5f09",
    headings: { en: title },
    android_channel_id: "cfc50b79-a76a-4114-8200-0b53d6d7bd30",
    contents: { en: `${textmessage}` },
    ios_badgeCount: 1,
    ios_badgeType: "Increase",
    include_player_ids: [IdOnesignal],
  };
  sendNotification(message);
};

export const sendNotificationStore = (
  IdOnesignal: any,
  textmessage: string,
  title: string
) => {
  var sendNotification = function (data: any) {
    var headers = {
      "Content-Type": "application/json; charset=utf-8",
    };
    var options = {
      host: "onesignal.com",
      port: 443,
      path: "/api/v1/notifications",
      method: "POST",
      headers: headers,
    };
    var https = require("https");
    var req = https.request(options, function (res: any) {
      res.on("data", function (data: any) {
        console.log("Response:");
      });
    });

    req.on("error", function (e: any) {
      console.log("ERROR:");
      console.log(e);
    });
    req.write(JSON.stringify(data));
    req.end();
  };

  var message = {
    app_id: "9a1893e6-8423-43cc-807a-d536a833b8e8",
    headings: { en: `${title}` },
    android_channel_id: "9009455c-8429-41f2-bb18-bd63a08a9d63",
    ios_sound: "despertador_minion.wav",
    contents: { en: `${textmessage}` },
    ios_badgeCount: 1,
    ios_badgeType: "Increase",
    include_player_ids: [IdOnesignal],
  };
  sendNotification(message);
};
