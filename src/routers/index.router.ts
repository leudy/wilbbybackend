import { Request, Response, Router } from "express";
import DownloadApp from "./../models/dowload_app";
import restauranteSchema from "../models/restaurant";
import ridersSchema from "../models/riders";
import citiesSchema from "../models/cityclose";
import HomeMessagesSchema from "../models/HomeMessage";
import { sendNotification } from "../GraphQL/sendNotificationRider";

class IndexRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/", (req: Request, res: Response) =>
      res.redirect("https://wilbby.com")
    );

    this.router.post("/download-app", async (req: Request, res: Response) => {
      const { source, referred, campaing } = req.body;
      const newData = new DownloadApp({
        referred: referred,
        source: source,
        date: new Date(),
        campaing: campaing,
      });
      newData
        .save()
        .then(() => {
          res.json({ success: true, messages: "done" });
        })
        .catch(() => res.json({ success: false, messages: "failed" }));
    });

    this.router.get(
      "/riders-available",
      async (req: Request, res: Response) => {
        const { city } = req.query;
        ridersSchema.find({ city: city, isAvalible: true }, (err, resp) => {
          if (err) {
            res
              .json({
                success: false,
                messages: "No riders available",
                data: null,
              })
              .end();
          } else {
            res
              .json({
                success: true,
                messages: "Riders available",
                data: resp,
              })
              .end();
          }
        });
      }
    );

    this.router.get(
      "/cities-available",
      async (req: Request, res: Response) => {
        const { city } = req.query;
        citiesSchema.findOne({ city: city }, (err, resp) => {
          if (err) {
            res
              .json({
                success: false,
                messages: "No citie available",
                data: null,
              })
              .end();
          } else {
            res
              .json({
                success: true,
                messages: "Citie available",
                data: resp,
              })
              .end();
          }
        });
      }
    );

    this.router.get(
      "/get-notification-home",
      async (req: Request, res: Response) => {
        const { city } = req.query;
        // @ts-ignore
        HomeMessagesSchema.findOne({ city: city }, (err, resp) => {
          if (err) {
            res
              .json({
                success: false,
                messages: "No message available",
                data: null,
              })
              .end();
          } else {
            res
              .json({
                success: true,
                messages: "Message available",
                data: resp,
              })
              .end();
          }
        });
      }
    );

    this.router.get("/store-wilbby", async (req, res) => {
      const data = await restauranteSchema.find({ city: `${req.query.city}` });

      res.json(data).end();
    });

    this.router.get("/tets-notification", async (req, res) => {
      const { OnesignalID } = req.query;
      sendNotification(
        OnesignalID,
        "Notificación de Prueba",
        "Notificación de Prueba",
        true
      );
      res.status(200).json({ data: "Mesanje enviado" }).end();
    });
  }
}

//d7618df2-594d-4b1c-9151-71b3afc3a11c

const indexRouter = new IndexRouter();
indexRouter.routes();

export default indexRouter.router;
