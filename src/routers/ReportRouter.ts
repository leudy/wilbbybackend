import { Request, Response, Router } from "express";
import xl from "excel4node";
import path from "path";
import moment from "moment";
import orderSchema from "../models/newOrder/order";

var wb = new xl.Workbook();

var ws = wb.addWorksheet("Reporte de pedido en Wilbby");

class ReportRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.post(
      "/genetate-report",
      async (req: Request, res: Response) => {
        const data = req.body;

        var style = wb.createStyle({
          font: {
            color: "#FFFFFF",
            size: 24,
            bold: true,
          },
          fill: {
            type: "pattern",
            patternType: "solid",
            bgColor: "#90C33C",
            fgColor: "#90C33C",
          },
        });

        var styleContest = wb.createStyle({
          font: {
            color: "#000000",
            size: 14,
          },
          alignment: {
            wrapText: false,
            horizontal: "left",
          },
          numberFormat: "€#,##0.00; (€#,##0.00); -",
        });

        var styleNumber = wb.createStyle({
          font: {
            color: "#29B2FF",
            size: 16,
            bold: true,
          },
          alignment: {
            wrapText: false,
            horizontal: "left",
          },
        });

        ws.cell(1, 1).string("ID Pedido").style(style);
        ws.cell(1, 2).string("Fecha").style(style);
        ws.cell(1, 3).string("Repartidor").style(style);
        ws.cell(1, 4).string("Total productos").style(style);
        ws.cell(1, 5).string("Precio de envío").style(style);
        ws.cell(1, 6).string("Propina").style(style);
        ws.cell(1, 7).string("Torifa por servicio").style(style);
        ws.cell(1, 8).string("Total").style(style);
        ws.cell(1, 9).string("Establecimiento").style(style);
        ws.cell(1, 10).string("Hora de entrada").style(style);
        ws.cell(1, 11).string("Hora de entrega").style(style);
        ws.cell(1, 12).string("Estado").style(style);
        ws.cell(1, 13).string("Ciudad").style(style);
        ws.cell(1, 14).string("Método de pago").style(style);

        ws.column(1).setWidth(20);
        ws.column(2).setWidth(40);
        ws.column(3).setWidth(40);
        ws.column(4).setWidth(30);
        ws.column(5).setWidth(30);
        ws.column(6).setWidth(20);
        ws.column(7).setWidth(35);
        ws.column(8).setWidth(20);
        ws.column(9).setWidth(40);
        ws.column(10).setWidth(30);
        ws.column(11).setWidth(30);
        ws.column(12).setWidth(30);
        ws.column(13).setWidth(40);
        ws.column(14).setWidth(40);

        let condition = {
          status: {
            $in: [
              "Nueva",
              "Confirmada",
              "En la cocina",
              "Listo para recoger",
              "Preparando para el envío",
              "En camino",
              "Entregada",
              "Finalizada",
            ],
          },
        };

        if (data.city) {
          condition = {
            // @ts-ignore
            "storeData.city": data.city,
            status: {
              $in: [
                "Nueva",
                "Confirmada",
                "En la cocina",
                "Listo para recoger",
                "Preparando para el envío",
                "En camino",
                "Entregada",
                "Finalizada",
              ],
            },
          };
        }

        // @ts-ignore
        if (data.Stores.length > 0) condition.store = { $in: data.Stores };

        // @ts-ignore
        if (data.courier.length > 0) condition.courier = { $in: data.courier };

        if (
          data.dateRange &&
          data.dateRange.fromDate &&
          data.dateRange.toDate
        ) {
          const toDate = moment(data.dateRange.toDate)
            .add(1, "day")
            .format("MM/DD/YYYY");

          // @ts-ignore
          condition.created_at = {
            $gte: new Date(data.dateRange.fromDate),
            $lte: new Date(toDate),
          };
        }

        if (
          data.dateRange &&
          data.dateRange.fromDate &&
          !data.dateRange.toDate
        ) {
          const toDate = moment(data.dateRange.fromDate)
            .add(1, "day")
            .format("MM/DD/YYYY");

          // @ts-ignore
          condition.created_at = {
            $gte: data.dateRange.fromDate,
            $lt: toDate,
          };
        }

        const orders = await orderSchema.find(condition).sort({ $natural: -1 });

        if (orders.length > 0) {
          orders.forEach((order, i) => {
            i == 0 ? (i = orders.length + 0) : i;
            i == 1 ? (i = orders.length + 1) : i;

            var totals = 0;

            order &&
              order.items.forEach(function (items: any) {
                const t = items.items.quantity * items.items.price;
                totals += t;
                items.items.subItems.forEach((x) => {
                  const s = x.quantity * x.price;
                  totals += s * items.items.quantity;
                  const t = x && x.subItems ? x.subItems : [];
                  t.forEach((y) => {
                    const u = y.quantity * y.price;
                    totals += u * items.items.quantity;
                  });
                });
              });

            const Statusselected = order.statusProcess.filter(
              (e) => e.status === "Entregada"
            );

            const delivere = Statusselected[0]
              ? Statusselected[0].date
              : "No consta";

            const startDate = order.scheduled
              ? order.deliveryTime
              : order.created_at;

            ws.cell(i, 1)
              .number(order.channelOrderDisplayId)
              .style(styleNumber);
            ws.cell(i, 2)
              .string(`${moment(order.created_at).format("LLLL")}`)
              .style(styleContest);
            ws.cell(i, 3)
              .string(
                `${order.courierData ? order.courierData.name : "Para"} ${
                  order.courierData ? order.courierData.lastName : "recoger"
                }`
              )
              .style(styleContest);
            ws.cell(i, 4)
              .number(totals / 100)
              .style(styleContest);
            ws.cell(i, 5)
              .number(order.deliveryCost / 100)
              .style(styleContest);
            ws.cell(i, 6)
              .number(order.tip / 100)
              .style(styleContest);
            ws.cell(i, 7)
              .number(order.serviceCharge / 100)
              .style(styleContest);
            ws.cell(i, 8)
              .number(order.payment / 100)
              .style(styleContest);
            ws.cell(i, 9)
              .string(`${order.storeData.title}`)
              .style(styleContest);
            ws.cell(i, 10)
              .string(`${moment(`${startDate}`).format("HH:mm A")}`)
              .style(styleContest);
            ws.cell(i, 11)
              .string(`${moment(delivere).format("HH:mm A")}`)
              .style(styleContest);
            ws.cell(i, 12).string(`${order.status}`).style(styleContest);
            ws.cell(i, 13)
              .string(`${order.storeData.city}`)
              .style(styleContest);
            ws.cell(i, 14).string(`${order.paymentMethod}`).style(styleContest);
          });

          const fileName = `report-${Math.round(Math.random() * 1000)}.xlsx`;

          const pathExcel = path.join(__dirname, fileName);

          wb.write(pathExcel, (error, data) => {
            if (error) {
              res.status(400).json({
                success: false,
                message: "Ha ocurrido un error intentalo de nuevo",
              });
            } else {
              res.status(200).json({
                success: true,
                message: "Reporte generado",
                file: fileName,
              });
            }
          });
        } else {
          res.status(200).json({
            success: false,
            message: "No hemos encontrado datos con estos filtros",
            file: null,
          });
        }
      }
    );

    this.router.post(
      "/genetate-report-client",
      async (req: Request, res: Response) => {
        const data = req.body;

        var style = wb.createStyle({
          font: {
            color: "#FFFFFF",
            size: 24,
            bold: true,
          },
          fill: {
            type: "pattern",
            patternType: "solid",
            bgColor: "#90C33C",
            fgColor: "#90C33C",
          },
        });

        var styleContest = wb.createStyle({
          font: {
            color: "#000000",
            size: 14,
          },
          alignment: {
            wrapText: false,
            horizontal: "left",
          },
          numberFormat: "€#,##0.00; (€#,##0.00); -",
        });

        var styleNumber = wb.createStyle({
          font: {
            color: "#29B2FF",
            size: 16,
            bold: true,
          },
          alignment: {
            wrapText: false,
            horizontal: "left",
          },
        });

        ws.cell(1, 1).string("ID Pedido").style(style);
        ws.cell(1, 2).string("Fecha").style(style);
        ws.cell(1, 3).string("Nombre del cliente").style(style);
        ws.cell(1, 4).string("Email").style(style);
        ws.cell(1, 5).string("Teléfono").style(style);
        ws.cell(1, 6).string("Dirección").style(style);
        ws.cell(1, 7).string("Ciudad").style(style);
        ws.cell(1, 8).string("Establecimiento").style(style);
        ws.cell(1, 9).string("Total productos").style(style);
        ws.cell(1, 10).string("Total").style(style);
        ws.cell(1, 11).string("Método de pago").style(style);
        
     
      
        ws.column(1).setWidth(20);
        ws.column(2).setWidth(40);
        ws.column(3).setWidth(40);
        ws.column(4).setWidth(30);
        ws.column(5).setWidth(20);
        ws.column(6).setWidth(80);
        ws.column(7).setWidth(30);
        ws.column(8).setWidth(30);
        ws.column(9).setWidth(40);
        ws.column(10).setWidth(40);
        ws.column(11).setWidth(40);
    

        let condition = {
          status: {
            $in: [
              "Nueva",
              "Confirmada",
              "En la cocina",
              "Listo para recoger",
              "Preparando para el envío",
              "En camino",
              "Entregada",
              "Finalizada",
            ],
          },
        };

        if (data.city) {
          condition = {
            // @ts-ignore
            "storeData.city": data.city,
            status: {
              $in: [
                "Nueva",
                "Confirmada",
                "En la cocina",
                "Listo para recoger",
                "Preparando para el envío",
                "En camino",
                "Entregada",
                "Finalizada",
              ],
            },
          };
        }

        // @ts-ignore
        if (data.Stores.length > 0) condition.store = { $in: data.Stores };

        if (
          data.dateRange &&
          data.dateRange.fromDate &&
          data.dateRange.toDate
        ) {
          const toDate = moment(data.dateRange.toDate)
            .add(1, "day")
            .format("MM/DD/YYYY");

          // @ts-ignore
          condition.created_at = {
            $gte: new Date(data.dateRange.fromDate),
            $lte: new Date(toDate),
          };
        }

        if (
          data.dateRange &&
          data.dateRange.fromDate &&
          !data.dateRange.toDate
        ) {
          const toDate = moment(data.dateRange.fromDate)
            .add(1, "day")
            .format("MM/DD/YYYY");

          // @ts-ignore
          condition.created_at = {
            $gte: data.dateRange.fromDate,
            $lt: toDate,
          };
        }

        const orders = await orderSchema.find(condition).sort({ $natural: -1 });

        if (orders.length > 0) {
          orders.forEach((order, i) => {
            i == 0 ? (i = orders.length + 0) : i;
            i == 1 ? (i = orders.length + 1) : i;

            var totals = 0;

            order &&
              order.items.forEach(function (items: any) {
                const t = items.items.quantity * items.items.price;
                totals += t;
                items.items.subItems.forEach((x) => {
                  const s = x.quantity * x.price;
                  totals += s * items.items.quantity;
                  const t = x && x.subItems ? x.subItems : [];
                  t.forEach((y) => {
                    const u = y.quantity * y.price;
                    totals += u * items.items.quantity;
                  });
                });
              });

            const Statusselected = order.statusProcess.filter(
              (e) => e.status === "Entregada"
            );

            const delivere = Statusselected[0]
              ? Statusselected[0].date
              : "No consta";

            const startDate = order.scheduled
              ? order.deliveryTime
              : order.created_at;

            ws.cell(i, 1)
              .number(order.channelOrderDisplayId)
              .style(styleNumber);
            ws.cell(i, 2)
              .string(`${moment(order.created_at).format("LLLL")}`)
              .style(styleContest);
            ws.cell(i, 3)
              .string(
                `${order.customerData.name} ${order.customerData.lastName}`
              )
              .style(styleContest);

              ws.cell(i, 4)
              .string(`${order.customerData.email}`)
              .style(styleContest);

            ws.cell(i, 5).string(`${order.customerData.telefono}`).style(styleContest);

            ws.cell(i, 6)
              .string(`${order.deliveryAddressData.formatted_address}, ${order.deliveryAddressData.puertaPiso}`)
              .style(styleContest);

              ws.cell(i, 7)
              .string(`${order.storeData.city}`)
              .style(styleContest);

              ws.cell(i, 8)
              .string(`${order.storeData.title}`)
              .style(styleContest);

            ws.cell(i, 9)
              .number(totals / 100)
              .style(styleContest);
            ws.cell(i, 10)
              .number(order.payment / 100)
              .style(styleContest);
          
            ws.cell(i, 11).string(`${order.paymentMethod}`).style(styleContest);
          });

          const fileName = `report-client${Math.round(Math.random() * 1000)}.xlsx`;

          const pathExcel = path.join(__dirname, fileName);

          wb.write(pathExcel, (error, data) => {
            if (error) {
              res.status(400).json({
                success: false,
                message: "Ha ocurrido un error intentalo de nuevo",
              });
            } else {
              res.status(200).json({
                success: true,
                message: "Reporte generado",
                file: fileName,
              });
            }
          });
        } else {
          res.status(200).json({
            success: false,
            message: "No hemos encontrado datos con estos filtros",
            file: null,
          });
        }
      }
    );

    this.router.get("/download-report", function (req, res) {
      const pathExcel = path.join(__dirname, `${req.query.file}`);
      res.download(pathExcel);
    });
  }
}

const reportRouter = new ReportRouter();
reportRouter.routes();

export default reportRouter.router;
