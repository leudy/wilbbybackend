import { Request, Response, Router } from "express";
import ContactEmail from "../RecoveryPassword/Email/ContactEmail";
import ContactEmailRider from "../RecoveryPassword/Email/ContactRider";
import ContactEmailAds from "../RecoveryPassword/Email/ContactAds";
const nodemailer = require("nodemailer");
const mailjetTransport = require('nodemailer-mailjet-transport');

const transporter = nodemailer.createTransport(mailjetTransport({
  service: 'Mailjet',
  host: "in-v3.mailjet.com", 
  secure: true,
  port: 587,
  auth: {
    apiKey: process.env.MJ_APIKEY_PUBLIC,
    apiSecret: process.env.MJ_APIKEY_PRIVATE
  }
}));

class ContactRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  
  routes() {
    this.router.post(
      "/contact-for-restaurant",
      (req: Request, res: Response) => {
        const { values } = req.body;
    
        const mailOptions = {
          from: process.env.EMAIL_ADDRESS,
          to: `info@wilbby.com`,
          subject: "Solicitud de para unirse a Wilbby",
          text: "Contacto nuevo establecimiento",
          html: ContactEmail(values),
        };

        transporter.sendMail(mailOptions);

        res.json({
          success: true,
          messages:
            "Solicitud enviada con éxito en 24 o 48 horas te contactaremos",
        });
      }
    );

    this.router.post("/contact-for-rider", (req: Request, res: Response) => {
      const { values } = req.body;
  
      const mailOptions = {
        from: process.env.EMAIL_ADDRESS,
        to: `info@wilbby.com`,
        subject: "Solicitud Riders",
        text: "Solicitud para repartir con Wilbby",
        html: ContactEmailRider(values),
      };
      transporter.sendMail(mailOptions);

      res.json({
        success: true,
        messages:
          "Solicitud enviada con éxito en 24 o 48 horas te contactaremos",
      });
    });

    this.router.post("/contact-for-ads", (req: Request, res: Response) => {
      const { values } = req.body;
      const mailOptions = {
        from: process.env.EMAIL_ADDRESS,
        to: `info@wilbby.com`,
        subject: "Solicitud informacion sobre piblicidad en Wilbby",
        text: "Solicitud informacion sobre piblicidad en Wilbby",
        html: ContactEmailAds(values),
      };
      transporter.sendMail(mailOptions);

      res.json({
        success: true,
        messages:
          "Información recibida el equipo de Wilbby te contactará lo antes posible",
      });
    });
  }
}

const constctRouter = new ContactRouter();
constctRouter.routes();

export default constctRouter.router;
