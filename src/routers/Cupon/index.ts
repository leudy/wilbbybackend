import { Router, Request, Response } from "express";
import cuponSchema from "../../models/cupones";
import orderSchema from "../../models/newOrder/order";

const routerCupon = Router();

routerCupon.get("/get-discount", async (req: Request, res: Response) => {
  const data = req.query;

  //@ts-ignore
  const cup = await cuponSchema.findOne({ clave: data.clave }).exec();

  if (cup) {
    const existOrder = await orderSchema.find({
      //@ts-ignore
      customer: data.user,
      cupon: cup?._id,
      status: { $ne: "Pendiente de pago" },
    });

    //@ts-ignore
    if (existOrder.length >= cup.usage) {
      res
        .status(200)
        .json({
          success: false,
          message: "Ya has utilizado este cupón",
          data: null,
        })
        .end();
      //@ts-ignore
    } else if (cup.exprirable) {
      //@ts-ignore
      if (cup.expire < new Date()) {
        res
          .status(200)
          .json({
            success: false,
            message: "Este cupón ha expirado",
            data: null,
          })
          .end();
      }
    } else {
      //@ts-ignore
      cuponSchema.findOne({ clave: data.clave }, (err: any, cupon) => {
        if (err) {
          res
            .status(400)
            .json({ success: false, message: "Error del servidor", data: [] })
            .end();
        } else if (!cupon) {
          res
            .status(200)
            .json({
              success: false,
              message: "Este cupón no existe",
              data: null,
            })
            .end();
        } else {
          res
            .status(200)
            .json({
              success: true,
              message: "Data extraida con éxito",
              data: cupon,
            })
            .end();
        }
      });
    }
  } else {
    res
      .status(200)
      .json({
        success: false,
        message: "Este cupón no existe",
        data: null,
      })
      .end();
  }
});

export default routerCupon;
