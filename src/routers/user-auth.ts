import { IUser } from './../models/user';
import { Request, Response, Router } from "express";
import dotenv from "dotenv";
import riderSchema, { IRider } from "../models/riders";
import userSchema from "../models/user";
import jwt from "jsonwebtoken";
import { Types } from "mongoose";
import moment from "moment";
import newOrderSchema from "../models/newOrder/order";

dotenv.config({ path: "variables.env" });

const { ObjectId } = Types;

class userAuth {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/get-user-auth", (req: Request, res: Response) => {
      const { token } = req.headers;
      if (token !== undefined || null) {
        try {
          //@ts-ignore
          const usuarioActual = jwt.verify(token, process.env.SECRETO);
          if (usuarioActual) {
            userSchema.findOne(
              //@ts-ignore
              { _id: usuarioActual._id },
              (err: any, user: IUser) => {
                if (err) {
                  res
                    .status(400)
                    .json({
                      success: false,
                      message: "Error del servidor",
                      error: err,
                      data: null,
                    })
                    .end();
                } else {
                  res
                    .status(200)
                    .json({
                      success: true,
                      message: "Data extraida con éxito",
                      data: user,
                    })
                    .end();
                }
              }
            );
          } else {
            res
              .status(404)
              .json({
                success: false,
                message: "Debes iniciar sesión para continuar",
                error: null,
                data: null,
              })
              .end();
          }
        } catch (err) {
          console.log(err);
          //@ts-ignore
          if (err.message === "jwt expired") {
            res
              .status(404)
              .json({
                success: false,
                message: "Tu sesión he expirado",
                error: err,
                data: null,
              })
              .end();
          }
        }
      } else {
        res
          .status(200)
          .json({
            success: false,
            message: "Debes iniciar sesión para continuar",
            error: null,
            data: null,
          })
          .end();
      }
    });

    this.router.get("/get-rider-auth", (req: Request, res: Response) => {
      const { token } = req.headers;
      if (token !== undefined || null) {
        try {
          //@ts-ignore
          const usuarioActual = jwt.verify(token, process.env.SECRETO);
          if (usuarioActual) {
            riderSchema.findOne(
              //@ts-ignore
              { _id: usuarioActual._id },
              (err: any, user: IRider) => {
                if (err) {
                  res
                    .status(400)
                    .json({
                      success: false,
                      message: "Error del servidor",
                      error: err,
                      data: null,
                    })
                    .end();
                } else {
                  res
                    .status(200)
                    .json({
                      success: true,
                      message: "Data extraida con éxito",
                      data: user,
                    })
                    .end();
                }
              }
            );
          } else {
            res
              .status(404)
              .json({
                success: false,
                message: "Debes iniciar sesión para continuar",
                error: null,
                data: null,
              })
              .end();
          }
        } catch (err) {
          console.log(err);
          //@ts-ignore
          if (err.message === "jwt expired") {
            res
              .status(404)
              .json({
                success: false,
                message: "Tu sesión he expirado",
                error: err,
                data: null,
              })
              .end();
          }
        }
      } else {
        res
          .status(200)
          .json({
            success: false,
            message: "Debes iniciar sesión para continuar",
            error: null,
            data: null,
          })
          .end();
      }
    });

    this.router.get("/get-order", (req: Request, res: Response) => {
      const { user, date } = req.query;
      // @ts-ignore
      const maxDate = moment(date).add(1, "days").format("YYYY-MM-DD");

      const today = moment().format("YYYY-MM-DD");

      const statusAll = [
        "Nueva",
        "Confirmada",
        "En la cocina",
        "Listo para recoger",
        "Preparando para el envío",
        "En camino",
        "Entregada",
        "Rechazado",
        "Rechazada por la tienda",
        "Rechazada por el rider",
        "Devuelto",
        "Finalizada",
        "Resolución",
      ];
      const statusSmall = [
        "Nueva",
        "Confirmada",
        "En la cocina",
        "Listo para recoger",
        "Preparando para el envío",
        "En camino",
      ];

      const status = date === today ? statusSmall : statusAll

      let matchQuery = {
        // @ts-ignore
        courier: ObjectId(user),
        status: {
          $in: status,
        },
        created_at: {
          // @ts-ignore
          $gte: new Date(date),
          $lte: new Date(maxDate),
        },
      };

      newOrderSchema
        .aggregate([
          { $match: matchQuery },
          {
            $group: {
              _id: { month: { $month: "$created_at" } },
              // @ts-ignore
              [moment(date).format("YYYY-MM-DD")]: {
                $addToSet: {
                  _id: "$_id",
                  status: "$status",
                  store: "$storeData.title",
                  customername: "$customerData.name",
                  customerlastname: "$customerData.lastName",
                  created_at: "$created_at",
                  addressStore: "$storeData.adress.calle",
                  addressStorenumber: "$storeData.adress.numero",
                  address: "$deliveryAddressData.formatted_address",
                  addressnumber: "$deliveryAddressData.puertaPiso",
                  type: "$storeData.storeData.categoryName",
                  scheduled: "$scheduled",
                  deliveryTime: "$deliveryTime",
                  channelOrderDisplayId: "$channelOrderDisplayId",
                },
              },
            },
          },
          {
            $sort: {
              "_id.month": -1,
            },
          },
        ])
        .then((resp) => {
          if (resp.length > 0) {
            const format = resp[0];
            delete format._id;
            res.status(200).json({ success: true, data: format }).end();
          } else {
            
            res
              .status(200)
              // @ts-ignore
              .json({ success: true, data: { [date]: [] } })
              .end();
          }
        })
        .catch((err) => {
          // @ts-ignore
          res
            .status(200)
            // @ts-ignore
            .json({ success: false, data: { [date]: [] } })
            .end();
        });
    });

    this.router.post("/exist-user", async (req: Request, res: Response) => {
        const {email} = req.body;
        if(email) {
          const user = await userSchema.findOne({email})
          if(user) {
            res.status(200).json({success: false, message: "Existe una cuenta con este email intentalo con otro diferente"}).end()
          }else {
            res.status(200).json({success: true, message: "Puedes cuntinuar"}).end()
          }
        } else {
          res.status(200).json({success: false, message: "Algo salio mal intentalo de nuevo por favor"}).end()
        }
    })
  }
}

const userAuths = new userAuth();

export default userAuths.router;
