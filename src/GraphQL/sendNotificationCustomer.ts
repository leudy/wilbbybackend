import { IOrders } from "../models/newOrder/order";
export const sendNotificationCustomer = (
  IdOnesignal: any,
  textmessage: string,
  title: string,
  order: any
) => {
  var sendNotification = function (data: any) {
    var headers = {
      "Content-Type": "application/json; charset=utf-8",
    };
    var options = {
      host: "onesignal.com",
      port: 443,
      path: "/api/v1/notifications",
      method: "POST",
      headers: headers,
    };
    var https = require("https");
    var req = https.request(options, function (res: any) {
      res.on("data", function (data: any) {
        console.log("Response:");
      });
    });

    req.on("error", function (e: any) {
      console.log("ERROR:");
      console.log(e);
    });
    req.write(JSON.stringify(data));
    req.end();
  };

  var message = {
    app_id: "db79b975-e551-4741-ae43-8968ceab5f09",
    headings: { en: title },
    android_channel_id: "cfc50b79-a76a-4114-8200-0b53d6d7bd30",
    contents: { en: `${textmessage}` },
    ios_badgeCount: 1,
    ios_badgeType: "Increase",
    include_player_ids: [IdOnesignal],
    ios_attachments: { id1: order ? order.storeData.image : "" },
    big_picture: order ? order.storeData.image : "",
    data: {
      navigate: true,
      router: "FallowOrder",
      data: { id: order ? order.channelOrderDisplayId : 0 },
    },
  };
  sendNotification(message);
};
