import userSchema from "../models/user";
import categoriaSchema from "../models/categorias";
import restaurantSchema from "../models/restaurant";
import favoritoSchema from "../models/Favorito";
import Cupones from "../models/cupones";
import { Types } from "mongoose";
import ridersSchema from "../models/riders";
import { STATUS_MESSAGES } from "./Status_messages";
import RatingSchema from "../models/rating";
import OpinionSchema from "../models/Opinion";
import pagoSchema from "../models/Pago";
import transSchema from "../models/transacciones";
import adressSchema from "../models/adress";
import adressStoreSchema from "../models/adressStore";
import tipoSchema from "../models/tipo";
import highkitchenCategorySchema from "../models/highkitchenCategory";
import tipotiendaSchema from "../models/tipotienda";
import userAdminSchema from "../models/userAdmin";
import postSchema from "../models/post";
import customorderSchema from "../models/custonorder";
import citySchema from "../models/cityclose";
import offerts from "../models/offerts";
import riderTransactionScheme from "../models/trasactionRider";
import collectionSchema from "../models/collections";
import SUBcollectionSchema from "../models/subcollection";
import adsSchema from "../models/ads";
import moment from "moment";
import coinsSchema from "../models/coins";

//newProducts

import MenusSchema from "../models/newMenu/Menu";
import bundlesSchema from "../models/newMenu/bundles";
import modifierGroupsSchema from "../models/newMenu/modifierGroups";
import productSchema from "../models/newMenu/products";
import addToCart from "../models/newOrder/addToCart";
import newOrderSchema from "../models/newOrder/order";
import { scheduleTime } from "../funtions/scheduleTime";

import dotenv from "dotenv";
import quincena from "../models/quincena";
import console from "console";
import modifiers from "../models/newMenu/modifiers";

const { ObjectId } = Types;

dotenv.config({ path: "variables.env" });

export const Query = {
  getRiderForAdmin: (root: any, { city }, { usuarioActual }) => {
    return new Promise((resolve, rejects) => {
      ridersSchema.find({ city: city }, (err, res) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: {},
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getRiderForAdminAll: (root: any, {}, { usuarioActual }) => {
    return new Promise((resolve, rejects) => {
      ridersSchema.find((err, res) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: {},
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getUsuario: (root: any, {}, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        success: false,
        messages: STATUS_MESSAGES.NOT_LOGGED_IN,
        data: {},
      };
    } else {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: usuarioActual._id }, (err, res) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        });
      });
    }
  },

  getUsuarioAdmin: (root: any, {}, { usuarioActual }) => {
    return new Promise((resolve, rejects) => {
      userAdminSchema.findOne(
        { _id: usuarioActual && usuarioActual._id },
        (err, res) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        }
      );
    });
  },

  getRiders: (root: any, { id }) => {
    if (!id) {
      return {
        success: false,
        messages: STATUS_MESSAGES.NOT_LOGGED_IN,
        data: {},
      };
    } else {
      return new Promise((resolve, rejects) => {
        ridersSchema.findOne({ _id: id }, (err, res) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        });
      });
    }
  },

  obtenerUsuario: (root: any, args: any, { usuarioActual }) => {
    if (!usuarioActual) {
      return null;
    }
    const usuario = userSchema.findOne({ _id: usuarioActual._id });

    return usuario;
  },

  obtenerAdmin: (root: any, args: any, { usuarioActual }) => {
    if (!usuarioActual) {
      return null;
    }
    const usuario = userAdminSchema.findOne({ _id: usuarioActual._id });

    return usuario;
  },

  getCategory: (root: any, { city }) => {
    return new Promise((resolve, rejects) => {
      categoriaSchema.find(
        {
          excludeCity: { $nin: [city] },
          visible: true,
        },
        (err, res) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            const categorys = res;
            categorys.sort((a, b) =>
              a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0
            );

            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: categorys,
            });
          }
        }
      );
    });
  },

  getCategoryLargue: (root: any, { city, limit }) => {
    return new Promise((resolve, rejects) => {
      categoriaSchema
        .find(
          {
            excludeCity: { $nin: [city] },
            visible: true,
            largue: true,
          },
          (err, res) => {
            if (err) {
              rejects({
                messages: STATUS_MESSAGES.S_WENT_WRONG,
                success: false,
                data: [],
              });
            } else {
              const categorys = res;
              categorys.sort((a, b) =>
                a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0
              );

              resolve({
                messages: STATUS_MESSAGES.DATA_SUCCESS,
                success: true,
                data: categorys,
              });
            }
          }
        )
        .limit(limit);
    });
  },

  getCategorySmall: (root: any, { city }) => {
    return new Promise((resolve, rejects) => {
      categoriaSchema.find(
        {
          excludeCity: { $nin: [city] },
          visible: true,
          largue: false,
        },
        (err, res) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            const categorys = res;
            categorys.sort((a, b) =>
              a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0
            );

            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: categorys,
            });
          }
        }
      );
    });
  },

  getTipo: (root: any) => {
    return new Promise((resolve, rejects) => {
      tipoSchema.find((err, res) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getHighkitchenCategory: (root: any) => {
    return new Promise((resolve, rejects) => {
      highkitchenCategorySchema.find((err, res) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getTipoTienda: (root: any) => {
    return new Promise((resolve, rejects) => {
      tipotiendaSchema.find((err, res) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getRestaurantHighkitchen: (root: any, { city, llevar, category }) => {
    let condition = {
      includeCity: { $all: [city] },
      highkitchen: true,
      open: true,
    };
    //@ts-ignore
    if (llevar) condition.llevar = llevar;
    // @ts-ignore
    if (category) condition.tipo = category;
    return new Promise((resolve, rejects) => {
      restaurantSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            const stores = res;
            stores.sort((a, b) => {
              return scheduleTime(a.schedule) === scheduleTime(b.schedule)
                ? 0
                : scheduleTime(a.schedule)
                ? -1
                : 1;
            });

            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: stores,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getRestaurant: (root: any, { city }) => {
    let condition = {
      includeCity: { $all: [city] },
    };

    return new Promise((resolve, rejects) => {
      restaurantSchema.find(condition, (err: any, res: any) => {
        if (err) {
          console.error(err);
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getAdress: (root: any, { id }) => {
    if (!id) {
      return {
        success: false,
        messages: "Aún no has iniciado sesión",
        data: null,
      };
    }
    return new Promise((resolve, rejects) => {
      adressSchema
        .find({ usuario: id }, (err: any, res: any) => {
          if (err) {
            rejects({
              success: false,
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              data: {},
            });
          } else {
            resolve({
              success: true,
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getAdressStore: (root: any, { id, city }) => {
    return new Promise((resolve, rejects) => {
      let condition = { store: id, ciudad: city };
      //@ts-ignore
      adressStoreSchema
        .findOne(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              data: {},
            });
          } else {
            resolve({
              success: true,
              message: STATUS_MESSAGES.DATA_SUCCESS,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getAdressStoreforSelect: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      let condition = { store: id };
      adressStoreSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              data: {},
            });
          } else {
            resolve({
              success: true,
              message: STATUS_MESSAGES.DATA_SUCCESS,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getRestaurantSearch: (
    root: any,
    { city, price, category, search, llevar, tipo }
  ) => {
    let condition = {
      city: city,
      open: true,
      $text: { $search: `"\"${search} \""` },
    };
    // @ts-ignore
    if (price) condition.minime = { $lt: price };
    //@ts-ignore
    if (llevar) condition.llevar = llevar;
    // @ts-ignore
    if (category) condition.categoryID = { $all: [category] };
    // @ts-ignore
    if (tipo) condition.tipo = { $all: [tipo] };

    return new Promise((resolve, rejects) => {
      restaurantSchema.find(condition, (err: any, res: any) => {
        if (err) {
          console.log(err);
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          const stores = res;
          stores.sort((a, b) => {
            return scheduleTime(a.schedule) === scheduleTime(b.schedule)
              ? 0
              : scheduleTime(a.schedule)
              ? -1
              : 1;
          });
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: stores,
          });
        }
      });
    });
  },

  getRestaurantSearchWeb: (
    root: any,
    { city, category, search, llevar, tipo, page }
  ) => {
    let condition = { includeCity: { $all: [city] }, open: true };
    // @ts-ignore
    if (search) condition = { $text: { $search: `"\"${search} \""` } };
    //@ts-ignore
    if (llevar) condition.llevar = llevar;
    // @ts-ignore
    if (category) condition.categoryID = { $all: [category] };
    // @ts-ignore
    if (tipo) condition.tipo = { $all: [tipo] };

    return new Promise((resolve, rejects) => {
      restaurantSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            console.log(err);
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            const stores = res;
            stores.sort((a, b) => {
              return scheduleTime(a.schedule) === scheduleTime(b.schedule)
                ? 0
                : scheduleTime(a.schedule)
                ? -1
                : 1;
            });
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: stores,
            });
          }
        })
        .sort(search ? null : { $natural: -1 })
        .limit(21 * 1)
        .skip((page - 1) * 21)
        .exec();
    });
  },

  getRestaurantSearchWebPAgination: (root: any, { city, category }) => {
    let condition = { includeCity: { $all: [city] }, open: true };
    // @ts-ignore
    if (category) condition.categoryID = { $all: [category] };

    return new Promise((resolve, rejects) => {
      restaurantSchema.find(condition, (err: any, res: any) => {
        if (err) {
          console.log(err);
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getRestaurantForCategory: (
    root: any,
    { city, category, tipo, llevar, search, page, limit, star, offert }
  ) => {
    return new Promise((resolve, rejects) => {
      let condition = { includeCity: { $all: [city] }, open: true };
      // @ts-ignore
      if (search) condition = { $text: { $search: `"\"${search} \""` } };
      //@ts-ignore
      if (llevar) condition.llevar = llevar;
      //@ts-ignore
      if (tipo) condition.tipo = { $all: [tipo] };
      // @ts-ignore
      if (category) condition.categoryID = { $all: [category] };
      // @ts-ignore
      if (star) condition.rating = star;
      // @ts-ignore
      if (offert) condition.inOffert = offert;

      restaurantSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            const stores = res;
            stores.sort((a, b) => {
              return scheduleTime(a.schedule) === scheduleTime(b.schedule)
                ? 0
                : scheduleTime(a.schedule)
                ? -1
                : 1;
            });

            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: stores,
            });
          }
        })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
    });
  },

  getNewStoreList: (
    root: any,
    { city, category, tipo, search }
  ) => {
    return new Promise((resolve, rejects) => {
      let condition = { includeCity: { $all: [city] }, open: true };
      // @ts-ignore
      if (search) condition = {includeCity: { $all: [city] }, open: true,  $text: { $search: `"\"${search} \""` } };
      //@ts-ignore
      if (tipo) condition.tipo = { $all: [tipo] };
      // @ts-ignore
      if (category) condition.categoryID = { $all: [category] };
      restaurantSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            const stores = res;
            stores.sort((a, b) => {
              return scheduleTime(a.schedule) === scheduleTime(b.schedule)
                ? 0
                : scheduleTime(a.schedule)
                ? -1
                : 1;
            });

            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: stores,
            });
          }
        })
    });
  },

  getRestaurantForID: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      restaurantSchema.findOne({ _id: id }, (err: any, res: any) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: {},
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getRestaurantForIDWeb: (root: any, { id, city }) => {
    return new Promise((resolve, rejects) => {
      restaurantSchema.findOne(
        { _id: id, includeCity: { $all: [city] } },
        (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        }
      );
    });
  },

  getRestaurantForDetails: (root: any, { slug, city }) => {
    return new Promise((resolve, rejects) => {
      restaurantSchema.findOne(
        { slug: slug, includeCity: { $all: [city] } },
        (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        }
      );
    });
  },

  getRestaurantForSlugWeb: (root: any, { slug }) => {
    return new Promise((resolve, rejects) => {
      restaurantSchema.findOne({ slug: slug }, (err: any, res: any) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: {},
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getRestaurantFavorito: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      favoritoSchema.find({ usuarioId: id }, (error: any, favourite: any) => {
        if (error) {
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        } else {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: favourite,
          });
        }
      });
    });
  },

  getStoreInOffert: (root: any, { city, page, limit }) => {
    return new Promise((resolve, rejects) => {
      let condition = {
        inOffert: true,
        includeCity: { $all: [city] },
        categoryID: { $all: ["5fb7a32cb234a46c09297804"] },
        open: true,
      };
      restaurantSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            const stores = res;
            stores.sort((a, b) => {
              return scheduleTime(a.schedule) === scheduleTime(b.schedule)
                ? 0
                : scheduleTime(a.schedule)
                ? -1
                : 1;
            });

            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: stores,
            });
          }
        })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getStorePromo: (root: any, { city, categoryID, page, limit }) => {
    return new Promise((resolve, rejects) => {
      let condition = {
        includeCity: { $all: [city] },
        categoryID: { $all: [categoryID] },
        tagOffert: { $ne: null },
        open: true,
      };
      restaurantSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            const stores = res;
            stores.sort((a, b) => {
              return scheduleTime(a.schedule) === scheduleTime(b.schedule)
                ? 0
                : scheduleTime(a.schedule)
                ? -1
                : 1;
            });

            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: stores,
            });
          }
        })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getStoreNew: (root: any, { city, page, limit }) => {
    return new Promise((resolve, rejects) => {
      let condition = {
        isnew: true,
        includeCity: { $all: [city] },
        open: true,
      };
      restaurantSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            const stores = res;
            stores.sort((a, b) => {
              return scheduleTime(a.schedule) === scheduleTime(b.schedule)
                ? 0
                : scheduleTime(a.schedule)
                ? -1
                : 1;
            });
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: stores,
            });
          }
        })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getFarmacyOffert: (root: any, { city }) => {
    return new Promise((resolve, rejects) => {
      let condition = {
        inOffert: true,
        includeCity: { $all: [city] },
        categoryID: { $all: ["5fb7aeaab234a46c0929780a"] },
        open: true,
      };
      restaurantSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            const stores = res;
            stores.sort((a, b) => {
              return scheduleTime(a.schedule) === scheduleTime(b.schedule)
                ? 0
                : scheduleTime(a.schedule)
                ? -1
                : 1;
            });

            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: stores,
            });
          }
        })
        .limit(2)
        .sort({ $natural: -1 });
    });
  },

  getSalvingPackInOffert: (root: any, { city }) => {
    return new Promise((resolve, rejects) => {
      let condition = {
        "salvingPack.isSavingPack": true,
        includeCity: { $all: [city] },
        open: true,
      };
      restaurantSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            const stores = res;
            stores.sort((a, b) => {
              return scheduleTime(a.schedule) === scheduleTime(b.schedule)
                ? 0
                : scheduleTime(a.schedule)
                ? -1
                : 1;
            });

            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: stores,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getTiendaInOffert: (root: any, { city, page, limit }) => {
    return new Promise((resolve, rejects) => {
      let condition = {
        inOffert: true,
        includeCity: { $all: [city] },
        categoryID: { $all: ["5fb7aec4b234a46c0929780b"] },
        open: true,
      };
      restaurantSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            const stores = res;
            stores.sort((a, b) => {
              return scheduleTime(a.schedule) === scheduleTime(b.schedule)
                ? 0
                : scheduleTime(a.schedule)
                ? -1
                : 1;
            });

            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: stores,
            });
          }
        })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getRestaurantforEdit: (root: any, { city, search, page, limit }) => {
    let condition = {
      city: city,
    };
    // @ts-ignore
    if (search)
      condition = {
        city: city,
        //@ts-ignore
        $text: { $search: `"\"${search} \""` },
      };

    return new Promise((resolve, rejects) => {
      restaurantSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .sort(search ? null : { $natural: -1 })
        .exec();
    });
  },

  getCupon: async (root: any, { clave }, { usuarioActual }) => {
    try {
      if (!usuarioActual) {
        return {
          success: false,
          message: "Debe iniciar sesión para continuar",
          data: null,
        };
      }
      const usuario = await userSchema.findOne({
        _id: usuarioActual._id,
      });
      if (!usuario) {
        return {
          success: false,
          message: "Debe iniciar sesión para continuar",
          data: null,
        };
      }
      return new Promise((resolve, reject) => {
        Cupones.findOne({ clave }, (error, cupon) => {
          if (error) {
            return reject(error);
          } else {
            return resolve(cupon);
          }
        });
      });
    } catch (error) {
      return {
        success: false,
        message: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getCuponAll: async (root: any) => {
    try {
      return new Promise((resolve, reject) => {
        Cupones.findOne((error: any, cupon: any) => {
          if (error) {
            return reject(error);
          } else {
            return resolve(cupon);
          }
        });
      });
    } catch (error) {
      return {
        success: false,
        message: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getValoraciones: async (root: any, { restaurant }) => {
    try {
      return new Promise((resolve, reject) => {
        RatingSchema.find(
          { restaurant: restaurant },
          (error: any, valoracion: any) => {
            if (error) {
              return reject(error);
            } else {
              resolve({
                messages: "Datos Obtenidos con éxito",
                success: true,
                data: valoracion,
              });
            }
          }
        )
          .limit(100)
          .sort({ $natural: -1 });
      });
    } catch (error) {
      return {
        success: false,
        message: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getValoracionesStore: async (root: any, { id, page, limit }) => {
    try {
      return new Promise((resolve, reject) => {
        RatingSchema.find({ restaurant: id }, (error: any, valoracion: any) => {
          if (error) {
            return reject(error);
          } else {
            resolve({
              messages: "Datos Obtenidos con éxito",
              success: true,
              data: valoracion,
            });
          }
        })
          .limit(limit * 1)
          .skip((page - 1) * 20)
          .sort({ $natural: -1 })
          .exec();
      });
    } catch (error) {
      return {
        success: false,
        message: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getOpinion: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      OpinionSchema.find({ plato: id }, (err, res) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      }).sort({ $natural: -1 });
    });
  },

  getPago: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      pagoSchema.findOne({ restaurantID: id }, (error: any, pagos: any) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
            data: null,
          });
        else
          resolve({
            messages: "Operacion realizada con éxito",
            success: true,
            data: pagos,
          });
      });
    });
  },

  getTransaction: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      transSchema
        .find({ restaurantID: id })
        .sort({ $natural: -1 })
        .exec((error, deposito) => {
          if (error)
            reject({
              messages: "Hay un problema con su solicitud",
              success: false,
              list: [],
            });
          else
            resolve({
              messages: "solicitud procesada con éxito",
              success: true,
              list: deposito,
            });
        });
    });
  },

  getStatistics: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      let matchQuery1 = {
        restaurant: id,
        estado: {
          $in: ["Entregada", "Valorada"],
        },
      };

      let matchQuery2 = {
        restaurant: id,
        estado: "Rechazada",
      };

      newOrderSchema
        .aggregate([
          { $match: matchQuery1 },
          {
            $group: {
              _id: {
                month: { $substr: ["$created_at", 5, 2] },
              },
              paypalAmount: { $first: "$total" },
              stripeAmount: { $first: "$total" },
              finishedCount: { $sum: 1 },
              created_at: { $first: "$created_at" },
            },
          },
          {
            $project: {
              your_year_variable: { $year: "$created_at" },
              paypalAmount: 1,
              finishedCount: 1,
              stripeAmountDivide: { $divide: [Number("$stripeAmount"), 100] },
            },
          },
          { $match: { your_year_variable: 2021 } },
        ])
        .then((res) => {
          let ordenes = {
            name: "Pedidos",
            Ene: 0,
            Feb: 0,
            Mar: 0,
            Abr: 0,
            May: 0,
            Jun: 0,
            Jul: 0,
            Aug: 0,
            Sep: 0,
            Oct: 0,
            Nov: 0,
            Dic: 0,
          };
          let ganacias = {
            name: "Total Ventas",
            Ene: 0,
            Feb: 0,
            Mar: 0,
            Abr: 0,
            May: 0,
            Jun: 0,
            Jul: 0,
            Aug: 0,
            Sep: 0,
            Oct: 0,
            Nov: 0,
            Dic: 0,
          };
          let devoluciones = {
            name: "Pedidos rechazados",
            Ene: 0,
            Feb: 0,
            Mar: 0,
            Abr: 0,
            May: 0,
            Jun: 0,
            Jul: 0,
            Aug: 0,
            Sep: 0,
            Oct: 0,
            Nov: 0,
            Dic: 0,
          };

          for (let i = 0; i < res.length; i++) {
            let month = res[i]._id.month;
            if (month == "01") {
              ganacias["Ene"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Ene"] = res[i].finishedCount;
            } else if (month == "02") {
              ganacias["Feb"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Feb"] = res[i].finishedCount;
            } else if (month == "03") {
              ganacias["Mar"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Mar"] = res[i].finishedCount;
            } else if (month == "04") {
              ganacias["Abr"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Abr"] = res[i].finishedCount;
            } else if (month == "05") {
              ganacias["May"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["May"] = res[i].finishedCount;
            } else if (month == "06") {
              ganacias["Jun"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Jun"] = res[i].finishedCount;
            } else if (month == "07") {
              ganacias["Jul"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Jul"] = res[i].finishedCount;
            } else if (month == "08") {
              ganacias["Aug"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Aug"] = res[i].finishedCount;
            } else if (month == "09") {
              ganacias["Sep"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Sep"] = res[i].finishedCount;
            } else if (month == "10") {
              ganacias["Oct"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Oct"] = res[i].finishedCount;
            } else if (month == "11") {
              ganacias["Nov"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Nov"] = res[i].finishedCount;
            } else if (month == "12") {
              ganacias["Dic"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Dic"] = res[i].finishedCount;
            }
          }

          newOrderSchema
            .aggregate([
              { $match: matchQuery2 },
              {
                $group: {
                  _id: {
                    month: { $substr: ["$created_at", 5, 2] },
                    estado: "Rechazada",
                  },

                  returnedCount: { $sum: 1 },
                  created_at: { $first: "$created_at" },
                },
              },
              {
                $project: {
                  your_year_variable: { $year: "$created_at" },
                  returnedCount: 1,
                },
              },
              { $match: { your_year_variable: 2021 } },
            ])
            .then((res1) => {
              for (let i = 0; i < res1.length; i++) {
                let month = res1[i]._id.month;
                if (month == "01") {
                  devoluciones["Ene"] = res1[i].returnedCount;
                } else if (month == "02") {
                  devoluciones["Feb"] = res1[i].returnedCount;
                } else if (month == "03") {
                  devoluciones["Mar"] = res1[i].returnedCount;
                } else if (month == "04") {
                  devoluciones["Abr"] = res1[i].returnedCount;
                } else if (month == "05") {
                  devoluciones["May"] = res1[i].returnedCount;
                } else if (month == "06") {
                  devoluciones["Jun"] = res1[i].returnedCount;
                } else if (month == "07") {
                  devoluciones["Jul"] = res1[i].returnedCount;
                } else if (month == "08") {
                  devoluciones["Aug"] = res1[i].returnedCount;
                } else if (month == "09") {
                  devoluciones["Sep"] = res1[i].returnedCount;
                } else if (month == "10") {
                  devoluciones["Oct"] = res1[i].returnedCount;
                } else if (month == "11") {
                  devoluciones["Nov"] = res1[i].returnedCount;
                } else if (month == "12") {
                  devoluciones["Dic"] = res1[i].returnedCount;
                }
              }
              resolve({
                success: true,
                message: "",
                data: [ordenes, ganacias, devoluciones],
              });
            });
        })
        .catch((err) => {});
    });
  },

  getPost: async (root: any, { country }) => {
    return new Promise((resolve, reject) => {
      postSchema
        .findOne({ country: country }, (error: any, post: any) => {
          if (error)
            reject({
              success: false,
              message: "Hay un problema con su solicitud",
              data: null,
            });
          else
            resolve({
              success: true,
              message: "Operacion realizada con éxito",
              data: post,
            });
        })
        .sort({ $natural: -1 });
    });
  },

  getPosts: async (root: any, { country, page }) => {
    return new Promise((resolve, reject) => {
      postSchema
        .find({ country: country }, (error: any, post: any) => {
          if (error)
            reject({
              success: false,
              message: "Hay un problema con su solicitud",
              data: null,
            });
          else
            resolve({
              success: true,
              message: "Operacion realizada con éxito",
              data: post,
            });
        })
        .limit(20 * 1)
        .skip((page - 1) * 20)
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getPostsForTags: async (root: any, { country, page, tag }) => {
    return new Promise((resolve, reject) => {
      postSchema
        .find(
          { country: country, tags: { $all: [tag] } },
          (error: any, post: any) => {
            if (error)
              reject({
                success: false,
                message: "Hay un problema con su solicitud",
                data: null,
              });
            else
              resolve({
                success: true,
                message: "Operacion realizada con éxito",
                data: post,
              });
          }
        )
        .limit(20 * 1)
        .skip((page - 1) * 20)
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getPostbyId: async (root: any, { slug, country }) => {
    return new Promise((resolve, reject) => {
      postSchema.findOne(
        { slug: slug, country: country },
        (error: any, post: any) => {
          if (error)
            reject({
              success: false,
              message: "Hay un problema con su solicitud",
              data: null,
            });
          else
            resolve({
              success: true,
              message: "Operacion realizada con éxito",
              data: post,
            });
        }
      );
    });
  },

  getCustomOrder: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        userID: id,
        estado: {
          $in: [
            "Pagado",
            "Asignada",
            "Aceptada",
            "En camino",
            "Entregado",
            "Rechazada por el rider",
            "Rechazada",
            "Valorada",
          ],
        },
      };

      customorderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              messages: "Hay un problema con su solicitud",
              data: null,
            });
          else
            resolve({
              success: true,
              messages: "Operacion realizada con éxito",
              data: order,
            });
        })
        .sort({ $natural: -1 });
    });
  },

  getCustomOrderRider: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        riders: id,
        estado: {
          $in: [
            "Pagado",
            "Asignada",
            "Aceptada",
            "En camino",
            "Entregado",
            "Valorada",
          ],
        },
      };

      customorderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              messages: "Hay un problema con su solicitud",
              data: null,
            });
          else
            resolve({
              success: true,
              messages: "Operacion realizada con éxito",
              data: order,
            });
        })
        .sort({ $natural: -1 });
    });
  },

  getCustomOrderRiderID: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      customorderSchema.findOne({ _id: id }, (error: any, order: any) => {
        if (error)
          reject({
            success: false,
            messages: "Hay un problema con su solicitud",
            data: null,
          });
        else
          resolve({
            success: true,
            messages: "Operacion realizada con éxito",
            data: order,
          });
      });
    });
  },

  getCustomOrderAdmin: async (
    root: any,
    { city, orderID, dateRange, limit, page },
    { usuarioActual }
  ) => {
    return new Promise((resolve, reject) => {
      let condition = {
        estado: { $ne: "Pendiente de pago" },
      };
      // @ts-ignore
      if (city) condition.city = city;

      if (orderID)
        // @ts-ignore
        condition.display_id = orderID;

      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      customorderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              messages: "Hay un problema con su solicitud",
              data: null,
            });
          else
            resolve({
              success: true,
              messages: "Operacion realizada con éxito",
              data: order,
            });
        })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .sort({ $natural: -1 });
    });
  },

  getCity: async (root: any, { city }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        city: city,
      };
      citySchema.findOne(condition, (error: any, city: any) => {
        if (error)
          reject({
            success: false,
            messages: "Aun no hemos llegado a esta zona",
            data: null,
          });
        else
          resolve({
            success: true,
            messages: "Ciudad operativa",
            data: city,
          });
      });
    });
  },

  getOfferts: async (root: any, { city }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        city: city,
      };
      offerts.find(condition, (error: any, city: any) => {
        if (error)
          reject({
            success: false,
            messages: "Aun no tenemos ofertas en esta zona",
            data: null,
          });
        else
          resolve({
            success: true,
            messages: "Ofertas obtenidas con éxito",
            data: city,
          });
      });
    });
  },

  getCustomers: async (
    root: any,
    { page, limit, email },
    { usuarioActual }
  ) => {
    let condition = {};
    // @ts-ignore
    if (email) condition.email = email;

    var value = await userSchema.countDocuments();

    return new Promise((resolve, reject) => {
      userSchema
        .find(condition, (error: any, user: any) => {
          if (error)
            reject({
              success: false,
              messages: "Hay un problema con su solicitud",
              count: 0,
              data: null,
            });
          else
            resolve({
              success: true,
              messages: "Operacion realizada con éxito",
              count: value,
              data: user,
            });
        })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getAllCustomers: async (root: any, { email }, { usuarioActual }) => {
    let condition = {};
    // @ts-ignore
    if (email) condition.email = email;
    return new Promise((resolve, reject) => {
      userSchema.find(condition, (error: any, user: any) => {
        if (error)
          reject({
            success: false,
            messages: "Hay un problema con su solicitud",
            data: null,
          });
        else
          resolve({
            success: true,
            messages: "Operacion realizada con éxito",
            data: user,
          });
      });
    });
  },

  getRiderTransaction: async (root: any, { id, dateRange, page }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        rider: id,
        created_at: {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        },
      };
      riderTransactionScheme
        .find(condition, (error: any, list: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: list,
            });
          }
        })
        .limit(20 * 1)
        .skip((page - 1) * 20)
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getQuincena: async (root: any) => {
    return new Promise((resolve, reject) => {
      quincena
        .findOne((error: any, quincena: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              data: {},
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              data: quincena,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getQuincenaAll: async (root: any) => {
    return new Promise((resolve, reject) => {
      quincena
        .find((error: any, quincena: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: quincena,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getCollection: async (root: any, { store }) => {
    return new Promise((resolve, reject) => {
      collectionSchema.find(
        { store: { $all: [store] } },
        (error: any, collection: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              data: [],
            });
          else {
            const collections = collection;
            collections.sort((a, b) =>
              a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0
            );
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              data: collections,
            });
          }
        }
      );
    });
  },

  getSubCollection: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      SUBcollectionSchema.find(
        { collectiontype: id },
        (error: any, collection: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              data: [],
            });
          else {
            const collections = collection;
            collections.sort((a, b) =>
              a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0
            );
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              data: collections,
            });
          }
        }
      );
    });
  },

  getProductSubCollection: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      productSchema
        .find({ parentId: id }, (error: any, product: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              data: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              data: product,
            });
          }
        })
        .limit(3);
    });
  },

  getNewMenu: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      MenusSchema.find({ storeId: id }, (error: any, menu: any) => {
        if (error) {
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        } else {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: menu,
          });
        }
      });
    });
  },

  getNewProductoSearch: (
    root: any,
    { store, search, page, limit, products }
  ) => {
    let condition = { snoozed: false, isBundled: false };
    // @ts-ignore
    if (products.length > 0) condition._id = products.map((p: string) => p);
    // @ts-ignore
    if (search) condition = { $text: { $search: `"\"${search} \""` } };
    // @ts-ignore
    if (store) condition.storeId = { $all: [store] };

    return new Promise((resolve, rejects) => {
      productSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
    });
  },

  getNewProducto: (root: any, { store, search }) => {
    return new Promise((resolve, rejects) => {
      let condition = { storeId: store, snoozed: false, isBundled: false };
      // @ts-ignore
      if (search) condition = { $text: { $search: `"\"${search} \""` } };

      productSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .sort(search ? null : { $natural: -1 })
        .exec();
    });
  },

  getNewProductoSearchStore: (root: any, { store, search }) => {
    return new Promise((resolve, rejects) => {
      let condition = {
        storeId: store,
        $text: { $search: `"\"${search} \""` },
      };
      productSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .exec();
    });
  },

  getNewProductoaAllStore: (root: any, { store }) => {
    return new Promise((resolve, rejects) => {
      let condition = {
        storeId: store,
        isBundled: false,
      };
      productSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getNewProductoBundled: (root: any, { store }) => {
    return new Promise((resolve, rejects) => {
      let condition = { storeId: store, isBundled: true };
      productSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getBundledStore: (root: any, { storeID }) => {
    return new Promise((resolve, rejects) => {
      let condition = { account: storeID };
      bundlesSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getNewProductoModifield: (root: any, { store }) => {
    return new Promise((resolve, rejects) => {
      let condition = { account: store };
      modifiers
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getModifiedGroupStore: (root: any, { storeID }) => {
    return new Promise((resolve, rejects) => {
      let condition = { account: storeID };
      modifierGroupsSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getNewCart: (root: any, { userId, storeId }) => {
    return new Promise((resolve, reject) => {
      addToCart.find(
        { userId: userId, storeId: storeId },
        (error: any, cart: any) => {
          if (error) {
            reject({
              success: false,
              message: "Hay un problema con su solicitud",
              list: [],
            });
          } else {
            resolve({
              success: true,
              message: "Operación realizada con éxito",
              list: cart,
            });
          }
        }
      );
    });
  },

  getNewOrder: async (
    root: any,
    { userId, status, dateRange},
    { usuarioActual }
  ) => {
    return new Promise((resolve, reject) => {
      let condition = {
        customer: userId,
        status: {
          $in: status,
        },
      };
      //let condition = { cliente: "5d8556514c10b81c6a65295a" };
      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      newOrderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Ordernes extraida con éxito",
              list: order,
            });
          }
        })
        .sort({ $natural: -1 })
        .limit(10 * 1)
        .skip((1 - 1) * 10)
      
    });
  },

  getNewOrderforStore: async (
    root: any,
    { storeId, status, search, dateRange, page, limit }
  ) => {
    return new Promise((resolve, reject) => {
      let condition = {
        store: storeId,
        status: {
          $in: status,
        },
      };

      // @ts-ignore
      if (search) condition.channelOrderDisplayId = search;

      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      newOrderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Ordernes extraida con éxito",
              list: order,
            });
          }
        })
        .limit(10 * 1)
        .skip((1 - 1) * 10)
        .sort({ $natural: -1 });
    });
  },

  getNewOrderforStoreWeb: async (
    root: any,
    { storeId, status, search, dateRange, page, limit }
  ) => {

    var value = await newOrderSchema.countDocuments({store: storeId});

    return new Promise((resolve, reject) => {
      let condition = {
        store: storeId,
        status: {
          $in: status,
        },
      };

      // @ts-ignore
      if (search) condition.channelOrderDisplayId = search;

      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      newOrderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              total: value,
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Ordernes extraida con éxito",
              total: value,
              list: order,
            });
          }
        })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .sort({ $natural: -1 });
    });
  },

  getNewOrderRider: async (
    root: any,
    { riderId, status, dateRange, page },
    { usuarioActual }
  ) => {
    return new Promise((resolve, reject) => {
      let condition = {
        courier: riderId,
        status: {
          $in: status,
        },
      };
      //let condition = { cliente: "5d8556514c10b81c6a65295a" };
      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      newOrderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Ordernes extraida con éxito",
              list: order,
            });
          }
        })
        .limit(20 * 1)
        .skip((page - 1) * 20)
        .sort({ $natural: -1 });
    });
  },

  getNewOrderBugetRider: async (root: any, { riderId }, { usuarioActual }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        budgetRider: { $all: [riderId] },
      };
      newOrderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Ordernes extraida con éxito",
              list: order,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getNewOrderforId: async (root: any, { id }, { usuarioActual }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        channelOrderDisplayId: id,
      };
      newOrderSchema.findOne(condition, (error: any, order: any) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: [],
          });
        else {
          resolve({
            success: true,
            message: "Ordernes extraida con éxito",
            data: order,
          });
        }
      });
    });
  },

  getNewOrderAdimin: async (
    root: any,
    { status, dateRange, page, limit, orderID, Stores, city, rider },
    { usuarioActual }
  ) => {
    var value = await newOrderSchema.countDocuments();

    return new Promise((resolve, reject) => {
      let condition = {
        status: {
          $in: status,
        },
      };

      if (city) {
        condition = {
          // @ts-ignore
          "storeData.city": city,
          status: {
            $in: status,
          },
        };
      }

      if (orderID)
        // @ts-ignore
        condition.channelOrderDisplayId = orderID;

      // @ts-ignore
      if (Stores) condition.store = { $in: Stores };

      // @ts-ignore
      if (rider.length > 0) condition.courier = { $in: rider };

      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        const toDate = moment(dateRange.toDate)
          .add(1, "day")
          .format("MM/DD/YYYY");

        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: toDate,
        };
      }

      if (dateRange && dateRange.fromDate && !dateRange.toDate) {
        const toDate = moment(dateRange.fromDate)
          .add(1, "day")
          .format("MM/DD/YYYY");

        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lt: toDate,
        };
      }

      newOrderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              count: 0,
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Ordernes extraida con éxito",
              count: value,
              list: order,
            });
          }
        })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .sort({ $natural: -1 });
    });
  },

  getProducto: (root: any, { id, page, limit }) => {
    let condition = { parentId: id, snoozed: false };
    return new Promise((resolve, rejects) => {
      productSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
    });
  },

  getProductoForCategory: (root: any, { category }) => {
    let condition = { parentId: category, snoozed: false };
    return new Promise((resolve, rejects) => {
      productSchema.find(condition, (err: any, res: any) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getPrivatePromocode: (root: any, { id }) => {
    let condition = { user: id };
    return new Promise((resolve, rejects) => {
      Cupones.find(condition, (err: any, res: any) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getPromocode: (root: any, { city }) => {
    let condition = { private: false };
    // @ts-ignore
    if (city) condition.city = { $all: [city] };

    return new Promise((resolve, rejects) => {
      Cupones.find(condition, (err: any, res: any) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getNewProductoForApp: async (root: any, { products, storeID }) => {
    const ifOrdatic = await restaurantSchema.findOne({
      _id: storeID,
      isBundled: false,
    });
    return new Promise((resolve, rejects) => {
      if (ifOrdatic?.isOrdaticPartner) {
        productSchema.find(
          {
            internalId: products.map((p: string) => {
              return p;
            }),
            snoozed: false,
          },
          (error, product) => {
            if (error) {
              rejects({
                messages: "Algo salio mal",
                success: false,
                data: [],
              });
            } else {
              if (!product)
                resolve({
                  messages: "Algo salio mal",
                  success: false,
                  data: [],
                });
              else {
                resolve({
                  messages: "Productos obtenidos",
                  success: true,
                  data: product,
                });
              }
            }
          }
        );
      } else {
        productSchema.find(
          {
            _id: products.map((p: string) => {
              return p;
            }),
            snoozed: false,
          },
          (error, product) => {
            if (error) {
              rejects({
                messages: "Algo salio mal",
                success: false,
                data: [],
              });
            } else {
              if (!product)
                resolve({
                  messages: "Algo salio mal",
                  success: false,
                  data: [],
                });
              else {
                resolve({
                  messages: "Productos obtenidos",
                  success: true,
                  data: product,
                });
              }
            }
          }
        );
      }
    });
  },

  getBundled: async (root: any, { products, storeID }) => {
    const storeType = await restaurantSchema.findOne({ _id: storeID });
    return new Promise((resolve, rejects) => {
      if (storeType?.isOrdaticPartner) {
        bundlesSchema.find(
          {
            internalId: products.map((p: string) => {
              return p;
            }),
            snoozed: false,
          },
          (error, product) => {
            if (error) {
              rejects({
                messages: "Algo salio mal",
                success: false,
                data: [],
              });
            } else {
              if (!product)
                resolve({
                  messages: "Algo salio mal",
                  success: false,
                  data: [],
                });
              else {
                resolve({
                  messages: "Productos obtenidos",
                  success: true,
                  data: product,
                });
              }
            }
          }
        );
      } else {
        bundlesSchema.find(
          {
            _id: products.map((p: string) => {
              return p;
            }),
            snoozed: false,
          },
          (error, product) => {
            if (error) {
              rejects({
                messages: "Algo salio mal",
                success: false,
                data: [],
              });
            } else {
              if (!product)
                resolve({
                  messages: "Algo salio mal",
                  success: false,
                  data: [],
                });
              else {
                resolve({
                  messages: "Productos obtenidos",
                  success: true,
                  data: product,
                });
              }
            }
          }
        );
      }
    });
  },

  getModifieldGroup: async (root: any, { products, storeID }) => {
    const storeType = await restaurantSchema.findOne({ _id: storeID });
    return new Promise((resolve, rejects) => {
      if (storeType?.isOrdaticPartner) {
        modifierGroupsSchema.find(
          {
            internalId: products.map((p: string) => {
              return p;
            }),
            snoozed: false,
          },
          (error, product) => {
            if (error) {
              rejects({
                messages: "Algo salio mal",
                success: false,
                data: [],
              });
            } else {
              if (!product)
                resolve({
                  messages: "Algo salio mal",
                  success: false,
                  data: [],
                });
              else {
                resolve({
                  messages: "Productos obtenidos",
                  success: true,
                  data: product,
                });
              }
            }
          }
        );
      } else {
        modifierGroupsSchema.find(
          {
            _id: products.map((p: string) => {
              return p;
            }),
            snoozed: false,
          },
          (error, product) => {
            if (error) {
              rejects({
                messages: "Algo salio mal",
                success: false,
                data: [],
              });
            } else {
              if (!product)
                resolve({
                  messages: "Algo salio mal",
                  success: false,
                  data: [],
                });
              else {
                resolve({
                  messages: "Productos obtenidos",
                  success: true,
                  data: product,
                });
              }
            }
          }
        );
      }
    });
  },

  getNewProductoForStore: async (root: any, { products, storeID }) => {
    const ifOrdatic = await restaurantSchema.findOne({
      _id: storeID,
      isBundled: false,
    });
    return new Promise((resolve, rejects) => {
      if (ifOrdatic?.isOrdaticPartner) {
        productSchema.find(
          {
            internalId: products.map((p: string) => {
              return p;
            }),
          },
          (error, product) => {
            if (error) {
              rejects({
                messages: "Algo salio mal",
                success: false,
                data: [],
              });
            } else {
              if (!product)
                resolve({
                  messages: "Algo salio mal",
                  success: false,
                  data: [],
                });
              else {
                resolve({
                  messages: "Productos obtenidos",
                  success: true,
                  data: product,
                });
              }
            }
          }
        );
      } else {
        productSchema.find(
          {
            _id: products.map((p: string) => {
              return p;
            }),
          },
          (error, product) => {
            if (error) {
              rejects({
                messages: "Algo salio mal",
                success: false,
                data: [],
              });
            } else {
              if (!product)
                resolve({
                  messages: "Algo salio mal",
                  success: false,
                  data: [],
                });
              else {
                resolve({
                  messages: "Productos obtenidos",
                  success: true,
                  data: product,
                });
              }
            }
          }
        );
      }
    });
  },

  getAds: (root: any, { city }) => {
    let matchQuery1 = {
      includeCity: { $all: [city] },
    };
    return new Promise((resolve, rejects) => {
      adsSchema.aggregate(
        [{ $match: matchQuery1 }, { $sample: { size: 150 } }],
        (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        }
      );
    });
  },

  getAleatoryProduct: (root: any, { store }) => {
    let matchQuery1 = {
      account: store,
      snoozed: false, 
      isBundled: false,
    };
    return new Promise((resolve, rejects) => {
      productSchema.aggregate(
        [{ $match: matchQuery1 }, { $sample: { size: 6 } }],
        (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        }
      );
    });
  },

  getNewOrderRepet: async (root: any, { userId, city }, { usuarioActual }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        customer: userId,
        "storeData.includeCity": { $all: [city] },
        status: { $in: ["Entregada", "Finalizada", "Devuelto"] },
      };
      newOrderSchema
        .find(condition, (error: any, order) => {
          if (error) {
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          } else {
            var hash = {};
            order = order.filter(function (current) {
              var exists = !hash[current.store];
              hash[current.store] = true;
              return exists;
            });

            resolve({
              success: true,
              message: "Ordernes extraida con éxito",
              list: order,
            });
          }
        })
        .sort({ $natural: -1 })
        .limit(20 * 1)
        .skip((1 - 1) * 20)
        .exec();
    });
  },

  getAllCoins: (root: any, { id, page, limit }) => {
    let condition = { user: id };
    return new Promise((resolve, rejects) => {
      coinsSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              message: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              message: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
    });
  },
};
