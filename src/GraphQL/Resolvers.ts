import { Query } from "./query";
import { Mutation } from "./mutation";
import userSchema from "../models/user";
import favoritoSchema from "../models/Favorito";
import restaurantSchema from "../models/restaurant";
import orderSchema from "../models/newOrder/order";
import ridersSchema from "../models/riders";
import userAdminSchema from "../models/userAdmin";
import subCollection from "../models/subcollection";
import categorySchema from "../models/newMenu/category";
import modifierGroupsSchema from "../models/newMenu/modifierGroups";
import modifiersSchema from "../models/newMenu/modifiers";
import productSchema from "../models/newMenu/products";

import { scheduleTime } from "../funtions/scheduleTime";

export const resolvers = {
  Query,
  Mutation,

  NewOrderRepeat: {
    Restaurant(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        restaurantSchema.findOne(
          { _id: parent.store, open: true },
          (error, store) => {
            if (error) {
              rejects(error);
            } else {
              if (!store) resolve(null);
              else {
                resolve(store);
              }
            }
          }
        );
      });
    },
  },

  Usuario: {
    numberCurrentOrder(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        orderSchema.find(
          {
            courier: parent._id,
            status: {
              $in: [
                "Nueva",
                "Confirmada",
                "En la cocina",
                "Listo para recoger",
                "Preparando para el envío",
                "En camino",
              ],
            },
          },
          (error, order) => {
            if (error) {
              rejects(error);
            } else {
              if (!order) resolve(0);
              else {
                resolve(order.length);
              }
            }
          }
        );
      });
    },
  },

  Cupon: {
    Store(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        restaurantSchema.findOne({ _id: parent.store }, (error, store) => {
          if (error) {
            rejects(error);
          } else {
            if (!store) resolve(null);
            else {
              resolve(store);
            }
          }
        });
      });
    },

    Usuario(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.user }, (error, user) => {
          if (error) {
            rejects(error);
          } else {
            if (!user) resolve(null);
            else {
              resolve(user);
            }
          }
        });
      });
    },

    canjeado(parent: any, args: any, { usuarioActual }) {
      return new Promise((resolve, rejects) => {
        orderSchema.findOne(
          {
            customer: usuarioActual ? usuarioActual._id : null,
            cupon: parent.id,
            status: { $ne: "Pendiente de pago" },
          },
          (error, user) => {
            if (error) {
              rejects(error);
            } else {
              if (!user) resolve(false);
              else {
                resolve(true);
              }
            }
          }
        );
      });
    },
    vencido(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        if (parent.exprirable) {
          if (parent.expire < new Date()) {
            resolve(true);
          } else {
            resolve(false);
          }
        } else {
          resolve(false);
        }
      });
    },
  },

  Menus: {
    Categories(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        categorySchema.find({ menu: parent._id }, (error, category) => {
          if (error) {
            rejects(error);
          } else {
            if (!category) resolve([]);
            else {
              const categorys = category;
              categorys.sort((a, b) =>
                a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0
              );
              resolve(categorys);
            }
          }
        });
      });
    },
  },

  CategoryNew: {
    async Products(parent: any, args: any) {
      const storeType = await restaurantSchema.findOne({ _id: parent.storeId });
      return new Promise((resolve, rejects) => {
        if (storeType?.isOrdaticPartner) {
          productSchema.find(
            {
              internalId: parent.products.map((p: string) => {
                return p;
              }),
              snoozed: false,
            },
            (error, product) => {
              if (error) {
                rejects(error);
              } else {
                if (!product) resolve([]);
                else {
                  resolve(product);
                }
              }
            }
          );
        } else {
          productSchema.find(
            {
              _id: parent.products.map((p: string) => {
                return p;
              }),
              snoozed: false,
            },
            (error, product) => {
              if (error) {
                rejects(error);
              } else {
                if (!product) resolve([]);
                else {
                  resolve(product);
                }
              }
            }
          );
        }
      });
    },
  },

  Bundles: {
    async Products(parent: any, args: any) {
      const storeType = await restaurantSchema.findOne({ _id: parent.account });
      return new Promise((resolve, rejects) => {
        if (storeType?.isOrdaticPartner) {
          productSchema.find(
            {
              internalId: parent.posProductCategoryId.map((p: string) => {
                return p;
              }),
            },
            (error, product) => {
              if (error) {
                rejects(error);
              } else {
                if (!product) resolve([]);
                else {
                  resolve(product);
                }
              }
            }
          );
        } else {
          productSchema.find(
            {
              _id: parent.subProducts.map((p: string) => {
                return p;
              }),
            },
            (error, product) => {
              if (error) {
                rejects(error);
              } else {
                if (!product) resolve([]);
                else {
                  resolve(product);
                }
              }
            }
          );
        }
      });
    },
  },

  Modifiers: {
    ModifierGroups(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        modifierGroupsSchema.find(
          {
            _id: parent.subProducts.map((p: string) => {
              return p;
            }),
          },
          (error, product) => {
            if (error) {
              rejects(error);
            } else {
              if (!product) resolve([]);
              else {
                resolve(product);
              }
            }
          }
        );
      });
    },
  },

  Products: {
    async isOpen(parent: any, args: any) {
      return new Promise((resolve) => {
        resolve(parent.subProducts.length > 0 ? true : false);
      });
    },
  },

  ModifierGroups: {
    async Modifiers(parent: any, args: any) {
      const storeType = await restaurantSchema.findOne({ _id: parent.account });
      return new Promise((resolve, rejects) => {
        if (storeType?.isOrdaticPartner) {
          modifiersSchema.find(
            {
              internalId: parent.posProductCategoryId.map((p: string) => {
                return p;
              }),
            },
            (error, product) => {
              if (error) {
                rejects(error);
              } else {
                if (!product) resolve([]);
                else {
                  resolve(product);
                }
              }
            }
          );
        } else {
          modifiersSchema.find(
            {
              _id: parent.subProducts.map((p: string) => {
                return p;
              }),
            },
            (error, product) => {
              if (error) {
                rejects(error);
              } else {
                if (!product) resolve([]);
                else {
                  resolve(product);
                }
              }
            }
          );
        }
      });
    },
  },

  Collections: {
    subCollectionItems(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        subCollection.find(
          { collectiontype: parent._id },
          (error, collection) => {
            if (error) {
              rejects(error);
            } else {
              if (!collection) resolve(false);
              else {
                const collections = collection;
                collections.sort((a, b) =>
                  //@ts-ignore
                  a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0
                );
                resolve(collections);
              }
            }
          }
        );
      });
    },
  },

  subCollection: {
    Product(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        productSchema
          .find({ parentId: parent._id }, (error, product) => {
            if (error) {
              rejects(error);
            } else {
              if (!product) resolve(false);
              else {
                resolve(product);
              }
            }
          })
          .limit(3);
      });
    },
  },

  CustonOrder: {
    Riders(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        ridersSchema.findOne({ _id: parent.riders }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },

    usuario(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.userID }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },

    Store(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        restaurantSchema.findOne(
          { _id: parent.storeID ? parent.storeID : null },
          (error, store) => {
            if (error) rejects(error);
            else {
              if (!store) resolve(null);
              else {
                resolve(store);
              }
            }
          }
        );
      });
    },
  },

  Post: {
    Author(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userAdminSchema.findOne({ _id: parent.author }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },
  },

  Opinion: {
    Usuario(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.user }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },
  },

  Valoracion: {
    Usuario(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.user }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },
  },

  Restaurant: {
    anadidoFavorito(parent: any, args: any, { usuarioActual }) {
      return new Promise((resolve, reject) => {
        if (!usuarioActual) resolve(false);
        userSchema.findOne({ _id: usuarioActual._id }, (error, usuario) => {
          if (error) reject(error);
          else {
            if (!usuario) resolve(false);
            else {
              favoritoSchema.findOne(
                { usuarioId: usuario._id, restaurantID: parent._id },
                (error, favorito) => {
                  if (error) reject(error);
                  else {
                    if (!favorito) resolve(false);
                    else resolve(true);
                  }
                }
              );
            }
          }
        });
      });
    },

    scheduleOnly(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        if (scheduleTime(parent.schedule)) {
          resolve(parent.scheduleOnly);
        } else {
          resolve({ available: true, hour: 0 });
        }
      });
    },
  },

  RestaurantID: {
    anadidoFavorito(parent: any, args: any, { usuarioActual }) {
      return new Promise((resolve, reject) => {
        if (!usuarioActual) resolve(false);
        userSchema.findOne({ _id: usuarioActual._id }, (error, usuario) => {
          if (error) reject(error);
          else {
            if (!usuario) resolve(false);
            else {
              favoritoSchema.findOne(
                { usuarioId: usuario._id, restaurantID: parent._id },
                (error, favorito) => {
                  if (error) reject(error);
                  else {
                    if (!favorito) resolve(false);
                    else resolve(true);
                  }
                }
              );
            }
          }
        });
      });
    },

    CategoryNew(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        categorySchema.find(
          { storeId: parent._id, snoozed: false },
          (error, menu) => {
            if (error) rejects(error);
            else {
              if (!menu) resolve(false);
              else {
                const categorys = menu;
                categorys.sort((a, b) =>
                  a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0
                );
                resolve(categorys);
              }
            }
          }
        );
      });
    },

    CategoryNewStore(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        categorySchema.find({ storeId: parent._id }, (error, menu) => {
          if (error) rejects(error);
          else {
            if (!menu) resolve(false);
            else {
              const categorys = menu;
              categorys.sort((a, b) =>
                a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0
              );
              resolve(categorys);
            }
          }
        });
      });
    },

    scheduleOnly(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        if (scheduleTime(parent.schedule)) {
          resolve(parent.scheduleOnly);
        } else {
          resolve({ available: true, hour: 0 });
        }
      });
    },
  },

  RestaurantFavorito: {
    restaurant(parent) {
      return new Promise((resolve, reject) => {
        restaurantSchema.findOne(
          { _id: parent.restaurantID, open: true },
          (error, restaurant) => {
            if (error) {
              reject(error);
            } else {
              resolve(restaurant);
            }
          }
        );
      });
    },
  },
};
