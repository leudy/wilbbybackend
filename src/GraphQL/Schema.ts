import { gql } from "apollo-server-express";

export const typeDefs = gql`
  scalar Date
  scalar JSON
  scalar JSONObject


  type AleatoryResponse {
    messages: String,
    success: Boolean,
    data: [Products],
  }

  type CoinsResponse {
    success: Boolean
    message: String
    data: [Coins]
  }

  type Coins {
    _id: ID
    value: Int
    type: String
    user: String
    order: String
    title: String
    description: String
    orderData: JSON
    created_at: Date
    updated_at: Date
  }

  type ResponseOrderID {
    success: Boolean
    message: String
    data: NewOrder
  }
  type ProductSubCollection {
    success: Boolean
    message: String
    data: [Products]
  }

  type CollectionResponse {
    success: Boolean!
    message: String
    data: [Collections]
  }

  type SubCollectionResponse {
    success: Boolean!
    message: String
    data: [subCollection]
  }

  type QuincenaResponse {
    success: Boolean!
    message: String
    data: Quincena
  }

  type QuincenaAllResponse {
    success: Boolean!
    message: String
    list: [Quincena]
  }

  type TransactionRiderResponse {
    success: Boolean!
    message: String
    list: [TransacctionRider]
  }

  type CrearUsuarioResponse {
    success: Boolean!
    message: String
    data: Usuario
  }

  type getStatisticsResponse {
    success: Boolean!
    message: String
    data: [Statistics]
  }

  type PagoResponseList {
    messages: String
    success: Boolean!
    data: Pago
  }

  type CustomerAdminResponse {
    success: Boolean
    messages: String
    count: Int
    data: [Usuario]
  }

  type transitionResponselist {
    messages: String
    success: Boolean!
    list: [Transacion]
  }

  type Transacion {
    id: ID
    fecha: String
    estado: String
    total: String
    restaurantID: String
    created_at: String
  }

  type generarResponse {
    messages: String
    success: Boolean
  }

  type RestaurantgenerarResponse {
    messages: String
    success: Boolean
    data: Restaurant
  }

  type AdresscreateResponse {
    messages: String!
    success: Boolean!
    data: Adress
  }

  type AutenticarRestaurantResponse {
    success: Boolean!
    message: String
    data: AutenticarRestaurent
  }

  type AutenticarRestaurent {
    token: String!
    id: String!
  }

  type AutenticarusuarioResponse {
    success: Boolean!
    message: String
    data: AutenticarUsuario
  }

  type AutenticarUsuario {
    token: String!
    id: String!
    verifyPhone: Boolean
    user: JSON
  }

  type Pago {
    _id: String
    nombre: String
    iban: String
    restaurantID: String
  }

  type AddressStoreResponse {
    success: Boolean!
    message: String
    data: AddresStore
  }

  type AddressStoreForselecResponse {
    success: Boolean!
    message: String
    data: [AddresStore]
  }

  type restaurantResponse {
    success: Boolean!
    message: String
    list: [RestaurantFavorito]
  }

  type CityResponse {
    success: Boolean
    messages: String
    data: City
  }

  type OffertsResponse {
    success: Boolean
    messages: String
    data: [Offerts]
  }

  type RiderAdminResponse {
    success: Boolean!
    messages: String
    data: [Usuario]
  }

  type AdressResponse {
    success: Boolean
    message: String
    data: [Adress]
  }

  type ValoracionResponse {
    messages: String
    success: Boolean
    data: [Valoracion]
  }

  type NewProductoSearchResponse {
    messages: String
    success: Boolean
    data: [Products]
  }

  type BundledResponse {
    messages: String
    success: Boolean
    data: [Bundles]
  }

  type ModifieldResponses {
    messages: String
    success: Boolean
    data: [Modifiers]
  }

  type ModifieldResponse {
    messages: String
    success: Boolean
    data: [ModifierGroups]
  }

  type OpinionResponse {
    messages: String
    success: Boolean
    data: [Opinion]
  }

  type PostResponse {
    success: Boolean
    message: String
    data: [Post]
  }

  type SinglePostResponse {
    success: Boolean
    message: String
    data: Post
  }

  type CustonOrderResponse {
    success: Boolean
    messages: String
    data: CustonOrder
  }

  type CustonOrderResponseQuery {
    success: Boolean
    messages: String
    data: [CustonOrder]
  }

  type CustonOrderResponseQueryID {
    success: Boolean
    messages: String
    data: CustonOrder
  }

  type CuponResponse {
    success: Boolean
    messages: String
    data: [Cupon]
  }

  type Cupon {
    id: ID
    clave: String!
    descuento: Int
    tipo: String
    usage: Int
    expire: Date
    exprirable: Boolean
    user: String
    description: String
    private: Boolean
    uniqueStore: Boolean
    canjeado: Boolean
    vencido: Boolean
    store: String
    Store: JSON
    Usuario: JSON
    city: [String]
    isShipping: Boolean
  }

  type Statistics {
    name: String
    Ene: Int
    Feb: Int
    Mar: Int
    Abr: Int
    May: Int
    Jun: Int
    Jul: Int
    Aug: Int
    Sep: Int
    Oct: Int
    Nov: Int
    Dic: Int
  }

  type Adress {
    id: String
    formatted_address: String
    puertaPiso: String
    city: String
    postalcode: String
    usuario: String
    type: String
    lat: String
    lgn: String
  }

  type ReportRiders {
    km: Int
    base: Int
    time: Int
    order: Int
    extra: Int
    combinado: Int
    lluvia: Int
    total: Int
    propina: Int
    iva: Int
  }

  type RestaurantFavorito {
    _id: String
    restaurantID: String
    usuarioId: String
    restaurant: Restaurant
  }

  type Opinion {
    id: String
    plato: String
    comment: String
    rating: Int
    created_at: Date
    user: String
    Usuario: Usuario
  }

  type Valoracion {
    id: String
    user: String
    comment: String
    value: Int
    created_at: Date
    restaurant: String
    Usuario: Usuario
  }

  type Days {
    day: String
    isOpen: Boolean
    openHour1: Int
    openHour2: Int
    openMinute1: Int
    openMinute2: Int
    closetHour1: Int
    closetHour2: Int
    closetMinute1: Int
    closetMinute2: Int
  }

  type Schedule {
    Monday: Days
    Tuesday: Days
    Wednesday: Days
    Thursday: Days
    Friday: Days
    Saturday: Days
    Sunday: Days
  }

  type AddresStore {
    _id: String
    calle: String
    numero: String
    codigoPostal: String
    ciudad: String
    store: String
    lat: String
    lgn: String
  }

  type AddresStoreNew {
    calle: String
    numero: String
    codigoPostal: String
    ciudad: String
    lat: String
    lgn: String
  }

  type SocialLink {
    instagram: String
    facebook: String
    twitter: String
    web: String
  }

  type scheduleOnlyData {
    available: Boolean
    hour: Int
  }

  type RestaurantID {
    _id: String
    title: String
    image: String
    description: String
    type: String
    anadidoFavorito: Boolean
    categoryName: String
    categoryID: [String]
    minime: Int
    shipping: Int
    extras: Int
    tagOffert: String
    stimateTime: String
    slug: String
    highkitchen: Boolean
    previous_shipping: Int
    alegeno_url: String
    autoshipping: Boolean
    inOffert: Boolean
    ispartners: Boolean
    phone: String
    email: String
    llevar: Boolean
    logo: String
    password: String
    schedule: Schedule
    includeCity: [String]
    isnew: Boolean
    isDeliverectPartner: Boolean
    tipo: [String]
    open: Boolean
    cashPayment: Boolean
    OnesignalID: String
    channelLinkId: String
    collections: Boolean
    CategoryNew: [CategoryNew]
    CategoryNewStore: [CategoryNew]
    adress: AddresStoreNew
    city: String
    contactCode: String
    apertura: Int
    cierre: Int
    aperturaMin: Int
    cierreMin: Int
    diaslaborales: [String]
    rating: String
    menu: String
    socialLink: SocialLink
    salvingPack: JSON
    scheduleOnly: scheduleOnlyData
    shorting: Int
    noScheduled: Boolean
    forttalezaURL: String
  }

  type Restaurant {
    _id: String
    title: String
    image: String
    description: String
    type: String
    anadidoFavorito: Boolean
    categoryName: String
    categoryID: [String]
    minime: Int
    shipping: Int
    extras: Int
    stimateTime: String
    slug: String
    highkitchen: Boolean
    previous_shipping: Int
    alegeno_url: String
    autoshipping: Boolean
    inOffert: Boolean
    ispartners: Boolean
    phone: String
    email: String
    cashPayment: Boolean
    llevar: Boolean
    logo: String
    password: String
    schedule: Schedule
    includeCity: [String]
    isnew: Boolean
    isDeliverectPartner: Boolean
    tipo: [String]
    open: Boolean
    tagOffert: String
    OnesignalID: String
    channelLinkId: String
    collections: Boolean
    adress: AddresStoreNew
    city: String
    contactCode: String
    apertura: Int
    cierre: Int
    aperturaMin: Int
    cierreMin: Int
    diaslaborales: [String]
    rating: String
    menu: String
    socialLink: SocialLink
    salvingPack: JSON
    scheduleOnly: scheduleOnlyData
    shorting: Int
    noScheduled: Boolean
    forttalezaURL: String
  }

  type RestaurantAdmin {
    _id: String
    title: String
    image: String
    description: String
    type: String
    anadidoFavorito: Boolean
    categoryName: String
    categoryID: [String]
    minime: Int
    shipping: Int
    extras: Int
    stimateTime: String
    slug: String
    highkitchen: Boolean
    previous_shipping: Int
    alegeno_url: String
    autoshipping: Boolean
    inOffert: Boolean
    ispartners: Boolean
    phone: String
    email: String
    cashPayment: Boolean
    llevar: Boolean
    logo: String
    password: String
    schedule: Schedule
    includeCity: [String]
    isnew: Boolean
    isDeliverectPartner: Boolean
    tipo: [String]
    open: Boolean
    tagOffert: String
    OnesignalID: String
    channelLinkId: String
    collections: Boolean
    adress: AddresStoreNew
    city: String
    contactCode: String
    apertura: Int
    cierre: Int
    aperturaMin: Int
    cierreMin: Int
    diaslaborales: [String]
    rating: String
    menu: String
    socialLink: SocialLink
    salvingPack: JSON
    scheduleOnly: scheduleOnlyData
    shorting: Int
    noScheduled: Boolean
    forttalezaURL: String
  }

  type Usuario {
    _id: String
    name: String
    lastName: String
    email: String
    city: String
    avatar: String
    telefono: String
    created_at: Date
    updated_at: Date
    termAndConditions: Boolean
    verifyPhone: Boolean
    isAvalible: Boolean
    StripeID: String
    PaypalID: String
    OnesignalID: String
    socialNetworks: String
    isSocial: Boolean
    rating: [Int]
    contactCode: String
    birthdayDate: Date
    numberCurrentOrder: Int
  }

  type UsuarioResponse {
    messages: String!
    success: Boolean!
    data: Usuario
  }

  type RestaurantResponse {
    messages: String!
    success: Boolean!
    data: [Restaurant]
  }

  type RestaurantAdminEditResponse {
    messages: String!
    success: Boolean!
    data: [RestaurantAdmin]
  }

  type RestaurantIDResponse {
    messages: String!
    success: Boolean!
    data: RestaurantID
  }

  type CategoryResponse {
    messages: String!
    success: Boolean!
    data: [Category]
  }

  type Category {
    _id: String
    title: String
    image: String
    description: String
    sorting: Int
    excludeCity: [String]
    visible: Boolean
    navigate: Boolean
    url: String
    largue: Boolean
  }

  type File {
    filename: String
    mimetype: String
    encoding: String
  }

  type FileAWS {
    data: JSON
  }

  type Post {
    id: String
    title: String
    image: String
    shortDescription: String
    like: Int
    tags: [String]
    author: String
    Author: Usuario
    category: String
    readTime: String
    content: String
    created_at: Date
    slug: String
    country: String
  }

  type address_custon {
    address_name: String
    address_number: String
    postcode: String
    type: String
    lat: String
    lgn: String
  }

  type CustonOrder {
    id: ID
    display_id: Int
    riders: String
    Riders: Usuario
    usuario: Usuario
    origin: address_custon
    destination: address_custon
    schedule: Boolean
    distance: String
    nota: String
    date: String
    city: String
    userID: ID
    estado: String
    status: String
    progreso: String
    created_at: Date
    total: String
    product_stimate_price: String
    reportRiders: ReportRiders
    pagoPaypal: JSON
    stripePaymentIntent: JSON
    customer: JSON
    fronStore: Boolean
    storeID: String
    Store: Restaurant
  }

  type City {
    id: ID
    close: Boolean
    city: String
    title: String
    subtitle: String
    imagen: String
  }

  type Offerts {
    id: String
    imagen: String
    city: String
    store: String
    slug: String
    apertura: Int
    cierre: Int
    open: Boolean
  }

  type TransacctionRider {
    id: String
    rider: String
    km: Int
    order: Int
    total: Int
    propina: Int
    iva: Int
    created_at: Date
  }

  type Quincena {
    _id: ID
    fromDate: String
    toDate: String
    numberQuincena: Int
  }

  type subCollection {
    _id: ID
    title: String
    collectiontype: String
    sorting: Int
    Product: [Products]
  }

  type Collections {
    _id: ID
    title: String
    image: String
    subCollectionItems: [subCollection]
    store: [String]
    sorting: Int
  }

  type NewMenuResponse {
    success: Boolean!
    message: String
    list: [Menus]
  }

  type Bundles {
    _id: ID
    name: String
    description: String
    account: String
    location: String
    productType: String
    plu: String
    price: Int
    sortOrder: Int
    deliveryTax: Int
    takeawayTax: Int
    multiply: Int
    multiMax: Int
    posProductId: String
    posProductCategoryId: [String]
    subProducts: [String]
    productTags: [String]
    posCategoryIds: [String]
    imageUrl: String
    max: Int
    min: Int
    recomended: Boolean
    quantity: Int
    capacityUsages: [String]
    parentId: String
    visible: Boolean
    snoozed: Boolean
    subProductSortOrder: [String]
    internalId: String
    Products: [Products]
    subItems: JSON
  }

  type ModifierGroups {
    _id: ID
    name: String
    description: String
    account: String
    location: String
    productType: String
    plu: String
    price: Int
    sortOrder: Int
    deliveryTax: Int
    takeawayTax: Int
    multiply: Int
    multiMax: Int
    posProductId: String
    posProductCategoryId: [String]
    subProducts: [String]
    productTags: [String]
    posCategoryIds: [String]
    imageUrl: String
    max: Int
    min: Int
    recomended: Boolean
    quantity: Int
    capacityUsages: [String]
    parentId: String
    visible: Boolean
    snoozed: Boolean
    subProductSortOrder: [String]
    internalId: String
    Modifiers: [Modifiers]
    subItems: JSON
  }

  type Modifiers {
    _id: ID
    name: String
    description: String
    account: String
    location: String
    productType: String
    plu: String
    price: Int
    sortOrder: Int
    deliveryTax: Int
    takeawayTax: Int
    multiply: Int
    multiMax: Int
    posProductId: String
    posProductCategoryId: [String]
    subProducts: [String]
    productTags: [String]
    posCategoryIds: [String]
    imageUrl: String
    max: Int
    min: Int
    recomended: Boolean
    quantity: Int
    capacityUsages: [String]
    parentId: String
    visible: Boolean
    snoozed: Boolean
    subProductSortOrder: [String]
    internalId: String
    ModifierGroups: [ModifierGroups]
    subItems: JSON
  }

  type CartItem {
    _id: ID
    userId: String
    storeId: String
    productId: String
    items: JSON
    addToCart: Boolean
  }

  type Products {
    _id: ID
    name: String
    description: String
    account: String
    location: String
    productType: Int
    plu: String
    price: Int
    sortOrder: Int
    deliveryTax: Int
    takeawayTax: Int
    multiply: Int
    multiMax: Int
    posProductId: String
    posProductCategoryId: [String]
    subProducts: [String]
    productTags: [String]
    posCategoryIds: [String]
    imageUrl: String
    max: Int
    min: Int
    capacityUsages: [String]
    parentId: String
    visible: Boolean
    snoozed: Boolean
    subProductSortOrder: [String]
    ModifierGroups: [ModifierGroups]
    recomended: Boolean
    quantity: Int
    Bundles: [Bundles]
    internalId: String
    new: Boolean
    popular: Boolean
    offert: Boolean
    previous_price: Int
    cartItems: CartItem
    storeId: [String]
    isOpen: Boolean
  }

  type CategoryNew {
    _id: ID
    name: String
    description: String
    account: String
    posLocationId: String
    posCategoryType: String
    subCategories: [String]
    posCategoryId: String
    imageUrl: String
    products: [String]
    Products: [Products]
    menu: String
    snoozed: Boolean
    storeId: String
    sortedChannelProductIds: [String]
    subProductSortOrder: [String]
    subProducts: [String]
    level: Int
    availabilities: [String]
    internalId: String
  }

  type Menus {
    _id: ID
    menu: String
    menuId: String
    description: String
    menuImageURL: String
    menuType: Int
    availabilities: JSON
    productTags: [Int]
    currency: Int
    validations: [String]
    nestedModifiers: Boolean
    channelLinkId: String
    storeId: String
    Categories: [CategoryNew]
  }

  type OrderProccess {
    status: String
    date: Date
  }

  type NewOrderResponse {
    success: Boolean
    message: String
    total: Int
    list: [NewOrder]
  }

  type NewOrderAdminResponse {
    success: Boolean
    message: String
    count: Int
    list: [NewOrder]
  }

  type NewOrderRepectResponse {
    success: Boolean
    message: String
    list: [NewOrderRepeat]
  }

  type CreateNewOrderResponse {
    success: Boolean
    message: String
    data: NewOrder
  }

  type AdressForOrder {
    _id: String
    formatted_address: String
    puertaPiso: String
    city: String
    postalcode: String
    usuario: String
    type: String
    lat: String
    lgn: String
  }

  type NewOrder {
    _id: ID
    channelOrderDisplayId: Int
    orderType: String
    pickupTime: Date
    estimatedPickupTime: Date
    deliveryTime: Date
    courier: ID
    courierData: Usuario
    customerData: Usuario
    customer: ID
    store: ID
    tip: Int
    storeData: JSON
    deliveryAddressData: AdressForOrder
    deliveryAddress: ID
    orderIsAlreadyPaid: Boolean
    payment: Int
    note: String
    items: JSON
    descuento: Cupon
    numberOfCustomers: Int
    deliveryCost: Int
    serviceCharge: Int
    discountTotal: Int
    IntegerValue: Int
    paymentMethod: String
    cupon: ID
    statusProcess: JSON
    Needcutlery: Boolean
    status: String
    holdedID: String
    holdedRidersID: String
    holdedPartnerID: String
    invoiceUrl: String
    reportRiders: ReportRiders
    offertFromRider: JSON
    riderAcceptOrder: Boolean
    scheduled: Boolean
    budgetRider: [String]
    created_at: Date
    eventId: String
    cuponName: String
    cuponTipe: String
    cuponValue: Int
    source: JSON
    platform: String
    pagoPaypal: JSON
    stripePaymentIntent: JSON
  }

  type NewOrderRepeat {
    store: ID
    Restaurant: Restaurant
  }

  type NewCartResponse {
    success: Boolean
    message: String
    list: [NewCart]
  }

  type NewCart {
    _id: ID
    userId: String
    storeId: String
    productId: String
    items: JSON
    addToCart: Boolean
  }

  type Ads {
    _id: ID
    name: String
    image: String
    sorting: Int
    visible: Boolean
    navigate: Boolean
    url: String
    email: String
    click: Int
    category: String
    isHome: Boolean
    dataEmail: JSON
    includeCity: [String]
    created_at: Date
    end_date: Date
  }

  type AdsRondomResponse {
    messages: String!
    success: Boolean!
    data: [Ads]
  }

  input OrderProccessInput {
    status: String
    date: Date
  }

  input NewOrderInput {
    _id: ID
    channelOrderDisplayId: Int
    orderType: String
    pickupTime: Date
    estimatedPickupTime: Date
    deliveryTime: Date
    courier: ID
    customer: ID
    store: ID
    tip: Int
    deliveryAddress: ID
    orderIsAlreadyPaid: Boolean
    payment: Int
    scheduled: Boolean
    note: String
    items: JSON
    numberOfCustomers: Int
    deliveryCost: Int
    serviceCharge: Int
    discountTotal: Int
    IntegerValue: Int
    paymentMethod: String
    cupon: ID
    Needcutlery: Boolean
    status: String
    holdedID: String
    holdedRidersID: String
    holdedPartnerID: String
    invoiceUrl: String
    clave: String
    offertFromRider: JSON
    riderAcceptOrder: Boolean
    eventId: String
    fromAdmin: Boolean
    pagoPaypal: JSON
    stripePaymentIntent: JSON
    statusProcess: JSON
    cuponName: String
    cuponTipe: String
    cuponValue: Int
    source: JSON
    platform: String
  }

  input NewCartInput {
    userId: String
    storeId: String
    productId: String
    items: JSON
    addToCart: Boolean
  }

  input DaysInput {
    day: String
    isOpen: Boolean
    openHour1: Int
    openHour2: Int
    openMinute1: Int
    openMinute2: Int
    closetHour1: Int
    closetHour2: Int
    closetMinute1: Int
    closetMinute2: Int
  }

  input ScheduleInput {
    Monday: DaysInput
    Tuesday: DaysInput
    Wednesday: DaysInput
    Thursday: DaysInput
    Friday: DaysInput
    Saturday: DaysInput
    Sunday: DaysInput
  }

  input Addressinput {
    _id: String
    calle: String
    numero: String
    codigoPostal: String
    ciudad: String
    store: String
    lat: String
    lgn: String
  }

  input AddressinputNew {
    calle: String
    numero: String
    codigoPostal: String
    ciudad: String
    lat: String
    lgn: String
  }

  input InpuSocialLink {
    instagram: String
    facebook: String
    twitter: String
    web: String
  }

  input scheduleOnlyInput {
    available: Boolean
    hour: Int
  }

  input RestaurantInput {
    _id: String
    title: String
    image: String
    description: String
    rating: String
    categoryName: String
    categoryID: [String]
    cashPayment: Boolean
    minime: Int
    shipping: Int
    extras: Int
    slug: String
    menu: String
    stimateTime: String
    highkitchen: Boolean
    inOffert: Boolean
    alegeno_url: String
    phone: String
    autoshipping: Boolean
    ispartners: Boolean
    previous_shipping: Int
    email: String
    tagOffert: String
    llevar: Boolean
    logo: String
    tipo: [String]
    password: String
    type: String
    schedule: ScheduleInput
    apertura: Int
    cierre: Int
    aperturaMin: Int
    cierreMin: Int
    diaslaborales: [String]
    includeCity: [String]
    open: Boolean
    isnew: Boolean
    channelLinkId: String
    collections: Boolean
    isDeliverectPartner: Boolean
    adress: AddressinputNew
    city: String
    contactCode: String
    socialLink: InpuSocialLink
    salvingPack: JSON
    scheduleOnly: scheduleOnlyInput
    shorting: Int
    noScheduled: Boolean
    OnesignalID: String
  }

  input ActualizarUsuarioInput {
    _id: String
    name: String
    lastName: String
    city: String
    email: String
    avatar: String
    telefono: String
    isAvalible: Boolean
    PaypalID: String
    rating: [Int]
    birthdayDate: Date
  }

  input FileInput {
    filename: String
    mimetype: String
    encoding: String
  }

  input CreatePlatoInput {
    data: JSON
  }

  input CuponCreationInput {
    clave: String!
    descuento: Int
    tipo: String
    usage: Int
    expire: Date
    exprirable: Boolean
    user: String
    description: String
    private: Boolean
    uniqueStore: Boolean
    store: String
    city: [String]
    isShipping: Boolean
  }

  input ValoracionCreationInput {
    user: String
    comment: String
    value: Int
    restaurant: String
  }

  input ReportRidersInput {
    km: Int
    base: Int
    time: Int
    order: Int
    extra: Int
    combinado: Int
    lluvia: Int
    total: Int
    propina: Int
    iva: Int
  }

  input DateRangeInput {
    fromDate: String
    toDate: String
  }

  input OpinionInput {
    plato: String
    comment: String
    rating: Int
    user: String
  }

  input PagoInput {
    nombre: String
    iban: String
    restaurantID: String
  }

  input trasitionInput {
    fecha: String
    estado: String
    total: String
    restaurantID: String
  }

  input UsuarioInput {
    nombre: String
    apellidos: String
    email: String
    password: String
    termAndConditions: Boolean
    telefono: String
    isAvalible: Boolean
    city: String
    contactCode: String
    birthdayDate: Date
  }

  input adreesUser {
    formatted_address: String
    puertaPiso: String
    usuario: String
    city: String
    postalcode: String
    type: String
    lat: String
    lgn: String
  }

  input adreesUserEdit {
    id: String
    formatted_address: String
    puertaPiso: String
    usuario: String
    city: String
    postalcode: String
    type: String
    lat: String
    lgn: String
  }

  input PostInput {
    id: String
    title: String
    image: String
    shortDescription: String
    like: Int
    tags: [String]
    author: String
    category: String
    readTime: String
    content: String
    slug: String
    country: String
  }

  input address_custon_order {
    address_name: String
    address_number: String
    postcode: String
    type: String
    lat: String
    lgn: String
  }

  input CustonOrderInput {
    id: ID
    display_id: Int
    riders: String
    origin: address_custon_order
    destination: address_custon_order
    schedule: Boolean
    distance: String
    nota: String
    date: String
    city: String
    userID: ID
    estado: String
    status: String
    progreso: String
    created_at: Date
    total: String
    product_stimate_price: String
    reportRiders: ReportRidersInput
    customer: JSON
    fronStore: Boolean
    storeID: String
  }

  input CityInput {
    id: ID
    close: Boolean
    city: String
    title: String
    subtitle: String
    imagen: String
  }

  input Offert {
    imagen: String
    city: String
    store: String
    slug: String
    apertura: Int
    cierre: Int
    open: Boolean
  }

  input TransacctionRiderInput {
    id: String
    rider: String
    km: Int
    order: Int
    total: Int
    propina: Int
    iva: Int
  }

  input QuincenaInput {
    _id: ID
    fromDate: String
    toDate: String
    numberQuincena: Int
  }

  input CollectionInput {
    _id: ID
    title: String
    image: String
    store: [String]
    sorting: Int
  }

  input subCollectionInput {
    _id: ID
    title: String
    collectiontype: String
    sorting: Int
  }

  input CategoryInput {
    title: String
    image: String
    description: String
    sorting: Int
    excludeCity: [String]
    visible: Boolean
    navigate: Boolean
    url: String
  }

  input CreateProductInput {
    data: JSON
  }

  input MenuInput {
    data: JSON
  }

  input BundledInput {
    data: JSON
  }

  input InputModifieldGroup {
    data: JSON
  }

  input AdsData {
    data: JSON
  }

  input DataStore {
    data: JSON
  }

  input CoinsData {
    value: Int
    user: String
    order: String
    type: String
    title: String
    description: String
  }

  type Query {
    getUsuario: UsuarioResponse
    getUsuarioAdmin: UsuarioResponse
    getRiders(id: ID): UsuarioResponse
    getRiderForAdmin(city: String): RiderAdminResponse
    getRiderForAdminAll: RiderAdminResponse
    getCategory(city: String): CategoryResponse
    getTipo: CategoryResponse
    getHighkitchenCategory: CategoryResponse
    getTipoTienda: CategoryResponse
    getCategoryLargue(city: String, limit: Int): CategoryResponse
    getCategorySmall(city: String): CategoryResponse
    getRestaurant(city: String): RestaurantResponse
    getStoreInOffert(city: String, page: Int, limit: Int): RestaurantResponse
    getStorePromo(
      city: String
      categoryID: String
      page: Int
      limit: Int
    ): RestaurantResponse
    getTiendaInOffert(city: String, page: Int, limit: Int): RestaurantResponse
    getSalvingPackInOffert(city: String): RestaurantResponse
    getStoreNew(city: String, page: Int, limit: Int): RestaurantResponse
    getFarmacyOffert(city: String): RestaurantResponse
    getRestaurantSearch(
      city: String
      price: Int
      category: String
      search: String
      llevar: Boolean
      tipo: String
    ): RestaurantResponse
    getRestaurantforEdit(
      city: String
      search: String
      page: Int
      limit: Int
    ): RestaurantAdminEditResponse
    getRestaurantSearchWeb(
      city: String
      category: String
      search: String
      llevar: Boolean
      tipo: String
      page: Int
    ): RestaurantResponse
    getRestaurantSearchWebPAgination(
      city: String
      category: String
    ): RestaurantResponse
    getRestaurantForCategory(
      city: String
      category: String
      tipo: String
      llevar: Boolean
      search: String
      page: Int
      limit: Int
      star: String
      offert: Boolean
    ): RestaurantResponse

    getNewStoreList(
      city: String
      category: String
      tipo: String
      search: String
    ): RestaurantResponse
    
    getRestaurantHighkitchen(
      city: String
      category: String
      llevar: Boolean
    ): RestaurantResponse
    getRestaurantForID(id: ID): RestaurantIDResponse
    getRestaurantForSlugWeb(slug: String): RestaurantIDResponse
    getRestaurantForDetails(slug: String, city: String): RestaurantIDResponse
    getRestaurantForIDWeb(id: ID, city: String): RestaurantIDResponse
    getRestaurantFavorito(id: ID): restaurantResponse
    getCupon(clave: String): Cupon
    getCuponAll: Cupon
    getPosts(country: String, page: Int): PostResponse
    getPostsForTags(country: String, page: Int, tag: String): PostResponse
    getPost(country: String): SinglePostResponse
    getPostbyId(slug: String, country: String): SinglePostResponse
    getValoraciones(restaurant: ID): ValoracionResponse
    getValoracionesStore(id: ID, page: Int, limit: Int): ValoracionResponse
    getOpinion(id: ID): OpinionResponse
    getPago(id: ID): PagoResponseList
    getTransaction(id: ID): transitionResponselist
    getStatistics(id: ID): getStatisticsResponse
    obtenerUsuario: Usuario
    obtenerAdmin: Usuario
    getAdress(id: ID): AdressResponse
    getAdressStore(id: ID, city: String): AddressStoreResponse
    getAdressStoreforSelect(id: ID): AddressStoreForselecResponse
    getCustomOrder(id: ID): CustonOrderResponseQuery
    getCustomOrderRider(id: ID): CustonOrderResponseQuery
    getCustomOrderRiderID(id: ID): CustonOrderResponseQueryID
    getCustomOrderAdmin(
      dateRange: DateRangeInput
      page: Int
      limit: Int
      orderID: Int
      city: String
    ): CustonOrderResponseQuery
    getCity(city: String): CityResponse
    getOfferts(city: String): OffertsResponse
    getCustomers(page: Int, limit: Int, email: String): CustomerAdminResponse
    getAllCustomers(email: String): CustomerAdminResponse
    getRiderTransaction(
      id: ID
      dateRange: DateRangeInput
      page: Int
    ): TransactionRiderResponse
    getQuincena: QuincenaResponse
    getQuincenaAll: QuincenaAllResponse
    getCollection(store: String): CollectionResponse
    getSubCollection(id: String): SubCollectionResponse
    getNewMenu(id: String): NewMenuResponse
    getNewProductoSearch(
      store: String
      search: String
      page: Int
      limit: Int
      products: [String]
    ): NewProductoSearchResponse
    getNewProducto(store: String, search: String): NewProductoSearchResponse
    getNewProductoSearchStore(
      store: String
      search: String
    ): NewProductoSearchResponse
    getNewProductoaAllStore(store: String): NewProductoSearchResponse
    getNewProductoBundled(store: String): NewProductoSearchResponse
    getNewCart(userId: String, storeId: String): NewCartResponse
    getNewOrder(
      userId: ID
      status: [String]
      dateRange: DateRangeInput
    ): NewOrderResponse
    getNewOrderRepet(userId: ID, city: String): NewOrderRepectResponse
    getNewOrderforStore(
      storeId: ID
      status: [String]
      search: Int
      dateRange: DateRangeInput
      page: Int
      limit: Int
    ): NewOrderResponse

    getNewOrderforStoreWeb(
      storeId: ID
      status: [String]
      search: Int
      dateRange: DateRangeInput
      page: Int
      limit: Int
    ): NewOrderResponse

    getNewOrderRider(
      riderId: ID
      status: [String]
      dateRange: DateRangeInput
      page: Int
    ): NewOrderResponse
    getNewOrderBugetRider(riderId: ID): NewOrderResponse
    getNewOrderAdimin(
      status: [String]
      dateRange: DateRangeInput
      page: Int
      limit: Int
      orderID: Int
      Stores: [String]
      city: String
      rider: [String]
    ): NewOrderAdminResponse
    getProducto(id: String, page: Int, limit: Int): NewProductoSearchResponse
    getProductoForCategory(category: ID): NewProductoSearchResponse
    getPrivatePromocode(id: String): CuponResponse
    getPromocode(city: String): CuponResponse
    getNewOrderforId(id: Int): ResponseOrderID
    getProductSubCollection(id: String): ProductSubCollection
    getNewProductoForApp(
      products: [String]
      storeID: ID
    ): NewProductoSearchResponse
    getNewProductoForStore(
      products: [String]
      storeID: ID
    ): NewProductoSearchResponse
    getBundled(products: [String], storeID: ID): BundledResponse
    getBundledStore(storeID: ID): BundledResponse
    getModifiedGroupStore(storeID: ID): ModifieldResponse
    getModifieldGroup(products: [String], storeID: ID): ModifieldResponse
    getNewProductoModifield(store: ID): ModifieldResponses
    getAds(city: String): AdsRondomResponse
    getAllCoins(id: ID, page: Int, limit: Int): CoinsResponse
    getAleatoryProduct(store: String): AleatoryResponse
  }

  type Mutation {
    createOffert(input: Offert): generarResponse
    eliminarOfferts(id: ID): generarResponse
    createCategory(input: CategoryInput): generarResponse
    createTipo(input: CategoryInput): generarResponse
    createHighkitchenCategory(input: CategoryInput): generarResponse
    eliminarTipo(id: ID): generarResponse
    createTipoTienda(input: CategoryInput): generarResponse
    eliminarTipoTienda(id: ID): generarResponse
    createRestaurant(input: DataStore): RestaurantgenerarResponse
    eliminarRestaurant: generarResponse
    eliminarCategory(id: ID): generarResponse
    actualizarRestaurant(input: RestaurantInput): generarResponse
    actualizarRestaurantAdmin(input: DataStore): generarResponse
    actualizarUsuario(input: ActualizarUsuarioInput): Usuario
    actualizarAdmin(input: ActualizarUsuarioInput): Usuario
    actualizarRiders(input: ActualizarUsuarioInput): Usuario
    singleUpload(file: Upload): File
    singleUploadToAws(file: Upload): FileAWS
    singleUploadToStoreImagenAws(file: Upload): FileAWS
    avatarUploadToAws(file: String, id: ID): FileAWS
    eliminarUsuario(id: ID!): generarResponse
    eliminarAdmin(id: ID!): generarResponse
    eliminarRiders(id: ID!): generarResponse
    crearFavorito(restaurantID: ID, usuarioId: ID): generarResponse
    eliminarFavorito(id: ID, userID: ID): generarResponse
    createProduct(input: CreateProductInput): generarResponse
    createBundled(input: BundledInput): generarResponse
    createModifieldGroup(input: InputModifieldGroup): generarResponse
    createModifield(input: InputModifieldGroup): generarResponse
    crearCupon(input: CuponCreationInput): Cupon
    eliminarCupon(id: ID): generarResponse
    crearValoracion(input: ValoracionCreationInput): Valoracion
    createOpinion(input: OpinionInput): generarResponse
    autenticarRestaurant(
      email: String
      password: String
    ): AutenticarRestaurantResponse

    autenticarUsuario(
      email: String
      password: String
    ): AutenticarusuarioResponse

    autenticarRiders(
      email: String
      password: String
    ): AutenticarRestaurantResponse

    autenticarAdmin(
      email: String
      password: String
    ): AutenticarRestaurantResponse

    OrdenProceed(
      orden: ID
      estado: String
      progreso: String
      status: String
    ): generarResponse
    eliminarMenu(id: ID): generarResponse
    crearPago(input: PagoInput): generarResponse
    eliminarPago(id: ID): generarResponse
    crearDeposito(input: trasitionInput): generarResponse
    eliminarDeposito(id: ID): generarResponse
    crearUsuario(input: UsuarioInput): CrearUsuarioResponse
    crearRiders(input: UsuarioInput): CrearUsuarioResponse
    crearAdmin(input: UsuarioInput): CrearUsuarioResponse
    createAdress(input: adreesUser): AdresscreateResponse
    actualizarAdress(input: adreesUserEdit): AdresscreateResponse
    eliminarAdress(id: ID): generarResponse
    eliminarAdressStore(id: ID): generarResponse
    createStoreAdress(input: Addressinput): AddressStoreResponse
    eliminarPost(id: ID): generarResponse
    createPost(input: PostInput): generarResponse
    actualizarPost(input: PostInput): SinglePostResponse
    createCustonOrder(input: CustonOrderInput): CustonOrderResponse
    createCustonOrderAdmin(input: JSON): generarResponse
    actualizarOrderProcess(input: CustonOrderInput): CustonOrderResponse
    createCity(input: CityInput): generarResponse
    actualizarCity(input: CityInput): generarResponse
    createTransactionRider(input: TransacctionRiderInput): generarResponse
    createQuincena(input: QuincenaInput): generarResponse
    createCollection(input: CollectionInput): generarResponse
    createsubCollection(input: subCollectionInput): generarResponse
    addToCart(input: NewCartInput): generarResponse
    deleteCartItem(id: ID, fromWeb: Boolean): generarResponse
    crearModificarNewOrden(input: NewOrderInput): CreateNewOrderResponse
    NewOrdenProceed(
      ordenId: String
      status: String
      IntegerValue: Int
      statusProcess: OrderProccessInput
    ): generarResponse
    NewOrdenAsigRider(ordenId: String, riderID: String): generarResponse
    RiderAccepOrder(id: ID, riderAcceptOrder: Boolean): generarResponse
    SendOffertOrder(id: ID, input: JSON): generarResponse
    snoozedProduct(id: ID, snoozed: Boolean): generarResponse
    snoozedBundled(id: ID, snoozed: Boolean): generarResponse
    snoozedModifield(id: ID, snoozed: Boolean): generarResponse
    snoozedModifieldGroup(id: ID, snoozed: Boolean): generarResponse
    createMenu(input: MenuInput): generarResponse
    createNewCategory(input: InputModifieldGroup): generarResponse
    eliminarProducto(id: ID): generarResponse
    actualizarProduct(input: CreateProductInput): generarResponse
    actualizarBundled(input: CreateProductInput): generarResponse
    actualizarModifiedGroup(input: InputModifieldGroup): generarResponse
    actualizarModified(input: InputModifieldGroup): generarResponse
    actualizarCategoria(input: InputModifieldGroup): generarResponse
    eliminarCategoria(id: ID): generarResponse
    createCardSource(customers: String, paymentMethod: String): generarResponse

    eliminarBundled(id: ID): generarResponse
    eliminarModifieldGroup(id: ID): generarResponse
    eliminarModifield(id: ID): generarResponse

    crearCoins(input: CoinsData): generarResponse
    deleteCoins(id: ID): generarResponse

    createMessageHome(input: JSON): generarResponse
    updateMessageHome(input: JSON): generarResponse
    deleteMessageHome(id: ID): generarResponse

    createAds(data: AdsData): generarResponse
    updateAds(data: JSON): generarResponse
    eliminarAds(id: String): generarResponse
  }
`;
