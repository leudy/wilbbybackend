import { sendNotification } from "../GraphQL/sendNotificationRider";
import { sendNotificationCustomer } from "./sendNotificationCustomer";

export const pushTopRider = (rider: any, onesignal: string, order: any) => {
  sendNotification(
    rider.OnesignalID,
    "Has recibido un nuevo pedido a por todas ✌️ ⚡️",
    "Has recibido un nuevo pedido",
    true
  );
  sendNotificationCustomer(
    onesignal,
    `Te hemos asignado a ${rider.name} ${rider.lastName} como rider para entregar tu pedido`,
    "💥💥 Repartidor asignado",
    order
  );
};
