export const sendNotification = (
  IdOnesignal: any,
  textmessage: string,
  title: string,
  isRiders?: boolean
) => {
  var sendNotification = function (data: any) {
    var headers = {
      "Content-Type": "application/json; charset=utf-8",
    };
    var options = {
      host: "onesignal.com",
      port: 443,
      path: "/api/v1/notifications",
      method: "POST",
      headers: headers,
    };
    var https = require("https");
    var req = https.request(options, function (res: any) {
      res.on("data", function (data: any) {
        console.log("Response:");
      });
    });

    req.on("error", function (e: any) {
      console.log("ERROR:");
      console.log(e);
    });
    req.write(JSON.stringify(data));
    req.end();
  };

  var message = {
    app_id: "e21467de-eaf6-4090-9b6c-a311d37c0b5e",
    headings: { en: title },
    android_channel_id: isRiders
      ? "9b0682ea-6f2d-4228-8802-280df76cb033"
      : null,
    ios_sound: isRiders ? "despertador_minion.wav" : null,
    contents: { en: `${textmessage}` },
    ios_badgeCount: 1,
    ios_badgeType: "Increase",
    include_player_ids: [IdOnesignal],
  };
  sendNotification(message);
};
