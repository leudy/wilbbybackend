import { Request, Response, Router } from "express";
import Menu from "../models/newMenu/category";

class MenuRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.options("/menu", function (req, res) {
      res.setHeader("Access-Control-Allow-Origin", "*");
      res.setHeader("Access-Control-Allow-Methods", "*");
      res.setHeader("Access-Control-Allow-Headers", "*");
      res.end();
    });

    this.router.post("/menu", async (req: Request, res: Response) => {
      const menu = new Menu(req.body);
      menu.sorting = await Menu.estimatedDocumentCount();
      await menu.save();
      res.json(menu);
    });

    this.router.get("/menu", async (req: Request, res: Response) => {
      const { idstore } = req.query;
      //@ts-ignore
      const menu = await Menu.find({ storeId: idstore });
      res.json(menu);
    });

    this.router.put("/menu", async (req: Request, res: Response) => {
      const tasksIds = req.body;
      for (const [i, id] of tasksIds.entries()) {
        await Menu.updateOne({ _id: id }, { sorting: i });
        // console.log(i, id)
      }
      res.json("the list was ordered");
    });
  }
}

const menuRouter = new MenuRouter();
menuRouter.routes();

export default menuRouter.router;
