import { key } from "./config";
import request from "request";

export const deleteRecipt = (data: any) => {
  const url = `https://api.holded.com/api/invoicing/v1/documents/salesreceipt/${data.holdedID}`;
  const optiones = {
    method: "DELETE",
    url: url,
    headers: { Accept: "application/json", key: key },
  };
  request(optiones, function (error, responses) {
    if (error) throw new Error(error);
  });

  const urlpartner = `https://api.holded.com/api/invoicing/v1/documents/purchase/${data.holdedPartnerID}`;
  const optionespartner = {
    method: "DELETE",
    url: urlpartner,
    headers: { Accept: "application/json", key: key },
  };
  request(optionespartner, function (error, responses) {
    if (error) throw new Error(error);
  });
};
