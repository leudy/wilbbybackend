declare namespace Express {
  export interface Request {
    userId: String;
    usuarioActual;
    auth;
    user;
    ServerResponse;
  }
}
