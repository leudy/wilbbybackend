"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_1 = __importDefault(require("../models/user"));
const riders_1 = __importDefault(require("../models/riders"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const sendNotification_1 = require("./sendNotification");
class OnesignalRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get("/save-userid-notification", (req, res) => {
            const { OnesignalID, id, city } = req.query;
            if (OnesignalID && id) {
                user_1.default
                    .findOneAndUpdate({ _id: id }, { $set: { OnesignalID: OnesignalID, city: city } }, (err, user) => { })
                    .catch((err) => {
                    console.log(err);
                });
            }
        });
        this.router.post("/save-riders-notification", (req, res) => {
            const { OnesignalID, id } = req.body;
            console.log(OnesignalID, id);
            if (OnesignalID && id) {
                riders_1.default
                    .findOneAndUpdate({ _id: id }, { OnesignalID: OnesignalID }, { new: true }, (err, user) => {
                    res.status(200).json({ success: true }).end();
                })
                    .catch((err) => {
                    console.log(err);
                });
            }
        });
        this.router.get("/save-restaurant-notification", (req, res, nex) => __awaiter(this, void 0, void 0, function* () {
            const { OnesignalID, id, sendTest } = req.query;
            if (OnesignalID && id) {
                restaurant_1.default
                    .findOneAndUpdate({ _id: id }, { $set: { OnesignalID: OnesignalID } }, (err, user) => { })
                    .then(() => {
                    if (sendTest === "true") {
                        sendNotification_1.sendNotificationStore(OnesignalID, "Notificaciones activada con exito", "Notificaciones activada");
                    }
                })
                    .catch(() => console.log({ data: "Error save id" }));
            }
            res.status(200).end();
        }));
        this.router.get("/send-push-notification", (req, res) => {
            const { IdOnesignal, textmessage } = req.query;
            var sendNotification = function (data) {
                var headers = {
                    "Content-Type": "application/json; charset=utf-8",
                };
                var options = {
                    host: "onesignal.com",
                    port: 443,
                    path: "/api/v1/notifications",
                    method: "POST",
                    headers: headers,
                };
                var https = require("https");
                var req = https.request(options, function (res) {
                    res.on("data", function (data) {
                        console.log("Response:");
                    });
                });
                req.on("error", function (e) {
                    console.log("ERROR:");
                    console.log(e);
                });
                req.write(JSON.stringify(data));
                req.end();
            };
            var message = {
                app_id: "db79b975-e551-4741-ae43-8968ceab5f09",
                headings: { en: "Wilbby" },
                android_channel_id: "cfc50b79-a76a-4114-8200-0b53d6d7bd30",
                contents: { en: `${textmessage}` },
                ios_badgeCount: 1,
                ios_badgeType: "Increase",
                include_player_ids: [`${IdOnesignal}`],
            };
            sendNotification(message);
        });
        this.router.get("/send-push-notification-riders", (req, res) => {
            const { IdOnesignal, textmessage } = req.query;
            var sendNotification = function (data) {
                var headers = {
                    "Content-Type": "application/json; charset=utf-8",
                };
                var options = {
                    host: "onesignal.com",
                    port: 443,
                    path: "/api/v1/notifications",
                    method: "POST",
                    headers: headers,
                };
                var https = require("https");
                var req = https.request(options, function (res) {
                    res.on("data", function (data) {
                        console.log("Response:");
                    });
                });
                req.on("error", function (e) {
                    console.log("ERROR:");
                    console.log(e);
                });
                req.write(JSON.stringify(data));
                req.end();
            };
            var message = {
                app_id: "e21467de-eaf6-4090-9b6c-a311d37c0b5e",
                headings: { en: "Wilbby" },
                android_channel_id: "9b0682ea-6f2d-4228-8802-280df76cb033",
                ios_sound: "despertador_minion.wav",
                contents: { en: `${textmessage}` },
                ios_badgeCount: 1,
                ios_badgeType: "Increase",
                include_player_ids: [`${IdOnesignal}`],
            };
            sendNotification(message);
        });
        this.router.post("/send-push-notification-restaurant", (req, res) => {
            const { IdOnesignal, textmessage } = req.body;
            var sendNotification = function (data) {
                var headers = {
                    "Content-Type": "application/json; charset=utf-8",
                };
                var options = {
                    host: "onesignal.com",
                    port: 443,
                    path: "/api/v1/notifications",
                    method: "POST",
                    headers: headers,
                };
                var https = require("https");
                var req = https.request(options, function (res) {
                    res.on("data", function (data) {
                        console.log("Response:");
                    });
                });
                req.on("error", function (e) {
                    console.log("ERROR:");
                    console.log(e);
                });
                req.write(JSON.stringify(data));
                req.end();
            };
            var message = {
                app_id: "9a1893e6-8423-43cc-807a-d536a833b8e8",
                headings: { en: "Nuevo pedido Wilbby" },
                android_channel_id: "9009455c-8429-41f2-bb18-bd63a08a9d63",
                ios_sound: "despertador_minion.wav",
                contents: { en: `${textmessage}` },
                ios_badgeCount: 1,
                ios_badgeType: "Increase",
                include_player_ids: [IdOnesignal],
            };
            sendNotification(message);
            res.send({
                data: "Restaurante notificado de tu pedido",
                success: true,
            });
        });
        this.router.post("/send-notification", (req, res) => {
            const { onesignalID, title, messages } = req.body;
            var sendNotification = function (data) {
                var headers = {
                    "Content-Type": "application/json; charset=utf-8",
                };
                var options = {
                    host: "onesignal.com",
                    port: 443,
                    path: "/api/v1/notifications",
                    method: "POST",
                    headers: headers,
                };
                var https = require("https");
                var req = https.request(options, function (res) {
                    res.on("data", function (data) { });
                });
                req.on("error", function (e) {
                    console.log(e);
                });
                req.write(JSON.stringify(data));
                req.end();
            };
            var message = {
                app_id: "db79b975-e551-4741-ae43-8968ceab5f09",
                headings: { en: `${title}` },
                contents: { en: `${messages}` },
                android_channel_id: "cfc50b79-a76a-4114-8200-0b53d6d7bd30",
                ios_badgeCount: 1,
                ios_badgeType: "Increase",
                include_player_ids: [`${onesignalID}`],
            };
            sendNotification(message);
            res.json({ data: "Mensaje enviado con éxito", success: true });
        });
    }
}
const onesignalRouter = new OnesignalRouter();
onesignalRouter.routes();
exports.default = onesignalRouter.router;
