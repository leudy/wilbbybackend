"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Load the AWS SDK for Node.js
const aws_sdk_1 = __importDefault(require("aws-sdk"));
// Set the region
aws_sdk_1.default.config.update({ region: "eu-west-3" });
// Create S3 service object
var s3 = new aws_sdk_1.default.S3({ apiVersion: "2006-03-01" });
// call S3 to retrieve upload file to specified bucket
var uploadParams = { Bucket: "products-wilbby", Key: "", Body: "" };
var file = "";
// Configure the file stream and obtain the upload parameters
var fs = require("fs");
var fileStream = fs.createReadStream(file);
fileStream.on("error", function (err) {
    console.log("File Error", err);
});
uploadParams.Body = fileStream;
var path = require("path");
uploadParams.Key = path.basename(file);
// call S3 to retrieve upload file to specified bucket
s3.upload(uploadParams, function (err, data) {
    if (err) {
        console.log("Error", err);
    }
    if (data) {
        console.log("Upload Success", data.Location);
    }
});
