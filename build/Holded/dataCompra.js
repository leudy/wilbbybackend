"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.datasHoldedCompra = void 0;
exports.datasHoldedCompra = (data) => {
    const items = data.items.map((element) => {
        const t = element && element.items && element.items.subItems
            ? element.items.subItems
            : [];
        let pric = 0;
        let pric2 = 0;
        t.forEach((x) => {
            const r = x && x.subItems ? x.subItems : [];
            r.forEach((y) => {
                pric2 += y.price;
            });
            pric += x.price;
        });
        const precio = element.items.price + pric + pric2;
        const item = {
            name: element.items.name,
            desc: element.items.description,
            units: element.items.quantity,
            subtotal: precio / 100,
        };
        return item;
    });
    return {
        applyContactDefaults: false,
        contactCode: data.storeData && data.storeData.contactCode
            ? data.storeData.contactCode
            : "B09603440",
        desc: "Compra partner Wilbby",
        date: data.deliveryTime,
        dueDate: data.deliveryTime,
        notes: "Procentaje de tarifa no incluidos",
        items: items,
        invoiceNum: Number(data.channelOrderDisplayId),
        currency: "EUR",
    };
};
