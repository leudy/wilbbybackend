"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setToHolded = void 0;
const request_1 = __importDefault(require("request"));
const config_1 = require("./config");
const order_1 = __importDefault(require("../models/newOrder/order"));
const dataRider_1 = require("./dataRider");
exports.setToHolded = (data, total, id) => {
    var optionsRiders = {
        method: "POST",
        url: config_1.urlRiders,
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            key: config_1.key,
        },
        body: JSON.stringify(dataRider_1.datas(data, total, id)),
    };
    request_1.default(optionsRiders, function (error, response) {
        if (error)
            throw new Error(error);
        const respo = JSON.parse(response.body);
        order_1.default.findOneAndUpdate({ _id: id }, {
            $set: {
                holdedPartnerID: respo.id,
            },
        }, (err, order) => { });
    });
};
