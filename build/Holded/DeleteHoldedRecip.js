"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteRecipt = void 0;
const config_1 = require("./config");
const request_1 = __importDefault(require("request"));
exports.deleteRecipt = (data) => {
    const url = `https://api.holded.com/api/invoicing/v1/documents/salesreceipt/${data.holdedID}`;
    const optiones = {
        method: "DELETE",
        url: url,
        headers: { Accept: "application/json", key: config_1.key },
    };
    request_1.default(optiones, function (error, responses) {
        if (error)
            throw new Error(error);
    });
    const urlpartner = `https://api.holded.com/api/invoicing/v1/documents/purchase/${data.holdedPartnerID}`;
    const optionespartner = {
        method: "DELETE",
        url: urlpartner,
        headers: { Accept: "application/json", key: config_1.key },
    };
    request_1.default(optionespartner, function (error, responses) {
        if (error)
            throw new Error(error);
    });
};
