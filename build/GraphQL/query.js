"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Query = void 0;
const user_1 = __importDefault(require("../models/user"));
const categorias_1 = __importDefault(require("../models/categorias"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const Favorito_1 = __importDefault(require("../models/Favorito"));
const cupones_1 = __importDefault(require("../models/cupones"));
const mongoose_1 = require("mongoose");
const riders_1 = __importDefault(require("../models/riders"));
const Status_messages_1 = require("./Status_messages");
const rating_1 = __importDefault(require("../models/rating"));
const Opinion_1 = __importDefault(require("../models/Opinion"));
const Pago_1 = __importDefault(require("../models/Pago"));
const transacciones_1 = __importDefault(require("../models/transacciones"));
const adress_1 = __importDefault(require("../models/adress"));
const adressStore_1 = __importDefault(require("../models/adressStore"));
const tipo_1 = __importDefault(require("../models/tipo"));
const highkitchenCategory_1 = __importDefault(require("../models/highkitchenCategory"));
const tipotienda_1 = __importDefault(require("../models/tipotienda"));
const userAdmin_1 = __importDefault(require("../models/userAdmin"));
const post_1 = __importDefault(require("../models/post"));
const custonorder_1 = __importDefault(require("../models/custonorder"));
const cityclose_1 = __importDefault(require("../models/cityclose"));
const offerts_1 = __importDefault(require("../models/offerts"));
const trasactionRider_1 = __importDefault(require("../models/trasactionRider"));
const collections_1 = __importDefault(require("../models/collections"));
const subcollection_1 = __importDefault(require("../models/subcollection"));
const ads_1 = __importDefault(require("../models/ads"));
const moment_1 = __importDefault(require("moment"));
const coins_1 = __importDefault(require("../models/coins"));
//newProducts
const Menu_1 = __importDefault(require("../models/newMenu/Menu"));
const bundles_1 = __importDefault(require("../models/newMenu/bundles"));
const modifierGroups_1 = __importDefault(require("../models/newMenu/modifierGroups"));
const products_1 = __importDefault(require("../models/newMenu/products"));
const addToCart_1 = __importDefault(require("../models/newOrder/addToCart"));
const order_1 = __importDefault(require("../models/newOrder/order"));
const scheduleTime_1 = require("../funtions/scheduleTime");
const dotenv_1 = __importDefault(require("dotenv"));
const quincena_1 = __importDefault(require("../models/quincena"));
const console_1 = __importDefault(require("console"));
const modifiers_1 = __importDefault(require("../models/newMenu/modifiers"));
const { ObjectId } = mongoose_1.Types;
dotenv_1.default.config({ path: "variables.env" });
exports.Query = {
    getRiderForAdmin: (root, { city }, { usuarioActual }) => {
        return new Promise((resolve, rejects) => {
            riders_1.default.find({ city: city }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: {},
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRiderForAdminAll: (root, {}, { usuarioActual }) => {
        return new Promise((resolve, rejects) => {
            riders_1.default.find((err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: {},
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getUsuario: (root, {}, { usuarioActual }) => {
        if (!usuarioActual) {
            return {
                success: false,
                messages: Status_messages_1.STATUS_MESSAGES.NOT_LOGGED_IN,
                data: {},
            };
        }
        else {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: usuarioActual._id }, (err, res) => {
                    if (err) {
                        rejects({
                            messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                            success: false,
                            data: {},
                        });
                    }
                    else {
                        resolve({
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            success: true,
                            data: res,
                        });
                    }
                });
            });
        }
    },
    getUsuarioAdmin: (root, {}, { usuarioActual }) => {
        return new Promise((resolve, rejects) => {
            userAdmin_1.default.findOne({ _id: usuarioActual && usuarioActual._id }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: {},
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRiders: (root, { id }) => {
        if (!id) {
            return {
                success: false,
                messages: Status_messages_1.STATUS_MESSAGES.NOT_LOGGED_IN,
                data: {},
            };
        }
        else {
            return new Promise((resolve, rejects) => {
                riders_1.default.findOne({ _id: id }, (err, res) => {
                    if (err) {
                        rejects({
                            messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                            success: false,
                            data: {},
                        });
                    }
                    else {
                        resolve({
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            success: true,
                            data: res,
                        });
                    }
                });
            });
        }
    },
    obtenerUsuario: (root, args, { usuarioActual }) => {
        if (!usuarioActual) {
            return null;
        }
        const usuario = user_1.default.findOne({ _id: usuarioActual._id });
        return usuario;
    },
    obtenerAdmin: (root, args, { usuarioActual }) => {
        if (!usuarioActual) {
            return null;
        }
        const usuario = userAdmin_1.default.findOne({ _id: usuarioActual._id });
        return usuario;
    },
    getCategory: (root, { city }) => {
        return new Promise((resolve, rejects) => {
            categorias_1.default.find({
                excludeCity: { $nin: [city] },
                visible: true,
            }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    const categorys = res;
                    categorys.sort((a, b) => a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0);
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: categorys,
                    });
                }
            });
        });
    },
    getCategoryLargue: (root, { city, limit }) => {
        return new Promise((resolve, rejects) => {
            categorias_1.default
                .find({
                excludeCity: { $nin: [city] },
                visible: true,
                largue: true,
            }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    const categorys = res;
                    categorys.sort((a, b) => a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0);
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: categorys,
                    });
                }
            })
                .limit(limit);
        });
    },
    getCategorySmall: (root, { city }) => {
        return new Promise((resolve, rejects) => {
            categorias_1.default.find({
                excludeCity: { $nin: [city] },
                visible: true,
                largue: false,
            }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    const categorys = res;
                    categorys.sort((a, b) => a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0);
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: categorys,
                    });
                }
            });
        });
    },
    getTipo: (root) => {
        return new Promise((resolve, rejects) => {
            tipo_1.default.find((err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getHighkitchenCategory: (root) => {
        return new Promise((resolve, rejects) => {
            highkitchenCategory_1.default.find((err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getTipoTienda: (root) => {
        return new Promise((resolve, rejects) => {
            tipotienda_1.default.find((err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurantHighkitchen: (root, { city, llevar, category }) => {
        let condition = {
            includeCity: { $all: [city] },
            highkitchen: true,
            open: true,
        };
        //@ts-ignore
        if (llevar)
            condition.llevar = llevar;
        // @ts-ignore
        if (category)
            condition.tipo = category;
        return new Promise((resolve, rejects) => {
            restaurant_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    const stores = res;
                    stores.sort((a, b) => {
                        return scheduleTime_1.scheduleTime(a.schedule) === scheduleTime_1.scheduleTime(b.schedule)
                            ? 0
                            : scheduleTime_1.scheduleTime(a.schedule)
                                ? -1
                                : 1;
                    });
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: stores,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    },
    getRestaurant: (root, { city }) => {
        let condition = {
            includeCity: { $all: [city] },
        };
        return new Promise((resolve, rejects) => {
            restaurant_1.default.find(condition, (err, res) => {
                if (err) {
                    console_1.default.error(err);
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getAdress: (root, { id }) => {
        if (!id) {
            return {
                success: false,
                messages: "Aún no has iniciado sesión",
                data: null,
            };
        }
        return new Promise((resolve, rejects) => {
            adress_1.default
                .find({ usuario: id }, (err, res) => {
                if (err) {
                    rejects({
                        success: false,
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        data: {},
                    });
                }
                else {
                    resolve({
                        success: true,
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    },
    getAdressStore: (root, { id, city }) => {
        return new Promise((resolve, rejects) => {
            let condition = { store: id, ciudad: city };
            //@ts-ignore
            adressStore_1.default
                .findOne(condition, (err, res) => {
                if (err) {
                    rejects({
                        success: false,
                        message: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        data: {},
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    },
    getAdressStoreforSelect: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            let condition = { store: id };
            adressStore_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        success: false,
                        message: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        data: {},
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    },
    getRestaurantSearch: (root, { city, price, category, search, llevar, tipo }) => {
        let condition = {
            city: city,
            open: true,
            $text: { $search: `"\"${search} \""` },
        };
        // @ts-ignore
        if (price)
            condition.minime = { $lt: price };
        //@ts-ignore
        if (llevar)
            condition.llevar = llevar;
        // @ts-ignore
        if (category)
            condition.categoryID = { $all: [category] };
        // @ts-ignore
        if (tipo)
            condition.tipo = { $all: [tipo] };
        return new Promise((resolve, rejects) => {
            restaurant_1.default.find(condition, (err, res) => {
                if (err) {
                    console_1.default.log(err);
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    const stores = res;
                    stores.sort((a, b) => {
                        return scheduleTime_1.scheduleTime(a.schedule) === scheduleTime_1.scheduleTime(b.schedule)
                            ? 0
                            : scheduleTime_1.scheduleTime(a.schedule)
                                ? -1
                                : 1;
                    });
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: stores,
                    });
                }
            });
        });
    },
    getRestaurantSearchWeb: (root, { city, category, search, llevar, tipo, page }) => {
        let condition = { includeCity: { $all: [city] }, open: true };
        // @ts-ignore
        if (search)
            condition = { $text: { $search: `"\"${search} \""` } };
        //@ts-ignore
        if (llevar)
            condition.llevar = llevar;
        // @ts-ignore
        if (category)
            condition.categoryID = { $all: [category] };
        // @ts-ignore
        if (tipo)
            condition.tipo = { $all: [tipo] };
        return new Promise((resolve, rejects) => {
            restaurant_1.default
                .find(condition, (err, res) => {
                if (err) {
                    console_1.default.log(err);
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    const stores = res;
                    stores.sort((a, b) => {
                        return scheduleTime_1.scheduleTime(a.schedule) === scheduleTime_1.scheduleTime(b.schedule)
                            ? 0
                            : scheduleTime_1.scheduleTime(a.schedule)
                                ? -1
                                : 1;
                    });
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: stores,
                    });
                }
            })
                .sort(search ? null : { $natural: -1 })
                .limit(21 * 1)
                .skip((page - 1) * 21)
                .exec();
        });
    },
    getRestaurantSearchWebPAgination: (root, { city, category }) => {
        let condition = { includeCity: { $all: [city] }, open: true };
        // @ts-ignore
        if (category)
            condition.categoryID = { $all: [category] };
        return new Promise((resolve, rejects) => {
            restaurant_1.default.find(condition, (err, res) => {
                if (err) {
                    console_1.default.log(err);
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurantForCategory: (root, { city, category, tipo, llevar, search, page, limit, star, offert }) => {
        return new Promise((resolve, rejects) => {
            let condition = { includeCity: { $all: [city] }, open: true };
            // @ts-ignore
            if (search)
                condition = { $text: { $search: `"\"${search} \""` } };
            //@ts-ignore
            if (llevar)
                condition.llevar = llevar;
            //@ts-ignore
            if (tipo)
                condition.tipo = { $all: [tipo] };
            // @ts-ignore
            if (category)
                condition.categoryID = { $all: [category] };
            // @ts-ignore
            if (star)
                condition.rating = star;
            // @ts-ignore
            if (offert)
                condition.inOffert = offert;
            restaurant_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    const stores = res;
                    stores.sort((a, b) => {
                        return scheduleTime_1.scheduleTime(a.schedule) === scheduleTime_1.scheduleTime(b.schedule)
                            ? 0
                            : scheduleTime_1.scheduleTime(a.schedule)
                                ? -1
                                : 1;
                    });
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: stores,
                    });
                }
            })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .exec();
        });
    },
    getNewStoreList: (root, { city, category, tipo, search }) => {
        return new Promise((resolve, rejects) => {
            let condition = { includeCity: { $all: [city] }, open: true };
            // @ts-ignore
            if (search)
                condition = { includeCity: { $all: [city] }, open: true, $text: { $search: `"\"${search} \""` } };
            //@ts-ignore
            if (tipo)
                condition.tipo = { $all: [tipo] };
            // @ts-ignore
            if (category)
                condition.categoryID = { $all: [category] };
            restaurant_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    const stores = res;
                    stores.sort((a, b) => {
                        return scheduleTime_1.scheduleTime(a.schedule) === scheduleTime_1.scheduleTime(b.schedule)
                            ? 0
                            : scheduleTime_1.scheduleTime(a.schedule)
                                ? -1
                                : 1;
                    });
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: stores,
                    });
                }
            });
        });
    },
    getRestaurantForID: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            restaurant_1.default.findOne({ _id: id }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: {},
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurantForIDWeb: (root, { id, city }) => {
        return new Promise((resolve, rejects) => {
            restaurant_1.default.findOne({ _id: id, includeCity: { $all: [city] } }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: {},
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurantForDetails: (root, { slug, city }) => {
        return new Promise((resolve, rejects) => {
            restaurant_1.default.findOne({ slug: slug, includeCity: { $all: [city] } }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: {},
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurantForSlugWeb: (root, { slug }) => {
        return new Promise((resolve, rejects) => {
            restaurant_1.default.findOne({ slug: slug }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: {},
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurantFavorito: (root, { id }) => {
        return new Promise((resolve, reject) => {
            Favorito_1.default.find({ usuarioId: id }, (error, favourite) => {
                if (error) {
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: favourite,
                    });
                }
            });
        });
    },
    getStoreInOffert: (root, { city, page, limit }) => {
        return new Promise((resolve, rejects) => {
            let condition = {
                inOffert: true,
                includeCity: { $all: [city] },
                categoryID: { $all: ["5fb7a32cb234a46c09297804"] },
                open: true,
            };
            restaurant_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    const stores = res;
                    stores.sort((a, b) => {
                        return scheduleTime_1.scheduleTime(a.schedule) === scheduleTime_1.scheduleTime(b.schedule)
                            ? 0
                            : scheduleTime_1.scheduleTime(a.schedule)
                                ? -1
                                : 1;
                    });
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: stores,
                    });
                }
            })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .sort({ $natural: -1 })
                .exec();
        });
    },
    getStorePromo: (root, { city, categoryID, page, limit }) => {
        return new Promise((resolve, rejects) => {
            let condition = {
                includeCity: { $all: [city] },
                categoryID: { $all: [categoryID] },
                tagOffert: { $ne: null },
                open: true,
            };
            restaurant_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    const stores = res;
                    stores.sort((a, b) => {
                        return scheduleTime_1.scheduleTime(a.schedule) === scheduleTime_1.scheduleTime(b.schedule)
                            ? 0
                            : scheduleTime_1.scheduleTime(a.schedule)
                                ? -1
                                : 1;
                    });
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: stores,
                    });
                }
            })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .sort({ $natural: -1 })
                .exec();
        });
    },
    getStoreNew: (root, { city, page, limit }) => {
        return new Promise((resolve, rejects) => {
            let condition = {
                isnew: true,
                includeCity: { $all: [city] },
                open: true,
            };
            restaurant_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    const stores = res;
                    stores.sort((a, b) => {
                        return scheduleTime_1.scheduleTime(a.schedule) === scheduleTime_1.scheduleTime(b.schedule)
                            ? 0
                            : scheduleTime_1.scheduleTime(a.schedule)
                                ? -1
                                : 1;
                    });
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: stores,
                    });
                }
            })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .sort({ $natural: -1 })
                .exec();
        });
    },
    getFarmacyOffert: (root, { city }) => {
        return new Promise((resolve, rejects) => {
            let condition = {
                inOffert: true,
                includeCity: { $all: [city] },
                categoryID: { $all: ["5fb7aeaab234a46c0929780a"] },
                open: true,
            };
            restaurant_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    const stores = res;
                    stores.sort((a, b) => {
                        return scheduleTime_1.scheduleTime(a.schedule) === scheduleTime_1.scheduleTime(b.schedule)
                            ? 0
                            : scheduleTime_1.scheduleTime(a.schedule)
                                ? -1
                                : 1;
                    });
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: stores,
                    });
                }
            })
                .limit(2)
                .sort({ $natural: -1 });
        });
    },
    getSalvingPackInOffert: (root, { city }) => {
        return new Promise((resolve, rejects) => {
            let condition = {
                "salvingPack.isSavingPack": true,
                includeCity: { $all: [city] },
                open: true,
            };
            restaurant_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    const stores = res;
                    stores.sort((a, b) => {
                        return scheduleTime_1.scheduleTime(a.schedule) === scheduleTime_1.scheduleTime(b.schedule)
                            ? 0
                            : scheduleTime_1.scheduleTime(a.schedule)
                                ? -1
                                : 1;
                    });
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: stores,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    },
    getTiendaInOffert: (root, { city, page, limit }) => {
        return new Promise((resolve, rejects) => {
            let condition = {
                inOffert: true,
                includeCity: { $all: [city] },
                categoryID: { $all: ["5fb7aec4b234a46c0929780b"] },
                open: true,
            };
            restaurant_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    const stores = res;
                    stores.sort((a, b) => {
                        return scheduleTime_1.scheduleTime(a.schedule) === scheduleTime_1.scheduleTime(b.schedule)
                            ? 0
                            : scheduleTime_1.scheduleTime(a.schedule)
                                ? -1
                                : 1;
                    });
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: stores,
                    });
                }
            })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .sort({ $natural: -1 })
                .exec();
        });
    },
    getRestaurantforEdit: (root, { city, search, page, limit }) => {
        let condition = {
            city: city,
        };
        // @ts-ignore
        if (search)
            condition = {
                city: city,
                //@ts-ignore
                $text: { $search: `"\"${search} \""` },
            };
        return new Promise((resolve, rejects) => {
            restaurant_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .sort(search ? null : { $natural: -1 })
                .exec();
        });
    },
    getCupon: (root, { clave }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            if (!usuarioActual) {
                return {
                    success: false,
                    message: "Debe iniciar sesión para continuar",
                    data: null,
                };
            }
            const usuario = yield user_1.default.findOne({
                _id: usuarioActual._id,
            });
            if (!usuario) {
                return {
                    success: false,
                    message: "Debe iniciar sesión para continuar",
                    data: null,
                };
            }
            return new Promise((resolve, reject) => {
                cupones_1.default.findOne({ clave }, (error, cupon) => {
                    if (error) {
                        return reject(error);
                    }
                    else {
                        return resolve(cupon);
                    }
                });
            });
        }
        catch (error) {
            return {
                success: false,
                message: "Hay un problema con su solicitud",
                data: null,
            };
        }
    }),
    getCuponAll: (root) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            return new Promise((resolve, reject) => {
                cupones_1.default.findOne((error, cupon) => {
                    if (error) {
                        return reject(error);
                    }
                    else {
                        return resolve(cupon);
                    }
                });
            });
        }
        catch (error) {
            return {
                success: false,
                message: "Hay un problema con su solicitud",
                data: null,
            };
        }
    }),
    getValoraciones: (root, { restaurant }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            return new Promise((resolve, reject) => {
                rating_1.default.find({ restaurant: restaurant }, (error, valoracion) => {
                    if (error) {
                        return reject(error);
                    }
                    else {
                        resolve({
                            messages: "Datos Obtenidos con éxito",
                            success: true,
                            data: valoracion,
                        });
                    }
                })
                    .limit(100)
                    .sort({ $natural: -1 });
            });
        }
        catch (error) {
            return {
                success: false,
                message: "Hay un problema con su solicitud",
                data: null,
            };
        }
    }),
    getValoracionesStore: (root, { id, page, limit }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            return new Promise((resolve, reject) => {
                rating_1.default.find({ restaurant: id }, (error, valoracion) => {
                    if (error) {
                        return reject(error);
                    }
                    else {
                        resolve({
                            messages: "Datos Obtenidos con éxito",
                            success: true,
                            data: valoracion,
                        });
                    }
                })
                    .limit(limit * 1)
                    .skip((page - 1) * 20)
                    .sort({ $natural: -1 })
                    .exec();
            });
        }
        catch (error) {
            return {
                success: false,
                message: "Hay un problema con su solicitud",
                data: null,
            };
        }
    }),
    getOpinion: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            Opinion_1.default.find({ plato: id }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            }).sort({ $natural: -1 });
        });
    },
    getPago: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            Pago_1.default.findOne({ restaurantID: id }, (error, pagos) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                        data: null,
                    });
                else
                    resolve({
                        messages: "Operacion realizada con éxito",
                        success: true,
                        data: pagos,
                    });
            });
        });
    }),
    getTransaction: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            transacciones_1.default
                .find({ restaurantID: id })
                .sort({ $natural: -1 })
                .exec((error, deposito) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                        list: [],
                    });
                else
                    resolve({
                        messages: "solicitud procesada con éxito",
                        success: true,
                        list: deposito,
                    });
            });
        });
    }),
    getStatistics: (root, { id }) => {
        return new Promise((resolve, reject) => {
            let matchQuery1 = {
                restaurant: id,
                estado: {
                    $in: ["Entregada", "Valorada"],
                },
            };
            let matchQuery2 = {
                restaurant: id,
                estado: "Rechazada",
            };
            order_1.default
                .aggregate([
                { $match: matchQuery1 },
                {
                    $group: {
                        _id: {
                            month: { $substr: ["$created_at", 5, 2] },
                        },
                        paypalAmount: { $first: "$total" },
                        stripeAmount: { $first: "$total" },
                        finishedCount: { $sum: 1 },
                        created_at: { $first: "$created_at" },
                    },
                },
                {
                    $project: {
                        your_year_variable: { $year: "$created_at" },
                        paypalAmount: 1,
                        finishedCount: 1,
                        stripeAmountDivide: { $divide: [Number("$stripeAmount"), 100] },
                    },
                },
                { $match: { your_year_variable: 2021 } },
            ])
                .then((res) => {
                let ordenes = {
                    name: "Pedidos",
                    Ene: 0,
                    Feb: 0,
                    Mar: 0,
                    Abr: 0,
                    May: 0,
                    Jun: 0,
                    Jul: 0,
                    Aug: 0,
                    Sep: 0,
                    Oct: 0,
                    Nov: 0,
                    Dic: 0,
                };
                let ganacias = {
                    name: "Total Ventas",
                    Ene: 0,
                    Feb: 0,
                    Mar: 0,
                    Abr: 0,
                    May: 0,
                    Jun: 0,
                    Jul: 0,
                    Aug: 0,
                    Sep: 0,
                    Oct: 0,
                    Nov: 0,
                    Dic: 0,
                };
                let devoluciones = {
                    name: "Pedidos rechazados",
                    Ene: 0,
                    Feb: 0,
                    Mar: 0,
                    Abr: 0,
                    May: 0,
                    Jun: 0,
                    Jul: 0,
                    Aug: 0,
                    Sep: 0,
                    Oct: 0,
                    Nov: 0,
                    Dic: 0,
                };
                for (let i = 0; i < res.length; i++) {
                    let month = res[i]._id.month;
                    if (month == "01") {
                        ganacias["Ene"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Ene"] = res[i].finishedCount;
                    }
                    else if (month == "02") {
                        ganacias["Feb"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Feb"] = res[i].finishedCount;
                    }
                    else if (month == "03") {
                        ganacias["Mar"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Mar"] = res[i].finishedCount;
                    }
                    else if (month == "04") {
                        ganacias["Abr"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Abr"] = res[i].finishedCount;
                    }
                    else if (month == "05") {
                        ganacias["May"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["May"] = res[i].finishedCount;
                    }
                    else if (month == "06") {
                        ganacias["Jun"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Jun"] = res[i].finishedCount;
                    }
                    else if (month == "07") {
                        ganacias["Jul"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Jul"] = res[i].finishedCount;
                    }
                    else if (month == "08") {
                        ganacias["Aug"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Aug"] = res[i].finishedCount;
                    }
                    else if (month == "09") {
                        ganacias["Sep"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Sep"] = res[i].finishedCount;
                    }
                    else if (month == "10") {
                        ganacias["Oct"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Oct"] = res[i].finishedCount;
                    }
                    else if (month == "11") {
                        ganacias["Nov"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Nov"] = res[i].finishedCount;
                    }
                    else if (month == "12") {
                        ganacias["Dic"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Dic"] = res[i].finishedCount;
                    }
                }
                order_1.default
                    .aggregate([
                    { $match: matchQuery2 },
                    {
                        $group: {
                            _id: {
                                month: { $substr: ["$created_at", 5, 2] },
                                estado: "Rechazada",
                            },
                            returnedCount: { $sum: 1 },
                            created_at: { $first: "$created_at" },
                        },
                    },
                    {
                        $project: {
                            your_year_variable: { $year: "$created_at" },
                            returnedCount: 1,
                        },
                    },
                    { $match: { your_year_variable: 2021 } },
                ])
                    .then((res1) => {
                    for (let i = 0; i < res1.length; i++) {
                        let month = res1[i]._id.month;
                        if (month == "01") {
                            devoluciones["Ene"] = res1[i].returnedCount;
                        }
                        else if (month == "02") {
                            devoluciones["Feb"] = res1[i].returnedCount;
                        }
                        else if (month == "03") {
                            devoluciones["Mar"] = res1[i].returnedCount;
                        }
                        else if (month == "04") {
                            devoluciones["Abr"] = res1[i].returnedCount;
                        }
                        else if (month == "05") {
                            devoluciones["May"] = res1[i].returnedCount;
                        }
                        else if (month == "06") {
                            devoluciones["Jun"] = res1[i].returnedCount;
                        }
                        else if (month == "07") {
                            devoluciones["Jul"] = res1[i].returnedCount;
                        }
                        else if (month == "08") {
                            devoluciones["Aug"] = res1[i].returnedCount;
                        }
                        else if (month == "09") {
                            devoluciones["Sep"] = res1[i].returnedCount;
                        }
                        else if (month == "10") {
                            devoluciones["Oct"] = res1[i].returnedCount;
                        }
                        else if (month == "11") {
                            devoluciones["Nov"] = res1[i].returnedCount;
                        }
                        else if (month == "12") {
                            devoluciones["Dic"] = res1[i].returnedCount;
                        }
                    }
                    resolve({
                        success: true,
                        message: "",
                        data: [ordenes, ganacias, devoluciones],
                    });
                });
            })
                .catch((err) => { });
        });
    },
    getPost: (root, { country }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            post_1.default
                .findOne({ country: country }, (error, post) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        message: "Operacion realizada con éxito",
                        data: post,
                    });
            })
                .sort({ $natural: -1 });
        });
    }),
    getPosts: (root, { country, page }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            post_1.default
                .find({ country: country }, (error, post) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        message: "Operacion realizada con éxito",
                        data: post,
                    });
            })
                .limit(20 * 1)
                .skip((page - 1) * 20)
                .sort({ $natural: -1 })
                .exec();
        });
    }),
    getPostsForTags: (root, { country, page, tag }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            post_1.default
                .find({ country: country, tags: { $all: [tag] } }, (error, post) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        message: "Operacion realizada con éxito",
                        data: post,
                    });
            })
                .limit(20 * 1)
                .skip((page - 1) * 20)
                .sort({ $natural: -1 })
                .exec();
        });
    }),
    getPostbyId: (root, { slug, country }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            post_1.default.findOne({ slug: slug, country: country }, (error, post) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        message: "Operacion realizada con éxito",
                        data: post,
                    });
            });
        });
    }),
    getCustomOrder: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                userID: id,
                estado: {
                    $in: [
                        "Pagado",
                        "Asignada",
                        "Aceptada",
                        "En camino",
                        "Entregado",
                        "Rechazada por el rider",
                        "Rechazada",
                        "Valorada",
                    ],
                },
            };
            custonorder_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        messages: "Hay un problema con su solicitud",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        messages: "Operacion realizada con éxito",
                        data: order,
                    });
            })
                .sort({ $natural: -1 });
        });
    }),
    getCustomOrderRider: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                riders: id,
                estado: {
                    $in: [
                        "Pagado",
                        "Asignada",
                        "Aceptada",
                        "En camino",
                        "Entregado",
                        "Valorada",
                    ],
                },
            };
            custonorder_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        messages: "Hay un problema con su solicitud",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        messages: "Operacion realizada con éxito",
                        data: order,
                    });
            })
                .sort({ $natural: -1 });
        });
    }),
    getCustomOrderRiderID: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            custonorder_1.default.findOne({ _id: id }, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        messages: "Hay un problema con su solicitud",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        messages: "Operacion realizada con éxito",
                        data: order,
                    });
            });
        });
    }),
    getCustomOrderAdmin: (root, { city, orderID, dateRange, limit, page }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                estado: { $ne: "Pendiente de pago" },
            };
            // @ts-ignore
            if (city)
                condition.city = city;
            if (orderID)
                // @ts-ignore
                condition.display_id = orderID;
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            custonorder_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        messages: "Hay un problema con su solicitud",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        messages: "Operacion realizada con éxito",
                        data: order,
                    });
            })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .sort({ $natural: -1 });
        });
    }),
    getCity: (root, { city }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                city: city,
            };
            cityclose_1.default.findOne(condition, (error, city) => {
                if (error)
                    reject({
                        success: false,
                        messages: "Aun no hemos llegado a esta zona",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        messages: "Ciudad operativa",
                        data: city,
                    });
            });
        });
    }),
    getOfferts: (root, { city }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                city: city,
            };
            offerts_1.default.find(condition, (error, city) => {
                if (error)
                    reject({
                        success: false,
                        messages: "Aun no tenemos ofertas en esta zona",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        messages: "Ofertas obtenidas con éxito",
                        data: city,
                    });
            });
        });
    }),
    getCustomers: (root, { page, limit, email }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        let condition = {};
        // @ts-ignore
        if (email)
            condition.email = email;
        var value = yield user_1.default.countDocuments();
        return new Promise((resolve, reject) => {
            user_1.default
                .find(condition, (error, user) => {
                if (error)
                    reject({
                        success: false,
                        messages: "Hay un problema con su solicitud",
                        count: 0,
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        messages: "Operacion realizada con éxito",
                        count: value,
                        data: user,
                    });
            })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .sort({ $natural: -1 })
                .exec();
        });
    }),
    getAllCustomers: (root, { email }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        let condition = {};
        // @ts-ignore
        if (email)
            condition.email = email;
        return new Promise((resolve, reject) => {
            user_1.default.find(condition, (error, user) => {
                if (error)
                    reject({
                        success: false,
                        messages: "Hay un problema con su solicitud",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        messages: "Operacion realizada con éxito",
                        data: user,
                    });
            });
        });
    }),
    getRiderTransaction: (root, { id, dateRange, page }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                rider: id,
                created_at: {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                },
            };
            trasactionRider_1.default
                .find(condition, (error, list) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: list,
                    });
                }
            })
                .limit(20 * 1)
                .skip((page - 1) * 20)
                .sort({ $natural: -1 })
                .exec();
        });
    }),
    getQuincena: (root) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            quincena_1.default
                .findOne((error, quincena) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: {},
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        data: quincena,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    }),
    getQuincenaAll: (root) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            quincena_1.default
                .find((error, quincena) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: quincena,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    }),
    getCollection: (root, { store }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            collections_1.default.find({ store: { $all: [store] } }, (error, collection) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: [],
                    });
                else {
                    const collections = collection;
                    collections.sort((a, b) => a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0);
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        data: collections,
                    });
                }
            });
        });
    }),
    getSubCollection: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            subcollection_1.default.find({ collectiontype: id }, (error, collection) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: [],
                    });
                else {
                    const collections = collection;
                    collections.sort((a, b) => a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0);
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        data: collections,
                    });
                }
            });
        });
    }),
    getProductSubCollection: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            products_1.default
                .find({ parentId: id }, (error, product) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        data: product,
                    });
                }
            })
                .limit(3);
        });
    }),
    getNewMenu: (root, { id }) => {
        return new Promise((resolve, reject) => {
            Menu_1.default.find({ storeId: id }, (error, menu) => {
                if (error) {
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: menu,
                    });
                }
            });
        });
    },
    getNewProductoSearch: (root, { store, search, page, limit, products }) => {
        let condition = { snoozed: false, isBundled: false };
        // @ts-ignore
        if (products.length > 0)
            condition._id = products.map((p) => p);
        // @ts-ignore
        if (search)
            condition = { $text: { $search: `"\"${search} \""` } };
        // @ts-ignore
        if (store)
            condition.storeId = { $all: [store] };
        return new Promise((resolve, rejects) => {
            products_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .exec();
        });
    },
    getNewProducto: (root, { store, search }) => {
        return new Promise((resolve, rejects) => {
            let condition = { storeId: store, snoozed: false, isBundled: false };
            // @ts-ignore
            if (search)
                condition = { $text: { $search: `"\"${search} \""` } };
            products_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .sort(search ? null : { $natural: -1 })
                .exec();
        });
    },
    getNewProductoSearchStore: (root, { store, search }) => {
        return new Promise((resolve, rejects) => {
            let condition = {
                storeId: store,
                $text: { $search: `"\"${search} \""` },
            };
            products_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .exec();
        });
    },
    getNewProductoaAllStore: (root, { store }) => {
        return new Promise((resolve, rejects) => {
            let condition = {
                storeId: store,
                isBundled: false,
            };
            products_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 })
                .exec();
        });
    },
    getNewProductoBundled: (root, { store }) => {
        return new Promise((resolve, rejects) => {
            let condition = { storeId: store, isBundled: true };
            products_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 })
                .exec();
        });
    },
    getBundledStore: (root, { storeID }) => {
        return new Promise((resolve, rejects) => {
            let condition = { account: storeID };
            bundles_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 })
                .exec();
        });
    },
    getNewProductoModifield: (root, { store }) => {
        return new Promise((resolve, rejects) => {
            let condition = { account: store };
            modifiers_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 })
                .exec();
        });
    },
    getModifiedGroupStore: (root, { storeID }) => {
        return new Promise((resolve, rejects) => {
            let condition = { account: storeID };
            modifierGroups_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 })
                .exec();
        });
    },
    getNewCart: (root, { userId, storeId }) => {
        return new Promise((resolve, reject) => {
            addToCart_1.default.find({ userId: userId, storeId: storeId }, (error, cart) => {
                if (error) {
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: cart,
                    });
                }
            });
        });
    },
    getNewOrder: (root, { userId, status, dateRange }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                customer: userId,
                status: {
                    $in: status,
                },
            };
            //let condition = { cliente: "5d8556514c10b81c6a65295a" };
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            order_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Ordernes extraida con éxito",
                        list: order,
                    });
                }
            })
                .sort({ $natural: -1 })
                .limit(10 * 1)
                .skip((1 - 1) * 10);
        });
    }),
    getNewOrderforStore: (root, { storeId, status, search, dateRange, page, limit }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                store: storeId,
                status: {
                    $in: status,
                },
            };
            // @ts-ignore
            if (search)
                condition.channelOrderDisplayId = search;
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            order_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Ordernes extraida con éxito",
                        list: order,
                    });
                }
            })
                .limit(10 * 1)
                .skip((1 - 1) * 10)
                .sort({ $natural: -1 });
        });
    }),
    getNewOrderforStoreWeb: (root, { storeId, status, search, dateRange, page, limit }) => __awaiter(void 0, void 0, void 0, function* () {
        var value = yield order_1.default.countDocuments({ store: storeId });
        return new Promise((resolve, reject) => {
            let condition = {
                store: storeId,
                status: {
                    $in: status,
                },
            };
            // @ts-ignore
            if (search)
                condition.channelOrderDisplayId = search;
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            order_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        total: value,
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Ordernes extraida con éxito",
                        total: value,
                        list: order,
                    });
                }
            })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .sort({ $natural: -1 });
        });
    }),
    getNewOrderRider: (root, { riderId, status, dateRange, page }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                courier: riderId,
                status: {
                    $in: status,
                },
            };
            //let condition = { cliente: "5d8556514c10b81c6a65295a" };
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            order_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Ordernes extraida con éxito",
                        list: order,
                    });
                }
            })
                .limit(20 * 1)
                .skip((page - 1) * 20)
                .sort({ $natural: -1 });
        });
    }),
    getNewOrderBugetRider: (root, { riderId }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                budgetRider: { $all: [riderId] },
            };
            order_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Ordernes extraida con éxito",
                        list: order,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    }),
    getNewOrderforId: (root, { id }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                channelOrderDisplayId: id,
            };
            order_1.default.findOne(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Ordernes extraida con éxito",
                        data: order,
                    });
                }
            });
        });
    }),
    getNewOrderAdimin: (root, { status, dateRange, page, limit, orderID, Stores, city, rider }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        var value = yield order_1.default.countDocuments();
        return new Promise((resolve, reject) => {
            let condition = {
                status: {
                    $in: status,
                },
            };
            if (city) {
                condition = {
                    // @ts-ignore
                    "storeData.city": city,
                    status: {
                        $in: status,
                    },
                };
            }
            if (orderID)
                // @ts-ignore
                condition.channelOrderDisplayId = orderID;
            // @ts-ignore
            if (Stores)
                condition.store = { $in: Stores };
            // @ts-ignore
            if (rider.length > 0)
                condition.courier = { $in: rider };
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                const toDate = moment_1.default(dateRange.toDate)
                    .add(1, "day")
                    .format("MM/DD/YYYY");
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: toDate,
                };
            }
            if (dateRange && dateRange.fromDate && !dateRange.toDate) {
                const toDate = moment_1.default(dateRange.fromDate)
                    .add(1, "day")
                    .format("MM/DD/YYYY");
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lt: toDate,
                };
            }
            order_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        count: 0,
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Ordernes extraida con éxito",
                        count: value,
                        list: order,
                    });
                }
            })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .sort({ $natural: -1 });
        });
    }),
    getProducto: (root, { id, page, limit }) => {
        let condition = { parentId: id, snoozed: false };
        return new Promise((resolve, rejects) => {
            products_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .exec();
        });
    },
    getProductoForCategory: (root, { category }) => {
        let condition = { parentId: category, snoozed: false };
        return new Promise((resolve, rejects) => {
            products_1.default.find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getPrivatePromocode: (root, { id }) => {
        let condition = { user: id };
        return new Promise((resolve, rejects) => {
            cupones_1.default.find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getPromocode: (root, { city }) => {
        let condition = { private: false };
        // @ts-ignore
        if (city)
            condition.city = { $all: [city] };
        return new Promise((resolve, rejects) => {
            cupones_1.default.find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getNewProductoForApp: (root, { products, storeID }) => __awaiter(void 0, void 0, void 0, function* () {
        const ifOrdatic = yield restaurant_1.default.findOne({
            _id: storeID,
            isBundled: false,
        });
        return new Promise((resolve, rejects) => {
            if (ifOrdatic === null || ifOrdatic === void 0 ? void 0 : ifOrdatic.isOrdaticPartner) {
                products_1.default.find({
                    internalId: products.map((p) => {
                        return p;
                    }),
                    snoozed: false,
                }, (error, product) => {
                    if (error) {
                        rejects({
                            messages: "Algo salio mal",
                            success: false,
                            data: [],
                        });
                    }
                    else {
                        if (!product)
                            resolve({
                                messages: "Algo salio mal",
                                success: false,
                                data: [],
                            });
                        else {
                            resolve({
                                messages: "Productos obtenidos",
                                success: true,
                                data: product,
                            });
                        }
                    }
                });
            }
            else {
                products_1.default.find({
                    _id: products.map((p) => {
                        return p;
                    }),
                    snoozed: false,
                }, (error, product) => {
                    if (error) {
                        rejects({
                            messages: "Algo salio mal",
                            success: false,
                            data: [],
                        });
                    }
                    else {
                        if (!product)
                            resolve({
                                messages: "Algo salio mal",
                                success: false,
                                data: [],
                            });
                        else {
                            resolve({
                                messages: "Productos obtenidos",
                                success: true,
                                data: product,
                            });
                        }
                    }
                });
            }
        });
    }),
    getBundled: (root, { products, storeID }) => __awaiter(void 0, void 0, void 0, function* () {
        const storeType = yield restaurant_1.default.findOne({ _id: storeID });
        return new Promise((resolve, rejects) => {
            if (storeType === null || storeType === void 0 ? void 0 : storeType.isOrdaticPartner) {
                bundles_1.default.find({
                    internalId: products.map((p) => {
                        return p;
                    }),
                    snoozed: false,
                }, (error, product) => {
                    if (error) {
                        rejects({
                            messages: "Algo salio mal",
                            success: false,
                            data: [],
                        });
                    }
                    else {
                        if (!product)
                            resolve({
                                messages: "Algo salio mal",
                                success: false,
                                data: [],
                            });
                        else {
                            resolve({
                                messages: "Productos obtenidos",
                                success: true,
                                data: product,
                            });
                        }
                    }
                });
            }
            else {
                bundles_1.default.find({
                    _id: products.map((p) => {
                        return p;
                    }),
                    snoozed: false,
                }, (error, product) => {
                    if (error) {
                        rejects({
                            messages: "Algo salio mal",
                            success: false,
                            data: [],
                        });
                    }
                    else {
                        if (!product)
                            resolve({
                                messages: "Algo salio mal",
                                success: false,
                                data: [],
                            });
                        else {
                            resolve({
                                messages: "Productos obtenidos",
                                success: true,
                                data: product,
                            });
                        }
                    }
                });
            }
        });
    }),
    getModifieldGroup: (root, { products, storeID }) => __awaiter(void 0, void 0, void 0, function* () {
        const storeType = yield restaurant_1.default.findOne({ _id: storeID });
        return new Promise((resolve, rejects) => {
            if (storeType === null || storeType === void 0 ? void 0 : storeType.isOrdaticPartner) {
                modifierGroups_1.default.find({
                    internalId: products.map((p) => {
                        return p;
                    }),
                    snoozed: false,
                }, (error, product) => {
                    if (error) {
                        rejects({
                            messages: "Algo salio mal",
                            success: false,
                            data: [],
                        });
                    }
                    else {
                        if (!product)
                            resolve({
                                messages: "Algo salio mal",
                                success: false,
                                data: [],
                            });
                        else {
                            resolve({
                                messages: "Productos obtenidos",
                                success: true,
                                data: product,
                            });
                        }
                    }
                });
            }
            else {
                modifierGroups_1.default.find({
                    _id: products.map((p) => {
                        return p;
                    }),
                    snoozed: false,
                }, (error, product) => {
                    if (error) {
                        rejects({
                            messages: "Algo salio mal",
                            success: false,
                            data: [],
                        });
                    }
                    else {
                        if (!product)
                            resolve({
                                messages: "Algo salio mal",
                                success: false,
                                data: [],
                            });
                        else {
                            resolve({
                                messages: "Productos obtenidos",
                                success: true,
                                data: product,
                            });
                        }
                    }
                });
            }
        });
    }),
    getNewProductoForStore: (root, { products, storeID }) => __awaiter(void 0, void 0, void 0, function* () {
        const ifOrdatic = yield restaurant_1.default.findOne({
            _id: storeID,
            isBundled: false,
        });
        return new Promise((resolve, rejects) => {
            if (ifOrdatic === null || ifOrdatic === void 0 ? void 0 : ifOrdatic.isOrdaticPartner) {
                products_1.default.find({
                    internalId: products.map((p) => {
                        return p;
                    }),
                }, (error, product) => {
                    if (error) {
                        rejects({
                            messages: "Algo salio mal",
                            success: false,
                            data: [],
                        });
                    }
                    else {
                        if (!product)
                            resolve({
                                messages: "Algo salio mal",
                                success: false,
                                data: [],
                            });
                        else {
                            resolve({
                                messages: "Productos obtenidos",
                                success: true,
                                data: product,
                            });
                        }
                    }
                });
            }
            else {
                products_1.default.find({
                    _id: products.map((p) => {
                        return p;
                    }),
                }, (error, product) => {
                    if (error) {
                        rejects({
                            messages: "Algo salio mal",
                            success: false,
                            data: [],
                        });
                    }
                    else {
                        if (!product)
                            resolve({
                                messages: "Algo salio mal",
                                success: false,
                                data: [],
                            });
                        else {
                            resolve({
                                messages: "Productos obtenidos",
                                success: true,
                                data: product,
                            });
                        }
                    }
                });
            }
        });
    }),
    getAds: (root, { city }) => {
        let matchQuery1 = {
            includeCity: { $all: [city] },
        };
        return new Promise((resolve, rejects) => {
            ads_1.default.aggregate([{ $match: matchQuery1 }, { $sample: { size: 150 } }], (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: {},
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getAleatoryProduct: (root, { store }) => {
        let matchQuery1 = {
            account: store,
            snoozed: false,
            isBundled: false,
        };
        return new Promise((resolve, rejects) => {
            products_1.default.aggregate([{ $match: matchQuery1 }, { $sample: { size: 6 } }], (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: {},
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getNewOrderRepet: (root, { userId, city }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                customer: userId,
                "storeData.includeCity": { $all: [city] },
                status: { $in: ["Entregada", "Finalizada", "Devuelto"] },
            };
            order_1.default
                .find(condition, (error, order) => {
                if (error) {
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    var hash = {};
                    order = order.filter(function (current) {
                        var exists = !hash[current.store];
                        hash[current.store] = true;
                        return exists;
                    });
                    resolve({
                        success: true,
                        message: "Ordernes extraida con éxito",
                        list: order,
                    });
                }
            })
                .sort({ $natural: -1 })
                .limit(20 * 1)
                .skip((1 - 1) * 20)
                .exec();
        });
    }),
    getAllCoins: (root, { id, page, limit }) => {
        let condition = { user: id };
        return new Promise((resolve, rejects) => {
            coins_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        message: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        message: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .exec();
        });
    },
};
