"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.resolvers = void 0;
const query_1 = require("./query");
const mutation_1 = require("./mutation");
const user_1 = __importDefault(require("../models/user"));
const Favorito_1 = __importDefault(require("../models/Favorito"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const order_1 = __importDefault(require("../models/newOrder/order"));
const riders_1 = __importDefault(require("../models/riders"));
const userAdmin_1 = __importDefault(require("../models/userAdmin"));
const subcollection_1 = __importDefault(require("../models/subcollection"));
const category_1 = __importDefault(require("../models/newMenu/category"));
const modifierGroups_1 = __importDefault(require("../models/newMenu/modifierGroups"));
const modifiers_1 = __importDefault(require("../models/newMenu/modifiers"));
const products_1 = __importDefault(require("../models/newMenu/products"));
const scheduleTime_1 = require("../funtions/scheduleTime");
exports.resolvers = {
    Query: query_1.Query,
    Mutation: mutation_1.Mutation,
    NewOrderRepeat: {
        Restaurant(parent, args) {
            return new Promise((resolve, rejects) => {
                restaurant_1.default.findOne({ _id: parent.store, open: true }, (error, store) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!store)
                            resolve(null);
                        else {
                            resolve(store);
                        }
                    }
                });
            });
        },
    },
    Usuario: {
        numberCurrentOrder(parent, args) {
            return new Promise((resolve, rejects) => {
                order_1.default.find({
                    courier: parent._id,
                    status: {
                        $in: [
                            "Nueva",
                            "Confirmada",
                            "En la cocina",
                            "Listo para recoger",
                            "Preparando para el envío",
                            "En camino",
                        ],
                    },
                }, (error, order) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!order)
                            resolve(0);
                        else {
                            resolve(order.length);
                        }
                    }
                });
            });
        },
    },
    Cupon: {
        Store(parent, args) {
            return new Promise((resolve, rejects) => {
                restaurant_1.default.findOne({ _id: parent.store }, (error, store) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!store)
                            resolve(null);
                        else {
                            resolve(store);
                        }
                    }
                });
            });
        },
        Usuario(parent, args) {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: parent.user }, (error, user) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!user)
                            resolve(null);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
        canjeado(parent, args, { usuarioActual }) {
            return new Promise((resolve, rejects) => {
                order_1.default.findOne({
                    customer: usuarioActual ? usuarioActual._id : null,
                    cupon: parent.id,
                    status: { $ne: "Pendiente de pago" },
                }, (error, user) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(true);
                        }
                    }
                });
            });
        },
        vencido(parent, args) {
            return new Promise((resolve, rejects) => {
                if (parent.exprirable) {
                    if (parent.expire < new Date()) {
                        resolve(true);
                    }
                    else {
                        resolve(false);
                    }
                }
                else {
                    resolve(false);
                }
            });
        },
    },
    Menus: {
        Categories(parent, args) {
            return new Promise((resolve, rejects) => {
                category_1.default.find({ menu: parent._id }, (error, category) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!category)
                            resolve([]);
                        else {
                            const categorys = category;
                            categorys.sort((a, b) => a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0);
                            resolve(categorys);
                        }
                    }
                });
            });
        },
    },
    CategoryNew: {
        Products(parent, args) {
            return __awaiter(this, void 0, void 0, function* () {
                const storeType = yield restaurant_1.default.findOne({ _id: parent.storeId });
                return new Promise((resolve, rejects) => {
                    if (storeType === null || storeType === void 0 ? void 0 : storeType.isOrdaticPartner) {
                        products_1.default.find({
                            internalId: parent.products.map((p) => {
                                return p;
                            }),
                            snoozed: false,
                        }, (error, product) => {
                            if (error) {
                                rejects(error);
                            }
                            else {
                                if (!product)
                                    resolve([]);
                                else {
                                    resolve(product);
                                }
                            }
                        });
                    }
                    else {
                        products_1.default.find({
                            _id: parent.products.map((p) => {
                                return p;
                            }),
                            snoozed: false,
                        }, (error, product) => {
                            if (error) {
                                rejects(error);
                            }
                            else {
                                if (!product)
                                    resolve([]);
                                else {
                                    resolve(product);
                                }
                            }
                        });
                    }
                });
            });
        },
    },
    Bundles: {
        Products(parent, args) {
            return __awaiter(this, void 0, void 0, function* () {
                const storeType = yield restaurant_1.default.findOne({ _id: parent.account });
                return new Promise((resolve, rejects) => {
                    if (storeType === null || storeType === void 0 ? void 0 : storeType.isOrdaticPartner) {
                        products_1.default.find({
                            internalId: parent.posProductCategoryId.map((p) => {
                                return p;
                            }),
                        }, (error, product) => {
                            if (error) {
                                rejects(error);
                            }
                            else {
                                if (!product)
                                    resolve([]);
                                else {
                                    resolve(product);
                                }
                            }
                        });
                    }
                    else {
                        products_1.default.find({
                            _id: parent.subProducts.map((p) => {
                                return p;
                            }),
                        }, (error, product) => {
                            if (error) {
                                rejects(error);
                            }
                            else {
                                if (!product)
                                    resolve([]);
                                else {
                                    resolve(product);
                                }
                            }
                        });
                    }
                });
            });
        },
    },
    Modifiers: {
        ModifierGroups(parent, args) {
            return new Promise((resolve, rejects) => {
                modifierGroups_1.default.find({
                    _id: parent.subProducts.map((p) => {
                        return p;
                    }),
                }, (error, product) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!product)
                            resolve([]);
                        else {
                            resolve(product);
                        }
                    }
                });
            });
        },
    },
    Products: {
        isOpen(parent, args) {
            return __awaiter(this, void 0, void 0, function* () {
                return new Promise((resolve) => {
                    resolve(parent.subProducts.length > 0 ? true : false);
                });
            });
        },
    },
    ModifierGroups: {
        Modifiers(parent, args) {
            return __awaiter(this, void 0, void 0, function* () {
                const storeType = yield restaurant_1.default.findOne({ _id: parent.account });
                return new Promise((resolve, rejects) => {
                    if (storeType === null || storeType === void 0 ? void 0 : storeType.isOrdaticPartner) {
                        modifiers_1.default.find({
                            internalId: parent.posProductCategoryId.map((p) => {
                                return p;
                            }),
                        }, (error, product) => {
                            if (error) {
                                rejects(error);
                            }
                            else {
                                if (!product)
                                    resolve([]);
                                else {
                                    resolve(product);
                                }
                            }
                        });
                    }
                    else {
                        modifiers_1.default.find({
                            _id: parent.subProducts.map((p) => {
                                return p;
                            }),
                        }, (error, product) => {
                            if (error) {
                                rejects(error);
                            }
                            else {
                                if (!product)
                                    resolve([]);
                                else {
                                    resolve(product);
                                }
                            }
                        });
                    }
                });
            });
        },
    },
    Collections: {
        subCollectionItems(parent, args) {
            return new Promise((resolve, rejects) => {
                subcollection_1.default.find({ collectiontype: parent._id }, (error, collection) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!collection)
                            resolve(false);
                        else {
                            const collections = collection;
                            collections.sort((a, b) => 
                            //@ts-ignore
                            a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0);
                            resolve(collections);
                        }
                    }
                });
            });
        },
    },
    subCollection: {
        Product(parent, args) {
            return new Promise((resolve, rejects) => {
                products_1.default
                    .find({ parentId: parent._id }, (error, product) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!product)
                            resolve(false);
                        else {
                            resolve(product);
                        }
                    }
                })
                    .limit(3);
            });
        },
    },
    CustonOrder: {
        Riders(parent, args) {
            return new Promise((resolve, rejects) => {
                riders_1.default.findOne({ _id: parent.riders }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
        usuario(parent, args) {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: parent.userID }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
        Store(parent, args) {
            return new Promise((resolve, rejects) => {
                restaurant_1.default.findOne({ _id: parent.storeID ? parent.storeID : null }, (error, store) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!store)
                            resolve(null);
                        else {
                            resolve(store);
                        }
                    }
                });
            });
        },
    },
    Post: {
        Author(parent, args) {
            return new Promise((resolve, rejects) => {
                userAdmin_1.default.findOne({ _id: parent.author }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
    },
    Opinion: {
        Usuario(parent, args) {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: parent.user }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
    },
    Valoracion: {
        Usuario(parent, args) {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: parent.user }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
    },
    Restaurant: {
        anadidoFavorito(parent, args, { usuarioActual }) {
            return new Promise((resolve, reject) => {
                if (!usuarioActual)
                    resolve(false);
                user_1.default.findOne({ _id: usuarioActual._id }, (error, usuario) => {
                    if (error)
                        reject(error);
                    else {
                        if (!usuario)
                            resolve(false);
                        else {
                            Favorito_1.default.findOne({ usuarioId: usuario._id, restaurantID: parent._id }, (error, favorito) => {
                                if (error)
                                    reject(error);
                                else {
                                    if (!favorito)
                                        resolve(false);
                                    else
                                        resolve(true);
                                }
                            });
                        }
                    }
                });
            });
        },
        scheduleOnly(parent, args) {
            return new Promise((resolve, rejects) => {
                if (scheduleTime_1.scheduleTime(parent.schedule)) {
                    resolve(parent.scheduleOnly);
                }
                else {
                    resolve({ available: true, hour: 0 });
                }
            });
        },
    },
    RestaurantID: {
        anadidoFavorito(parent, args, { usuarioActual }) {
            return new Promise((resolve, reject) => {
                if (!usuarioActual)
                    resolve(false);
                user_1.default.findOne({ _id: usuarioActual._id }, (error, usuario) => {
                    if (error)
                        reject(error);
                    else {
                        if (!usuario)
                            resolve(false);
                        else {
                            Favorito_1.default.findOne({ usuarioId: usuario._id, restaurantID: parent._id }, (error, favorito) => {
                                if (error)
                                    reject(error);
                                else {
                                    if (!favorito)
                                        resolve(false);
                                    else
                                        resolve(true);
                                }
                            });
                        }
                    }
                });
            });
        },
        CategoryNew(parent, args) {
            return new Promise((resolve, rejects) => {
                category_1.default.find({ storeId: parent._id, snoozed: false }, (error, menu) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!menu)
                            resolve(false);
                        else {
                            const categorys = menu;
                            categorys.sort((a, b) => a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0);
                            resolve(categorys);
                        }
                    }
                });
            });
        },
        CategoryNewStore(parent, args) {
            return new Promise((resolve, rejects) => {
                category_1.default.find({ storeId: parent._id }, (error, menu) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!menu)
                            resolve(false);
                        else {
                            const categorys = menu;
                            categorys.sort((a, b) => a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0);
                            resolve(categorys);
                        }
                    }
                });
            });
        },
        scheduleOnly(parent, args) {
            return new Promise((resolve, rejects) => {
                if (scheduleTime_1.scheduleTime(parent.schedule)) {
                    resolve(parent.scheduleOnly);
                }
                else {
                    resolve({ available: true, hour: 0 });
                }
            });
        },
    },
    RestaurantFavorito: {
        restaurant(parent) {
            return new Promise((resolve, reject) => {
                restaurant_1.default.findOne({ _id: parent.restaurantID, open: true }, (error, restaurant) => {
                    if (error) {
                        reject(error);
                    }
                    else {
                        resolve(restaurant);
                    }
                });
            });
        },
    },
};
