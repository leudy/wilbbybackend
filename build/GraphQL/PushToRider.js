"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pushTopRider = void 0;
const sendNotificationRider_1 = require("../GraphQL/sendNotificationRider");
const sendNotificationCustomer_1 = require("./sendNotificationCustomer");
exports.pushTopRider = (rider, onesignal, order) => {
    sendNotificationRider_1.sendNotification(rider.OnesignalID, "Has recibido un nuevo pedido a por todas ✌️ ⚡️", "Has recibido un nuevo pedido", true);
    sendNotificationCustomer_1.sendNotificationCustomer(onesignal, `Te hemos asignado a ${rider.name} ${rider.lastName} como rider para entregar tu pedido`, "💥💥 Repartidor asignado", order);
};
