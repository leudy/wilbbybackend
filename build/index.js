"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const helmet_1 = __importDefault(require("helmet"));
const mongoose_1 = __importDefault(require("mongoose"));
const compression_1 = __importDefault(require("compression"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const path_1 = __importDefault(require("path"));
const cors_1 = __importDefault(require("cors"));
const index_router_1 = __importDefault(require("./routers/index.router"));
const Apis_1 = __importDefault(require("./LoginSocial/Apis"));
const router_1 = __importDefault(require("./Paycomet/router"));
const Recovery_1 = __importDefault(require("./RecoveryPassword/Recovery"));
const Paypal_1 = __importDefault(require("./Paypal"));
const ConfirmNumber_1 = __importDefault(require("./Twilio/ConfirmNumber"));
const userRouter_1 = __importDefault(require("./routers/userRouter"));
const stripe_1 = __importDefault(require("./stripe/stripe"));
const category_1 = __importDefault(require("./MercadonaApis/category"));
const saveProduct_1 = __importDefault(require("./MercadonaApis/saveProduct"));
const OneSignal_1 = __importDefault(require("./OneSignal"));
const constact_router_1 = __importDefault(require("./routers/constact.router"));
const apis_1 = __importDefault(require("./Deliverect/apis"));
const webHook_1 = __importDefault(require("./Ordatic/webHook"));
const wenHook_1 = __importDefault(require("./Otter/wenHook"));
const apiEmails_1 = __importDefault(require("./Emails/apiEmails"));
const nemu_api_1 = __importDefault(require("./Api_Stores/nemu_api"));
const Resolvers_1 = require("./GraphQL/Resolvers");
const Schema_1 = require("./GraphQL/Schema");
const consolidate_1 = __importDefault(require("consolidate"));
const index_1 = __importDefault(require("./whatsApp/index"));
const apollo_server_express_1 = require("apollo-server-express");
const Recursos_1 = require("./Recursos");
const newStripe_1 = __importDefault(require("./stripe/newStripe"));
const newStripeCustomOrder_1 = __importDefault(require("./stripe/newStripeCustomOrder"));
const ReportRouter_1 = __importDefault(require("./routers/ReportRouter"));
const Cupon_1 = __importDefault(require("./routers/Cupon"));
const Sentry = __importStar(require("@sentry/node"));
require("@sentry/tracing");
const user_auth_1 = __importDefault(require("./routers/user-auth"));
Sentry.init({
    dsn: "https://171f16458e72475a919248a3d8f01b36@o311423.ingest.sentry.io/5357673",
    tracesSampleRate: 1.0,
});
Recursos_1.RecursosUpdate();
class Server {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.router();
    }
    config() {
        const MONGO_URI = process.env.MONGO_URI || "nodata";
        mongoose_1.default.set("useFindAndModify", true);
        mongoose_1.default
            .connect(MONGO_URI, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            autoIndex: false,
        })
            .then((db) => console.log("Database is conected"));
        //Settings
        this.app.set("port", process.env.PORT || 5001);
        //Middleware
        this.app.use(morgan_1.default("dev"));
        this.app.use(helmet_1.default());
        this.app.use(compression_1.default());
        this.app.use(function (req, res, next) {
            //to allow cross domain requests to send cookie information.
            //@ts-ignore
            res.header("Access-Control-Allow-Credentials", true);
            // origin can not be '*' when crendentials are enabled. so need to set it to the request origin
            res.header("Access-Control-Allow-Origin", req.headers.origin);
            // list of methods that are supported by the server
            res.header("Access-Control-Allow-Methods", "OPTIONS,GET,PUT,POST,DELETE");
            res.header("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, X-XSRF-TOKEN");
            next();
        });
    }
    router() {
        this.app.use(cors_1.default());
        this.app.use(express_1.default.json());
        this.app.use(index_router_1.default);
        this.app.use("/api/user", userRouter_1.default);
        this.app.use(Apis_1.default);
        this.app.use(Recovery_1.default);
        this.app.use(ConfirmNumber_1.default);
        this.app.use(Paypal_1.default);
        this.app.use(category_1.default);
        this.app.use(constact_router_1.default);
        this.app.use(newStripe_1.default);
        this.app.use(newStripeCustomOrder_1.default);
        this.app.use(saveProduct_1.default);
        this.app.use(index_1.default);
        this.app.use(apiEmails_1.default);
        this.app.use(apis_1.default);
        this.app.use(webHook_1.default);
        this.app.use(wenHook_1.default);
        this.app.use(ReportRouter_1.default);
        this.app.use(nemu_api_1.default);
        this.app.use(Cupon_1.default);
        this.app.use(router_1.default);
        this.app.use(user_auth_1.default);
        this.app.engine("ejs", consolidate_1.default.ejs);
        this.app.set("views", "views");
        this.app.set("view engine", "ejs");
        this.app.use("/assets", express_1.default.static(path_1.default.join(__dirname + "/../uploads")));
        this.app.use(stripe_1.default);
        this.app.use(OneSignal_1.default);
        this.app.use(cors_1.default());
        this.app.use(morgan_1.default("dev"));
        this.app.use(helmet_1.default({
            contentSecurityPolicy: process.env.NODE_ENV === "production" ? undefined : false,
        }));
        this.app.use(express_1.default.urlencoded({
            extended: true,
            limit: "500mb",
            parameterLimit: 500000,
        }));
        this.app.use(express_1.default.json({ limit: "500mb" }));
        this.app.use(compression_1.default());
    }
    start(paht) {
        this.app.listen(this.app.get("port"), () => {
            console.log(`Server on port http://localhost:${this.app.get("port")}${paht}`);
        });
    }
}
const servers = new apollo_server_express_1.ApolloServer({
    typeDefs: Schema_1.typeDefs,
    resolvers: Resolvers_1.resolvers,
    playground: true,
    introspection: true,
    context: ({ req }) => __awaiter(void 0, void 0, void 0, function* () {
        const token = req.headers["authorization"];
        if (token !== null || undefined) {
            try {
                const usuarioActual = jsonwebtoken_1.default.verify(JSON.parse(token || ""), "supersecreto");
                req.usuarioActual = usuarioActual;
                return {
                    usuarioActual,
                };
            }
            catch (err) {
                console.log("err: graphQL Token", err);
            }
        }
        else {
            console.log("No token provide");
        }
    }),
});
const server = new Server();
servers.applyMiddleware({
    app: server.app,
    path: "/graphql",
    bodyParserConfig: {
        limit: "500mb",
    },
});
server.start(servers.graphqlPath);
