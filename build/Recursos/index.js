"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecursosUpdate = void 0;
const order_1 = __importDefault(require("../models/newOrder/order"));
const platos_1 = __importDefault(require("../models/nemu/platos"));
const menu_1 = __importDefault(require("../models/nemu/menu"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const collections_1 = __importDefault(require("../models/collections"));
const categorias_1 = __importDefault(require("../models/categorias"));
const storeFormat_1 = require("../storeFormat");
const restaurant_2 = __importDefault(require("../models/restaurant"));
const products_1 = __importDefault(require("../models/newMenu/products"));
const category_1 = __importDefault(require("../models/newMenu/category"));
const bundles_1 = __importDefault(require("../models/newMenu/bundles"));
const fs_1 = __importDefault(require("fs"));
const tipo_1 = __importDefault(require("../models/tipo"));
exports.RecursosUpdate = () => {
    //new restaurant(storeFormat()).save();
    const updateMainCategoria = () => __awaiter(void 0, void 0, void 0, function* () {
        const categorys = yield categorias_1.default.find();
        categorys.forEach((x) => {
            categorias_1.default.updateMany({ _id: x }, {
                largue: false,
            }, { upsert: true }, () => {
                console.log("done");
            });
        });
    });
    //updateMainCategoria();
    const setCategoryDuplicate = () => __awaiter(void 0, void 0, void 0, function* () {
        const cat = yield category_1.default.find({ account: "60faf41f0ea294528074a5e9" });
        cat.forEach((x, i) => __awaiter(void 0, void 0, void 0, function* () {
            const newCat = new category_1.default({
                name: x.name,
                description: x.description,
                account: "624426e2221328477ef5686d",
                posLocationId: x.posCategoryId,
                posCategoryType: x.posCategoryType,
                subCategories: [],
                posCategoryId: "",
                imageUrl: "",
                products: x.products,
                menu: "624426e2221328477ef5686d",
                storeId: "624426e2221328477ef5686d",
                sortedChannelProductIds: [],
                subProductSortOrder: [],
                subProducts: [],
                level: 2,
                sorting: x.sorting,
                availabilities: [],
            });
            newCat.save().then(() => {
                console.log("save");
            });
        }));
    });
    //setCategoryDuplicate();
    const updateProductPrice = () => __awaiter(void 0, void 0, void 0, function* () {
        const g = yield products_1.default.find({
            account: "60faf41f0ea294528074a622",
        });
        g.forEach((x) => {
            const pri = (20 / 100) * x.price;
            // 
            console.log(pri);
            products_1.default.updateMany({ _id: x._id }, {
                price: x.previous_price,
                offert: false,
                previous_price: 0 //x.price + Math.round(pri)
            }, { upsert: true }, () => {
                console.log("done");
            });
        });
    });
    //updateProductPrice();
    const updateOrders = () => __awaiter(void 0, void 0, void 0, function* () {
        const g = yield order_1.default.find({ status: "Listo para recoger" });
        //@ts-ignore
        g.forEach((x) => __awaiter(void 0, void 0, void 0, function* () {
            const st = { status: "Entregada", date: new Date() };
            order_1.default.findOneAndUpdate({ _id: x === null || x === void 0 ? void 0 : x._id }, {
                status: "Entregada",
                $push: { statusProcess: st },
            }, { upsert: true }, () => {
                console.log("done");
            });
        }));
    });
    const updateProducVariables = () => __awaiter(void 0, void 0, void 0, function* () {
        const products = yield products_1.default.find({
            account: "61a7a1bbc5cc0874468cc368",
        });
        //@ts-ignore
        products.forEach((x) => __awaiter(void 0, void 0, void 0, function* () {
            products_1.default.findOneAndUpdate({ _id: x === null || x === void 0 ? void 0 : x._id }, {
                //@ts-ignore
                subProducts: ["61b899a18585936d282b35f4"],
            }, { upsert: true }, () => {
                console.log("done");
            });
        }));
    });
    const updateProducVariablesFromVariables = () => __awaiter(void 0, void 0, void 0, function* () {
        const products = yield category_1.default.findOne({
            _id: "61f544c5fbcc65557b3edf70",
        });
        //@ts-ignore
        products.products.forEach((x) => __awaiter(void 0, void 0, void 0, function* () {
            products_1.default.findOneAndUpdate({ _id: x }, {
                //@ts-ignore
                subProducts: [
                    "61f9945887d5507d43040b70",
                    "61f994a287d5507d43040b74",
                    "61f9953787d5507d43040b7a",
                ],
            }, { upsert: true }, () => {
                console.log("done");
            });
        }));
    });
    //updateProducVariablesFromVariables();
    //isBundled;
    const updateProducBundled = () => __awaiter(void 0, void 0, void 0, function* () {
        const g = yield bundles_1.default.find();
        g &&
            g.forEach((x) => __awaiter(void 0, void 0, void 0, function* () {
                x &&
                    x.subProducts.forEach((y) => __awaiter(void 0, void 0, void 0, function* () {
                        const prod = yield products_1.default.findOne({ _id: y });
                        products_1.default.findOneAndUpdate({ _id: prod === null || prod === void 0 ? void 0 : prod._id }, {
                            isBundled: true,
                        }, { upsert: true }, () => {
                            console.log("done");
                        });
                    }));
            }));
    });
    const updateProducS = () => __awaiter(void 0, void 0, void 0, function* () {
        const Products = yield products_1.default.find();
        Products &&
            Products.forEach((y) => __awaiter(void 0, void 0, void 0, function* () {
                products_1.default.findOneAndUpdate({ _id: y === null || y === void 0 ? void 0 : y._id }, {
                    isBundled: (y === null || y === void 0 ? void 0 : y.isBundled) ? true : false,
                }, { upsert: true }, () => {
                    console.log("done");
                });
            }));
    });
    const updateCollection = () => __awaiter(void 0, void 0, void 0, function* () {
        const g = yield collections_1.default.find({
            store: "5fb24df8c292c23bed148076",
        });
        g.forEach((x) => {
            collections_1.default.updateMany({ _id: x._id }, {
                store: x.store.concat("624423c4221328477ef5686b"),
            }, { upsert: true }, () => {
                console.log("done");
            });
        });
    });
    //updateCollection();
    const updateRestaurant = () => __awaiter(void 0, void 0, void 0, function* () {
        const g = yield restaurant_2.default.find();
        g.forEach((x, i) => __awaiter(void 0, void 0, void 0, function* () {
            restaurant_2.default.findOneAndUpdate({ _id: x._id }, {
                scheduleOnly: {
                    available: false,
                    hour: 0,
                },
                noScheduled: false,
                cashPayment: true,
            }, () => {
                console.log("done");
            });
        }));
    });
    //updateRestaurant();
    const updateRestaurantOne = () => __awaiter(void 0, void 0, void 0, function* () {
        restaurant_2.default.updateMany({ _id: "60fc33acfbf3fe598d4c2a36" }, storeFormat_1.setHorario(), { upsert: true }, () => {
            console.log("done");
        });
    });
    //updateRestaurantOne();
    const updateProd = () => __awaiter(void 0, void 0, void 0, function* () {
        const g = yield products_1.default.find({ account: "610003c7fb1a92e312078816" });
        g.forEach((x) => {
            const price = (x.price * 10) / 100;
            products_1.default.updateMany({ _id: x._id }, {
                price: x.previous_price,
                previous_price: 0,
                offert: false,
            }, { upsert: true }, () => {
                console.log("done");
            });
        });
    });
    const updateStore = () => __awaiter(void 0, void 0, void 0, function* () {
        const g = yield restaurant_2.default.find({ city: "Tomelloso" });
        g.forEach((x) => {
            restaurant_2.default.findOneAndUpdate({ _id: x._id }, {
                includeCity: x.includeCity.concat("Vereda de Socuéllamos, s/n, Tomelloso"),
            }, { upsert: true }, () => {
                console.log("done");
            });
        });
    });
    //updateProd();
    //updateRestaurant()
    //updateCollection()
    //update();
    /*  Order.deleteMany({ store: "61f045bcb3e94555a0caafe5" }).then(() => {
       console.log("hecho");
     }); */
    /* custonorder.deleteMany({ status: "Pendiente de pago" }).then(() => {
      console.log("hecho");
    }); */
    /* category.deleteMany({ account: "60faf41f0ea294528074a602" }).then(() => {
    console.log("hecho");
  }); */
    const setCategoryFronViejas = () => __awaiter(void 0, void 0, void 0, function* () {
        const cat = yield menu_1.default.find({
            restaurant: "5ff1d57185c1c068931a3860",
        });
        cat.forEach((x, i) => __awaiter(void 0, void 0, void 0, function* () {
            const pro = yield platos_1.default.find({ menu: x._id });
            const sub = pro.map((y) => {
                //@ts-ignore
                return y._id;
            });
            const newCat = new restaurant_1.default({
                name: x.title,
                description: "",
                account: "5ff1d57185c1c068931a3860",
                posLocationId: "",
                posCategoryType: "",
                subCategories: [],
                posCategoryId: "",
                imageUrl: "",
                products: sub,
                menu: "60be7ee0395b91df865649d5",
                storeId: "5ff1d57185c1c068931a3860",
                sortedChannelProductIds: [],
                subProductSortOrder: [],
                subProducts: [],
                level: 2,
                sorting: i,
                availabilities: [],
            });
            newCat.save().then(() => {
                console.log("save");
            });
        }));
    });
    /* setOrders().then(() => {
    console.log("guardado");
  }); */
    //setMenu()
    //console.log(new Date());
};
const createdJSON = () => __awaiter(void 0, void 0, void 0, function* () {
    const g = yield tipo_1.default.find();
    let data = JSON.stringify(g);
    fs_1.default.writeFileSync('tipo.json', data);
});
//createdJSON()
