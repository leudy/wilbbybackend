"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const ordenUpdate_1 = require("../Deliverect/ordenUpdate");
const sendNotification_1 = require("../Deliverect/sendNotification");
const order_1 = __importDefault(require("../models/newOrder/order"));
const UpdateOrder_1 = require("./UpdateOrder");
class OtterRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.post("/v1/otter/update-status", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const data = req.body;
            const orden = yield order_1.default.findOne({
                _id: data.metadata.resourceId,
            });
            data.metadata.payload.orderStatusHistory.forEach((element) => {
                if (element.status === "RECIVED_ORDER") {
                    sendNotification_1.sendNotification(
                    //@ts-ignore
                    orden === null || 
                    //@ts-ignore
                    orden === void 0 ? void 0 : 
                    //@ts-ignore
                    orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} Ha recibido tu pedido.`, orden);
                }
                else if (element.status === "ORDER_ACCEPTED") {
                    order_1.default.findOneAndUpdate({ _id: data.metadata.resourceId }, {
                        $set: {
                            eventId: data.eventId,
                        },
                    }, (err, order) => { });
                    if (data.metadata.payload.orderStatusHistory.length === 1) {
                        ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Confirmada", 60);
                        sendNotification_1.sendNotification(
                        //@ts-ignore
                        orden === null || 
                        //@ts-ignore
                        orden === void 0 ? void 0 : 
                        //@ts-ignore
                        orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} Ha confirmado tu pedido.`, orden);
                    }
                }
                else if (element.status === "ORDER_READY_TO_PICKUP") {
                    if (orden === null || orden === void 0 ? void 0 : orden.storeData.autoshipping) {
                        UpdateOrder_1.UpdateOrder(orden, "FULFILLED");
                        ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Entregada", 90);
                        sendNotification_1.sendNotification(
                        //@ts-ignore
                        orden === null || 
                        //@ts-ignore
                        orden === void 0 ? void 0 : 
                        //@ts-ignore
                        orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} a salido a entregar tu pedido.`, orden);
                    }
                    else {
                        ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Listo para recoger", 70);
                        sendNotification_1.sendNotification(
                        //@ts-ignore
                        orden === null || 
                        //@ts-ignore
                        orden === void 0 ? void 0 : 
                        //@ts-ignore
                        orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} tiene tu pedido listo para recoger.`, orden);
                        sendNotification_1.sendNotificationRider(orden === null || orden === void 0 ? void 0 : orden.courierData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} tiene el pedido listo para recoger.`);
                    }
                }
                else if (element.status === "ORDER_FULFILLED") {
                    sendNotification_1.sendNotification(
                    //@ts-ignore
                    orden === null || 
                    //@ts-ignore
                    orden === void 0 ? void 0 : 
                    //@ts-ignore
                    orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} a finalizado tu pedido.`, orden);
                }
                else if (element.status === "CANCELED") {
                    ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Devuelto", 10);
                    sendNotification_1.sendNotification(
                    //@ts-ignore
                    orden === null || 
                    //@ts-ignore
                    orden === void 0 ? void 0 : 
                    //@ts-ignore
                    orden.customerData.OnesignalID, `Upps el pedido ha fallado lo sentimos.`, orden);
                }
            });
            res.status(200).end();
        }));
    }
}
const otterRouter = new OtterRouter();
otterRouter.routes();
exports.default = otterRouter.router;
