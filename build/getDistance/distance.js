"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setDistanceToOrder = void 0;
const order_1 = __importDefault(require("../models/newOrder/order"));
var distance = require("google-distance-matrix");
distance.key("AIzaSyDy80slwpOlrhOhU-qLYFmGKN3FWgAsmGA");
distance.units("imperial");
exports.setDistanceToOrder = (OrderID) => __awaiter(void 0, void 0, void 0, function* () {
    const order = yield order_1.default.findOne({ _id: OrderID });
    if ((order === null || order === void 0 ? void 0 : order.orderType) === "delivery") {
        var origins = [
            `${order === null || order === void 0 ? void 0 : order.deliveryAddressData.lat},${order === null || order === void 0 ? void 0 : order.deliveryAddressData.lgn}`,
        ];
        var destinations = [
            `${order === null || order === void 0 ? void 0 : order.storeData.adress.lat},${order === null || order === void 0 ? void 0 : order.storeData.adress.lgn}`,
        ];
        distance.matrix(origins, destinations, function (err, distances) {
            if (err) {
                return console.log(err);
            }
            if (!distances) {
                return console.log("no distances");
            }
            if (distances.status === "OK") {
                for (var i = 0; i < origins.length; i++) {
                    for (var j = 0; j < destinations.length; j++) {
                        var origin = distances.origin_addresses[i];
                        var destination = distances.destination_addresses[j];
                        if (distances.rows[0].elements[j].status == "OK") {
                            var distance = distances.rows[i].elements[j].distance.value;
                            const dis = distance / 1000;
                            const km = dis * 100;
                            const propina = order.tip > 0 ? order.tip : 0;
                            const totalFinal = 0;
                            const kmformat = km.toFixed(0);
                            const input = {
                                km: Number(kmformat),
                                order: order === null || order === void 0 ? void 0 : order.channelOrderDisplayId,
                                total: totalFinal,
                                propina: propina,
                            };
                            order_1.default.findOneAndUpdate({ _id: OrderID }, {
                                $set: {
                                    reportRiders: input,
                                },
                            }, (err, order) => { });
                        }
                        else {
                            console.log(destination + " is not reachable by land from " + origin);
                        }
                    }
                }
            }
        });
    }
    else {
        null;
    }
});
