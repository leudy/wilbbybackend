"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const products_1 = __importDefault(require("../models/newMenu/products"));
const isla_1 = require("./restaurantProduct/isla");
class ProductSaveRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    createProduct(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            isla_1.data.forEach((element) => {
                const produto = new products_1.default(element);
                produto
                    .save()
                    .then(() => {
                    res.json({ data: "Product save" });
                })
                    .catch(() => res.json({ data: "Error save Product" }));
            });
        });
    }
    routes() {
        this.router.get("/create-products", this.createProduct);
    }
}
const productSaveRouter = new ProductSaveRouter();
productSaveRouter.routes();
exports.default = productSaveRouter.router;
