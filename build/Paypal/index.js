"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const paypal_rest_sdk_1 = __importDefault(require("paypal-rest-sdk"));
const dotenv_1 = __importDefault(require("dotenv"));
const custonorder_1 = __importDefault(require("../models/custonorder"));
const scheduleTime_1 = require("../funtions/scheduleTime");
const ScheduledMaria_1 = require("../funtions/ScheduledMaria");
const asingRidersCustonOrder_1 = require("../funtions/asingRidersCustonOrder");
dotenv_1.default.config({ path: "variables.env" });
paypal_rest_sdk_1.default.configure({
    mode: "live",
    client_id: process.env.PAYPALCLIENTID || "",
    client_secret: process.env.PAYPALCLIENTSECRET || "",
});
class PaypalRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get("/paypal", (req, res) => {
            const { price } = req.query;
            let create_payment_json = {
                intent: "sale",
                payer: {
                    payment_method: "paypal",
                },
                redirect_urls: {
                    return_url: `https://api.v2.wilbby.com/success?price=${price}`,
                    cancel_url: "https://api.v2.wilbby.com/cancel",
                },
                transactions: [
                    {
                        amount: {
                            currency: "EUR",
                            total: req.query.price,
                        },
                        description: "Peinado & Perales SL (Wilbby)",
                    },
                ],
            };
            // @ts-ignore
            paypal_rest_sdk_1.default.payment.create(create_payment_json, function (error, payment) {
                if (error) {
                    res.render("cancel");
                }
                else {
                    // @ts-ignore
                    res.redirect(payment.links[1].href);
                }
            });
        });
        this.router.get("/success", (req, res) => {
            var PayerID = req.query.PayerID;
            var paymentId = req.query.paymentId;
            const { price } = req.query;
            var execute_payment_json = {
                payer_id: PayerID,
                transactions: [
                    {
                        amount: {
                            currency: "EUR",
                            total: price,
                        },
                    },
                ],
            };
            // @ts-ignore
            paypal_rest_sdk_1.default.payment.execute(
            // @ts-ignore
            paymentId, execute_payment_json, function (error, payment) {
                if (error) {
                    res.render("cancel");
                }
                if (payment) {
                    res.render("success");
                }
            });
        });
        this.router.get("/cancel", (req, res) => {
            res.render("cancel");
        });
        this.router.get("/paypal-custom-order", (req, res) => {
            const { price, order, currency } = req.query;
            let create_payment_json = {
                intent: "sale",
                payer: {
                    payment_method: "paypal",
                },
                redirect_urls: {
                    return_url: `https://api.v2.wilbby.com/success-custom?price=${price}&order=${order}&currency=${currency}`,
                    cancel_url: "https://api.v2.wilbby.com/cancel-custom",
                },
                transactions: [
                    {
                        amount: {
                            currency: "EUR",
                            total: req.query.price,
                        },
                        description: "Peinado & Perales SL (Wilbby)",
                    },
                ],
            };
            // @ts-ignore
            paypal_rest_sdk_1.default.payment.create(create_payment_json, function (error, payment) {
                if (error) {
                    res.render("cancel");
                }
                else {
                    // @ts-ignore
                    res.redirect(payment.links[1].href);
                }
            });
        });
        this.router.get("/success-custom", (req, res) => {
            var PayerID = req.query.PayerID;
            var paymentId = req.query.paymentId;
            const { price, order, currency } = req.query;
            var execute_payment_json = {
                payer_id: PayerID,
                transactions: [
                    {
                        amount: {
                            currency: "EUR",
                            total: price,
                        },
                    },
                ],
            };
            // @ts-ignore
            paypal_rest_sdk_1.default.payment.execute(
            // @ts-ignore
            paymentId, execute_payment_json, function (error, payment) {
                if (error) {
                    res.render("cancel");
                }
                if (payment) {
                    custonorder_1.default.findOneAndUpdate({ _id: order }, {
                        estado: "Pagado",
                        progreso: "25",
                        status: "active",
                        pagoPaypal: payment,
                    }, (err, orde) => {
                        if (orde) {
                            if (scheduleTime_1.scheduleTime(ScheduledMaria_1.getMariaHorario().schedule)) {
                                asingRidersCustonOrder_1.AsingRiderCustonOrder(orde);
                            }
                            res.render("success");
                        }
                    });
                }
            });
        });
        this.router.get("/cancel-custom", (req, res) => {
            res.render("cancel");
        });
    }
}
const paypalRouter = new PaypalRouter();
paypalRouter.routes();
exports.default = paypalRouter.router;
