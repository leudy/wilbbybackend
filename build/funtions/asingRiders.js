"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AsingRider = void 0;
const mongoose_1 = require("mongoose");
const order_1 = __importDefault(require("../models/newOrder/order"));
const riders_1 = __importDefault(require("../models/riders"));
const sendNotificationRider_1 = require("../GraphQL/sendNotificationRider");
const sendNotificationCustomer_1 = require("../GraphQL/sendNotificationCustomer");
const ordenUpdate_1 = require("../Deliverect/ordenUpdate");
const { ObjectId } = mongoose_1.Types;
exports.AsingRider = (orde) => __awaiter(void 0, void 0, void 0, function* () {
    const isRider = () => {
        if (orde.storeData.city === "Tomelloso") {
            return "611f847ef0668e8ae9c80605";
        }
        else if (orde.storeData.city === "Alcázar de San Juan") {
            return "624c5c667fb6d1090c6f03fd";
        }
        else if (orde.storeData.city === "Valdepeñas") {
            return "61e939f61a762b315010859d";
        }
        else if (orde.storeData.city === "Socuéllamos") {
            return "62570d762657f25bf53765c6";
        }
        else if (orde.storeData.city === "Quintanar de la Orden") {
            return "62433995221328477ef567fa";
        }
    };
    //@ts-ignore
    const rider = yield riders_1.default.findOne({
        _id: isRider(),
    });
    order_1.default.findOneAndUpdate({ _id: orde._id }, {
        courier: ObjectId(rider._id),
        courierData: rider,
    }, { upsert: true }, (err, order) => {
        if (err) {
            console.log(">>>", err);
        }
        else {
            if (!orde.storeData.ispartners) {
                ordenUpdate_1.ordenUpdate(orde._id, "Listo para recoger", 70);
                setTimeout(() => {
                    sendNotificationRider_1.sendNotification(rider.OnesignalID, "Tienes el pedido listo para recoger ✌️ ⚡️", "Listo para recoger", true);
                }, 30000);
            }
            sendNotificationRider_1.sendNotification(rider.OnesignalID, "Has recibido un nuevo pedido a por todas ✌️ ⚡️", "Has recibido un nuevo pedido", true);
            sendNotificationCustomer_1.sendNotificationCustomer(orde.customerData.OnesignalID, `Te hemos asignado a ${rider.name} para entregarte el pedido`, "Repartidor asignado", orde);
        }
    });
});
