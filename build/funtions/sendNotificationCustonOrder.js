"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendNotificationCustomOrder = void 0;
const user_1 = __importDefault(require("../models/user"));
const sendNotificationCustomerCuston_1 = require("../GraphQL/sendNotificationCustomerCuston");
exports.SendNotificationCustomOrder = (orde) => __awaiter(void 0, void 0, void 0, function* () {
    //@ts-ignore
    const user = yield user_1.default.findOne({
        _id: orde.userID,
    });
    let title = "";
    let description = "";
    switch (orde.estado) {
        case "Aceptada":
            title = "El repartidor ya está buscando tu pedido";
            description =
                "El repartidor ya está buscando tu pedido en la dirección indicada";
            break;
        case "En camino":
            title = "El repartidor ya está de camino";
            description = "El repartidor ya está de camino para entregarte el pedido";
            break;
        case "Entregado":
            title = "⭐️ 🌟 Pedido entregado";
            description =
                "⭐️ 🌟 El pedido ha sido entregado es hora de decirno que te ha parecido";
            break;
        default:
            break;
    }
    sendNotificationCustomerCuston_1.sendNotificationCustomerCuston(user.OnesignalID, description, title);
});
