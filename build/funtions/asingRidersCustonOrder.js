"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AsingRiderCustonOrder = void 0;
const custonorder_1 = __importDefault(require("../models/custonorder"));
const riders_1 = __importDefault(require("../models/riders"));
const user_1 = __importDefault(require("../models/user"));
const sendNotificationRider_1 = require("../GraphQL/sendNotificationRider");
const sendNotificationCustomerCuston_1 = require("../GraphQL/sendNotificationCustomerCuston");
exports.AsingRiderCustonOrder = (orde) => __awaiter(void 0, void 0, void 0, function* () {
    const isRider = () => {
        if (orde.city === "Tomelloso") {
            return "611f847ef0668e8ae9c80605";
        }
        else if (orde.city === "Burgos") {
            return "5fb7de125fd35b9c72ce2423";
        }
        else if (orde.city === "Alcázar de San Juan") {
            return "611f83cdf0668e8ae9c80604";
        }
    };
    //@ts-ignore
    const rider = yield riders_1.default.findOne({
        _id: isRider(),
    });
    //@ts-ignore
    const user = yield user_1.default.findOne({
        _id: orde.userID,
    });
    custonorder_1.default.findOneAndUpdate({ _id: orde._id }, {
        riders: rider._id,
        estado: "Asignada",
    }, { upsert: true }, (err, order) => {
        if (err) {
            console.log(">>>", err);
        }
        else {
            sendNotificationRider_1.sendNotification(rider.OnesignalID, "Has recibido un nuevo PEDIDO EXPRESS 🚨🚨🚨", "🚨 Has recibido un nuevo PEDIDO EXPRESS 🚨", true);
            sendNotificationCustomerCuston_1.sendNotificationCustomerCuston(user.OnesignalID, `Te hemos asignado a ${rider.name} para entregarte el pedido`, "Repartidor asignado");
        }
    });
});
