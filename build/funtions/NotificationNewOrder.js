"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendNotificationToParnert = void 0;
const Deliverect_1 = require("../Deliverect/Deliverect");
const api_1 = require("../Ordatic/api");
const api_2 = require("../Otter/api");
const Apis_1 = require("../Holded/Apis");
const sendNotificationToStore_1 = require("../stripe/sendNotificationToStore");
const scheduleTime_1 = require("../funtions/scheduleTime");
const ScheduledMaria_1 = require("../funtions/ScheduledMaria");
const asingRiders_1 = require("../funtions/asingRiders");
exports.sendNotificationToParnert = (order, store) => {
    if (store.isDeliverectPartner) {
        Deliverect_1.setOrderToDeliverect(order);
    }
    else if (store.isOrdaticPartner) {
        api_1.pushOrderTopOrdatic(order);
    }
    else if (store.isOtterPartner) {
        api_2.pushOrderToOtter(order);
    }
    else {
        sendNotificationToStore_1.sendNotification(store.OnesignalID, "Has recibido un nuevo pedido a por todas 🌮 🌯 🫔 🥗 🥘");
    }
    if (scheduleTime_1.scheduleTime(ScheduledMaria_1.getMariaHorario().schedule) &&
        order.orderType === "delivery") {
        asingRiders_1.AsingRider(order);
    }
    Apis_1.setOrderToHolded(order);
};
