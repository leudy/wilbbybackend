"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.scheduleTime = void 0;
exports.scheduleTime = (data) => {
    const now = new Date();
    const time1 = new Date(now);
    const time2 = new Date(now);
    var days = [
        data.Sunday.day,
        data.Monday.day,
        data.Tuesday.day,
        data.Wednesday.day,
        data.Thursday.day,
        data.Friday.day,
        data.Saturday.day,
    ];
    var d = new Date();
    var dayName = days[d.getDay()];
    const open1 = new Date(now);
    const close1 = new Date(now);
    const open2 = new Date(now);
    const close2 = new Date(now);
    if (dayName === data.Sunday.day && data.Sunday.isOpen) {
        open1.setHours(data.Sunday.openHour1);
        open1.setMinutes(data.Sunday.openMinute1);
        close1.setHours(data.Sunday.closetHour1);
        close1.setMinutes(data.Sunday.closetMinute1);
        open2.setHours(data.Sunday.openHour2);
        open2.setMinutes(data.Sunday.openMinute2);
        close2.setHours(data.Sunday.closetHour2);
        close2.setMinutes(data.Sunday.closetMinute2);
        if (time1 >= open1 && time2 <= close1) {
            return true;
        }
        else if (time1 >= open2 && time2 <= close2) {
            return true;
        }
        else {
            return false;
        }
    }
    else if (dayName === data.Monday.day && data.Monday.isOpen) {
        open1.setHours(data.Monday.openHour1);
        open1.setMinutes(data.Monday.openMinute1);
        close1.setHours(data.Monday.closetHour1);
        close1.setMinutes(data.Monday.closetMinute1);
        open2.setHours(data.Monday.openHour2);
        open2.setMinutes(data.Monday.openMinute2);
        close2.setHours(data.Monday.closetHour2);
        close2.setMinutes(data.Monday.closetMinute2);
        if (time1 >= open1 && time2 <= close1) {
            return true;
        }
        else if (time1 >= open2 && time2 <= close2) {
            return true;
        }
        else {
            return false;
        }
    }
    else if (dayName === data.Tuesday.day && data.Tuesday.isOpen) {
        open1.setHours(data.Tuesday.openHour1);
        open1.setMinutes(data.Tuesday.openMinute1);
        close1.setHours(data.Tuesday.closetHour1);
        close1.setMinutes(data.Tuesday.closetMinute1);
        open2.setHours(data.Tuesday.openHour2);
        open2.setMinutes(data.Tuesday.openMinute2);
        close2.setHours(data.Tuesday.closetHour2);
        close2.setMinutes(data.Tuesday.closetMinute2);
        if (time1 >= open1 && time2 <= close1) {
            return true;
        }
        else if (time1 >= open2 && time2 <= close2) {
            return true;
        }
        else {
            return false;
        }
    }
    else if (dayName === data.Wednesday.day && data.Wednesday.isOpen) {
        open1.setHours(data.Wednesday.openHour1);
        open1.setMinutes(data.Wednesday.openMinute1);
        close1.setHours(data.Wednesday.closetHour1);
        close1.setMinutes(data.Wednesday.closetMinute1);
        open2.setHours(data.Wednesday.openHour2);
        open2.setMinutes(data.Wednesday.openMinute2);
        close2.setHours(data.Wednesday.closetHour2);
        close2.setMinutes(data.Wednesday.closetMinute2);
        if (time1 >= open1 && time2 <= close1) {
            return true;
        }
        else if (time1 >= open2 && time2 <= close2) {
            return true;
        }
        else {
            return false;
        }
    }
    else if (dayName === data.Thursday.day && data.Thursday.isOpen) {
        open1.setHours(data.Thursday.openHour1);
        open1.setMinutes(data.Thursday.openMinute1);
        close1.setHours(data.Thursday.closetHour1);
        close1.setMinutes(data.Thursday.closetMinute1);
        open2.setHours(data.Thursday.openHour2);
        open2.setMinutes(data.Thursday.openMinute2);
        close2.setHours(data.Thursday.closetHour2);
        close2.setMinutes(data.Thursday.closetMinute2);
        if (time1 >= open1 && time2 <= close1) {
            return true;
        }
        else if (time1 >= open2 && time2 <= close2) {
            return true;
        }
        else {
            return false;
        }
    }
    else if (dayName === data.Friday.day && data.Friday.isOpen) {
        open1.setHours(data.Friday.openHour1);
        open1.setMinutes(data.Friday.openMinute1);
        close1.setHours(data.Friday.closetHour1);
        close1.setMinutes(data.Friday.closetMinute1);
        open2.setHours(data.Friday.openHour2);
        open2.setMinutes(data.Friday.openMinute2);
        close2.setHours(data.Friday.closetHour2);
        close2.setMinutes(data.Friday.closetMinute2);
        if (time1 >= open1 && time2 <= close1) {
            return true;
        }
        else if (time1 >= open2 && time2 <= close2) {
            return true;
        }
        else {
            return false;
        }
    }
    else if (dayName === data.Saturday.day && data.Saturday.isOpen) {
        open1.setHours(data.Saturday.openHour1);
        open1.setMinutes(data.Saturday.openMinute1);
        close1.setHours(data.Saturday.closetHour1);
        close1.setMinutes(data.Saturday.closetMinute1);
        open2.setHours(data.Saturday.openHour2);
        open2.setMinutes(data.Saturday.openMinute2);
        close2.setHours(data.Saturday.closetHour2);
        close2.setMinutes(data.Saturday.closetMinute2);
        if (time1 >= open1 && time2 <= close1) {
            return true;
        }
        else if (time1 >= open2 && time2 <= close2) {
            return true;
        }
        else {
            return false;
        }
    }
    return false;
};
//formatedate    ------>  timedatectl set-timezone Europe/Madrid
