"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const orderSchema = new mongoose_1.default.Schema({
    display_id: {
        type: Number,
    },
    restaurant: {
        type: String,
    },
    userID: {
        type: String,
    },
    userData: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
    AdreesStoreData: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
    AdreesData: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
    StoreData: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
    adress: {
        type: String,
    },
    adresStore: {
        type: String,
    },
    city: {
        type: String,
    },
    riders: {
        type: String,
    },
    aceptaTerminos: {
        type: Boolean,
    },
    propina: {
        type: Boolean,
    },
    cupon: { type: mongoose_1.default.Schema.Types.ObjectId, ref: "cupones" },
    cantidad: { type: Number, default: 1 },
    pagoPaypal: { type: mongoose_1.default.Schema.Types.Mixed },
    stripePaymentIntent: { type: mongoose_1.default.Schema.Types.Mixed },
    estado: {
        type: String,
        enum: [
            "Pendiente de pago",
            "Pagado",
            "Confirmada",
            "Listo para recoger",
            "Preparando para el envío",
            "En camino",
            "Entregada",
            "Rechazado",
            "Rechazada por la tienda",
            "Rechazada por el rider",
            "Devuelto",
            "Valorada",
            "Resolución",
        ],
        default: "Pendiente de pago",
    },
    progreso: {
        type: String,
        enum: ["0", "25", "50", "75", "100"],
        default: "25",
    },
    status: {
        type: String,
        enum: ["success", "exception", "normal", "active"],
        default: "active",
    },
    platos: {
        type: [mongoose_1.default.Schema.Types.Mixed],
    },
    time: {
        type: String,
    },
    nota: {
        type: String,
    },
    total: {
        type: String,
    },
    isvalored: {
        type: Boolean,
        default: false,
    },
    cubiertos: {
        type: Boolean,
        default: false,
    },
    llevar: {
        type: Boolean,
        default: false,
    },
    subtotal: {
        type: String,
    },
    complement: {
        type: String,
    },
    descuentopromo: {
        type: String,
    },
    tarifa: {
        type: String,
    },
    envio: {
        type: String,
    },
    propinaTotal: {
        type: String,
    },
    schedule: {
        type: Boolean,
    },
    holdedID: {
        type: String,
    },
    holdedRidersID: {
        type: String,
    },
    holdedPartnerID: {
        type: String,
    },
    invoiceUrl: {
        type: String,
    },
    reportRiders: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
orderSchema.virtual("descuento", {
    ref: "cupones",
    localField: "cupon",
    foreignField: "_id",
    justOne: true,
});
exports.default = mongoose_1.default.model("order", orderSchema);
