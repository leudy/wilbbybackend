"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const conisSchema = new mongoose_1.default.Schema({
    value: {
        type: Number,
        required: true,
        default: 0,
    },
    user: { type: mongoose_1.default.Schema.Types.ObjectId, ref: "user", required: true },
    order: { type: mongoose_1.default.Schema.Types.ObjectId, ref: "newOrder" },
    type: {
        type: String,
        enum: ["Discount", "Plus", "Detained"],
        required: true,
    },
    orderData: { type: mongoose_1.default.Schema.Types.Mixed },
    title: { type: String },
    description: { type: String },
    status: {
        type: String,
        enum: ["Success", "Cancelled", "Retired", "Pending", "Error"],
        required: true,
    },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("coin", conisSchema);
