"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const loginSchema = new mongoose_1.default.Schema({
    user: {
        type: String,
        required: true,
    },
    data: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
});
exports.default = mongoose_1.default.model("login", loginSchema);
