"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const categoriaSchema = new mongoose_1.default.Schema({
    title: {
        type: String,
        required: true,
        text: true,
        unique: true,
    },
    image: {
        type: String,
        required: true,
        text: true,
    },
    description: {
        type: String,
        required: true,
        unique: true,
    },
    sorting: { type: Number },
    excludeCity: { type: [String] },
    visible: { type: Boolean },
    navigate: { type: Boolean },
    url: { type: String },
    largue: { type: Boolean },
});
exports.default = mongoose_1.default.model("categoria", categoriaSchema);
