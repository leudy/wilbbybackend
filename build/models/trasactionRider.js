"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const transactionSchema = new mongoose_1.default.Schema({
    rider: {
        type: String,
        required: true,
    },
    km: {
        type: Number,
        required: true,
    },
    order: {
        type: Number,
    },
    total: {
        type: Number,
    },
    propina: {
        type: Number,
    },
    iva: {
        type: Number,
    },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("transactionRider", transactionSchema);
