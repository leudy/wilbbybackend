"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const orderSchema = new mongoose_1.default.Schema({
    channelOrderDisplayId: { type: Number, unique: true },
    orderType: { type: String, enum: ["pickup", "delivery", "eat in"] },
    pickupTime: { type: Date },
    estimatedPickupTime: { type: Date },
    deliveryTime: { type: Date },
    courier: { type: mongoose_1.default.Schema.Types.ObjectId, ref: "rider" },
    courierData: { type: mongoose_1.default.Schema.Types.Mixed },
    customerData: { type: mongoose_1.default.Schema.Types.Mixed },
    customer: { type: mongoose_1.default.Schema.Types.ObjectId, ref: "user" },
    store: { type: mongoose_1.default.Schema.Types.ObjectId, ref: "restaurant" },
    storeData: { type: mongoose_1.default.Schema.Types.Mixed },
    deliveryAddressData: { type: mongoose_1.default.Schema.Types.Mixed },
    deliveryAddress: { type: mongoose_1.default.Schema.Types.ObjectId, ref: "adress" },
    orderIsAlreadyPaid: { type: Boolean, default: true },
    payment: { type: mongoose_1.default.Schema.Types.Mixed },
    note: { type: String },
    items: { type: mongoose_1.default.Schema.Types.Mixed },
    numberOfCustomers: { type: Number },
    deliveryCost: { type: Number },
    serviceCharge: { type: Number },
    discountTotal: { type: Number },
    IntegerValue: {
        type: Number,
        enum: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140],
        default: 10,
    },
    tip: { type: Number, default: 0 },
    paymentMethod: {
        type: String,
        enum: [
            "Paypal",
            "Tarjeta de credito",
            "Efectivo",
            "Apple Pay",
            "Google Pay",
            "Datafono",
        ],
        default: "Tarjeta de credito",
    },
    cupon: { type: mongoose_1.default.Schema.Types.ObjectId, ref: "cupones" },
    statusProcess: { type: mongoose_1.default.Schema.Types.Mixed },
    pagoPaypal: { type: mongoose_1.default.Schema.Types.Mixed },
    stripePaymentIntent: { type: mongoose_1.default.Schema.Types.Mixed },
    Needcutlery: {
        type: Boolean,
        default: false,
    },
    status: {
        type: String,
        enum: [
            "Pendiente de pago",
            "Nueva",
            "Confirmada",
            "En la cocina",
            "Listo para recoger",
            "Preparando para el envío",
            "En camino",
            "Entregada",
            "Rechazado",
            "Rechazada por la tienda",
            "Rechazada por el rider",
            "Devuelto",
            "Finalizada",
            "Resolución",
        ],
        default: "Nueva",
    },
    holdedID: {
        type: String,
    },
    holdedRidersID: {
        type: String,
    },
    holdedPartnerID: {
        type: String,
    },
    invoiceUrl: {
        type: String,
    },
    reportRiders: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
    offertFromRider: {
        type: [mongoose_1.default.Schema.Types.Mixed],
    },
    budgetRider: {
        type: [String],
    },
    riderAcceptOrder: {
        type: Boolean,
        default: false,
    },
    scheduled: {
        type: Boolean,
        default: false,
    },
    eventId: {
        type: String,
    },
    cuponName: {
        type: String,
    },
    cuponTipe: {
        type: String,
    },
    cuponValue: {
        type: Number,
    },
    source: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
    platform: {
        type: String,
    },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
orderSchema.virtual("descuento", {
    ref: "cupones",
    localField: "cupon",
    foreignField: "_id",
    justOne: true,
});
exports.default = mongoose_1.default.model("newOrder", orderSchema);
