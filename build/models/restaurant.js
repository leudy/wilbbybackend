"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
mongoose_1.default.Promise = global.Promise;
const restaurantSchema = new mongoose_1.default.Schema({
    title: {
        type: String,
        required: true,
        text: true,
    },
    image: {
        type: String,
        required: true,
    },
    description: {
        type: String,
    },
    shipping: {
        type: Number,
        default: 199,
    },
    extras: {
        type: Number,
        default: 120,
    },
    minime: {
        type: Number,
    },
    menu: {
        type: String,
    },
    rating: {
        type: String,
    },
    apertura: {
        type: Number,
    },
    cierre: {
        type: Number,
    },
    aperturaMin: {
        type: Number,
    },
    cierreMin: {
        type: Number,
    },
    diaslaborales: {
        type: [String],
    },
    categoryName: {
        type: String,
    },
    categoryID: {
        type: [String],
    },
    phone: {
        type: String,
    },
    email: {
        type: String,
    },
    logo: {
        type: String,
    },
    password: {
        type: String,
    },
    type: {
        type: String,
    },
    tipo: {
        type: [String],
    },
    schedule: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
    scheduleOnly: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
    ispartners: {
        type: Boolean,
        default: true,
    },
    isnew: {
        type: Boolean,
        default: true,
    },
    llevar: {
        type: Boolean,
        default: true,
    },
    alegeno_url: {
        type: String,
    },
    autoshipping: {
        type: Boolean,
        default: false,
    },
    open: {
        type: Boolean,
        default: true,
    },
    previous_shipping: {
        type: Number,
    },
    stimateTime: {
        type: String,
    },
    slug: {
        type: String,
    },
    highkitchen: {
        type: Boolean,
        default: false,
    },
    inOffert: {
        type: Boolean,
        default: false,
    },
    includeCity: {
        type: [String],
    },
    OnesignalID: {
        type: String,
        unique: true,
    },
    channelLinkId: {
        type: String,
    },
    collections: {
        type: Boolean,
        default: false,
    },
    isDeliverectPartner: {
        type: Boolean,
        default: false,
    },
    salvingPack: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
    isOrdaticPartner: {
        type: Boolean,
        default: false,
    },
    isOrdaticPartnerID: {
        type: String,
    },
    adress: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
    city: {
        type: String,
    },
    contactCode: {
        type: String,
    },
    socialLink: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
    isOtterPartner: {
        type: Boolean,
        default: false,
    },
    OtterPartnerId: {
        type: String,
    },
    shorting: {
        type: Number,
    },
    noScheduled: {
        type: Boolean,
    },
    tagOffert: { type: String, default: null },
    cashPayment: { type: Boolean, default: false },
    forttalezaURL: { type: String, default: null },
});
// hashear los password antes de guardar
restaurantSchema.pre("save", function (next) {
    // Si el password no esta hasheado...
    if (!this.isModified("password")) {
        return next();
    }
    bcryptjs_1.default.genSalt(10, (err, salt) => {
        if (err)
            return next(err);
        bcryptjs_1.default.hash(this.password, salt, (err, hash) => {
            if (err)
                return next(err);
            this.password = hash;
            next();
        });
    });
});
exports.default = mongoose_1.default.model("restaurant", restaurantSchema);
