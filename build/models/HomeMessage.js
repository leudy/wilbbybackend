"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const homeMessageSchema = new mongoose_1.default.Schema({
    title: {
        type: String,
        required: true,
        text: true,
    },
    message: {
        type: String,
        required: true,
        text: true,
    },
    active: {
        type: Boolean,
        required: true,
    },
    city: {
        type: String,
        required: true,
    },
});
exports.default = mongoose_1.default.model("homeMessage", homeMessageSchema);
