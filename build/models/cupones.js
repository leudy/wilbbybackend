"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
//javiermartinez1990@gmail.com
mongoose_1.default.Promise = global.Promise;
const cuponesSchema = new mongoose_1.default.Schema({
    clave: { type: String, unique: true },
    descuento: Number,
    tipo: {
        type: String,
        enum: ["porcentaje", "dinero"],
        default: "porcentaje",
    },
    usage: {
        type: Number,
        default: 1,
    },
    expire: {
        type: Date,
        default: new Date(),
    },
    exprirable: {
        type: Boolean,
        default: false,
    },
    user: {
        type: String,
    },
    description: {
        type: String,
    },
    private: {
        type: Boolean,
        default: false,
    },
    uniqueStore: {
        type: Boolean,
        default: false,
    },
    store: {
        type: String,
    },
    city: { type: [String] },
    isShipping: {
        type: Boolean,
        default: false,
    },
});
exports.default = mongoose_1.default.model("cupones", cuponesSchema);
