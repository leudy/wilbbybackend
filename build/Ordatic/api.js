"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pushOrderTopOrdatic = void 0;
const config_1 = require("./config");
const data_1 = require("./data");
var request = require("request");
const data = JSON.stringify({
    appId: config_1.appId,
    appSecret: config_1.appSecret,
    name: "Wilbby DEMO",
    scopes: "api",
});
var options = {
    method: "POST",
    url: config_1.authURL,
    headers: {
        "Content-Type": "application/json",
    },
    body: data,
};
exports.pushOrderTopOrdatic = (order) => {
    request(options, function (error, response) {
        const credential = JSON.parse(response.body);
        var optionsHook = {
            method: "PATCH",
            url: `${config_1.webHookURL}/${config_1.appId}`,
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${credential.token}`,
            },
            body: JSON.stringify({
                app: {
                    webhookAuthHeader: "",
                    webhookUrl: "https://api.v1.wilbby.com/api/ordaticEvents",
                },
            }),
        };
        request(optionsHook, function (error, response) {
            if (error)
                throw new Error(error);
            console.log(response.body);
        });
        var options = {
            method: "POST",
            url: config_1.ordersURL,
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${credential.token}`,
            },
            body: JSON.stringify(data_1.OrderData(order)),
        };
        request(options, function (error, response) {
            if (error)
                throw new Error(error);
            console.log(response.body);
        });
    });
};
