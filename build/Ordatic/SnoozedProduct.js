"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SnoozedProduct = void 0;
const products_1 = __importDefault(require("../models/newMenu/products"));
const modifiers_1 = __importDefault(require("../models/newMenu/modifiers"));
exports.SnoozedProduct = (data) => {
    if (data.data.type === "item") {
        if (data.eventType === "item-disabled") {
            products_1.default.findOneAndUpdate({ plu: data.data.posRef }, {
                $set: {
                    snoozed: true,
                },
            }, (err, order) => { });
        }
        else {
            products_1.default.findOneAndUpdate({ plu: data.data.posRef }, {
                $set: {
                    snoozed: false,
                },
            }, (err, order) => { });
        }
    }
    else {
        if (data.eventType === "item-disabled") {
            modifiers_1.default.findOneAndUpdate({ plu: data.data.posRef }, {
                $set: {
                    snoozed: true,
                },
            }, (err, order) => { });
        }
        else {
            modifiers_1.default.findOneAndUpdate({ plu: data.data.posRef }, {
                $set: {
                    snoozed: false,
                },
            }, (err, order) => { });
        }
    }
};
