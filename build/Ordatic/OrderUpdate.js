"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderUpdate = void 0;
const ordenUpdate_1 = require("../Deliverect/ordenUpdate");
const sendNotification_1 = require("../Deliverect/sendNotification");
const order_1 = __importDefault(require("../models/newOrder/order"));
exports.OrderUpdate = (data) => __awaiter(void 0, void 0, void 0, function* () {
    const orden = yield order_1.default.findOne({ _id: data.data.order.orderId });
    switch (data.eventType) {
        case "order-created":
            sendNotification_1.sendNotification(
            //@ts-ignore
            orden === null || 
            //@ts-ignore
            orden === void 0 ? void 0 : 
            //@ts-ignore
            orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} Ha recibido tu pedido tu pedido.`, orden);
            break;
        case "order-acepted":
            ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Confirmada", 60);
            sendNotification_1.sendNotification(
            //@ts-ignore
            orden === null || 
            //@ts-ignore
            orden === void 0 ? void 0 : 
            //@ts-ignore
            orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} Ha confirmado tu pedido.`, orden);
            break;
        case "order-accepted":
            ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Confirmada", 60);
            sendNotification_1.sendNotification(
            //@ts-ignore
            orden === null || 
            //@ts-ignore
            orden === void 0 ? void 0 : 
            //@ts-ignore
            orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} Ha confirmado tu pedido.`, orden);
            break;
        case "order-in-kitchen":
            sendNotification_1.sendNotification(
            //@ts-ignore
            orden === null || 
            //@ts-ignore
            orden === void 0 ? void 0 : 
            //@ts-ignore
            orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} esta preparando tu comida en la cocina.`, orden);
            break;
        case "order-ready-for-delivery":
            sendNotification_1.sendNotification(
            //@ts-ignore
            orden === null || 
            //@ts-ignore
            orden === void 0 ? void 0 : 
            //@ts-ignore
            orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} ya ha terminado de preparar tu pedido en la cocina.`, orden);
            break;
        case "order-ready-for-delivery":
            ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Listo para recoger", 70);
            sendNotification_1.sendNotification(
            //@ts-ignore
            orden === null || 
            //@ts-ignore
            orden === void 0 ? void 0 : 
            //@ts-ignore
            orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} tiene tu pedido listo para recoger.`, orden);
            break;
        case "order-picked-up":
            ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "En camino", 80);
            sendNotification_1.sendNotification(
            //@ts-ignore
            orden === null || 
            //@ts-ignore
            orden === void 0 ? void 0 : 
            //@ts-ignore
            orden.customerData.OnesignalID, `Tu pedido ha salido del establecimiento y va de camino.`, orden);
            break;
        case "order-ready-for-delivery":
            ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Listo para recoger", 70);
            if ((orden === null || orden === void 0 ? void 0 : orden.orderType) === "delivery") {
                sendNotification_1.sendNotificationRider(orden.courierData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} tiene el pedido listo para recoger.`);
            }
            sendNotification_1.sendNotification(
            //@ts-ignore
            orden === null || 
            //@ts-ignore
            orden === void 0 ? void 0 : 
            //@ts-ignore
            orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} tiene el pedido listo para recoger por el repartidor.`, orden);
            break;
        case "order-canceled":
            ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Rechazada por la tienda", 10);
            sendNotification_1.sendNotification(
            //@ts-ignore
            orden === null || 
            //@ts-ignore
            orden === void 0 ? void 0 : 
            //@ts-ignore
            orden.customerData.OnesignalID, `Upps ${orden === null || orden === void 0 ? void 0 : orden.storeData.title} no ha podido aceptar tu pedido lo sentimos.`, orden);
            break;
        case "order-canceled":
            ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Rechazada por la tienda", 10);
            sendNotification_1.sendNotification(
            //@ts-ignore
            orden === null || 
            //@ts-ignore
            orden === void 0 ? void 0 : 
            //@ts-ignore
            orden.customerData.OnesignalID, `Upps ${orden === null || orden === void 0 ? void 0 : orden.storeData.title} no ha podido aceptar tu pedido lo sentimos.`, orden);
            break;
        default:
            break;
    }
});
