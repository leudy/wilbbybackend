"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const cupones_1 = __importDefault(require("../../models/cupones"));
const order_1 = __importDefault(require("../../models/newOrder/order"));
const routerCupon = express_1.Router();
routerCupon.get("/get-discount", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const data = req.query;
    //@ts-ignore
    const cup = yield cupones_1.default.findOne({ clave: data.clave }).exec();
    if (cup) {
        const existOrder = yield order_1.default.find({
            //@ts-ignore
            customer: data.user,
            cupon: cup === null || cup === void 0 ? void 0 : cup._id,
            status: { $ne: "Pendiente de pago" },
        });
        //@ts-ignore
        if (existOrder.length >= cup.usage) {
            res
                .status(200)
                .json({
                success: false,
                message: "Ya has utilizado este cupón",
                data: null,
            })
                .end();
            //@ts-ignore
        }
        else if (cup.exprirable) {
            //@ts-ignore
            if (cup.expire < new Date()) {
                res
                    .status(200)
                    .json({
                    success: false,
                    message: "Este cupón ha expirado",
                    data: null,
                })
                    .end();
            }
        }
        else {
            //@ts-ignore
            cupones_1.default.findOne({ clave: data.clave }, (err, cupon) => {
                if (err) {
                    res
                        .status(400)
                        .json({ success: false, message: "Error del servidor", data: [] })
                        .end();
                }
                else if (!cupon) {
                    res
                        .status(200)
                        .json({
                        success: false,
                        message: "Este cupón no existe",
                        data: null,
                    })
                        .end();
                }
                else {
                    res
                        .status(200)
                        .json({
                        success: true,
                        message: "Data extraida con éxito",
                        data: cupon,
                    })
                        .end();
                }
            });
        }
    }
    else {
        res
            .status(200)
            .json({
            success: false,
            message: "Este cupón no existe",
            data: null,
        })
            .end();
    }
}));
exports.default = routerCupon;
