"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const ContactEmail_1 = __importDefault(require("../RecoveryPassword/Email/ContactEmail"));
const ContactRider_1 = __importDefault(require("../RecoveryPassword/Email/ContactRider"));
const ContactAds_1 = __importDefault(require("../RecoveryPassword/Email/ContactAds"));
const nodemailer = require("nodemailer");
const mailjetTransport = require('nodemailer-mailjet-transport');
const transporter = nodemailer.createTransport(mailjetTransport({
    service: 'Mailjet',
    host: "in-v3.mailjet.com",
    secure: true,
    port: 587,
    auth: {
        apiKey: process.env.MJ_APIKEY_PUBLIC,
        apiSecret: process.env.MJ_APIKEY_PRIVATE
    }
}));
class ContactRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.post("/contact-for-restaurant", (req, res) => {
            const { values } = req.body;
            const mailOptions = {
                from: process.env.EMAIL_ADDRESS,
                to: `info@wilbby.com`,
                subject: "Solicitud de para unirse a Wilbby",
                text: "Contacto nuevo establecimiento",
                html: ContactEmail_1.default(values),
            };
            transporter.sendMail(mailOptions);
            res.json({
                success: true,
                messages: "Solicitud enviada con éxito en 24 o 48 horas te contactaremos",
            });
        });
        this.router.post("/contact-for-rider", (req, res) => {
            const { values } = req.body;
            const mailOptions = {
                from: process.env.EMAIL_ADDRESS,
                to: `info@wilbby.com`,
                subject: "Solicitud Riders",
                text: "Solicitud para repartir con Wilbby",
                html: ContactRider_1.default(values),
            };
            transporter.sendMail(mailOptions);
            res.json({
                success: true,
                messages: "Solicitud enviada con éxito en 24 o 48 horas te contactaremos",
            });
        });
        this.router.post("/contact-for-ads", (req, res) => {
            const { values } = req.body;
            const mailOptions = {
                from: process.env.EMAIL_ADDRESS,
                to: `info@wilbby.com`,
                subject: "Solicitud informacion sobre piblicidad en Wilbby",
                text: "Solicitud informacion sobre piblicidad en Wilbby",
                html: ContactAds_1.default(values),
            };
            transporter.sendMail(mailOptions);
            res.json({
                success: true,
                messages: "Información recibida el equipo de Wilbby te contactará lo antes posible",
            });
        });
    }
}
const constctRouter = new ContactRouter();
constctRouter.routes();
exports.default = constctRouter.router;
