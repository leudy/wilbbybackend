"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const dotenv_1 = __importDefault(require("dotenv"));
const riders_1 = __importDefault(require("../models/riders"));
const user_1 = __importDefault(require("../models/user"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const mongoose_1 = require("mongoose");
const moment_1 = __importDefault(require("moment"));
const order_1 = __importDefault(require("../models/newOrder/order"));
dotenv_1.default.config({ path: "variables.env" });
const { ObjectId } = mongoose_1.Types;
class userAuth {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get("/get-user-auth", (req, res) => {
            const { token } = req.headers;
            if (token !== undefined || null) {
                try {
                    //@ts-ignore
                    const usuarioActual = jsonwebtoken_1.default.verify(token, process.env.SECRETO);
                    if (usuarioActual) {
                        user_1.default.findOne(
                        //@ts-ignore
                        { _id: usuarioActual._id }, (err, user) => {
                            if (err) {
                                res
                                    .status(400)
                                    .json({
                                    success: false,
                                    message: "Error del servidor",
                                    error: err,
                                    data: null,
                                })
                                    .end();
                            }
                            else {
                                res
                                    .status(200)
                                    .json({
                                    success: true,
                                    message: "Data extraida con éxito",
                                    data: user,
                                })
                                    .end();
                            }
                        });
                    }
                    else {
                        res
                            .status(404)
                            .json({
                            success: false,
                            message: "Debes iniciar sesión para continuar",
                            error: null,
                            data: null,
                        })
                            .end();
                    }
                }
                catch (err) {
                    console.log(err);
                    //@ts-ignore
                    if (err.message === "jwt expired") {
                        res
                            .status(404)
                            .json({
                            success: false,
                            message: "Tu sesión he expirado",
                            error: err,
                            data: null,
                        })
                            .end();
                    }
                }
            }
            else {
                res
                    .status(200)
                    .json({
                    success: false,
                    message: "Debes iniciar sesión para continuar",
                    error: null,
                    data: null,
                })
                    .end();
            }
        });
        this.router.get("/get-rider-auth", (req, res) => {
            const { token } = req.headers;
            if (token !== undefined || null) {
                try {
                    //@ts-ignore
                    const usuarioActual = jsonwebtoken_1.default.verify(token, process.env.SECRETO);
                    if (usuarioActual) {
                        riders_1.default.findOne(
                        //@ts-ignore
                        { _id: usuarioActual._id }, (err, user) => {
                            if (err) {
                                res
                                    .status(400)
                                    .json({
                                    success: false,
                                    message: "Error del servidor",
                                    error: err,
                                    data: null,
                                })
                                    .end();
                            }
                            else {
                                res
                                    .status(200)
                                    .json({
                                    success: true,
                                    message: "Data extraida con éxito",
                                    data: user,
                                })
                                    .end();
                            }
                        });
                    }
                    else {
                        res
                            .status(404)
                            .json({
                            success: false,
                            message: "Debes iniciar sesión para continuar",
                            error: null,
                            data: null,
                        })
                            .end();
                    }
                }
                catch (err) {
                    console.log(err);
                    //@ts-ignore
                    if (err.message === "jwt expired") {
                        res
                            .status(404)
                            .json({
                            success: false,
                            message: "Tu sesión he expirado",
                            error: err,
                            data: null,
                        })
                            .end();
                    }
                }
            }
            else {
                res
                    .status(200)
                    .json({
                    success: false,
                    message: "Debes iniciar sesión para continuar",
                    error: null,
                    data: null,
                })
                    .end();
            }
        });
        this.router.get("/get-order", (req, res) => {
            const { user, date } = req.query;
            // @ts-ignore
            const maxDate = moment_1.default(date).add(1, "days").format("YYYY-MM-DD");
            const today = moment_1.default().format("YYYY-MM-DD");
            const statusAll = [
                "Nueva",
                "Confirmada",
                "En la cocina",
                "Listo para recoger",
                "Preparando para el envío",
                "En camino",
                "Entregada",
                "Rechazado",
                "Rechazada por la tienda",
                "Rechazada por el rider",
                "Devuelto",
                "Finalizada",
                "Resolución",
            ];
            const statusSmall = [
                "Nueva",
                "Confirmada",
                "En la cocina",
                "Listo para recoger",
                "Preparando para el envío",
                "En camino",
            ];
            const status = date === today ? statusSmall : statusAll;
            let matchQuery = {
                // @ts-ignore
                courier: ObjectId(user),
                status: {
                    $in: status,
                },
                created_at: {
                    // @ts-ignore
                    $gte: new Date(date),
                    $lte: new Date(maxDate),
                },
            };
            order_1.default
                .aggregate([
                { $match: matchQuery },
                {
                    $group: {
                        _id: { month: { $month: "$created_at" } },
                        // @ts-ignore
                        [moment_1.default(date).format("YYYY-MM-DD")]: {
                            $addToSet: {
                                _id: "$_id",
                                status: "$status",
                                store: "$storeData.title",
                                customername: "$customerData.name",
                                customerlastname: "$customerData.lastName",
                                created_at: "$created_at",
                                addressStore: "$storeData.adress.calle",
                                addressStorenumber: "$storeData.adress.numero",
                                address: "$deliveryAddressData.formatted_address",
                                addressnumber: "$deliveryAddressData.puertaPiso",
                                type: "$storeData.storeData.categoryName",
                                scheduled: "$scheduled",
                                deliveryTime: "$deliveryTime",
                                channelOrderDisplayId: "$channelOrderDisplayId",
                            },
                        },
                    },
                },
                {
                    $sort: {
                        "_id.month": -1,
                    },
                },
            ])
                .then((resp) => {
                if (resp.length > 0) {
                    const format = resp[0];
                    delete format._id;
                    res.status(200).json({ success: true, data: format }).end();
                }
                else {
                    res
                        .status(200)
                        // @ts-ignore
                        .json({ success: true, data: { [date]: [] } })
                        .end();
                }
            })
                .catch((err) => {
                // @ts-ignore
                res
                    .status(200)
                    // @ts-ignore
                    .json({ success: false, data: { [date]: [] } })
                    .end();
            });
        });
        this.router.post("/exist-user", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { email } = req.body;
            if (email) {
                const user = yield user_1.default.findOne({ email });
                if (user) {
                    res.status(200).json({ success: false, message: "Existe una cuenta con este email intentalo con otro diferente" }).end();
                }
                else {
                    res.status(200).json({ success: true, message: "Puedes cuntinuar" }).end();
                }
            }
            else {
                res.status(200).json({ success: false, message: "Algo salio mal intentalo de nuevo por favor" }).end();
            }
        }));
    }
}
const userAuths = new userAuth();
exports.default = userAuths.router;
