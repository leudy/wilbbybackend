"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const dowload_app_1 = __importDefault(require("./../models/dowload_app"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const riders_1 = __importDefault(require("../models/riders"));
const cityclose_1 = __importDefault(require("../models/cityclose"));
const HomeMessage_1 = __importDefault(require("../models/HomeMessage"));
const sendNotificationRider_1 = require("../GraphQL/sendNotificationRider");
class IndexRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get("/", (req, res) => res.redirect("https://wilbby.com"));
        this.router.post("/download-app", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { source, referred, campaing } = req.body;
            const newData = new dowload_app_1.default({
                referred: referred,
                source: source,
                date: new Date(),
                campaing: campaing,
            });
            newData
                .save()
                .then(() => {
                res.json({ success: true, messages: "done" });
            })
                .catch(() => res.json({ success: false, messages: "failed" }));
        }));
        this.router.get("/riders-available", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { city } = req.query;
            riders_1.default.find({ city: city, isAvalible: true }, (err, resp) => {
                if (err) {
                    res
                        .json({
                        success: false,
                        messages: "No riders available",
                        data: null,
                    })
                        .end();
                }
                else {
                    res
                        .json({
                        success: true,
                        messages: "Riders available",
                        data: resp,
                    })
                        .end();
                }
            });
        }));
        this.router.get("/cities-available", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { city } = req.query;
            cityclose_1.default.findOne({ city: city }, (err, resp) => {
                if (err) {
                    res
                        .json({
                        success: false,
                        messages: "No citie available",
                        data: null,
                    })
                        .end();
                }
                else {
                    res
                        .json({
                        success: true,
                        messages: "Citie available",
                        data: resp,
                    })
                        .end();
                }
            });
        }));
        this.router.get("/get-notification-home", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { city } = req.query;
            // @ts-ignore
            HomeMessage_1.default.findOne({ city: city }, (err, resp) => {
                if (err) {
                    res
                        .json({
                        success: false,
                        messages: "No message available",
                        data: null,
                    })
                        .end();
                }
                else {
                    res
                        .json({
                        success: true,
                        messages: "Message available",
                        data: resp,
                    })
                        .end();
                }
            });
        }));
        this.router.get("/store-wilbby", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const data = yield restaurant_1.default.find({ city: `${req.query.city}` });
            res.json(data).end();
        }));
        this.router.get("/tets-notification", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { OnesignalID } = req.query;
            sendNotificationRider_1.sendNotification(OnesignalID, "Notificación de Prueba", "Notificación de Prueba", true);
            res.status(200).json({ data: "Mesanje enviado" }).end();
        }));
    }
}
//d7618df2-594d-4b1c-9151-71b3afc3a11c
const indexRouter = new IndexRouter();
indexRouter.routes();
exports.default = indexRouter.router;
