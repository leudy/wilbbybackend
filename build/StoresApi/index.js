"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const restaurant_1 = __importDefault(require("../models/restaurant"));
class MenuRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.options("/stores", function (req, res) {
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader("Access-Control-Allow-Methods", "*");
            res.setHeader("Access-Control-Allow-Headers", "*");
            res.end();
        });
        this.router.get("/stores", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { city, limit, page } = req.query;
            const store = yield restaurant_1.default.find({ includeCity: { $all: [city] }, open: true })
                .limit(Number(limit) * 1)
                .skip((Number(page) - 1) * Number(limit))
                .sort({ $natural: -1 })
                .exec();
            res.json(store);
        }));
    }
}
const menuRouter = new MenuRouter();
menuRouter.routes();
exports.default = menuRouter.router;
