"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const category_1 = __importDefault(require("../models/newMenu/category"));
class MenuRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.options("/menu", function (req, res) {
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader("Access-Control-Allow-Methods", "*");
            res.setHeader("Access-Control-Allow-Headers", "*");
            res.end();
        });
        this.router.post("/menu", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const menu = new category_1.default(req.body);
            menu.sorting = yield category_1.default.estimatedDocumentCount();
            yield menu.save();
            res.json(menu);
        }));
        this.router.get("/menu", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { idstore } = req.query;
            //@ts-ignore
            const menu = yield category_1.default.find({ storeId: idstore });
            res.json(menu);
        }));
        this.router.put("/menu", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const tasksIds = req.body;
            for (const [i, id] of tasksIds.entries()) {
                yield category_1.default.updateOne({ _id: id }, { sorting: i });
                // console.log(i, id)
            }
            res.json("the list was ordered");
        }));
    }
}
const menuRouter = new MenuRouter();
menuRouter.routes();
exports.default = menuRouter.router;
