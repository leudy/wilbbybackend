"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dataOrder = void 0;
exports.dataOrder = (data) => {
    const items = data.items.map((element) => {
        const SubItem = element.items.subItems.map((elements) => {
            const sub = elements && elements.subItems ? elements.subItems : [];
            const su = sub.map((elemento) => {
                const item2 = {
                    plu: elemento.plu,
                    name: elemento.name,
                    price: elemento.price,
                    quantity: elemento.quantity,
                    productType: 1,
                    remark: "",
                    subItems: [],
                };
                return item2;
            });
            const item1 = {
                plu: elements.plu,
                name: elements.name,
                price: elements.price,
                quantity: elements.quantity,
                productType: 1,
                remark: "",
                subItems: su,
            };
            return item1;
        });
        const item = {
            plu: element.items.plu,
            name: element.items.name,
            price: element.items.price,
            quantity: element.items.quantity,
            productType: 1,
            remark: "",
            subItems: SubItem,
        };
        return item;
    });
    return {
        channelOrderId: data._id,
        channelOrderDisplayId: data.channelOrderDisplayId,
        channelLinkId: data.storeData.channelLinkId,
        by: "Wilbby",
        orderType: data.orderType === "delivery" ? 2 : 1,
        table: "Wilbby",
        channel: 10000,
        pickupTime: data.pickupTime,
        estimatedPickupTime: data.estimatedPickupTime,
        deliveryTime: data.deliveryTime,
        deliveryIsAsap: true,
        courier: "Wilbby",
        customer: {
            name: `${data.customerData.name} ${data.customerData.lastName}`,
            companyName: "Wilbby",
            phoneNumber: data.customerData.telefono,
            email: data.customerData.email,
        },
        deliveryAddress: {
            street: data.deliveryAddressData
                ? data.deliveryAddressData.formatted_address
                : "NO Addres for takeaway",
            streetNumber: data.deliveryAddressData
                ? data.deliveryAddressData.puertaPiso
                : "00",
            postalCode: data.deliveryAddressData
                ? data.deliveryAddressData.postalcode
                : "000000",
            city: data.deliveryAddressData
                ? data.deliveryAddressData.city
                : "No city",
            extraAddressInfo: data.deliveryAddressData
                ? data.deliveryAddressData.type
                : "",
        },
        orderIsAlreadyPaid: true,
        payment: { amount: data.payment, type: 3 },
        note: data.note,
        items: items,
        decimalDigits: 2,
        numberOfCustomers: data.numberOfCustomers,
        deliveryCost: data.deliveryCost,
        serviceCharge: data.serviceCharge,
        discountTotal: -data.discountTotal,
        tip: data.tip,
    };
};
