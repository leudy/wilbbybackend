"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const restaurant_1 = __importDefault(require("../models/restaurant"));
const order_1 = __importDefault(require("../models/newOrder/order"));
const riders_1 = __importDefault(require("../models/riders"));
const ordenUpdate_1 = require("./ordenUpdate");
const sendNotification_1 = require("./sendNotification");
const category_1 = __importDefault(require("../models/newMenu/category"));
const bundles_1 = __importDefault(require("../models/newMenu/bundles"));
const Menu_1 = __importDefault(require("../models/newMenu/Menu"));
const modifierGroups_1 = __importDefault(require("../models/newMenu/modifierGroups"));
const modifiers_1 = __importDefault(require("../models/newMenu/modifiers"));
const products_1 = __importDefault(require("../models/newMenu/products"));
class WebHookRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.post("/api/deliverect/hook", (req, res) => {
            res.json({
                statusUpdateURL: "https://api.v1.wilbby.com/api/deliverect/statusUpdateURL",
                menuUpdateURL: "https://api.v1.wilbby.com/api/deliverect/menuUpdateURL",
                disabledProductsURL: "https://api.v1.wilbby.com/api/deliverect/disabledProductsURL",
                snoozeUnsnoozeURL: "https://api.v1.wilbby.com/api/deliverect/snoozeUnsnoozeURL",
                busyModeURL: "https://api.v1.wilbby.com/api/deliverect/busyModeURL",
            });
            restaurant_1.default.findOneAndUpdate({ slug: req.body.channelLocationId }, { channelLinkId: req.body.channelLinkId, isDeliverectPartner: true }, { new: true }, (error) => {
                if (error) {
                    res.status(400).end();
                }
                else {
                    res.status(200).end();
                }
            });
        });
        this.router.post("/api/deliverect/statusUpdateURL", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const orden = yield order_1.default.findById({ _id: req.body.channelOrderId });
            const rider = yield riders_1.default.findById({
                _id: (orden === null || orden === void 0 ? void 0 : orden.courier) ? orden === null || orden === void 0 ? void 0 : orden.courier : "5fb7de125fd35b9c72ce2423",
            });
            const orderUpdate = () => {
                switch (req.body.status) {
                    case 10:
                        sendNotification_1.sendNotification(
                        //@ts-ignore
                        orden === null || 
                        //@ts-ignore
                        orden === void 0 ? void 0 : 
                        //@ts-ignore
                        orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} Ha recibido tu pedido tu pedido.`, orden);
                        break;
                    case 20:
                        ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Confirmada", 60);
                        sendNotification_1.sendNotification(
                        //@ts-ignore
                        orden === null || 
                        //@ts-ignore
                        orden === void 0 ? void 0 : 
                        //@ts-ignore
                        orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} Ha confirmado tu pedido.`, orden);
                        break;
                    case 30:
                        ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Nueva", 20);
                        sendNotification_1.sendNotification(
                        //@ts-ignore
                        orden === null || 
                        //@ts-ignore
                        orden === void 0 ? void 0 : 
                        //@ts-ignore
                        orden.customerData.OnesignalID, `El pedido se ha recibido varias veces en el TPV debido a un error técnico.`, orden);
                        break;
                    case 50:
                        sendNotification_1.sendNotification(
                        //@ts-ignore
                        orden === null || 
                        //@ts-ignore
                        orden === void 0 ? void 0 : 
                        //@ts-ignore
                        orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} esta preparando tu comida en la cocina.`, orden);
                        break;
                    case 60:
                        sendNotification_1.sendNotification(
                        //@ts-ignore
                        orden === null || 
                        //@ts-ignore
                        orden === void 0 ? void 0 : 
                        //@ts-ignore
                        orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} ya ha terminado de preparar tu pedido en la cocina.`, orden);
                        break;
                    case 70:
                        ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Listo para recoger", 70);
                        sendNotification_1.sendNotification(
                        //@ts-ignore
                        orden === null || 
                        //@ts-ignore
                        orden === void 0 ? void 0 : 
                        //@ts-ignore
                        orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} tiene tu pedido listo para recoger.`, orden);
                        break;
                    case 80:
                        ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "En camino", 80);
                        sendNotification_1.sendNotification(
                        //@ts-ignore
                        orden === null || 
                        //@ts-ignore
                        orden === void 0 ? void 0 : 
                        //@ts-ignore
                        orden.customerData.OnesignalID, `Tu pedido ha salido del establecimiento y va de camino.`, orden);
                        break;
                    case 90:
                        ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Listo para recoger", 70);
                        if ((orden === null || orden === void 0 ? void 0 : orden.orderType) === "delivery") {
                            sendNotification_1.sendNotificationRider(rider === null || rider === void 0 ? void 0 : rider.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} tiene el pedido listo para recoger.`);
                        }
                        sendNotification_1.sendNotification(
                        //@ts-ignore
                        orden === null || 
                        //@ts-ignore
                        orden === void 0 ? void 0 : 
                        //@ts-ignore
                        orden.customerData.OnesignalID, `${orden === null || orden === void 0 ? void 0 : orden.storeData.title} tiene el pedido listo para recoger por el rider.`, orden);
                        break;
                    case 100:
                        ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Rechazada por la tienda", 10);
                        sendNotification_1.sendNotification(
                        //@ts-ignore
                        orden === null || 
                        //@ts-ignore
                        orden === void 0 ? void 0 : 
                        //@ts-ignore
                        orden.customerData.OnesignalID, `Upps ${orden === null || orden === void 0 ? void 0 : orden.storeData.title} no ha podido aceptar tu pedido lo sentimos.`, orden);
                        break;
                    case 110:
                        ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Rechazada por la tienda", 10);
                        sendNotification_1.sendNotification(
                        //@ts-ignore
                        orden === null || 
                        //@ts-ignore
                        orden === void 0 ? void 0 : 
                        //@ts-ignore
                        orden.customerData.OnesignalID, `Upps ${orden === null || orden === void 0 ? void 0 : orden.storeData.title} no ha podido aceptar tu pedido lo sentimos.`, orden);
                        break;
                    case 120:
                        ordenUpdate_1.ordenUpdate(orden === null || orden === void 0 ? void 0 : orden._id, "Devuelto", 10);
                        sendNotification_1.sendNotification(
                        //@ts-ignore
                        orden === null || 
                        //@ts-ignore
                        orden === void 0 ? void 0 : 
                        //@ts-ignore
                        orden.customerData.OnesignalID, `Upps el pedido ha fallado lo sentimos.`, orden);
                        break;
                    default:
                        break;
                }
            };
            orderUpdate();
            res.status(200).end();
        }));
        this.router.post("/api/deliverect/menuUpdateURL", (req, res) => {
            req.body.forEach((element) => __awaiter(this, void 0, void 0, function* () {
                console.log(element);
                const restaurant = yield restaurant_1.default.findOne({
                    channelLinkId: element.channelLinkId,
                });
                element.storeId = restaurant === null || restaurant === void 0 ? void 0 : restaurant._id;
                const newMenu = new Menu_1.default(element);
                Menu_1.default.deleteMany({ storeId: restaurant === null || restaurant === void 0 ? void 0 : restaurant._id })
                    .then(function () {
                    newMenu.save();
                })
                    .catch(function (error) {
                    console.log(error); // Failure
                });
                element.categories.forEach((category, i) => __awaiter(this, void 0, void 0, function* () {
                    const dataCategory = category;
                    dataCategory.storeId = restaurant === null || restaurant === void 0 ? void 0 : restaurant._id;
                    dataCategory.menu = newMenu._id;
                    dataCategory.sorting = i;
                    category_1.default
                        .deleteMany({ storeId: restaurant === null || restaurant === void 0 ? void 0 : restaurant._id })
                        .then(function () {
                        const newCategory = new category_1.default(dataCategory);
                        newCategory.save();
                    })
                        .catch(function (error) {
                        console.log(error); // Failure
                    });
                }));
                var products = element.products;
                var Product = Object.keys(products);
                Product.forEach((key) => __awaiter(this, void 0, void 0, function* () {
                    // product for Key
                    const product = products[key];
                    product.storeId = restaurant === null || restaurant === void 0 ? void 0 : restaurant._id;
                    const newProduct = new products_1.default(product);
                    products_1.default
                        .deleteMany({ storeId: restaurant === null || restaurant === void 0 ? void 0 : restaurant._id })
                        .then(function () {
                        return __awaiter(this, void 0, void 0, function* () {
                            newProduct.save();
                        });
                    })
                        .catch(function (error) {
                        console.log(error); // Failure
                    });
                }));
                var bundles = element.bundles;
                var Bundle = Object.keys(bundles);
                Bundle.forEach((key) => __awaiter(this, void 0, void 0, function* () {
                    const bundle = bundles[key];
                    const newBundle = new bundles_1.default(bundle);
                    bundles_1.default
                        .deleteMany({ parentId: bundle.parentId })
                        .then(function () {
                        return __awaiter(this, void 0, void 0, function* () {
                            newBundle.save();
                        });
                    })
                        .catch(function (error) {
                        console.log(error); // Failure
                    });
                }));
                var modifierGroup = element.modifierGroups;
                var ModifierGroup = Object.keys(modifierGroup);
                ModifierGroup.forEach((key) => __awaiter(this, void 0, void 0, function* () {
                    const modifierGroups = modifierGroup[key];
                    const newmodifierGroupsSchema = new modifierGroups_1.default(modifierGroups);
                    modifierGroups_1.default
                        .deleteMany({ parentId: modifierGroups.parentId })
                        .then(function () {
                        return __awaiter(this, void 0, void 0, function* () {
                            newmodifierGroupsSchema.save();
                        });
                    })
                        .catch(function (error) {
                        console.log(error); // Failure
                    });
                }));
                var modifier = element.modifiers;
                var Modifier = Object.keys(modifier);
                Modifier.forEach((key) => __awaiter(this, void 0, void 0, function* () {
                    const modifiers = modifier[key];
                    const newmodifier = new modifiers_1.default(modifiers);
                    modifiers_1.default
                        .deleteMany({ parentId: modifiers.parentId })
                        .then(function () {
                        return __awaiter(this, void 0, void 0, function* () {
                            newmodifier.save();
                        });
                    })
                        .catch(function (error) {
                        console.log(error); // Failure
                    });
                }));
            }));
            res.status(200).end();
        });
        this.router.post("/api/deliverect/snoozeUnsnoozeURL", (req, res) => {
            req.body.operations.forEach((element) => {
                element.data.items.forEach((x) => {
                    products_1.default.findOneAndUpdate({ plu: x.plu }, {
                        $set: {
                            snoozed: element.action === "snooze" ? true : false,
                        },
                    }, (e, r) => console.log(e, r));
                });
            });
            res.status(200).end();
        });
        this.router.post("/api/deliverect/busyModeURL", (req, res) => {
            restaurant_1.default.findOneAndUpdate({ channelLinkId: req.body.channelLinkId }, {
                $set: {
                    open: req.body.status === "PAUSED" ? false : true,
                },
            }, (e, r) => console.log(e, r));
            res.status(200).end();
        });
    }
}
const webHookRouter = new WebHookRouter();
webHookRouter.routes();
exports.default = webHookRouter.router;
