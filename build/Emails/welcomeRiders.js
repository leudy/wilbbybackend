"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.welcomeRiderEmail = void 0;
const emailRider_1 = __importDefault(require("./emailRider"));
const riders_1 = __importDefault(require("../models/riders"));
const crypto_1 = __importDefault(require("crypto"));
const nodemailer_1 = __importDefault(require("nodemailer"));
const mailjetTransport = require('nodemailer-mailjet-transport');
exports.welcomeRiderEmail = (email) => {
    const token = crypto_1.default.randomBytes(20).toString("hex");
    var newvalues = { $set: { forgotPasswordToken: token } };
    const transporter = nodemailer_1.default.createTransport(mailjetTransport({
        service: 'Mailjet',
        host: "in-v3.mailjet.com",
        secure: true,
        port: 587,
        auth: {
            apiKey: process.env.MJ_APIKEY_PUBLIC,
            apiSecret: process.env.MJ_APIKEY_PRIVATE
        }
    }));
    const mailOptions = {
        from: process.env.EMAIL_ADDRESS,
        to: email,
        subject: `ACTIVAR CUENTA WILBBY - Bienvenido a Wilbby para Riders`,
        text: `ACTIVAR CUENTA WILBBY - Bienvenido a Wilbby para Riders`,
        html: emailRider_1.default(token, email),
    };
    transporter.sendMail(mailOptions, (err) => {
        if (err) {
            console.log("Error al enviar");
        }
        else {
            riders_1.default.findOneAndUpdate({ email: email }, newvalues, {
                //options
                new: true,
                strict: false,
                useFindAndModify: false,
            }, (err, updated) => { });
        }
    });
};
