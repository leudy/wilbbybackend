"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_1 = __importDefault(require("../models/user"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const riders_1 = __importDefault(require("../models/riders"));
const crypto_1 = __importDefault(require("crypto"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const nodemailer = require("nodemailer");
const RecoveryPassword_1 = __importDefault(require("../RecoveryPassword/Email/RecoveryPassword"));
const RecoveryPasswordrestaurant_1 = __importDefault(require("../RecoveryPassword/Email/RecoveryPasswordrestaurant"));
const RecoveryPasswordriders_1 = __importDefault(require("../RecoveryPassword/Email/RecoveryPasswordriders"));
const mailjetTransport = require('nodemailer-mailjet-transport');
class RecoveryRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get("/forgotpassword", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const email = req.query.email;
            yield user_1.default.find({ email: email }, (err, data) => {
                if (data.length < 1 || err) {
                    res.status(200).json({
                        message: "Aún no tenemos este correo eletrónico",
                        success: false,
                        noEmail: true,
                    });
                }
                else {
                    const token = crypto_1.default.randomBytes(20).toString("hex");
                    // write to database
                    const transporter = nodemailer.createTransport(mailjetTransport({
                        service: 'Mailjet',
                        host: "in-v3.mailjet.com",
                        secure: true,
                        port: 587,
                        auth: {
                            apiKey: process.env.MJ_APIKEY_PUBLIC,
                            apiSecret: process.env.MJ_APIKEY_PRIVATE
                        }
                    }));
                    const mailOptions = {
                        from: process.env.EMAIL_ADDRESS,
                        to: email,
                        subject: "Recuperar contraseña Wilbby App",
                        text: "Recuperar contraseña Wilbby App",
                        html: RecoveryPassword_1.default(token, email),
                    };
                    user_1.default.findOneAndUpdate({ email: email }, { forgotPasswordToken: token }, {
                        new: true,
                    }, (err, updated) => {
                        if (err) {
                            res
                                .status(200)
                                .json({ message: "Algo salió mal intentalo de nuevo", success: false });
                        }
                        else {
                            res
                                .status(200)
                                .json({ message: "Email enviado a tu bandeja de entrada", success: true });
                        }
                    });
                    transporter.sendMail(mailOptions);
                }
            });
        }));
        this.router.post("/tokenValidation", (req, res) => {
            user_1.default.find({ forgotPasswordToken: req.body.token }, (err, data) => {
                if (err || data.length < 1) {
                    if (err)
                        console.log(err);
                    res.status(200).json({ isValid: false, email: "" });
                }
                else if (data.length > 0) {
                    res.status(200).json({ isValid: true, email: data[0].email });
                }
            });
        });
        this.router.post("/resetPassword", (req, res) => {
            var newvalues = { $unset: { forgotPasswordToken: req.body.token } };
            user_1.default.findOneAndUpdate({ email: req.body.email }, newvalues, {
                //options
                new: true,
                strict: false,
                useFindAndModify: false,
            }, (err, updated) => {
                if (err)
                    console.log(err);
                bcryptjs_1.default.genSalt(10, (err, salt) => {
                    if (err)
                        console.log(err);
                    bcryptjs_1.default.hash(req.body.password, salt, (err, hash) => {
                        if (err)
                            console.log(err);
                        var newvalues2 = { $set: { password: hash } };
                        user_1.default.findOneAndUpdate({ email: req.body.email }, newvalues2, {
                            //options
                            new: true,
                            strict: false,
                            useFindAndModify: false,
                        }, (err, updated) => {
                            if (err)
                                console.log(err);
                            res.status(200).json({ changed: true });
                        });
                    });
                });
            });
        });
        /// recovery restaurant
        this.router.get("/forgotpassword-restaurant", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const email = req.query.email;
            yield restaurant_1.default.find({ email: email }, (err, data) => {
                if (data.length < 1 || err) {
                    res.status(400).json({
                        message: "Aún no tenemos este correo eletrónico",
                        success: false,
                        noEmail: true,
                    });
                }
                else {
                    const token = crypto_1.default.randomBytes(20).toString("hex");
                    // write to database
                    const transporter = nodemailer.createTransport(mailjetTransport({
                        service: 'Mailjet',
                        host: "in-v3.mailjet.com",
                        secure: true,
                        port: 587,
                        auth: {
                            apiKey: process.env.MJ_APIKEY_PUBLIC,
                            apiSecret: process.env.MJ_APIKEY_PRIVATE
                        }
                    }));
                    const mailOptions = {
                        from: process.env.EMAIL_ADDRESS,
                        to: email,
                        subject: "Recuperar contraseña Wilbby para restaurantes",
                        text: "Recuperar contraseña Wilbby para restaurantes",
                        html: RecoveryPasswordrestaurant_1.default(token, email),
                    };
                    restaurant_1.default.findOneAndUpdate({ email: email }, { forgotPasswordToken: token }, {
                        new: true,
                    }, (err, updated) => {
                        if (err) {
                            res
                                .status(200)
                                .json({ message: "Algo salió mal intentalo de nuevo", success: false });
                        }
                        else {
                            res
                                .status(200)
                                .json({ message: "Email enviado a tu bandeja de entrada", success: true });
                        }
                    });
                    transporter.sendMail(mailOptions);
                }
            });
        }));
        this.router.post("/tokenValidation-restaurant", (req, res) => {
            restaurant_1.default.find({ forgotPasswordToken: req.body.token }, (err, data) => {
                if (err || data.length < 1) {
                    if (err)
                        console.log(err);
                    res.status(200).json({ isValid: false, email: "" });
                }
                else if (data.length > 0) {
                    res.status(200).json({ isValid: true, email: data[0].email });
                }
            });
        });
        this.router.post("/resetPassword-restaurant", (req, res) => {
            var newvalues = { $unset: { forgotPasswordToken: req.body.token } };
            restaurant_1.default.findOneAndUpdate({ email: req.body.email }, newvalues, {
                //options
                new: true,
                strict: false,
                useFindAndModify: false,
            }, (err, updated) => {
                if (err)
                    console.log(err);
                bcryptjs_1.default.genSalt(10, (err, salt) => {
                    if (err)
                        console.log(err);
                    bcryptjs_1.default.hash(req.body.password, salt, (err, hash) => {
                        if (err)
                            console.log(err);
                        var newvalues2 = { $set: { password: hash } };
                        restaurant_1.default.findOneAndUpdate({ email: req.body.email }, newvalues2, {
                            //options
                            new: true,
                            strict: false,
                            useFindAndModify: false,
                        }, (err, updated) => {
                            if (err)
                                console.log(err);
                            res.status(200).json({ changed: true });
                        });
                    });
                });
            });
        });
        /// recovery riders
        this.router.get("/forgotpassword-riders", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const email = req.query.email;
            yield riders_1.default.find({ email: email }, (err, data) => {
                if (data.length < 1 || err) {
                    res.status(400).json({
                        message: "Aún no tenemos este correo eletrónico",
                        success: false,
                        noEmail: true,
                    });
                }
                else {
                    const token = crypto_1.default.randomBytes(20).toString("hex");
                    // write to database
                    const transporter = nodemailer.createTransport(mailjetTransport({
                        service: 'Mailjet',
                        host: "in-v3.mailjet.com",
                        secure: true,
                        port: 587,
                        auth: {
                            apiKey: process.env.MJ_APIKEY_PUBLIC,
                            apiSecret: process.env.MJ_APIKEY_PRIVATE
                        }
                    }));
                    const mailOptions = {
                        from: process.env.EMAIL_ADDRESS,
                        to: email,
                        subject: "Recuperar contraseña Wilbby para Riders",
                        text: "Recuperar contraseña Wilbby para Riders",
                        html: RecoveryPasswordriders_1.default(token, email),
                    };
                    riders_1.default.findOneAndUpdate({ email: email }, { forgotPasswordToken: token }, {
                        new: true,
                    }, (err, updated) => {
                        if (err) {
                            res
                                .status(200)
                                .json({ message: "Algo salió mal intentalo de nuevo", success: false });
                        }
                        else {
                            res
                                .status(200)
                                .json({ message: "Email enviado a tu bandeja de entrada", success: true });
                        }
                    });
                    transporter.sendMail(mailOptions);
                }
            });
        }));
        this.router.post("/tokenValidation-riders", (req, res) => {
            riders_1.default.find({ forgotPasswordToken: req.body.token }, (err, data) => {
                if (err || data.length < 1) {
                    if (err)
                        console.log(err);
                    res.status(200).json({ isValid: false, email: "" });
                }
                else if (data.length > 0) {
                    res.status(200).json({ isValid: true, email: data[0].email });
                }
            });
        });
        this.router.post("/resetPassword-riders", (req, res) => {
            var newvalues = { $unset: { forgotPasswordToken: req.body.token } };
            riders_1.default.findOneAndUpdate({ email: req.body.email }, newvalues, {
                //options
                new: true,
                strict: false,
                useFindAndModify: false,
            }, (err, updated) => {
                if (err)
                    console.log(err);
                bcryptjs_1.default.genSalt(10, (err, salt) => {
                    if (err)
                        console.log(err);
                    bcryptjs_1.default.hash(req.body.password, salt, (err, hash) => {
                        if (err)
                            console.log(err);
                        var newvalues2 = { $set: { password: hash } };
                        riders_1.default.findOneAndUpdate({ email: req.body.email }, newvalues2, {
                            //options
                            new: true,
                            strict: false,
                            useFindAndModify: false,
                        }, (err, updated) => {
                            if (err)
                                console.log(err);
                            res.status(200).json({ changed: true });
                        });
                    });
                });
            });
        });
    }
}
const recoveryRouter = new RecoveryRouter();
recoveryRouter.routes();
exports.default = recoveryRouter.router;
