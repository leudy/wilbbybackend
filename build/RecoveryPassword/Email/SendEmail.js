"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.welcomeEmail = void 0;
const nodemailer = require("nodemailer");
const welcomeEmail_1 = __importDefault(require("./welcomeEmail"));
const mailjetTransport = require('nodemailer-mailjet-transport');
exports.welcomeEmail = (email, name) => {
    const transporter = nodemailer.createTransport(mailjetTransport({
        service: 'Mailjet',
        host: "in-v3.mailjet.com",
        secure: true,
        port: 587,
        auth: {
            apiKey: process.env.MJ_APIKEY_PUBLIC,
            apiSecret: process.env.MJ_APIKEY_PRIVATE
        }
    }));
    const mailOptions = {
        from: process.env.EMAIL_ADDRESS,
        to: email ? email : "info@wilbby.com",
        subject: "Bienvenido a Wilbby",
        text: "Con Wilbby lo tienes todo",
        html: welcomeEmail_1.default(name),
    };
    transporter.sendMail(mailOptions);
};
