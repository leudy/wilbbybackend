"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const t = (name) => {
    return `<head>
  <meta charset="utf-8"> <!-- utf-8 works for most cases -->
  <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
  <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
  <title>Bienvenido a Wilbby</title> <!-- The title tag shows in email notifications, like Android 4.4. -->
  
 <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;500;600;700;800;900&display=swap" rel="stylesheet">
  
  <!-- CSS Reset : BEGIN -->
  <style>
  
      /* What it does: Remove spaces around the email design added by some email clients. */
      /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
      html,
  body {
  margin: 0 auto !important;
  padding: 0 !important;
  height: 100% !important;
  width: 100% !important;
  background: #f1f1f1;
  }
  
  /* What it does: Stops email clients resizing small text. */
  * {
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
  }
  
  /* What it does: Centers email on Android 4.4 */
  div[style*="margin: 16px 0"] {
  margin: 0 !important;
  }
  
  /* What it does: Stops Outlook from adding extra spacing to tables. */
  table,
  td {
  mso-table-lspace: 0pt !important;
  mso-table-rspace: 0pt !important;
  }
  
  /* What it does: Fixes webkit padding issue. */
  table {
  border-spacing: 0 !important;
  border-collapse: collapse !important;
  table-layout: fixed !important;
  margin: 0 auto !important;
  }
  
  /* What it does: Uses a better rendering method when resizing images in IE. */
  img {
  -ms-interpolation-mode:bicubic;
  }
  
  /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
  a {
  text-decoration: none;
  }
  
  /* What it does: A work-around for email clients meddling in triggered links. */
  *[x-apple-data-detectors],  /* iOS */
  .unstyle-auto-detected-links *,
  .aBn {
  border-bottom: 0 !important;
  cursor: default !important;
  color: inherit !important;
  text-decoration: none !important;
  font-size: inherit !important;
  font-family: inherit !important;
  font-weight: inherit !important;
  line-height: inherit !important;
  }
  
  /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
  .a6S {
  display: none !important;
  opacity: 0.01 !important;
  }
  
  /* What it does: Prevents Gmail from changing the text color in conversation threads. */
  .im {
  color: inherit !important;
  }
  
  /* If the above doesn't work, add a .g-img class to any image in question. */
  img.g-img + div {
  display: none !important;
  }
  
  /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
  /* Create one of these media queries for each additional viewport size you'd like to fix */
  
  /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
  @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
  u ~ div .email-container {
      min-width: 320px !important;
  }
  }
  /* iPhone 6, 6S, 7, 8, and X */
  @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
  u ~ div .email-container {
      min-width: 375px !important;
  }
  }
  /* iPhone 6+, 7+, and 8+ */
  @media only screen and (min-device-width: 414px) {
  u ~ div .email-container {
      min-width: 414px !important;
  }
  }
  
  
  </style>
  
  <!-- CSS Reset : END -->
  
  <!-- Progressive Enhancements : BEGIN -->
  <style>
  
    .primary{
  background: #90C33C;
  }
  .bg_white{
  background: #ffffff;
  }
  .bg_light{
  background: #fafafa;
  }
  .bg_black{
  background: #000000;
  }
  .bg_dark{
  background: rgba(0,0,0,.8);
  }
  .email-section{
  padding:2.5em;
  }
  
  /*BUTTON*/
  .btn{
  padding: 5px 15px;
  display: inline-block;
  }
  .btn.btn-primary{
  border-radius: 100px;
  background: #90C33C;
  color: #fff;
  padding: 20px 5rem;
  margin-bottom: 30px;
  }
  .btn.btn-white{
  border-radius: 5px;
  background: #ffffff;
  color: #000000;
  }
  .btn.btn-white-outline{
  border-radius: 5px;
  background: transparent;
  border: 1px solid #fff;
  color:  #212121;
  }
  
  h1,h2,h3,h4,h5,h6{
  font-family: 'Poppins', sans-serif;
  color: #000000;
  margin-top: 0;
  font-weight: 400;
  }
  
  body{
  font-family: 'Poppins', sans-serif;
  font-weight: 400;
  font-size: 16px;
  line-height: 1.8;
  color: rgba(0,0,0,.4);
  }
  
  a{
  color: #90C33C;
  }
  
  table{
  }     
  
  .content_img{
    width: 100%;
    max-width: 600px;
    margin: 3rem auto;
    display: grid;
    grid-template-columns: 50% 50%;
    gap: 1rem;
    color: #212121 !important;
    text-align: center;
    justify-content: center;

  }

  .content_img img {
    width: 250px;
    text-align: center;
  }

  .content_img h2 {
    line-height: 1.3;
    font-size: 18px;
    font-weight: 900 !important;
  }

  .content_img p {
    line-height: 1.3;
    font-size: 14px;
    font-weight: 300;
    color: #222222;
  }

  .hero_wel {
    width: 100% !important;
    max-width: 600px; 
    margin: 0 !important;
    padding: 0 !important; 
    background-color: #90C33C !important; 
    height: auto; 
    min-height: 250px; 
    text-align: center; 
    align-items: center; 
    justify-content: center; 
  }

  .all {
    text-align: center;
    margin: 4rem 0;
    font-size: 38px;
    font-weight: 900 !important;
    color: #2d2861 !important;
  }

  /*LOGO*/
  
  .logo h1{
  margin: 0;
  line-height: 1;
  }
  .logo h1 a{
  color: #000000;
  font-size: 20px;
  font-weight: 700;
  text-transform: uppercase;
  font-family: 'Poppins', sans-serif;
  padding-top: 0;
  }
  
  /*HERO*/
  .hero{
  position: relative;
  z-index: 0;
  }
  .hero .overlay{
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  content: '';
  width: 100%;
  z-index: -1;
  background:  #0c1633;
  }
  
  .hero .text{
  color: rgba(255,255,255,.9);
  }
  .hero .text h2{
  color: #212121;
  font-size: 30px;
  margin-bottom: 0;
  font-weight: 500;
  }
  .hero .text h2 span{
  font-weight: 600;
  color: #90C33C;
  }
  
  
  ul.social{
  padding: 0;
  margin: 0;
  }
  ul.social li{
  display: inline-block;
  margin-right: 10px;
  }
  ul.social li a{
  border: 1px solid red;
  display: block;
  margin-bottom: 0;
  }
  
  /*HEADING SECTION*/
  .heading-section{
  }
  .heading-section h2{
  color: #000000;
  font-size: 28px;
  margin-top: 0;
  line-height: 1.4;
  font-weight: 400;
  }
  .heading-section .subheading{
  margin-bottom: 20px !important;
  display: inline-block;
  font-size: 13px;
  text-transform: uppercase;
  letter-spacing: 2px;
  color: rgba(0,0,0,.4);
  position: relative;
  }
  .heading-section .subheading::after{
  position: absolute;
  left: 0;
  right: 0;
  bottom: -10px;
  content: '';
  width: 100%;
  height: 2px;
  background: #90C33C;
  margin: 0 auto;
  }
  
  .heading-section-white{
  color: rgba(255,255,255,.8);
  }
  .heading-section-white h2{
  font-family: 'Poppins', sans-serif;
  line-height: 1;
  padding-bottom: 0;
  }
  .heading-section-white h2{
  color: #ffffff;
  }
  .heading-section-white .subheading{
  margin-bottom: 0;
  display: inline-block;
  font-size: 13px;
  text-transform: uppercase;
  letter-spacing: 2px;
  color: rgba(255,255,255,.4);
  }
  
  /*PROJECT*/
  .project-entry{
  position: relative;
  }
  .text-project{
  padding-top: 10px;
  position: absolute;
  bottom: 10px;
  left: 0;
  right: 0;
  }
  .text-project h3{
  margin-bottom: 0;
  font-size: 16px;
  font-weight: 600;
  }
  .text-project h3 a{
  color: #fff;
  }
  .text-project span{
  font-size: 13px;
  color: rgba(255,255,255,.8);
  }
  
  /*FOOTER*/

  .social {
    display: flex;
    flex-direction: row;
    text-align: center !important;
    align-items: center;
    justify-content: center;
    margin-left: auto;
    margin-right: auto;
    margin-top: 2rem;
    width: 200px;
  }

  .social a {
    margin: 0 5px;
    text-align: center !important;
  }

  .social a img {
    width: 40px;
    object-position: center center;
    object-fit: cover;
    text-align: center !important;
  }
  
  .footer{
  color: rgba(0,0,0,.8);
  }
  .footer a{
  /*color: rgba(0,0,0,1);*/
  }
  
  @media screen and (max-width: 768px) {
  .content_img{
    width: 100%;
    max-width: 300px;
    grid-template-columns: 100%;

  }
  
  }
  
  
  </style>
  
  
  </head>
  
  <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #222222;">
  <center style="width: 100%; background-color: #f1f1f1;">
  <div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
  </div>
  <div style="max-width: 600px; margin: 0 auto;" class="email-container">
    <!-- BEGIN BODY -->
    <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
      <tr>
        <td valign="middle" class="bg_white" style="padding: 1em 2.5em;">
          <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
              <td valign="middle" class="logo" style="text-align: center;">
                <a href="https://wilbby.com"><img src="https://wilbby.s3.eu-west-3.amazonaws.com/logoblack.png" style="width: 100px;"/></a>
              </td>
            </tr>
          </table>
        </td>
      </tr><!-- end tr -->
      <tr>
  
        <td valign="middle" class="hero" style="background-size: cover; height: 400px; background:  #f1f1f1;">
          <table>
            <tr>
              <td>
                <div class="text" style="text-align: center;">
                  <div class="hero_wel">
                    <p style="padding-top: 1rem;">Hola ${name}</p>

                  <h2 style="padding: 1rem 3rem; text-align: center; font-weight: bold; color: #fff !important;font-size: 36px;">Bienvenido a Wilbby</h2>

                  <p style="padding-top: 1rem;">Las tiendas y restaurantes de tu barrio en una app.</p>
                  </div>

                  <div class="content_img">
                    <div>
                    <img src="https://wilbby.s3.eu-west-3.amazonaws.com/movil.png" alt="Wilbby">
                  </div>
                  <div>
                    <h2>Pedidos desde la app</h2>
                    <p>Ahora puedes hacer tus pedidos de las tiendas de tu barrio a domicilio, con envío a tan sólo 1,95€ y entregas en menos de 30 minutos.</p>
                  </div>
                  <div style="margin-top: 5rem;">
                    <h2>Ofertas exclusivas</h2>
                    <p>Otra de las ventajas que puedes disfrutar con la app, es poder acceder a todas nuestras ofertas exclusivas y pedir que te lo llevemos donde estés.</p>
                  </div>
                  <div style="margin-top: 3rem;">
                    <img src="https://wilbby.s3.eu-west-3.amazonaws.com/movil1.png" alt="Wilbby">
                  </div>
                  </div>

                  <h1 class="all">Con Wilbby lo tienes todo</h1>


                  <h3 style="padding: 1em 4em; text-align: center;">Descubre las nuevas tiendas en Wilbby</h3>
                  <p></p><a href="https://wilbby.com" class="btn btn-primary">Ver que hay nuevo</a></p>


                </div>
              </td>
            </tr>
          </table>
        </td>
      </tr><!-- end tr -->
     
      </tr><!-- end:tr -->
    <!-- 1 Column Text + Button : END -->
    </table>
    <table style="background-color: #fff;">
            <tr>
              <td valign="top">
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                  <tr>

                  <div style="text-align: center; padding-top: 2rem;">
                    <h3>Añade Wilbby a tu móvil</h3>
                      <a href="https://apps.apple.com/es/app/foodyapp-pick-up/id1528906592"><img src="https://foody-img.s3.eu-west-3.amazonaws.com/unnamed.png" alt="Apple Store" style="width: 120px; margin: 10px;"/></a>
                      <a href="https://play.google.com/store/apps/details?id=com.foodyapp"><img src="https://foody-img.s3.eu-west-3.amazonaws.com/unnamed+(1).png" alt="Google Play" style="width: 120px; margin: 10px;"/></a>
                    </div>
                  </tr>
                  
                  <div class="social">
                   <a href="https://www.facebook.com/wilbbyapp" target="_blank">
                    <img src="https://cdn4.iconfinder.com/data/icons/social-messaging-ui-color-shapes-2-free/128/social-facebook-circle-512.png" alt="Facebook">
                    </a>
                    <a href="https://www.instagram.com/wilbbyapp/" target="_blank">
                    <img src="https://icons-for-free.com/iconfiles/png/512/colored+gradient+instagram+media+social+social+media+icon-1320192520147384507.png" alt="Instagram">
                    </a>
                    <a href="https://twitter.com/wilbbyapp" target="_blank">
                    <img src="https://www.freeiconspng.com/uploads/circle-twitter-logo-png-photo-13.png" alt="Twitter">
                    </a>
                    <a href="https://www.linkedin.com/company/wilbbyapp/" target="_blank">
                    <img src="https://cdn.iconscout.com/icon/free/png-256/linkedin-42-151143.png" alt="Linkedin">
                    </a>
                  </div>
                  <tr>
                    <td style="text-align: center; padding: 2rem;">
                      <p style="color: #222222;">Si tienes alguna duda sobre entregas o pagos, háblanos a través del chat, envíanos un correo electrónico a <a href="mailto:info@wilbby.com" style="color: #90C33C;">info@wilbby.com</a> o llámanos al <a href="tel:+34664028161" style="color: #90C33C;">+34 664 02 81 61</a></p>

                      <p>Copyright © 2019 Wilbby App © de sus respectivos propietarios.</p>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
  
  </div>
  </center>
  </body>`;
};
exports.default = t;
