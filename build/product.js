"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductFormat = void 0;
exports.ProductFormat = (x, store) => {
    const Pric = Number(x.price) * 100;
    const pri = Pric.toFixed(0);
    const desc = x.short_description.replace(/<[^>]*>?/g, '');
    const description = desc.replace(/(\r\n|\n|\r)/gm, "");
    return {
        name: x.name,
        description: description,
        nameTranslations: {},
        descriptionTranslations: {},
        account: store,
        location: store,
        productType: 1,
        plu: "",
        price: Number(pri),
        previous_price: null,
        priceLevels: {},
        sortOrder: 0,
        deliveryTax: 0,
        takeawayTax: 0,
        multiply: 1,
        multiMax: 1,
        posProductId: "",
        posProductCategoryId: [],
        subProducts: [],
        productTags: [],
        posCategoryIds: [],
        imageUrl: x.images[0].src,
        max: 1,
        min: 1,
        capacityUsages: [],
        parentId: "",
        visible: true,
        snoozed: false,
        subProductSortOrder: [],
        internalId: "",
        new: false,
        popular: false,
        offert: false,
        recomended: false,
        quantity: 1,
        storeId: store,
    };
};
