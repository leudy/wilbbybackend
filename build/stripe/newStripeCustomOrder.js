"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const dotenv_1 = __importDefault(require("dotenv"));
const custonorder_1 = __importDefault(require("../models/custonorder"));
const scheduleTime_1 = require("../funtions/scheduleTime");
const ScheduledMaria_1 = require("../funtions/ScheduledMaria");
const asingRidersCustonOrder_1 = require("../funtions/asingRidersCustonOrder");
dotenv_1.default.config({ path: "variables.env" });
var Stripe = require("stripe")(process.env.STRIPECLIENTSECRET);
class NewStripeOrderRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.post("/create-payment-intent-order", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { amount, orderID, customer, uderID, payment_type, currency, request_three_d_secure, payment_method_types = [], } = req.body;
            const params = {
                amount: amount,
                currency: "EUR",
                customer: customer,
                payment_method_options: {
                    card: {
                        request_three_d_secure: request_three_d_secure || "automatic",
                    },
                },
                payment_method_types: payment_method_types,
            };
            try {
                const paymentIntent = yield Stripe.paymentIntents.create(params);
                custonorder_1.default.findOneAndUpdate({ _id: orderID }, {
                    stripePaymentIntent: paymentIntent,
                }, (err, orde) => {
                    if (!err) {
                        return res.send({
                            clientSecret: paymentIntent.client_secret,
                        });
                    }
                });
            }
            catch (error) {
                console.log("error payment", error);
                return res.send({
                    //@ts-ignore
                    error: error.raw.message,
                });
            }
        }));
        this.router.post("/payment-card-order", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { payment_method, customers, amount, currency, order } = req.body;
            const total = amount.toFixed(0);
            try {
                const paymentIntent = yield Stripe.paymentIntents.create({
                    payment_method: payment_method,
                    amount: total,
                    currency: "EUR",
                    customer: customers,
                    confirmation_method: "automatic",
                    confirm: true,
                });
                if (paymentIntent.status === "succeeded") {
                    custonorder_1.default.findOneAndUpdate({ _id: order }, {
                        estado: "Pagado",
                        progreso: "25",
                        status: "active",
                        stripePaymentIntent: paymentIntent,
                    }, (err, orde) => {
                        if (err) {
                            res.status(400).json(err).end();
                        }
                        else {
                            if (scheduleTime_1.scheduleTime(ScheduledMaria_1.getMariaHorario().schedule)) {
                                asingRidersCustonOrder_1.AsingRiderCustonOrder(orde);
                            }
                            res.status(200).json(paymentIntent).end();
                        }
                    });
                }
                else if (paymentIntent.status === "canceled") {
                    res.status(400).json(paymentIntent).end();
                }
                else if (paymentIntent.status === "requires_action") {
                    custonorder_1.default.findOneAndUpdate({ _id: order }, {
                        stripePaymentIntent: paymentIntent,
                    }, (err, orde) => {
                        if (err) {
                            res.status(400).json(err).end();
                        }
                        else {
                            res.status(200).json(paymentIntent).end();
                        }
                    });
                }
                else {
                    res.status(400).json(paymentIntent).end();
                }
            }
            catch (err) {
                console.log("Error code is: ", err);
                const paymentIntentRetrieved = yield Stripe.paymentIntents.retrieve(
                //@ts-ignore
                err.raw.payment_intent.id);
                console.log("PI retrieved: ", paymentIntentRetrieved.id);
                res.status(400).json(paymentIntentRetrieved).end();
            }
        }));
        this.router.post("/update-order-3d-secure-order", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { order } = req.body;
            custonorder_1.default.findOneAndUpdate({ _id: order }, {
                estado: "Pagado",
                progreso: "25",
                status: "active",
            }, (err, orde) => {
                if (err) {
                    res.status(400).json(err).end();
                }
                else {
                    if (scheduleTime_1.scheduleTime(ScheduledMaria_1.getMariaHorario().schedule)) {
                        asingRidersCustonOrder_1.AsingRiderCustonOrder(orde);
                    }
                    res.status(200).json({ success: true }).end();
                }
            });
        }));
    }
}
const newStripeOrderRouterRouter = new NewStripeOrderRouter();
newStripeOrderRouterRouter.routes();
exports.default = newStripeOrderRouterRouter.router;
