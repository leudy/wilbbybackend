"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const dotenv_1 = __importDefault(require("dotenv"));
const user_1 = __importDefault(require("../models/user"));
const changeToken_1 = require("./changeToken");
const order_1 = __importDefault(require("../models/newOrder/order"));
const addToCart_1 = __importDefault(require("../models/newOrder/addToCart"));
const custonorder_1 = __importDefault(require("../models/custonorder"));
const Deliverect_1 = require("../Deliverect/Deliverect");
const api_1 = require("../Ordatic/api");
const api_2 = require("../Otter/api");
const Apis_1 = require("../Holded/Apis");
const sendNotificationToStore_1 = require("./sendNotificationToStore");
const scheduleTime_1 = require("../funtions/scheduleTime");
const ScheduledMaria_1 = require("../funtions/ScheduledMaria");
const asingRiders_1 = require("../funtions/asingRiders");
const asingRidersCustonOrder_1 = require("../funtions/asingRidersCustonOrder");
var stripe = require("stripe")(process.env.STRIPECLIENTSECRET);
dotenv_1.default.config({ path: "variables.env" });
class StripeRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get("/create-client", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { userID, nameclient, email } = req.query;
            yield stripe.customers.create({
                name: nameclient,
                email: email,
                description: "Clientes de Wilbby",
            }, function (err, customer) {
                user_1.default.findOneAndUpdate({ _id: userID }, {
                    $set: {
                        StripeID: customer.id,
                    },
                }, (err, customers) => {
                    if (err) {
                        console.log(err);
                    }
                });
            });
        }));
        this.router.get("/card-create", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { customers, token } = req.query;
            const paymentMethod = yield stripe.paymentMethods.attach(token, {
                customer: customers,
            });
            if (paymentMethod) {
                res.status(200).json({ success: true, data: paymentMethod }).end();
            }
            else {
                res
                    .status(400)
                    .json({ success: false, data: "Algo salio mas intentalo de nuevo" })
                    .end();
            }
        }));
        this.router.get("/get-card", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { customers } = req.query;
            if (customers) {
                const paymentMethods = yield stripe.paymentMethods.list({
                    customer: customers,
                    type: "card",
                });
                res.status(200).json(paymentMethods).end();
            }
            else {
                res.status(400).json(null).end();
            }
        }));
        this.router.get("/delete-card", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { cardID, customers } = req.query;
            yield stripe.customers.deleteSource(customers, cardID, function (err, confirmation) {
                res.json(confirmation);
            });
        }));
        this.router.post("/create-card", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { customer, paymentMethod } = req.body;
            const paymentMethods = yield stripe.paymentMethods.attach(paymentMethod, {
                customer: customer,
            });
            if (paymentMethods) {
                res
                    .status(200)
                    .json({
                    success: true,
                    message: "success_add_card",
                    data: paymentMethods,
                })
                    .end();
            }
            else {
                res
                    .status(400)
                    .json({ success: false, message: "error_add_card", data: null })
                    .end();
            }
        }));
        this.router.get("/delete-card-web", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { cardID } = req.query;
            const done = yield stripe.paymentMethods.detach(cardID);
            if (done) {
                res
                    .status(200)
                    .json({ success: true, message: "success_delete_card" })
                    .end();
            }
            else {
                res
                    .status(400)
                    .json({ success: false, message: "error_delete_card" })
                    .end();
            }
        }));
        this.router.post("/payment-existing-card-custom-order", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { card, customers, amount, orders, currency } = req.body;
            console.log(card, customers, amount, orders, currency);
            const total = Number(amount).toFixed(0);
            try {
                const paymentIntent = yield stripe.paymentIntents.create({
                    payment_method: card,
                    amount: total,
                    currency: currency ? currency : "EUR",
                    customer: customers,
                    confirmation_method: "automatic",
                    confirm: true,
                });
                if (paymentIntent.status === "succeeded") {
                    custonorder_1.default.findOneAndUpdate({ _id: orders }, {
                        estado: "Pagado",
                        progreso: "25",
                        status: "active",
                        stripePaymentIntent: paymentIntent,
                    }, (err, orde) => {
                        if (err) {
                            res.status(400).json(err).end();
                        }
                        else {
                            if (scheduleTime_1.scheduleTime(ScheduledMaria_1.getMariaHorario().schedule)) {
                                asingRidersCustonOrder_1.AsingRiderCustonOrder(orde);
                            }
                            res.status(200).json(paymentIntent).end();
                        }
                    });
                }
                else if (paymentIntent.status === "canceled") {
                    res.status(200).json(paymentIntent).end();
                }
                else if (paymentIntent.status === "requires_action") {
                    custonorder_1.default.findOneAndUpdate({ _id: orders }, {
                        estado: "Pagado",
                        progreso: "25",
                        status: "active",
                        stripePaymentIntent: paymentIntent,
                    }, (err, orde) => {
                        if (err) {
                            res.status(400).json(err).end();
                        }
                        else {
                            if (scheduleTime_1.scheduleTime(ScheduledMaria_1.getMariaHorario().schedule)) {
                                asingRidersCustonOrder_1.AsingRiderCustonOrder(orde);
                            }
                            res.status(200).json(paymentIntent).end();
                        }
                    });
                }
                else {
                    res.status(400).json(paymentIntent).end();
                }
            }
            catch (err) {
                console.log("Error code is: ", err);
                const paymentIntentRetrieved = yield stripe.paymentIntents.retrieve(
                //@ts-ignore
                err.raw.payment_intent.id);
                console.log("PI retrieved: ", paymentIntentRetrieved.id);
                res.status(400).json(paymentIntentRetrieved).end();
            }
        }));
        this.router.post("/stripe/chargeToken", (req, res) => {
            const { stripeToken, amount, orders, currency } = req.body;
            if (stripeToken && amount != undefined) {
                changeToken_1.chargeToken(amount, stripeToken, currency)
                    .then((response) => __awaiter(this, void 0, void 0, function* () {
                    if (response.status == "succeeded") {
                        custonorder_1.default.findOneAndUpdate({ _id: orders }, {
                            estado: "Pagado",
                            progreso: "25",
                            status: "active",
                            stripePaymentIntent: response,
                        }, (err, orde) => {
                            if (scheduleTime_1.scheduleTime(ScheduledMaria_1.getMariaHorario().schedule)) {
                                asingRidersCustonOrder_1.AsingRiderCustonOrder(orde);
                            }
                            res.json(response).status(200).end();
                        });
                    }
                    else {
                        res.status(401);
                    }
                }))
                    .catch((err) => {
                    res.status(401).send({ success: false, error: err });
                });
            }
            else {
                res.status(403).send({ success: false, error: "Invalid token" });
            }
        });
        this.router.post("/stripe/chargeToken/order", (req, res) => {
            const { stripeToken, amount, orders, userID, currency } = req.body;
            if (stripeToken && amount != undefined) {
                changeToken_1.chargeToken(amount, stripeToken, currency)
                    .then((response) => __awaiter(this, void 0, void 0, function* () {
                    if (response.status == "succeeded") {
                        order_1.default.findOneAndUpdate({ _id: orders }, {
                            status: "Nueva",
                            IntegerValue: 30,
                            paymentMethod: "Tarjeta de credito",
                            stripePaymentIntent: response,
                            orderIsAlreadyPaid: true,
                            //@ts-ignore
                            $push: {
                                statusProcess: { status: "Nueva", date: new Date() },
                            },
                        }, { upsert: true }, 
                        // @ts-ignore
                        (err, order) => {
                            if (!err) {
                                if (order === null || order === void 0 ? void 0 : order.storeData.isDeliverectPartner) {
                                    Deliverect_1.setOrderToDeliverect(order);
                                }
                                else if (order === null || order === void 0 ? void 0 : order.storeData.isOrdaticPartner) {
                                    api_1.pushOrderTopOrdatic(order);
                                }
                                else if (order === null || order === void 0 ? void 0 : order.storeData.isOtterPartner) {
                                    api_2.pushOrderToOtter(order);
                                }
                                else {
                                    sendNotificationToStore_1.sendNotification(order === null || order === void 0 ? void 0 : order.storeData.OnesignalID, "Has recibido un nuevo pedido a por todas 🌮 🌯 🫔 🥗 🥘");
                                    if (scheduleTime_1.scheduleTime(ScheduledMaria_1.getMariaHorario().schedule) &&
                                        order.orderType === "delivery") {
                                        asingRiders_1.AsingRider(order);
                                    }
                                }
                                Apis_1.setOrderToHolded(order);
                                addToCart_1.default
                                    .deleteMany({ userId: String(userID) })
                                    .then(function () {
                                    console.log("Data deleted cart"); // Success
                                })
                                    .catch(function (error) {
                                    console.log(error); // Failure
                                });
                            }
                        });
                        res.status(200).json(response).end();
                    }
                    else {
                        res.status(200).json(response).end();
                    }
                }))
                    .catch((err) => {
                    console.log(err);
                    res.status(401).send({ success: false, error: err });
                });
            }
            else {
                res.status(403).send({ success: false, error: "Invalid token" });
            }
        });
    }
}
const stripeRouter = new StripeRouter();
stripeRouter.routes();
exports.default = stripeRouter.router;
