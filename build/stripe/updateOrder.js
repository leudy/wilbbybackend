"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateOrder = void 0;
const order_1 = __importDefault(require("../models/newOrder/order"));
const Deliverect_1 = require("../Deliverect/Deliverect");
const api_1 = require("../Ordatic/api");
const api_2 = require("../Otter/api");
const Apis_1 = require("../Holded/Apis");
const sendNotificationToStore_1 = require("./sendNotificationToStore");
const scheduleTime_1 = require("../funtions/scheduleTime");
const ScheduledMaria_1 = require("../funtions/ScheduledMaria");
const asingRiders_1 = require("../funtions/asingRiders");
exports.UpdateOrder = (order, paymentIntent, res) => {
    order_1.default.findOneAndUpdate({ _id: order }, {
        status: "Nueva",
        IntegerValue: 30,
        paymentMethod: "Tarjeta de credito",
        stripePaymentIntent: paymentIntent,
        orderIsAlreadyPaid: true,
        //@ts-ignore
        $push: {
            statusProcess: { status: "Nueva", date: new Date() },
        },
    }, { upsert: true }, 
    // @ts-ignore
    (err, order) => {
        if (!err) {
            if (order === null || order === void 0 ? void 0 : order.storeData.isDeliverectPartner) {
                Deliverect_1.setOrderToDeliverect(order);
            }
            else if (order === null || order === void 0 ? void 0 : order.storeData.isOrdaticPartner) {
                api_1.pushOrderTopOrdatic(order);
            }
            else if (order === null || order === void 0 ? void 0 : order.storeData.isOtterPartner) {
                api_2.pushOrderToOtter(order);
            }
            else {
                sendNotificationToStore_1.sendNotification(order === null || order === void 0 ? void 0 : order.storeData.OnesignalID, "Has recibido un nuevo pedido a por todas 🌮 🌯 🫔 🥗 🥘");
            }
            if (scheduleTime_1.scheduleTime(ScheduledMaria_1.getMariaHorario().schedule) &&
                order.orderType === "delivery") {
                asingRiders_1.AsingRider(order);
            }
            res.status(200).json(paymentIntent).end();
            Apis_1.setOrderToHolded(order);
        }
    });
};
