"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: "variables.env" });
var Stripe = require("stripe")(process.env.STRIPECLIENTSECRET);
class NewStripeRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.post("/create-payment-intent", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { amount, customer, payment_type, currency, request_three_d_secure, payment_method_types = [], } = req.body;
            const params = {
                amount: amount,
                currency: "EUR",
                customer: customer,
                payment_method_options: {
                    card: {
                        request_three_d_secure: request_three_d_secure || "automatic",
                    },
                },
                payment_method_types: payment_method_types,
            };
            try {
                const paymentIntent = yield Stripe.paymentIntents.create(params);
                return res
                    .status(200)
                    .send({
                    paymentIntent: paymentIntent,
                })
                    .end();
            }
            catch (error) {
                return res
                    .send({
                    //@ts-ignore
                    error: error.raw.message,
                })
                    .end();
            }
        }));
        this.router.post("/payment-card", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { payment_method, customers, amount, currency } = req.body;
            const total = amount.toFixed(0);
            try {
                const paymentIntent = yield Stripe.paymentIntents.create({
                    payment_method: payment_method,
                    amount: total,
                    currency: "EUR",
                    customer: customers,
                    confirmation_method: "automatic",
                    confirm: true,
                });
                if (paymentIntent.status === "succeeded") {
                    res.status(200).json(paymentIntent).end();
                }
                else if (paymentIntent.status === "canceled") {
                    res.status(400).json(paymentIntent).end();
                }
                else if (paymentIntent.status === "requires_action") {
                    res.status(200).json(paymentIntent).end();
                }
                else {
                    res.status(400).json(paymentIntent).end();
                }
            }
            catch (err) {
                console.log("Error code is: ", err);
                const paymentIntentRetrieved = yield Stripe.paymentIntents.retrieve(
                //@ts-ignore
                err.raw.payment_intent.id);
                console.log("PI retrieved: ", paymentIntentRetrieved.id);
                res.status(400).json(paymentIntentRetrieved).end();
            }
        }));
        this.router.post("/cash-payment", (req, res) => __awaiter(this, void 0, void 0, function* () {
            setTimeout(() => {
                res.status(200).json({ success: true }).end();
            }, 2000);
        }));
    }
}
const newStripeRouterRouter = new NewStripeRouter();
newStripeRouterRouter.routes();
exports.default = newStripeRouterRouter.router;
